using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Parallelizable.Interfaces;

namespace FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Conditional.Interface {
	[AboutName("C", "Conditional")]
	[AboutName("H", "Handling")]
	public interface ICH<in T> : ISupportsParallelization {
		bool IsUsed(T obj);
		void Process(T obj);
	}
}