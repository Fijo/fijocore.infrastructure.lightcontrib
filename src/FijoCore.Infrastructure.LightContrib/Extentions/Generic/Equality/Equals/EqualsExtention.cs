using System;
using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Generic.Equality.Equals {
	[PublicAPI]
	public static class EqualsExtention {
		[Pure, PublicAPI, DebuggerStepThrough]
		[Desc("Compares two instances of a type if they are the equal using a selector to get the values to comare")]
		public static bool Equals<TSource, TEquals>(this TSource me, TSource other, [NotNull] Func<TSource, TEquals> innerSelector) {
			return Equals(innerSelector(me), innerSelector(other));
		}
	}
}