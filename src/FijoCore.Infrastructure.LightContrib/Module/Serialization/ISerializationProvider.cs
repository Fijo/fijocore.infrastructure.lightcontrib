using FijoCore.Infrastructure.LightContrib.Enums;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization {
	public interface ISerializationProvider {
		ISerialization Get(SerializationFormat format);
	}
}