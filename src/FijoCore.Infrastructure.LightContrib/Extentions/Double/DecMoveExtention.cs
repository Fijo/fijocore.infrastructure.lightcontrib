using System;
using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Double {
	[PublicAPI]
	public static class DecMoveExtention {
		[Pure, DebuggerStepThrough, PublicAPI]
		[Desc("Moves the comma of x decimal digits left and returns the result.")]
		public static double DecMove(this double me, int movement) {
			return me / Math.Pow(10, movement);
		}
	}
}