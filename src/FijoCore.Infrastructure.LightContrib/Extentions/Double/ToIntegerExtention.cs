using System;
using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Double {
	[PublicAPI]
	public static class ToIntegerExtention {
		[Pure, DebuggerStepThrough, PublicAPI]
		[Desc("Converts a double to int")]
		public static int ToInteger(this double me) {
			return (int) me;
		}

		[Pure, DebuggerStepThrough, PublicAPI]
		[Desc("Converts a double to int, supporting rounding (if you wana use it)")]
		public static int ToInteger(this double me, bool round) {
			return !round
				       ? ToInteger(me)
				       : Convert.ToInt32(me);
		}
	}
}