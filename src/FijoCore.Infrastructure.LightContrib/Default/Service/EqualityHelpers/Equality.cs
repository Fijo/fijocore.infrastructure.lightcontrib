﻿using System;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Default.Service.EqualityHelpers {
	[PublicAPI]
	public static class Equality {
		[Pure, PublicAPI, DebuggerStepThrough]
		public static bool Equals<T>(object obj, [NotNull] Func<T, bool> equalsFunc) {
			return obj is T && equalsFunc((T) obj);
		}
	}
}