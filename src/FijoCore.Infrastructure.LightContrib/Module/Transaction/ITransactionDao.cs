using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Transaction {
	[PublicAPI, Dao]
	public interface ITransactionDao {
		void Commit(Transaction transaction);
		void Rollback(Transaction transaction);
	}
}