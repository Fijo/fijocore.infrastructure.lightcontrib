using System;
using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Boolean {
	public static class ToDoubleExtention {
		[Pure, DebuggerStepThrough, PublicAPI]
		[Desc("Converts a bool to double")]
		public static double ToDouble(this bool me) {
			return Convert.ToDouble(me);
		}
	}
}