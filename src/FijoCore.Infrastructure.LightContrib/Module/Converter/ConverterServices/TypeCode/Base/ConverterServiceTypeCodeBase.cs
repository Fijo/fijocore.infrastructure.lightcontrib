using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using FijoCore.Infrastructure.LightContrib.Module.Converter.ConverterServices.Base;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Converter.ConverterServices.TypeCode.Base {
	[PublicAPI, Service]
	public abstract class ConverterServiceTypeCodeBase<TResult> : ConverterServiceBase<System.TypeCode, TResult> {
		[Pure]
		protected override System.TypeCode GetType<T>() {
			return System.Type.GetTypeCode(typeof (T));
		}
	}
}