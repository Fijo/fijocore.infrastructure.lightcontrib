using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.ICollection.Generic {
	public static class NoneExtention {
		[Pure, PublicAPI, DebuggerStepThrough]
		public static bool None<T>(this ICollection<T> me) {
			return me.Count == 0;
		}
	}
}