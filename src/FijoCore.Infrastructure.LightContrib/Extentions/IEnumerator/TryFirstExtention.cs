using System.Collections.Generic;
using System.Diagnostics;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator {
	[PublicAPI]
	public static class TryFirstExtention {
		private static IOut _out;

		internal static void Init() {
			_out = Kernel.Resolve<IOut>();
		}

		[Pure, PublicAPI, DebuggerStepThrough]
		public static bool TryFirst<TSource>([NotNull] this IEnumerator<TSource> me, out TSource result) {
			#region PreCondition
			Debug.Assert(_out != null, "_out != null");
			#endregion
			return me.MoveNext()
				       ? _out.True(out result, me.Current)
				       : _out.False(out result);
		}
	}
}