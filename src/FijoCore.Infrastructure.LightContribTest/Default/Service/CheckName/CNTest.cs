﻿using System;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Default.Service.CheckName;
using FijoCore.Infrastructure.LightContrib.Enums;
using JetBrains.Annotations;
using NUnit.Framework;

namespace FijoCore.Infrastructure.LightContribTest.Default.Service.CheckName {
	[TestFixture]//(typeof(CN))
	public class CNTest {
		[TestCase(typeof(string), "String")]
		[TestCase(typeof(CBool), "CBool")]
		public void Type_Get_MustReturnCorrectName(Type type, string name) {
			var actual = CN.Get(type);
			Assert.AreEqual(name, actual);
		}

		[Test]
		public void FuncBool_Get_MustReturnCorrectFuncName() {
			var actual = CN.Get<CNTest, bool>(x => x.TestMethode1());
			Assert.AreEqual("TestMethode1", actual);
		}

		[Test]
		[Link("http://msdn.microsoft.com/de-de/library/system.linq.expressions.unaryexpression.aspx")]
		public void FuncBoolWithUnary_Get_MustReturnCorrectFuncName() {
			var actual = CN.Get<CNTest, bool>(x => !x.TestMethode1());
			Assert.AreEqual("TestMethode1", actual);
		}
		
		[Test]
		[Link("http://msdn.microsoft.com/de-de/library/system.linq.expressions.unaryexpression.aspx")]
		public void FuncNullableIntWithUnary_Get_MustReturnCorrectFuncName() {
			var actual = CN.Get<CNTest, int?>(x => (int?) x.TestMethode2());
			Assert.AreEqual("TestMethode2", actual);
		}
		
		[Test]
		public void FuncIntWithField_Get_MustReturnCorrectFuncName() {
			var actual = CN.Get<CNTest, int>(x => x._testField.TestMethode2());
			Assert.AreEqual("TestMethode2", actual);
		}
		
		[Test]
		public void Action_Get_MustReturnCorrectFuncName() {
			var actual = CN.Get<CNTest>(x => x.TestMethode3());
			Assert.AreEqual("TestMethode3", actual);
		}

		[UsedImplicitly]
		#pragma warning disable 649
		private CNTest _testField;
		#pragma warning restore 649

		private bool TestMethode1() {
			return true;
		}

		private int TestMethode2() {
			return 0;
		}

		private bool TestMethode3() {
			return true;
		}
	}
}