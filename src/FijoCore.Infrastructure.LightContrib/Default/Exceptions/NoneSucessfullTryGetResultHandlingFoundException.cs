﻿using System;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.TryGetResult.Interface;

namespace FijoCore.Infrastructure.LightContrib.Default.Exceptions
{
	[Serializable]
	[Note("This class was called ´NoneSucessfullMethodeFoundException´ in the past.")]
	public class NoneSucessfullTryGetResultHandlingFoundException<TSource, TResult> : Exception {
		private readonly ITryGetResultHandlingProcessor<TSource, TResult> _tryGetResultHandlingProcessor;
		private readonly TSource _source;

		public NoneSucessfullTryGetResultHandlingFoundException() {}
		public NoneSucessfullTryGetResultHandlingFoundException(string message) : base(message) {}
		public NoneSucessfullTryGetResultHandlingFoundException(ITryGetResultHandlingProcessor<TSource, TResult> tryGetResultHandlingProcessor, TSource source) : base(string.Format("All sucessfully methodes available in the ´{0}´. Tryed to process ´{1}´.", tryGetResultHandlingProcessor.GetType().Name, source)) {
			_tryGetResultHandlingProcessor = tryGetResultHandlingProcessor;
			_source = source;
		}
	}
}
