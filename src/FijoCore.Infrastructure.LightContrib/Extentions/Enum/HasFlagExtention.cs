using System;
using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Enum {
	[PublicAPI]
	public static class HasFlagExtention {
		[Pure, DebuggerStepThrough, PublicAPI]
		[RelatedLink("http://msdn.microsoft.com/en-us/library/cc138362.aspx")]
		[Desc("Checks if an instance of an flags enum has a flag �x� set or not.")]
		public static bool HasFlag<T>(this T me, T flag) where T : struct, IConvertible {
			return InternalHasFlag(typeof (T), me, flag);
		}

		[Pure, DebuggerStepThrough, PublicAPI]
		[RelatedLink("http://msdn.microsoft.com/en-us/library/cc138362.aspx")]
		[Desc("Checks if an instance of an flags enum has a flag �x� set or not.")]
		public static bool HasFlag(this System.Type me, object value, object flag) {
			return InternalHasFlag(me, value, flag);
		}

		[Pure]
// ReSharper disable UnusedParameter.Local
		private static bool InternalHasFlag([NotNull] System.Type type, [NotNull] object value, [NotNull] object flag) {
// ReSharper restore UnusedParameter.Local
			#region PreCondition
			Debug.Assert(type == value.GetType(), "type == value.GetType()");
			Debug.Assert(type == flag.GetType(), "type == flag.GetType()");
			Debug.Assert(type.IsEnum, "type.IsEnum");
			Debug.Assert(type.IsFlagsEnum(), "type.IsFlagsEnum()");
			Debug.Assert(value is IConvertible, "value is IConvertible");
			Debug.Assert(((IConvertible) value).GetTypeCode().IsIn(TypeCode.Byte, TypeCode.Char, TypeCode.Decimal, TypeCode.Double, TypeCode.Int16, TypeCode.Int32, TypeCode.Int64, TypeCode.UInt16, TypeCode.UInt32, TypeCode.UInt64));
			#endregion
			var isLongEnum = value is long;
			return isLongEnum
			       	? InternalHasFlag(Convert.ToInt32(value), Convert.ToInt32(flag))
			       	: InternalHasFlag(Convert.ToInt64(value), Convert.ToInt64(flag));
		}

		[Pure]
		private static bool InternalHasFlag(int me, int flag) {
			return (me & flag) == flag;
		}
		
		[Pure]
		private static bool InternalHasFlag(long me, long flag) {
			return (me & flag) == flag;
		}
	}
}