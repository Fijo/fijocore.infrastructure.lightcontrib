using System;
using System.Collections.Generic;
using System.Linq;
using FijoCore.Infrastructure.LightContrib.Extentions.Enum;
using FijoCore.Infrastructure.LightContrib.Module.SelectedAggregation.Base;
using FijoCore.Infrastructure.LightContrib.Module.SelectedAggregation.Interface;

namespace FijoCore.Infrastructure.LightContrib.Module.SelectedAggregation.Module {
	public abstract class EnumAggregation<TEnum, TAccumulate, TSource, TResult, TParam> : SelectedAggregation<TEnum, ICollection<TEnum>, TAccumulate, TSource, TResult, TParam>, IEnumAggregation<TEnum, TAccumulate, TSource, TResult, TParam> where TEnum : struct, IConvertible {
		#region Overrides of SelectedAggregation<TEnum,ICollection<TEnum>,TAccumulate,TSource,TResult>
		protected override bool IsFuncUsed(TEnum key, ICollection<TEnum> funcKey, TParam param) {
			return funcKey.All(flag => key.HasFlag(flag));
		}
		#endregion
	}
	
	public abstract class EnumAggregation<TEnum, TAccumulate, TParam> : EnumAggregation<TEnum, TAccumulate, TAccumulate, TAccumulate, TParam>, IEnumAggregation<TEnum, TAccumulate, TParam> where TEnum : struct, IConvertible {
		#region Overrides of SelectedAggregation<TEnum,ICollection<TEnum>,TAccumulate,TAccumulate,TAccumulate>
		protected override TAccumulate GetAccumulate(TAccumulate source, TParam param, TEnum key) {
			return source;
		}

		protected override TAccumulate GetResult(TAccumulate accumulate, TParam param, TEnum key) {
			return accumulate;
		}
		#endregion
	}
}