using FijoCore.Infrastructure.LightContrib.Module.Number.Service.Base;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Number.Service.Module {
	[UsedImplicitly]
	public class ULongNumberService : NumberServiceBase<ulong> {
		#region Implementation of INumberService<byte>
		public override ulong MaxValue { get { return ulong.MaxValue; } }
		public override ulong MinValue { get { return ulong.MinValue; } }
		public override bool IsInteger { get { return true; } }

		public override ulong Sun(ulong left, ulong right) {
			return left + right;
		}

		public override ulong Diff(ulong left, ulong right) {
			return left - right;
		}

		public override ulong Times(ulong left, ulong right) {
			return left * right;
		}

		public override ulong Divide(ulong left, ulong right) {
			return left / right;
		}

		public override ulong Modulo(ulong left, ulong right) {
			return left % right;
		}
		#endregion
	}
}