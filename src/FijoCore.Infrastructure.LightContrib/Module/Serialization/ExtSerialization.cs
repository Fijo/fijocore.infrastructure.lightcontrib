using FijoCore.Infrastructure.LightContrib.Default.Exceptions;
using FijoCore.Infrastructure.LightContrib.Enums;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Settings;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization {
	public abstract class ExtSerialization<TSettings> : ISerialization where TSettings : ISerializationSettings {
		#region Implementation of ISerialization
		public void Serialize(object obj, System.IO.Stream stream, SerializationFormatting formatting, System.Text.Encoding encoding, ISerializationSettings serializationSettings = null) {
			Serialize(obj, stream, formatting, encoding, CastSetting(serializationSettings));
		}

		public void Serialize<T>(T obj, System.IO.Stream stream, SerializationFormatting formatting, System.Text.Encoding encoding, ISerializationSettings serializationSettings = null) {
			Serialize(obj, stream, formatting, encoding, CastSetting(serializationSettings));
		}

		public object Deserialize(System.IO.Stream stream, System.Text.Encoding encoding, ISerializationSettings serializationSettings = null) {
			return Deserialize(stream, encoding, CastSetting(serializationSettings));
		}

		public T Deserialize<T>(System.IO.Stream stream, System.Text.Encoding encoding, ISerializationSettings serializationSettings = null) {
			return Deserialize<T>(stream, encoding, CastSetting(serializationSettings));
		}
		#endregion

		private TSettings CastSetting([CanBeNull] ISerializationSettings settings) {
			if(settings == null) return default(TSettings);
			if(!(settings is TSettings)) throw new InvalidSerializationSettingsException(this, typeof (TSettings), settings);
			return (TSettings) settings;
		}

		protected abstract void Serialize(object obj, [NotNull] System.IO.Stream stream, SerializationFormatting formatting, [CanBeNull] System.Text.Encoding encoding, [CanBeNull] TSettings serializationSettings = default(TSettings));
		protected abstract void Serialize<T>(T obj, [NotNull] System.IO.Stream stream, SerializationFormatting formatting, [CanBeNull] System.Text.Encoding encoding, [CanBeNull] TSettings serializationSettings = default(TSettings));
		protected abstract object Deserialize([NotNull] System.IO.Stream stream, [CanBeNull] System.Text.Encoding encoding, [CanBeNull] TSettings serializationSettings = default(TSettings));
		protected abstract T Deserialize<T>([NotNull] System.IO.Stream stream, [CanBeNull] System.Text.Encoding encoding, [CanBeNull] TSettings serializationSettings = default(TSettings));
	}
}