﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Typing.DictionaryType {
	public class DictionaryTypeService : IDictionaryTypeService {
		#region nested classes
		protected class DictionaryTypeCache {
			public DictionaryTypeDto DictionaryTypeDto;
			public Type DictionaryType;
		}
		#endregion

		protected readonly Type DictionaryType = typeof (IDictionary);
		protected readonly Type GenericDictionaryType = typeof (IDictionary<,>);

		#if DictionaryTypeCache
		protected readonly IDictionary<Type, DictionaryTypeCache> Cache = new Dictionary<Type, DictionaryTypeCache>();
		protected readonly ISet<Type> NonDictionaryTypes = new HashSet<Type>();
		#endif

		#region GetDictionaryTypeDto
		public DictionaryTypeDto GetDictionaryTypeDto([NotNull] Type me) {
			var cache = GetCache(me);
			var dictionaryTypeDto = cache.DictionaryTypeDto;
			if (dictionaryTypeDto != null) return dictionaryTypeDto;
			return cache.DictionaryTypeDto = GetInternDictionaryTypeDto(me);
		}

		private DictionaryTypeDto GetInternDictionaryTypeDto([NotNull] Type me) {
			var dictType = GetDictType(me);
			Debug.Assert(dictType != null || DictionaryType.IsAssignableFrom(me), "The type has to be a IDictionary or IDictionary<,>");
			return dictType == null
				       ? new DictionaryTypeDto(typeof (object), typeof (object), false)
				       : MapDictionaryType(dictType);
		}

		private DictionaryTypeDto MapDictionaryType([NotNull] Type dictType) {
			#region PreCondition
			Debug.Assert(dictType.GetGenericTypeDefinition() == GenericDictionaryType);
			#endregion
			var arguments = dictType.GetGenericArguments();
			Debug.Assert(arguments.Count() == 2);
			return new DictionaryTypeDto(arguments[0], arguments[1], true);
		}
		#endregion
		
		#region IsDictionary
		public bool IsDictionary([NotNull] Type me) {
			lock (Cache) {
				if (NonDictionaryTypes.Contains(me)) return false;
				if (Cache.ContainsKey(me)) return true;

				var isDictionary = IsInternDictionary(me);
				if (isDictionary) Cache.Add(me, new DictionaryTypeCache());
				else NonDictionaryTypes.Add(me);

				return isDictionary;
			}
		}

		private bool IsInternDictionary([NotNull] Type me) {
			return DictionaryType.IsAssignableFrom(me) || GetInterfaces(me).Any(IsGenericIDictionary);
		}
		#endregion

		#region GetDictionaryType
		public Type GetDictionaryType([NotNull] Type me) {
			var dictionaryTypeCache = GetCache(me);
			var dictionaryType = dictionaryTypeCache.DictionaryType;
			if (dictionaryType != null) return dictionaryType;
			return dictionaryTypeCache.DictionaryType = GetInternDictionaryType(me);
		}

		private Type GetInternDictionaryType(Type me) {
			var dictionaryType = GetDictType(me);
			Debug.Assert(dictionaryType != null || DictionaryType.IsAssignableFrom(me), "The type has to be a IDictionary or IDictionary<,>");
			return dictionaryType ?? DictionaryType;
		}
		#endregion

		private Type GetDictType([NotNull] Type me) {
			try {
				return GetInterfaces(me).SingleOrDefault(IsGenericIDictionary);
			}
			catch (InvalidOperationException e) {
				throw new InvalidOperationException("The type (arg ´me´) should not implement IDictionary<,> more than one time. (I don´t know which to use in this case)");
			}
		}

		private IEnumerable<Type> GetInterfaces([NotNull] Type me) {
			if (me.IsInterface) yield return me;
			foreach (var @interface in me.GetInterfaces()) yield return @interface;
		}

		private bool IsGenericIDictionary([NotNull] Type type) {
			return type.IsGenericType && !type.IsGenericTypeDefinition &&
			       type.GetGenericTypeDefinition() == GenericDictionaryType;
		}
		
		private DictionaryTypeCache GetCache([NotNull] Type me) {
			return Cache.GetOrCreate(me, () => new DictionaryTypeCache());
		}
	}
}