using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic.KeyValuePair {
	[PublicAPI]
	public static class WhereValueExtention {
		[NotNull, Pure, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<TKey> WhereValue<TKey, TValue>([NotNull] this IEnumerable<KeyValuePair<TKey, TValue>> me, TValue needle) {
			return me.Where(x => x.Value.Equals(needle)).Select(x => x.Key);
		}
	}
}