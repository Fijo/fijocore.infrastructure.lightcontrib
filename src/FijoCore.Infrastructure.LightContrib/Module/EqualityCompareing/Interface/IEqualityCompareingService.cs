using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.EqualityCompareing.Interface {
	[PublicAPI, Service]
	public interface IEqualityComparingService<in T> {
		[PublicAPI] bool Equals(T me, T obj);
		[PublicAPI] bool Equals(object me, object obj);
		[PublicAPI] bool Equals(T me, object obj);
		[PublicAPI] int GetHashCode(T obj);
	}
}