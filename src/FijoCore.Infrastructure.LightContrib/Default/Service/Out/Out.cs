﻿using System;
using System.Diagnostics;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Default.Service.Out {
	[UsedImplicitly]
	public class Out : IOut {
		[DebuggerStepThrough]
		public bool True<T>(out T obj, T value = default(T)) {
			obj = value;
			return true;
		}

		[DebuggerStepThrough]
		public bool False<T>(out T obj, T value = default(T)) {
			obj = value;
			return false;
		}

		[DebuggerStepThrough]
		public bool Generic<T>(out T obj, bool returnType, T trueValue = default(T), T falseValue = default(T)) {
			obj = returnType ? trueValue : falseValue;
			return returnType;
		}

		[DebuggerStepThrough]
		public bool Generic<T>(out T obj, bool returnType, Func<T> trueValueResolver, T falseValue = default(T)) {
			obj = returnType ? trueValueResolver() : falseValue;
			return returnType;
		}

		[DebuggerStepThrough]
		public bool Generic<T>(out T obj, bool returnType, T trueValue, Func<T> falseValueResolver) {
			obj = returnType ? trueValue : falseValueResolver();
			return returnType;
		}

		[DebuggerStepThrough]
		public bool Generic<T>(out T obj, bool returnType, Func<T> trueValueResolver, Func<T> falseValueResolver) {
			obj = returnType ? trueValueResolver() : falseValueResolver();
			return returnType;
		}
	}
}