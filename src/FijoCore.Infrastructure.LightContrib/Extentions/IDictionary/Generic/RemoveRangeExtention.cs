using System.Collections.Generic;
using System.Diagnostics;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic {
	[PublicAPI]
	public static class RemoveRangeExtention {
		[PublicAPI, DebuggerStepThrough]
		public static void RemoveRange<TKey, TValue>(this IDictionary<TKey, TValue> me, IEnumerable<TKey> candidates) {
			candidates.ForEach(candidate => me.Remove(candidate));
		}

		[PublicAPI, DebuggerStepThrough]
		public static void RemoveRange<TKey, TValue>(this IDictionary<TKey, TValue> me, IEnumerable<KeyValuePair<TKey, TValue>> candidates) {
			candidates.ForEach(candidate => me.Remove(candidate));
		}
	}
}