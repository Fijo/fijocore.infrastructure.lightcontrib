using System;
using FijoCore.Components.Extentions.Double;
using NUnit.Framework;

namespace FijoCore.ComponentTest.Extentions.Double {
	[TestFixture]
	public class IsNegativeExtentionTest
	{
		[Test]
		public void Test() {
			var random = new Random();
			for (var i = 0; i < 10000000; i++) {
				var number = (0.5 - random.NextDouble()) * 2.0 * double.MaxValue;
				Assert.AreEqual(number.IsNegative(), number < 0);
			}
		}
	}
}