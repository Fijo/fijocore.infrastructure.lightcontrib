﻿using System;
using System.Collections.Generic;
using System.Linq;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic.Equality.Equals;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Lookup;
using MapReduce.NET.Collections;

namespace FijoCore.Infrastructure.LightContrib.Factories {
	public class LookupFactory : ILookupFactory {
		#region Implementation of ILookupFactory
		public IDictionary<TKey, TValue> Create<TKey, TValue>(IEnumerable<KeyValuePair<TKey, TValue>> collection) {
			// You have to use System.Linq because otherwise you´ll get a recursive call
			return Create(collection, null);
			//return collection.ToDict(x => x.Key, x => x.Value);
		}

		public IDictionary<TKey, TValue> Create<TKey, TValue>(IEnumerable<KeyValuePair<TKey, TValue>> collection, IEqualityComparer<TKey> comparer) {
			//var customDictionary = new CustomDictionary<TKey, TValue>(comparer);
			//foreach (var value in collection)
			//	customDictionary.Add(value);
			//return customDictionary;
			//comparer == null && typeof(TKey).IsAssignableFrom(typeof(IComparable)) ? (IDictionary<TKey, TValue>) new SortedDictionary<TKey, TValue>() : 
			var dictionary = new Dictionary<TKey, TValue>(comparer);

			foreach (var value in collection)
				dictionary.Add(value.Key, value.Value);
			return dictionary;
		}

		public IDictionary<TKey, TValue> CreatePerf<TKey, TValue>(IEnumerable<KeyValuePair<TKey, TValue>> collection, bool isReadonly) where TKey : IConvertible {
			return Create(collection);
			//return isReadonly
			//		   ? (IDictionary<TKey, TValue>) new ArrayLookup<TKey, TValue>(collection)
			//		   : new ListLookup<TKey, TValue>(collection);
		}
		#endregion
	}
}