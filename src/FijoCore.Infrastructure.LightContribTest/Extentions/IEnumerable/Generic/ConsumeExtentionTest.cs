using System.Collections.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using NUnit.Framework;

namespace FijoCore.Infrastructure.LightContribTest.Extentions.IEnumerable.Generic {
	[TestFixture]
	public class ConsumeExtentionTest {
		private int _pos;
		private const int End = 100;

		[Test]
		public void Enumerable_Consume_MustHaveEnumerableExecutedFully() {
			var enumerable = GetTestEnumerable();
			enumerable.Consume();
			Assert.AreEqual(End, _pos);
		}

		private IEnumerable<int> GetTestEnumerable() {
			for (; _pos < End; _pos++) yield return _pos;
		}
	}
}