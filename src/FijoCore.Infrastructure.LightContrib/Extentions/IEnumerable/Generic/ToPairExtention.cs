using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fijo.Infrastructure.Model.Pair.Interface.Base;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic.To;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic
{
	public static class ToPairExtention
	{
		#region Enumerbale
		[NotNull, Pure, DebuggerStepThrough, PublicAPI]
		public static IEnumerable<IPair<TKey, TValue>> ToPair<TSource, TKey, TValue>([NotNull] this IEnumerable<TSource> me, [NotNull] System.Func<TSource, TKey> keySelector, [NotNull] System.Func<TSource, TValue> valueSelector) {
			return me.Select(x => ToPairSelector(x, keySelector, valueSelector));
		}

		[NotNull, Pure, DebuggerStepThrough, PublicAPI]
		public static IEnumerable<KeyValuePair<TKey, TValue>> ToKeyValuePair<TSource, TKey, TValue>([NotNull] this IEnumerable<TSource> me, [NotNull] System.Func<TSource, TKey> keySelector, [NotNull] System.Func<TSource, TValue> valueSelector) {
			return me.Select(x => ToKeyValuePairSelector(x, keySelector, valueSelector));
		}
		#endregion

		#region ParallelQuery
		[NotNull, Pure, DebuggerStepThrough, PublicAPI]
		public static ParallelQuery<IPair<TKey, TValue>> ToPair<TSource, TKey, TValue>([NotNull] this ParallelQuery<TSource> me, [NotNull] System.Func<TSource, TKey> keySelector, [NotNull] System.Func<TSource, TValue> valueSelector) {
			return me.Select(x => ToPairSelector(x, keySelector, valueSelector));
		}

		[NotNull, Pure, DebuggerStepThrough, PublicAPI]
		public static ParallelQuery<KeyValuePair<TKey, TValue>> ToKeyValuePair<TSource, TKey, TValue>([NotNull] this ParallelQuery<TSource> me, [NotNull] System.Func<TSource, TKey> keySelector, [NotNull] System.Func<TSource, TValue> valueSelector) {
			return me.Select(x => ToKeyValuePairSelector(x, keySelector, valueSelector));
		}
		#endregion

		private static IPair<TKey, TValue> ToPairSelector<TSource, TKey, TValue>(TSource x, Func<TSource, TKey> keySelector, Func<TSource, TValue> valueSelector)
		{
			return x.ToPair(keySelector, valueSelector);
		}

		private static KeyValuePair<TKey, TValue> ToKeyValuePairSelector<TSource, TKey, TValue>(TSource x, Func<TSource, TKey> keySelector, Func<TSource, TValue> valueSelector)
		{
			return x.ToKeyValuePair(keySelector, valueSelector);
		}
	}
}