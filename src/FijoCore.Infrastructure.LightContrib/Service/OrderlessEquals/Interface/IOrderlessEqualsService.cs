using System;
using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace FijoCore.Infrastructure.LightContrib.Service.OrderlessEquals.Interface
{
    public interface IOrderlessEqualsService
    {
    	[Note("Only use this function on two uniqued IEnumerables.")]
    	bool OrderlessUniqueEquals<T>(IEnumerable<T> me, IEnumerable<T> other);

    	[Note("Only use this function on two uniqued IEnumerables.")]
    	bool OrderlessUniqueEquals<T>(IEnumerable<T> me, IEnumerable<T> other, Func<T, T, bool> equals);

    	bool OrderlessEquals<T>(IEnumerable<T> me, IEnumerable<T> other);
    	bool OrderlessEquals<T>(IEnumerable<T> me, IEnumerable<T> other, Func<T, T, bool> equals);
    }
}