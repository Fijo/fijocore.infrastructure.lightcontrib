﻿using System;
using System.IO;
using System.Linq;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Stream;
using FijoCore.Infrastructure.LightContribTest.Properties;
using NUnit.Framework;
using IO = System.IO;

namespace FijoCore.Infrastructure.LightContribTest.Module.Stream {
	[TestFixture]//(typeof (IStreamService))
	public class StreamServiceTest {
		private IStreamService _streamService;

		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
			_streamService = Kernel.Resolve<IStreamService>();
		}

		#region ReadToBytes, ReadToSequence
		#region ReadToBytes
		[TestCase(1)]
		[TestCase(8)]
		[TestCase(1024)]
		[TestCase(4096)]
		public void Stream_ReadToBytes_MustReturnCorrectBytes(int bufferSize) {
			ReadToBytesMustReturnCorrectBytes(_streamService.ReadToBytes, bufferSize);
		}

		[TestCase(1)]
		[TestCase(8)]
		[TestCase(1024)]
		[TestCase(4096)]
		public void Stream_ReadToBytesWithCount_MustReturnCorrectBytes(int bufferSize) {
			ReadWithCountMustReturnCorrectBytes(_streamService.ReadToBytes, bufferSize);
		}

		[TestCase(1)]
		[TestCase(8)]
		[TestCase(1024)]
		[TestCase(4096)]
		public void Stream_ReadToBytesWithCount_ReadAsExpected(int bufferSize) {
			ReadWithCountReadAsExcepted(_streamService.ReadToBytes, bufferSize);
		}
		#endregion

		#region ReadToBytes
		[TestCase(1)]
		[TestCase(8)]
		[TestCase(1024)]
		[TestCase(4096)]
		public void Stream_ReadToSequence_MustReturnCorrectBytes(int bufferSize) {
			ReadToBytesMustReturnCorrectBytes(ReadToSequence, bufferSize);
		}

		[TestCase(1)]
		[TestCase(8)]
		[TestCase(1024)]
		[TestCase(4096)]
		public void Stream_ReadToSequenceWithCount_MustReturnCorrectBytes(int bufferSize) {
			ReadWithCountMustReturnCorrectBytes(ReadToSequence, bufferSize);
		}

		[TestCase(1)]
		[TestCase(8)]
		[TestCase(1024)]
		[TestCase(4096)]
		public void Stream_ReadToSequenceWithCount_ReadAsExpected(int bufferSize) {
			ReadWithCountReadAsExcepted(ReadToSequence, bufferSize);
		}

		private byte[] ReadToSequence(IO.Stream stream, int bufferSize) {
			return _streamService.ReadToBytesSequence(stream, bufferSize).ToArray();
		}

		private byte[] ReadToSequence(IO.Stream stream, int count, int bufferSize) {
			return _streamService.ReadToBytesSequence(stream, count, bufferSize).ToArray();
		}
		#endregion

		#region Base
		private void ReadToBytesMustReturnCorrectBytes(Func<IO.Stream, int, byte[]> readToBytes, int bufferSize) {
			var stream = GetStream();
			var contentBytes = GetByteRange();

			WriteToStream(stream, contentBytes);

			var actual = readToBytes(stream, bufferSize);
			CollectionAssert.AreEqual(contentBytes, actual);
		}

		private void ReadWithCountMustReturnCorrectBytes(Func<IO.Stream, int, int, byte[]> readToBytes, int bufferSize) {
			var stream = GetStream();
			var contentBytes = GetByteRange();

			WriteToStream(stream, contentBytes);

			FirstXAssertation(readToBytes, stream, contentBytes, 0, 4, bufferSize);
		}

		private void ReadWithCountReadAsExcepted(Func<IO.Stream, int, int, byte[]> readToBytes, int bufferSize) {
			var stream = GetStream();
			var contentBytes = GetByteRange();

			WriteToStream(stream, contentBytes);

			var pos = FirstXAssertation(readToBytes, stream, contentBytes, 0, 4, bufferSize);
			FirstXAssertation(readToBytes, stream, contentBytes, pos, 12, bufferSize);
		}

		private int FirstXAssertation(Func<IO.Stream, int, int, byte[]> readToBytes, IO.Stream stream, byte[] contentBytes, int startIndex, int count, int bufferSize) {
			var actualFirstX = readToBytes(stream, count, bufferSize);
			var length = actualFirstX.Length;
			var wantedFirstX = contentBytes.Skip(startIndex).Take(length);
			CollectionAssert.AreEqual(wantedFirstX, actualFirstX);
			return length;
		}
		#endregion
		#endregion

		#region WriteToStream
		[Test]
		public void Bytes_WriteToStream_MustWriteCorrectlyToStream() {
			var stream = GetStream();
			var contentBytes = GetByteRange();

			_streamService.WriteToStream(stream, contentBytes);
			CheckBytesInStream(stream, contentBytes);
		}

		[Test]
		public void Bytes_WriteToStreamWithCount_MustWriteCorrectlyToStream() {
			var stream = GetStream();
			var contentBytes = GetByteRange();

			_streamService.WriteToStream(stream, contentBytes, 12);
			CheckBytesInStream(stream, contentBytes.Take(12).ToArray());
		}

		private void CheckBytesInStream(IO.Stream stream, byte[] contentBytes) {
			stream.Position = 0;
			var length = stream.Length;
			var actual = new byte[length];
			var read = stream.Read(actual, 0, (int) length);
			contentBytes = contentBytes.Take(read).ToArray();
			CollectionAssert.AreEqual(contentBytes, actual);
		}
		#endregion

		private void WriteToStream(IO.Stream stream, byte[] contentBytes) {
			stream.Write(contentBytes, 0, contentBytes.Length);
			stream.Position = 0;
		}

		private byte[] GetByteRange() {
			return Enumerable.Range(0, 1024)
				.Select(x => (byte) x)
				.ToArray();
		}

		private IO.Stream GetStream() {
			return new MemoryStream();
		}
	}
}