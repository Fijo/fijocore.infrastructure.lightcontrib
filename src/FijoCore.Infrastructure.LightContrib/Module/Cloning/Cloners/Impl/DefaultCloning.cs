﻿using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners.Impl {
	[PublicAPI]
	public class DefaultCloning : ICloning {
		#region Implementation of ICloning
		public CloningType Type { get { return CloningType.Default; } }

		public T Pass<T>(T source) {
			return source;
		}

		public ICloning GetDeeper() {
			return this;
		}
		#endregion
	}
}