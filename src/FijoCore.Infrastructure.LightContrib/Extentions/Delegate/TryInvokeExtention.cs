using System;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Delegate {
	[PublicAPI]
	public static class TryInvokeExtention {
		[DebuggerStepThrough, PublicAPI]
		public static void TryInvoke(this Action me) {
			if(me != null) me();
		}

		[DebuggerStepThrough, PublicAPI]
		public static void TryInvoke<T>(this Action<T> me, T arg1) {
			if(me != null) me(arg1);
		}

		[DebuggerStepThrough, PublicAPI]
		public static void TryInvoke<T1, T2>(this Action<T1, T2> me, T1 arg1, T2 arg2) {
			if(me != null) me(arg1, arg2);
		}
		
		[DebuggerStepThrough, PublicAPI]
		public static void TryInvoke<T1, T2, T3>(this Action<T1, T2, T3> me, T1 arg1, T2 arg2, T3 arg3) {
			if(me != null) me(arg1, arg2, arg3);
		}
		
		[DebuggerStepThrough, PublicAPI]
		public static void TryInvoke<T1, T2, T3, T4>(this Action<T1, T2, T3, T4> me, T1 arg1, T2 arg2, T3 arg3, T4 arg4) {
			if(me != null) me(arg1, arg2, arg3, arg4);
		}
	}
}