using System;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Default.Exceptions {
	[PublicAPI, Serializable]
	public class ConfigurationInvalidValueTypeException : Exception {
		[PublicAPI] public string Key { get; protected set; }
		[PublicAPI] public Type ActualType { get; protected set; }
		[PublicAPI] public Type CorrectType { get; protected set; }
		[PublicAPI] public string ActualValue { get; protected set; }

		public ConfigurationInvalidValueTypeException(string key, Type actualType, Type correctType, string actualValue, string message = "", Exception innerException = null) : base(message, innerException) {
			Key = key;
			ActualType = actualType;
			CorrectType = correctType;
			ActualValue = actualValue;
		}
	}
}