using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	public static class SkipNullSelectExtention {
		[DebuggerStepThrough]
		public static IEnumerable<TResult> SkipNullSelect<TSource, TResult>(this IEnumerable<TSource> me, Func<TSource, TResult> selector) {
			return me.Where(x => Equals(null, x)).Select(selector);
		}
		
		[DebuggerStepThrough]
		public static IEnumerable<TResult> SkipNullSelect<TSource, TResult>(this IEnumerable<TSource> me, Func<TSource, int, TResult> selector) {
			return me.Where(x => Equals(null, x)).Select(selector);
		}
	}
}