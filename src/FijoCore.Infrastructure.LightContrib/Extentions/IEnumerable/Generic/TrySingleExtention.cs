using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.Infrastructure.Documentation.Enums;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	[PublicAPI]
	public static class TrySingleExtention {
		[Related(ObjectType.Function, "System.Linq.Enumerable.SingleOrDefault<TSource>(IEnumerable<TSource>)", "same functionality, but with try-out style, same more than 1 element handling")]
		[Desc("Will return the succsess (whether me.Any()). In a successful case result will be set to the single entry in the sequence. More than 1 element will be handled using a InvalidOperationException.")]
		[Pure, PublicAPI, DebuggerStepThrough]
		public static bool TrySingle<TSource>([NotNull] this IEnumerable<TSource> me, out TSource result) {
			using(var enumerator = me.GetEnumerator()) return enumerator.TrySingle(out result);
		}
		
		[Related(ObjectType.Function, "System.Linq.Enumerable.SingleOrDefault<TSource>(IEnumerable<TSource>, Func<TSource, bool>)", "same functionality, but differences as in TrySingleExtention.TrySingle<TSource>(IEnumerable<TSource>, out TSource)")]
		[Related(ObjectType.Function, "FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic.TrySingleExtention.TrySingle<TSource>(IEnumerable<TSource, out TSource)", "same functionality, but only candidates that matches the predicate will be used.")]
		[Pure, PublicAPI, DebuggerStepThrough]
		public static bool TrySingle<TSource>([NotNull] this IEnumerable<TSource> me, [NotNull] Func<TSource, bool> predicate, out TSource result) {
			return me.Where(predicate).TrySingle(out result);
		}
		
		[Related(ObjectType.Function, "FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic.TrySingleExtention.TrySingle<TSource>(IEnumerable<TSource>, Func<TSource, bool>, out TSource)", "the same but the the current index of the element in the source sequence is avalible to predicate.")]
		[Pure, PublicAPI, DebuggerStepThrough]
		public static bool TrySingle<TSource>([NotNull] this IEnumerable<TSource> me, [NotNull] Func<TSource, int, bool> predicate, out TSource result) {
			return me.Where(predicate).TrySingle(out result);
		}
	}
}