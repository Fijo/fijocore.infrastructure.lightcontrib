using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic
{
	public static class HasOnlyExtention
	{
		[DebuggerStepThrough]
		public static bool HasOnly<T>([NotNull] this IEnumerable<T> me, T entry)
		{
			return me.All(x => Equals(x, entry));
		}
	}
}