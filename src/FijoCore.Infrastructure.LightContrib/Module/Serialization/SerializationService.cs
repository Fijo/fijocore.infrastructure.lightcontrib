using System.IO;
using FijoCore.Infrastructure.LightContrib.Enums;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Settings;
using FijoCore.Infrastructure.LightContrib.Module.Stream;
using JetBrains.Annotations;
using IO = System.IO;
using Text = System.Text;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization {
	[UsedImplicitly]
	public class SerializationService : ISerializationService {
		private readonly IStreamService _streamService;
		public SerializationService(IStreamService streamService) {
			_streamService = streamService;
		}

		public void Serialize(ISerialization serialization, object obj, IO.Stream stream, SerializationFormatting formatting = SerializationFormatting.Default, Text.Encoding encoding = null, ISerializationSettings serializationSettings = null) {
			serialization.Serialize(obj, stream, formatting, encoding, serializationSettings);
		}
		
		public void Serialize<T>(ISerialization serialization, T obj, IO.Stream stream, SerializationFormatting formatting = SerializationFormatting.Default, Text.Encoding encoding = null, ISerializationSettings serializationSettings = null) {
			serialization.Serialize(obj, stream, formatting, encoding, serializationSettings);
		}

		public string Serialize(ISerialization serialization, object obj, SerializationFormatting formatting = SerializationFormatting.Default, Text.Encoding encoding = null, ISerializationSettings serializationSettings = null) {
			var stream = new MemoryStream();
			Serialize(serialization, obj, stream, formatting, encoding, serializationSettings);
			stream.Position = 0;
			return _streamService.ReadToString(stream, encoding);
		}

		public string Serialize<T>(ISerialization serialization, T obj, SerializationFormatting formatting = SerializationFormatting.Default, Text.Encoding encoding = null, ISerializationSettings serializationSettings = null) {
			var stream = GetStream();
			Serialize(serialization, obj, stream, formatting, encoding, serializationSettings);
			stream.Position = 0;
			return _streamService.ReadToString(stream, encoding);
		}

		public object Deserialize(ISerialization serialization, IO.Stream stream, Text.Encoding encoding = null, ISerializationSettings serializationSettings = null) {
			return serialization.Deserialize(stream, encoding, serializationSettings);
		}

		public T Deserialize<T>(ISerialization serialization, IO.Stream stream, Text.Encoding encoding = null, ISerializationSettings serializationSettings = null) {
			return serialization.Deserialize<T>(stream, encoding, serializationSettings);
		}

		public object Deserialize(ISerialization serialization, string source, Text.Encoding encoding = null, ISerializationSettings serializationSettings = null) {
			return Deserialize(serialization, GetStream(source, encoding), encoding, serializationSettings);
		}

		public T Deserialize<T>(ISerialization serialization, string source, Text.Encoding encoding = null, ISerializationSettings serializationSettings = null) {
			return Deserialize<T>(serialization, GetStream(source, encoding), encoding, serializationSettings);
		}

		[NotNull, Pure]
		private IO.Stream GetStream(string content, Text.Encoding encoding) {
			var stream = new MemoryStream();
			_streamService.WriteToStream(stream, content, encoding);
			stream.Position = 0;
			return stream;
		}

		[NotNull, Pure]
		private IO.Stream GetStream() {
			return new MemoryStream();
		}
	}
}