using System;

namespace FijoCore.Infrastructure.LightContrib.Enums {
	[Serializable]
	public enum RelationOperation : byte {
		Equals,
		IsIn,
		NotIn,
		NotEquals,
		AtLeast,
		More,
		Less,
		LessOrEqual
	}
}