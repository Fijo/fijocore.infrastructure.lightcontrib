﻿namespace FijoCore.Infrastructure.LightContrib.Delegates {
	public delegate TAccumulate AggregateFunc<TAccumulate, in TParam, in TKey>(TAccumulate aggregate, TParam param, TKey key);
}