using System.Collections.Generic;
using FijoCore.Infrastructure.LightContrib.Module.Perf.ListType.Interface;

namespace FijoCore.Infrastructure.LightContrib.Module.Perf.ListType.Impl {
	public class FixedListType : IListType {
		#region Implementation of IListType
		public void Add<T>(IList<T> list, int index, T item) {
			list[index] = item;
		}

		public IList<T> Create<T>(int size) {
			return new T[size];
		}
		#endregion
	}
}