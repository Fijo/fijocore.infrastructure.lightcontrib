using FijoCore.Infrastructure.LightContrib.Enums;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Settings;
using JetBrains.Annotations;
using Text = System.Text;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization {
	public interface ISerializationService {
		void Serialize(ISerialization serialization, object obj, System.IO.Stream stream, SerializationFormatting formatting = SerializationFormatting.Default, [CanBeNull] Text.Encoding encoding = null, [CanBeNull] ISerializationSettings serializationSettings = null);
		void Serialize<T>(ISerialization serialization, T obj, System.IO.Stream stream, SerializationFormatting formatting = SerializationFormatting.Default, [CanBeNull] Text.Encoding encoding = null, [CanBeNull] ISerializationSettings serializationSettings = null);
		string Serialize(ISerialization serialization, object obj, SerializationFormatting formatting = SerializationFormatting.Default, [CanBeNull] Text.Encoding encoding = null, [CanBeNull] ISerializationSettings serializationSettings = null);
		string Serialize<T>(ISerialization serialization, T obj, SerializationFormatting formatting = SerializationFormatting.Default, [CanBeNull] Text.Encoding encoding = null, [CanBeNull] ISerializationSettings serializationSettings = null);
		object Deserialize(ISerialization serialization, System.IO.Stream stream, [CanBeNull] Text.Encoding encoding = null, [CanBeNull] ISerializationSettings serializationSettings = null);
		T Deserialize<T>(ISerialization serialization, System.IO.Stream stream, [CanBeNull] Text.Encoding encoding = null, [CanBeNull] ISerializationSettings serializationSettings = null);
		object Deserialize(ISerialization serialization, string source, [CanBeNull] Text.Encoding encoding = null, [CanBeNull] ISerializationSettings serializationSettings = null);
		T Deserialize<T>(ISerialization serialization, string source, [CanBeNull] Text.Encoding encoding = null, [CanBeNull] ISerializationSettings serializationSettings = null);
	}
}