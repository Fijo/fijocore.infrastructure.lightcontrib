using System.Diagnostics;
using System.Globalization;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Char {
	[PublicAPI]
	public static class ToLowerExtention {
		[Pure, DebuggerStepThrough, PublicAPI]
		[Desc("Gets the lowered value of the current char (�me�) if any using an invariant culture settings. Otherwise it returns the current char.")]
		public static char InvariantToLower(this char me) {
			return char.ToLowerInvariant(me);
		}

		[Pure, DebuggerStepThrough, PublicAPI]
		[Desc("Gets the lowered value of the current char (�me�) if any. Otherwise it returns the current char.")]
		public static char ToLower(this char me, [NotNull] CultureInfo culture) {
			return char.ToLower(me, culture);
		}
	}
}