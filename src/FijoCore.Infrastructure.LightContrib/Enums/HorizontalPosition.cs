namespace FijoCore.Infrastructure.LightContrib.Enums {
	public enum HorizontalPosition {
		Left,
		Right,
		Both
	}
}