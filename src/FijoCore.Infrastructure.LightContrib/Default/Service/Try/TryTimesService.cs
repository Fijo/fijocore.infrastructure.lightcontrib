using System;
using System.Collections.Generic;
using System.Linq;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Default.Exceptions;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Perf.ListType.Interface;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Default.Service.Try {
	[UsedImplicitly]
	public class TryTimesService : TryServiceBase, ITryTimesService {
		private readonly IListTypeProvider _listTypeProvider = Kernel.Resolve<IListTypeProvider>();
		private readonly IOut _out = Kernel.Resolve<IOut>();

		#region abstract functionality
		protected virtual TResult InnerTryTimes<TResult, TCaughtException, TRetiesReachedException>(TryCatchTemplate<TResult, TCaughtException> tryCatchTemplate, IListType listType, Func<IList<TCaughtException>, TRetiesReachedException> exceptionFactory, Func<TResult> func, int maxTries)
			where TRetiesReachedException : ExMaxRetriesReachedException {
			#region PreCondition
#if DEBUG
			PreCondition(maxTries);
#endif
			#endregion
			var exceptions = listType.Create<TCaughtException>(maxTries);
			for (var @try = 0; @try != maxTries; @try++) {
				TResult result;
				TCaughtException ex;
				if(tryCatchTemplate(func, out result, out ex)) return result;
				listType.Add(exceptions, @try, ex);
			}
			throw GetExRetriesException<TResult, TCaughtException, TRetiesReachedException>(exceptionFactory, maxTries, exceptions);
		}

		protected virtual Exception GetExRetriesException<TResult, TCaughtException, TRetiesReachedException>(Func<IList<TCaughtException>, TRetiesReachedException> exceptionFactory, int maxTries, IList<TCaughtException> exceptions) where TRetiesReachedException : ExMaxRetriesReachedException {
			var retiesReachedException = exceptionFactory(exceptions);
			retiesReachedException.RetryLimit = maxTries;
			return retiesReachedException;
		}

		protected TResult InnerTryTimes<TResult, TCaughtException, TRetiesReachedException>(TryCatchTemplate<TResult, TCaughtException> tryCatchTemplate, Func<IList<TCaughtException>, TRetiesReachedException> exceptionFactory, Func<TResult> func, int maxTries)
			where TRetiesReachedException : ExMaxRetriesReachedException {
			#region PreCondition
			PreCondition(maxTries);
			#endregion
			return InnerTryTimes(tryCatchTemplate, _listTypeProvider.GetPreferedList(maxTries), exceptionFactory, func, maxTries);
		}

		protected TResult InnerTryTimes<TResult, TCaughtException, TRetiesReachedException>(Func<IList<TCaughtException>, TRetiesReachedException> exceptionFactory, Func<TResult> func, int maxTries)
			where TRetiesReachedException : ExMaxRetriesReachedException where TCaughtException : Exception {
			return InnerTryTimes(GenericTryCatchTemplate, exceptionFactory, func, maxTries);
		}

		protected TResult InnerTryTimes<TResult, TRetiesReachedException>(Func<IList<Exception>, TRetiesReachedException> exceptionFactory, Func<TResult> func, int maxTries)
			where TRetiesReachedException : ExMaxRetriesReachedException {
			return InnerTryTimes(ExceptionTryCatchTemplate, exceptionFactory, func, maxTries);
		}
		#endregion
		
		#region public overloadings
		public TResult TryTimes<TResult, TCaughtException, TRetiesReachedException>(Func<TRetiesReachedException> exceptionFactory, Func<TResult> func, int maxTries)
			where TRetiesReachedException : ExMaxRetriesReachedException where TCaughtException : Exception {
			return InnerTryTimes((IList<TCaughtException> list) => ExceptionFactoryWrapper(exceptionFactory, list), func, maxTries);
		}

		protected TRetiesReachedException ExceptionFactoryWrapper<TCaughtException, TRetiesReachedException>(Func<TRetiesReachedException> exceptionFactory, IEnumerable<TCaughtException> list) where TRetiesReachedException : ExMaxRetriesReachedException where TCaughtException : Exception {
			var ex = exceptionFactory();
			ex.GenericInnerTrysExceptions = list.Cast<Exception>().ToArray();
			return ex;
		}

		public TResult TryTimes<TResult, TRetiesReachedException>(Func<TRetiesReachedException> exceptionFactory, Func<TResult> func, int maxTries)
			where TRetiesReachedException : ExMaxRetriesReachedException {
			return InnerTryTimes(list => ExceptionFactoryWrapper(exceptionFactory, list), func, maxTries);
		}

		protected TRetiesReachedException ExceptionFactoryWrapper<TRetiesReachedException>(Func<TRetiesReachedException> exceptionFactory, IList<Exception> list) where TRetiesReachedException : ExMaxRetriesReachedException {
			var ex = exceptionFactory();
			ex.GenericInnerTrysExceptions = list;
			return ex;
		}

		public TResult TryTimes<TResult, TCaughtException, TRetiesReachedException>(Func<TResult> func, int maxTries)
			where TRetiesReachedException : ExMaxRetriesReachedException, new() where TCaughtException : Exception {
			return InnerTryTimes<TResult, TCaughtException, TRetiesReachedException>(GenericExceptionFactory<TRetiesReachedException, TCaughtException>, func, maxTries);
		}

		private TRetiesReachedException GenericExceptionFactory<TRetiesReachedException, TCaughtException>(IList<TCaughtException> list) where TRetiesReachedException : ExMaxRetriesReachedException, new() {
			return new TRetiesReachedException {GenericInnerTrysExceptions = list.Cast<Exception>().ToArray()};
		}

		public TResult TryTimes<TResult, TRetiesReachedException>(Func<TResult> func, int maxTries)
			where TRetiesReachedException : ExMaxRetriesReachedException, new() {
			return InnerTryTimes(SimpleExceptionFactory<TRetiesReachedException>, func, maxTries);
		}

		private TRetiesReachedException SimpleExceptionFactory<TRetiesReachedException>(IList<Exception> list) where TRetiesReachedException : ExMaxRetriesReachedException, new() {
			return new TRetiesReachedException {GenericInnerTrysExceptions = list};
		}
		#endregion

		#region TryCatchTemplates
		protected bool GenericTryCatchTemplate<TResult, TException>(Func<TResult> func, out TResult result, out TException exception) where TException : Exception {
			try {
				result = func();
			}
			catch (TException ex) {
				exception = ex;
				return _out.False(out result);
			}
			return _out.True(out exception);
		}
		
		protected bool ExceptionTryCatchTemplate<TResult>(Func<TResult> func, out TResult result, out Exception exception) {
			try {
				result = func();
			}
			catch (Exception ex) {
				exception = ex;
				return _out.False(out result);
			}
			return _out.True(out exception);
		}

		protected delegate bool TryCatchTemplate<TResult, TException>(Func<TResult> func, out TResult result, out TException exception);
		#endregion
	}
}