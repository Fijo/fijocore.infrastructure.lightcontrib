using System.Diagnostics;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Generic {
	[PublicAPI]
	public static class IsInExtention {
		[Pure, DebuggerStepThrough, PublicAPI]
		[Desc("returns if this is contained in a the given param array")]
		public static bool IsIn<T>(this T me, [NotNull] params T[] searchParam) {
			return searchParam.Contains(me);
		}

		[Pure, DebuggerStepThrough, PublicAPI]
		[Desc("returns if this is contained in a the given collection")]
		public static bool IsIn<T>(this T me, [NotNull] System.Collections.Generic.IEnumerable<T> searchParam) {
			return searchParam.Contains(me);
		}
	}
}