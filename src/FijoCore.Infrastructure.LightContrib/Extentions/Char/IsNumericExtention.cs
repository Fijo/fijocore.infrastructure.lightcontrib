using System.Collections.Generic;
using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Char {
	[PublicAPI]
	public static class IsNumericExtention {
		[NotNull] private static readonly ICollection<char> NumericChars = "0123456789".ToCharArray();

		[Pure, DebuggerStepThrough, PublicAPI]
		[Desc("Returns if if the given char is one of these �0123456789�. Otherwise it will return false.")]
		public static bool IsNumeric([NotNull] this char? me) {
			// ReSharper disable PossibleInvalidOperationException
			return ((char) me).IsNumeric();
			// ReSharper restore PossibleInvalidOperationException
		}

		[Pure, DebuggerStepThrough, PublicAPI]
		[Desc("Returns if if the given char is one of these �0123456789�. Otherwise it will return false.")]
		public static bool IsNumeric(this char me) {
			return NumericChars.Contains(me);
		}
	}
}