using System;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream.Impl.Specific {
	[UsedImplicitly]
	public class UInt16StreamSerializer : FixedSizeStreamSerializer<ushort> {
		#region Overrides of FixedSizeStreamSerializer<ushort>
		protected override int Size { get { return 2; } }
		protected override byte[] Serialize(ushort obj) {
			return BitConverter.GetBytes(obj);
		}

		protected override ushort Deserialize(byte[] bytes) {
			return BitConverter.ToUInt16(bytes, 0);
		}
		#endregion
	}
}