using System.Collections.Generic;
using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	[PublicAPI]
	public static class ConsumeExtention {
		[PublicAPI, DebuggerStepThrough]
		[Desc("Executes the IEnumerable fully without doing anything with the results")]
		public static void Consume<T>([NotNull] this IEnumerable<T> me) {
			me.ForEach(x => {});
		}
	}
}