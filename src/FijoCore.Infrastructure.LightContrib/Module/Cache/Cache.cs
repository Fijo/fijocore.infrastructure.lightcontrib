using System;
using FijoCore.Infrastructure.LightContrib.Module.Cache.Base;
using FijoCore.Infrastructure.LightContrib.Module.Cache.Interface;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Cache {
	[PublicAPI]
	public abstract class Cache : CacheBase<Type, object>, ICache {
		protected override Type ConvertKey(Type type) {
			return type;
		}

		protected override object Create<TObj>() {
			throw new NotImplementedException();
		}
	}
}