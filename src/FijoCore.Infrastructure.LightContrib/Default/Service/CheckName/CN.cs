﻿using System;
using System.Linq.Expressions;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Default.Service.CheckName {
	[About("CompilerCheckNamesHelper")]
	[AboutName("C", "Check")]
	[AboutName("N", "Name")]
	public static class CN {
		[Pure]
		public static string Get<T>() {
			return Get(typeof (T));
		}

		[Pure]
		public static string Get(Type type) {
			return type.Name;
		}

		[Pure]
		public static string Get<T>(Expression<Action<T>> me) {
			Expression expression = me.Body;
			return Name(expression);
		}

		[Pure]
		public static string Get<T>(Expression<Func<T>> me) {
			var expression = me.Body;
			return Name(expression);
		}

		[Pure]
		public static string Get<TSource, TResult>(Expression<Func<TSource, TResult>> me) {
			var expression = me.Body;
			return Name(expression);
		}

		[Pure]
		[Link("http://msdn.microsoft.com/de-de/library/system.linq.expressions.unaryexpression.aspx", "http://stackoverflow.com/questions/3567857/why-are-some-object-properties-unaryexpression-and-others-memberexpression")]
		private static string Name(Expression expression) {
			// removes uninteresing casts (generaly type changes), not statements, ...
			UnaryExpression unAry;
			if ((unAry = (expression as UnaryExpression)) != null) return Name(unAry.Operand);

			var methodCallExpression = expression as MethodCallExpression;
			if (methodCallExpression != null) return methodCallExpression.Method.Name;
			// ToDo test this
			var memberExpression = expression as MemberExpression;
			if (memberExpression != null) return memberExpression.Member.Name;
			throw new NotSupportedException();
		}
	}
}
