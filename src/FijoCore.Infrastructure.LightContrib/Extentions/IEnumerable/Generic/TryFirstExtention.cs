using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.Infrastructure.Documentation.Enums;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	[PublicAPI]
	public static class TryFirstExtention {
		[Related(ObjectType.Function, "System.Linq.Enumerable.FirstOrDefault<TSource>(IEnumerable<TSource>)", "same functionality, but with try-out style")]
		[Desc("Will return the succsess (whether me.Any()). In a successful case result will be set to the first entry in the sequence.")]
		[Pure, PublicAPI, DebuggerStepThrough]
		public static bool TryFirst<TSource>([NotNull] this IEnumerable<TSource> me, out TSource result) {
			using(var enumerator = me.GetEnumerator()) return enumerator.TryFirst(out result);
		}
		
		[Related(ObjectType.Function, "System.Linq.Enumerable.FirstOrDefault<TSource>(IEnumerable<TSource>, Func<TSource, bool>)", "same functionality, but differences as in TryFirstExtention.TryFirst<TSource>(IEnumerable<TSource>, out TSource)")]
		[Related(ObjectType.Function, "FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic.TryFirstExtention.TryFirst<TSource>(IEnumerable<TSource, out TSource)", "same functionality, but only candidates that matches the predicate will be used.")]
		[Pure, PublicAPI, DebuggerStepThrough]
		public static bool TryFirst<TSource>([NotNull] this IEnumerable<TSource> me, [NotNull] Func<TSource, bool> predicate, out TSource result) {
			return me.Where(predicate).TryFirst(out result);
		}
		
		[Related(ObjectType.Function, "FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic.TryFirstExtention.TryFirst<TSource>(IEnumerable<TSource>, Func<TSource, bool>, out TSource)", "the same but the the current index of the element in the source sequence is avalible to predicate.")]
		[Pure, PublicAPI, DebuggerStepThrough]
		public static bool TryFirst<TSource>([NotNull] this IEnumerable<TSource> me, [NotNull] Func<TSource, int, bool> predicate, out TSource result) {
			return me.Where(predicate).TryFirst(out result);
		}
	}
}