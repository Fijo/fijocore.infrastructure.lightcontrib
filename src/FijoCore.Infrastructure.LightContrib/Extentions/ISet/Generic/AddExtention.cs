using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using FijoCore.Infrastructure.LightContrib.Default.Exceptions;
using JetBrains.Annotations;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic.Equality.GetHashCode;

namespace FijoCore.Infrastructure.LightContrib.Extentions.ISet.Generic {
	[PublicAPI]
	public static class AddExtention {
		[PublicAPI, DebuggerStepThrough]
		public static void AddSecure<T>([NotNull] this ISet<T> me, T item) {
			if (!me.Add(item)) throw new KeyAlreadyExistException<ISet<T>, T>(me, item);
		}
	}
}