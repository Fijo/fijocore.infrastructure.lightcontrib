using FijoCore.Infrastructure.LightContrib.Extentions.Type;
using NUnit.Framework;
using SType = System.Type;

namespace FijoCore.Infrastructure.LightContribTest.Extentions.Type {
	[TestFixture]//(typeof (ImplementsTypeExtention))
	public class ImplementsTypeExtentionTest {
		#region TestClasses
		private class BaseClass {}

		private interface ITest1 : ITest2 {}

		private interface ITest2 {}

		private class Testclass1 : Testclass2, ITest1 {}

		private class Testclass2 : BaseClass, ITest2 {}

		private class Testclass3 : BaseClass {}

		private class Testclass4 {}
		#endregion

		[TestCase(false, typeof (Testclass1), typeof (Testclass1))]
		[TestCase(true, typeof (Testclass2), typeof (Testclass1))]
		[TestCase(false, typeof (Testclass3), typeof (Testclass1))]
		[TestCase(false, typeof (Testclass4), typeof (Testclass1))]
		[TestCase(true, typeof (BaseClass), typeof (Testclass1))]
		[TestCase(true, typeof (BaseClass), typeof (Testclass2))]
		[TestCase(true, typeof (BaseClass), typeof (Testclass3))]
		[TestCase(false, typeof (BaseClass), typeof (Testclass4))]
		[TestCase(true, typeof (ITest1), typeof (Testclass1))]
		[TestCase(true, typeof (ITest2), typeof (Testclass1))]
		[TestCase(false, typeof (ITest1), typeof (Testclass2))]
		[TestCase(true, typeof (ITest2), typeof (Testclass2))]
		[TestCase(false, typeof (ITest1), typeof (Testclass3))]
		[TestCase(false, typeof (ITest2), typeof (Testclass3))]
		[TestCase(false, typeof (ITest2), typeof (BaseClass))]
		[TestCase(true, typeof (ITest2), typeof (ITest1))]
		[TestCase(false, typeof (ITest1), typeof (ITest1))]
		[TestCase(false, typeof (ITest1), typeof (ITest2))]
		public void BaseTypeAndType_ImplementsType_MustReturnValue(bool expected, SType baseType, SType type) {
			var actual = type.ImplementsType(baseType);
			Assert.AreEqual(expected, actual);
		}
	}
}