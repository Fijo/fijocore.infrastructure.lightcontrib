using System;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Configuration.ConverterKey {
	public interface ITypeChangeFactory {
		[NotNull]
		TypeChangeDto Create([NotNull] Type source, [NotNull] Type target);
	}
}