using System.Threading.Tasks;
using Fijo.Infrastructure.DesignPattern.Repository;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;

namespace FijoCore.Infrastructure.LightContrib.Module.Perf.Configuration {
	public class PerfConfigurationProvider : SingletonRepositoryBase<PerfConfiguration> {
		private readonly PerfConfigurationUpdaterState _perfConfigurationUpdaterState = new PerfConfigurationUpdaterState();

		#region Overrides of SingletonRepositoryBase<PerfConfiguration>
		protected override PerfConfiguration Create() {
			var perfConfiguration = InternalCreate();
			Kernel.Resolve<IPerfConfigurationService>().Adjust(perfConfiguration);
			HandleUpdaterAndState(perfConfiguration);
			return perfConfiguration;
		}

		private PerfConfiguration InternalCreate() {
			return new PerfConfiguration();
		}

		public override PerfConfiguration Get() {
			_perfConfigurationUpdaterState.Requests++;
			return base.Get();
		}
		#endregion

		#region Updater interaction
		protected virtual void HandleUpdaterAndState(PerfConfiguration perfConfiguration) {
			_perfConfigurationUpdaterState.PerfConfiguration = perfConfiguration;
			AsyncStartBackgroundThread();
		}

		protected void AsyncStartBackgroundThread() {
			new Task(StartBackgroundThread).Start();
		}

		protected virtual void StartBackgroundThread() {
			Kernel.Resolve<IPerfConfigurationUpdater>().Start(_perfConfigurationUpdaterState);
		}
		#endregion
	}
}