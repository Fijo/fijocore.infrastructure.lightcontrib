using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.GAll.Interface;

namespace FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.CGAll.Interface {
	public interface ICGAllHandling<in TSource, out TResult> : IGAllHandling<TSource, TResult> {
		bool IsUsed(TSource source);
	}
}