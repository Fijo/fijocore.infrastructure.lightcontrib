using FijoCore.Components.Extentions.Double;
using NUnit.Framework;

namespace FijoCore.ComponentTest.Extentions.Double {
	[TestFixture]
	public class ToCustomPointStringTest
	{
		[TestCase(0, 15, 15, "0")]
		[TestCase(1, 15, 15, "1")]
		[TestCase(1, 5, 5, "1")]
		[TestCase(10, 15, 15, "10")]
		[TestCase(100, 15, 15, "100")]
		[TestCase(1000, 15, 15, "1000")]
		[TestCase(10000, 15, 15, "1e+4")]
		[TestCase(342.23, 15, 15, "342.23")]
		[TestCase(342, 15, 15, "342")]
		public void Test(double value, int maxLeft, int maxRight, string wanted) {
			Assert.AreEqual(wanted, value.ToCustomPointString(maxLeft, maxRight));
		}
		
		[TestCase(0, 15, 15, "0", "0")]
		[TestCase(0, 15, 15, "0.0", "0.0")]
		[TestCase(1, 15, 15, "0.0", "1")]
		[TestCase(-1, 15, 15, "0", "-1")]
		public void CustomNullTest(double value, int maxLeft, int maxRight, string nullValue, string wanted) {
			Assert.AreEqual(wanted, value.ToCustomPointString(maxLeft, maxRight, nullValue));
		}
	}
}