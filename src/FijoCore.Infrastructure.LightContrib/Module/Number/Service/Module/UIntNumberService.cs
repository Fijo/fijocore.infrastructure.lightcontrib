using FijoCore.Infrastructure.LightContrib.Module.Number.Service.Base;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Number.Service.Module {
	[UsedImplicitly]
	public class UIntNumberService : NumberServiceBase<uint> {
		#region Implementation of INumberService<byte>
		public override uint MaxValue { get { return uint.MaxValue; } }
		public override uint MinValue { get { return uint.MinValue; } }
		public override bool IsInteger { get { return true; } }

		public override uint Sun(uint left, uint right) {
			return left + right;
		}

		public override uint Diff(uint left, uint right) {
			return left - right;
		}

		public override uint Times(uint left, uint right) {
			return left * right;
		}

		public override uint Divide(uint left, uint right) {
			return left / right;
		}

		public override uint Modulo(uint left, uint right) {
			return left % right;
		}
		#endregion
	}
}