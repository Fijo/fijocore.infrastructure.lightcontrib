using System;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Default.Exceptions {
	[PublicAPI, Serializable]
	public class NotSupportedReadOnlyCollectionModifyException : NotSupportedModifyException {
		public NotSupportedReadOnlyCollectionModifyException() {}

		public NotSupportedReadOnlyCollectionModifyException(string message) : base(message) {}

		public NotSupportedReadOnlyCollectionModifyException(string message, Exception innerException) : base(message, innerException) {}
	}
}