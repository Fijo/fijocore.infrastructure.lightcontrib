using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic.KeyValuePair {
	[PublicAPI]
	public static class ContainsValueExtention {
		[Pure, PublicAPI]
		public static bool ContainsValue<TKey, TValue>([NotNull] this IEnumerable<KeyValuePair<TKey, TValue>> me, TValue value) {
			return me.WhereValue(value).Any();
		}
	}
}