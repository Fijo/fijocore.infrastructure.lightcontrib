using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Service.UniqueAdd.Interface
{
    public interface IUniqueAddRangeService
    {
    	void AssertUniqueAddRange<TAddable, T>(TAddable me, [NotNull] IEnumerable<T> collection, [NotNull] Func<TAddable, T, bool> conatins, [NotNull] Action<TAddable, IEnumerable<T>> addRange);
    	void UniqueAddRange<TAddable, T>(TAddable me, [NotNull] IEnumerable<T> collection, [NotNull] Func<TAddable, T, bool> conatins, [NotNull] Action<TAddable, IEnumerable<T>> addRange);
    }
}