using System;
using FijoCore.Infrastructure.LightContrib.Module.Pool.Dto;

namespace FijoCore.Infrastructure.LightContrib.Module.Pool.Interface {
	public interface IBlockablePool<T, in TEntry> : IPool<T, TEntry> where TEntry : PoolEntry<T> {
		void Get(Action<T> action);
		void Finish(T obj);
	}
}