namespace FijoCore.Infrastructure.LightContrib.Dto {
	public class DictionaryType {
		public readonly System.Type Key;
		public readonly System.Type Value;
		public readonly bool IsGeneric;

		public DictionaryType(System.Type key, System.Type value, bool isGeneric) {
			Key = key;
			Value = value;
			IsGeneric = isGeneric;
		}
	}
}