using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace FijoCore.Infrastructure.LightContrib.Module.Pool.Dto {
	[Dto]
	public class PoolEntry<T> {
		public T Content;
		
		#if DEBUG
		[Desc("Says if the entity is initialized and availible to be used")] public bool IsActive;
		[Desc("Says if the entry is �blocked�")] public bool IsInUse;
		#endif
	}
}