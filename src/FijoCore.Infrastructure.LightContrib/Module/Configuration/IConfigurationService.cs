using System;

namespace FijoCore.Infrastructure.LightContrib.Module.Configuration {
	public interface IConfigurationService {
		T Get<T>(Type context, string key);
		T Get<T, TContext>(string key);
		T Get<T>(string key);
	}
}