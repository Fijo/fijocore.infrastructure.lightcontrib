using System;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface {
	public interface IOut {
		bool True<T>(out T obj, [CanBeNull] T value = default(T));
		bool False<T>(out T obj, [CanBeNull] T value = default(T));
		bool Generic<T>(out T obj, bool returnType, [CanBeNull] T trueValue = default(T), [CanBeNull] T falseValue = default(T));
		bool Generic<T>(out T obj, bool returnType, [NotNull] Func<T> trueValueResolver, [CanBeNull] T falseValue = default(T));
		bool Generic<T>(out T obj, bool returnType, [CanBeNull] T trueValue, [NotNull] Func<T> falseValueResolver);
		bool Generic<T>(out T obj, bool returnType, [NotNull] Func<T> trueValueResolver, [NotNull] Func<T> falseValueResolver);
	}
}