using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace FijoCore.Infrastructure.LightContrib.Repositories {
	[Repository]
	public interface IAllFileRepository {
		IEnumerable<string> Get(string folder);
	}
}