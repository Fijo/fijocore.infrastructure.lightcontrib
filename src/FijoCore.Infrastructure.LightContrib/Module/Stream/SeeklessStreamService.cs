using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fijo.Infrastructure.DesignPattern.Lazy.Interface;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Module.Encoding;
using JetBrains.Annotations;
using IO = System.IO;

namespace FijoCore.Infrastructure.LightContrib.Module.Stream {
	[UsedImplicitly]
	public class SeeklessStreamService : StreamServiceBase, ISeeklessStreamService {
		#region SeeklessStreamSequenceReader classes
		class SeeklessStreamSequenceReader {
			[NotNull]
			public virtual IEnumerable<IEnumerable<byte>> ReadToBytes([NotNull] IO.Stream stream, int count, int bufferSize = 1024) {
				if (bufferSize <= 0) throw new ArgumentOutOfRangeException("bufferSize", bufferSize, "must be at least 1");
				do {
					var nextBufferSize = GetNextBufferSize(bufferSize, count);
					var buffer = new byte[nextBufferSize];
					var read = stream.Read(buffer, 0, nextBufferSize);
					if (read != nextBufferSize) {
						yield return buffer.Take(read);
						yield break;
					}
					yield return buffer;
					if(CheckEnd(ref count, read)) yield break;
				} while (true);
			}

			protected virtual int GetNextBufferSize(int bufferSize, int count) {
				return bufferSize;
			}

			protected virtual bool CheckEnd(ref int count, int read) {
				return false;
			}
		}

		class SeeklessStreamWithCountSequenceReader : SeeklessStreamSequenceReader {
			public override IEnumerable<IEnumerable<byte>> ReadToBytes(IO.Stream stream, int count, int bufferSize = 1024) {
				if (count < 0) throw new ArgumentOutOfRangeException("count", count, "must at least 0");
				return count == 0
					       ? Enumerable.Empty<IEnumerable<byte>>()
					       : base.ReadToBytes(stream, count, bufferSize);
			}

			protected override int GetNextBufferSize(int bufferSize, int count) {
				#region PreCondition
				Debug.Assert(bufferSize > 0);
				Debug.Assert(count > 0);
				#endregion
				return Math.Min(bufferSize, count);
			}

			protected override bool CheckEnd(ref int count, int read) {
				count -= read;
				Debug.Assert(count >= 0);
				return count == 0;
			}
		}
		#endregion

		private readonly SeeklessStreamSequenceReader _streamSequenceReader = new SeeklessStreamSequenceReader();
		private readonly SeeklessStreamWithCountSequenceReader _streamWithCountSequenceReader = new SeeklessStreamWithCountSequenceReader();
		
		public SeeklessStreamService(ILazy<IEncodingProvider> encodingProvider) : base(encodingProvider) {}

		#region Bytes
		public override byte[] ReadToBytes(IO.Stream stream, int bufferSize = 1024) {
			return InternalSeeklessReadToBytes(stream).ToArray();
		}

		protected override byte[] InternalReadToBytes(IO.Stream stream, int count, int bufferSize = 1024) {
			return InternalSeeklessReadToBytes(stream, count: count).ToArray();
		}
		#endregion

		#region Sequence
		public override IEnumerable<byte> ReadToBytesSequence(IO.Stream stream, int bufferSize = 1024) {
			return InternalSeeklessReadToBytes(stream, bufferSize);
		}

		public override IEnumerable<byte> ReadToBytesSequence(IO.Stream stream, int count, int bufferSize = 1024) {
			return InternalSeeklessReadToBytes(stream, count, bufferSize);
		}
		#endregion

		#region Base
		private IEnumerable<byte> InternalSeeklessReadToBytes(IO.Stream stream, int bufferSize = 1024) {
			return _streamSequenceReader.ReadToBytes(stream, 0, bufferSize).OneFlat();
		}
		
		private IEnumerable<byte> InternalSeeklessReadToBytes(IO.Stream stream, int count, int bufferSize = 1024) {
			return _streamWithCountSequenceReader.ReadToBytes(stream, count, bufferSize).OneFlat();
		}
		#endregion

		//public byte[] SeeklessReadToBytes(IO.Stream stream, int bufferSize = 1024) {
		//	return SeeklessReadToBytesSequence(stream, bufferSize).ToArray();
		//}

		//public IEnumerable<byte> SeeklessReadToBytesSequence(IO.Stream stream, int bufferSize = 1024) {
		//	return InternalReadToBytes(stream, bufferSize).OneFlat();
		//}

		//private IEnumerable<IEnumerable<byte>> InternalReadToBytes(IO.Stream stream, int bufferSize = 1024) {
		//	if (bufferSize <= 0) throw new ArgumentOutOfRangeException("bufferSize", bufferSize, "must be at least 1");
		//	do {
		//		var buffer = new byte[bufferSize];
		//		var read = stream.Read(buffer, 0, bufferSize);
		//		//Debug.WriteLine("read " + read + " bytes");
		//		if (read != bufferSize) {
		//			yield return buffer.Take(read);
		//			yield break;
		//		}
		//		yield return buffer;
		//	} while (true);
		//}
	}
}