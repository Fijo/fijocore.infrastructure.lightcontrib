﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Service.OrderlessEquals.Interface;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Service.OrderlessEquals {
	// ToDo improve IEnumerable executions to get a better performance here
	[UsedImplicitly]
	public class OrderlessEqualsService : IOrderlessEqualsService {
		[DebuggerStepThrough]
		[Note("Only use this function on two uniqued IEnumerables.")]
		public bool OrderlessUniqueEquals<T>(IEnumerable<T> me, IEnumerable<T> other) {
			return OrderlessUniqueEqualsBase(me, other, (eqMe, eqOther) => eqMe.ContainsAny(eqOther));
		}

		[DebuggerStepThrough]
		[Note("Only use this function on two uniqued IEnumerables.")]
		public bool OrderlessUniqueEquals<T>(IEnumerable<T> me, IEnumerable<T> other, Func<T, T, bool> equals) {
			#region PreCondition
			if (equals == null) throw new ArgumentNullException("equals");
			#endregion

			return OrderlessUniqueEqualsBase(me, other, (eqMe, eqOther) => eqMe.ContainsAny(eqOther, equals));
		}

		[DebuggerStepThrough]
		private bool OrderlessUniqueEqualsBase<T>(IEnumerable<T> me, IEnumerable<T> other, Func<IEnumerable<T>, IEnumerable<T>, bool> containsAny) {
			#region PreCondition
			if (me == null) throw new ArgumentNullException("me");
			if (other == null) throw new ArgumentNullException("other");
#if DEBUG
			me = me.Execute();
			other = other.Execute();
			Debug.Assert(me.IsUnique());
			Debug.Assert(other.IsUnique());
#endif
			#endregion
			return MainOrderlessEqualsBase(me, other, containsAny);
		}

		[DebuggerStepThrough]
		private bool MainOrderlessEqualsBase<T>(IEnumerable<T> me, IEnumerable<T> other, Func<IEnumerable<T>, IEnumerable<T>, bool> containsAny) {
			#region PreCondition
			if (containsAny == null) throw new ArgumentNullException("containsAny");
			#endregion
			var collection = me.Execute();
			var otherCollection = other.Execute();
			return collection.Count == otherCollection.Count && containsAny(collection, otherCollection);
		}

		[DebuggerStepThrough]
		private bool OrderlessEqualsBase<T>(IEnumerable<T> me, IEnumerable<T> other, Func<IEnumerable<T>, IEnumerable<T>, bool> containsAny) {
			#region PreCondition
			if (me == null) throw new ArgumentNullException("me");
			if (other == null) throw new ArgumentNullException("other");
			#endregion
			return MainOrderlessEqualsBase(me, other, containsAny);
		}

		[DebuggerStepThrough]
		public bool OrderlessEquals<T>(IEnumerable<T> me, IEnumerable<T> other) {
			return OrderlessEqualsBase(me, other, (eqMe, eqOther) => eqMe.All(x => eqMe.CountWhere(x) == eqOther.CountWhere(x)));
		}

		[DebuggerStepThrough]
		public bool OrderlessEquals<T>(IEnumerable<T> me, IEnumerable<T> other, Func<T, T, bool> equals) {
			#region PreCondition
			if (equals == null) throw new ArgumentNullException("equals");
			#endregion
			return OrderlessEqualsBase(me, other, (eqMe, eqOther) => eqMe.All(x => eqMe.CountWhere(x, equals) == eqOther.CountWhere(x, equals)));
		}
	}
}