using System;
using System.Net;
using FijoCore.Infrastructure.LightContrib.Module.Converter.Simple.Base;

namespace FijoCore.Infrastructure.LightContrib.Module.Converter.Simple {
	public class EndPointConverter : ConverterBase<string, EndPoint> {
		#region Overrides of ConverterBase<string,EndPoint>
		public override EndPoint Convert(string source) {
			var portSeperator = source.LastIndexOf(':');
			if(portSeperator == -1) throw new ArgumentException("A string in a format like 255.255.255.255:9999 was expected. It have to contain a �:�, that seperates ip and port, but it doesn�t.", "source");
			var ip = source.Substring(0, portSeperator);
			var portNumberBegin = portSeperator + 1;
			if(portNumberBegin == source.Length) throw new ArgumentException("A string in a format like 255.255.255.255:9999 was expected. It cannot end with �:�, after that the port number should follow.", "source");
			var port = int.Parse(source.Substring(portNumberBegin));
			return new IPEndPoint(IPAddress.Parse(ip), port);
		}
		#endregion
	}
}