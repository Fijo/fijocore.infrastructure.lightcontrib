using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	public static class IsSingleExtention
	{
		[DebuggerStepThrough]
		public static bool IsSingle<TSource>([NotNull] this IEnumerable<TSource> me) {
			return me.Count() == 1;
		}
		
		[DebuggerStepThrough]
		public static bool IsSingle<TSource>([NotNull] this IEnumerable<TSource> me, [NotNull] Func<TSource, bool> predicate) {
			return me.Count(predicate) == 1;
		}
	}
}