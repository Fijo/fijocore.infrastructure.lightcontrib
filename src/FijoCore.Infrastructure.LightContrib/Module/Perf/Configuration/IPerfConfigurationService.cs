namespace FijoCore.Infrastructure.LightContrib.Module.Perf.Configuration {
	public interface IPerfConfigurationService {
		void Adjust(PerfConfiguration perfConfiguration);
	}
}