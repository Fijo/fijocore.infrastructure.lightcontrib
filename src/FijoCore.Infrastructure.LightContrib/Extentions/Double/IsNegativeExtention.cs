using System;
using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Double {
	[PublicAPI]
	public static class IsNegativeExtention {
		[Pure, DebuggerStepThrough, PublicAPI]
		[Desc("returns if the given double is smaller than 0 what means that it is negative or not")]
		public static bool IsNegative(this double me) {
			return (BitConverter.GetBytes(me)[7] & 128) == 128;
		}
	}
}