using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	public static class RecursiveSelectManyExtention
	{
		[DebuggerStepThrough]
		public static IEnumerable<T> RecursiveSelectMany<T>([NotNull] this IEnumerable<T> me, [NotNull] Func<T, IEnumerable<T>> predicate, int times) {
			return me.RecursiveDo(x => x.AsParallel().WithExecutionMode(ParallelExecutionMode.ForceParallelism).SelectMany(predicate), times);
		}
	}
}