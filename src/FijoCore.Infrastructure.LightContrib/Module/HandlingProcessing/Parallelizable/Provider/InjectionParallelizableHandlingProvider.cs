using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Base.Provider;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Parallelizable.Interfaces;

namespace FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Parallelizable.Provider {
	public class InjectionParallelizableHandlingProvider<THandling> : InjectionHandlingProvider<THandling>, IParallelizableHandlingProvider<THandling> {
		#region Implementation of IParallelizableHandlingProvider<out THandling>
		public virtual bool SupportParallelization { get { return true; } }
		#endregion
	}
	
	public class InjectionParallelizableHandlingProvider<THandling, TInjectHandling> : InjectionHandlingProvider<THandling, TInjectHandling>, IParallelizableHandlingProvider<THandling> where TInjectHandling : THandling {
		#region Implementation of IParallelizableHandlingProvider<out THandling>
		public virtual bool SupportParallelization { get { return true; } }
		#endregion
	}
}