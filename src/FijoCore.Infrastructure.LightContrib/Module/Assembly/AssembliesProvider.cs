using System;
using System.Collections.Generic;
using Fijo.Infrastructure.DesignPattern.Repository;
using JetBrains.Annotations;
using RAssembly = System.Reflection.Assembly;

namespace FijoCore.Infrastructure.LightContrib.Module.Assembly {
	[UsedImplicitly]
	public class AssembliesProvider {
		public IEnumerable<RAssembly> GetAssemblies() {
			return AppDomain.CurrentDomain.GetAssemblies();
		}
	}
}