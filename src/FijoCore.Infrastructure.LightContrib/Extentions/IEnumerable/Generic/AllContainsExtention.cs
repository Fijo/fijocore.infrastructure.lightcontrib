using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic
{
	[PublicAPI, Obsolete("use ContainsAllExtention instead (with me and collection flipped)", true)]
	public static class AllContainsExtention
	{
		[Pure, PublicAPI, Obsolete("use ContainsAll instead (with me and collection flipped)", true), DebuggerStepThrough]
		public static bool AllContains<T>([NotNull] this IEnumerable<T> me, [NotNull] IEnumerable<T> collection)
		{
			return me.All(collection.Contains);
		}
		
		[Pure, PublicAPI, Obsolete("use ContainsAll instead (with me and collection flipped)", true), DebuggerStepThrough]
		public static bool AllContains<T>([NotNull] this IEnumerable<T> me, [NotNull] IEnumerable<T> collection, IEqualityComparer<T> equalityComparer)
		{
			return me.All(x => collection.Contains(x, equalityComparer));
		}

		[Pure, PublicAPI, Obsolete("use ContainsAll instead (with me and collection flipped)", true), DebuggerStepThrough]
		public static bool AllContains<T1, T2>([NotNull] this IEnumerable<T1> me, [NotNull] IEnumerable<T2> collection, [NotNull] Func<T1, T2, bool> comparer)
		{
			return me.All(x => collection.Any(y => comparer(x, y)));
		}
	}
}