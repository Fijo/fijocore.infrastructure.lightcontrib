﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using FijoCore.Infrastructure.LightContrib.Service.UniqueAdd.Interface;
using JetBrains.Annotations;
#if DEBUG
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
#endif

namespace FijoCore.Infrastructure.LightContrib.Service.UniqueAdd {
	[UsedImplicitly]
	public class UniqueAddRangeService : IUniqueAddRangeService {
		public void AssertUniqueAddRange<TAddable, T>(TAddable me, IEnumerable<T> collection, Func<TAddable, T, bool> conatins, Action<TAddable, IEnumerable<T>> addRange) {
			#region PreCondition
			#if DEBUG
			collection = collection.Execute();
			Debug.Assert(!collection.Any(x => conatins(me, x)));
			#endif
			#endregion
			addRange(me, collection);
		}

		public void UniqueAddRange<TAddable, T>(TAddable me, IEnumerable<T> collection, Func<TAddable, T, bool> conatins, Action<TAddable, IEnumerable<T>> addRange) {
			addRange(me, collection.Where(x => !conatins(me, x)));
		}
	}
}
