using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Settings;
using FijoCore.Infrastructure.LightContrib.Module.Stream;
using YAXLib;
using FijoCore.Infrastructure.LightContrib.Enums;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization.Impl {
	public class XMLSerialization : ISerialization {
		private readonly YAXSerializer _yaxSerializer = new YAXSerializer(typeof(object));
		private readonly IStreamService _streamService;

		public XMLSerialization(IStreamService streamService) {
			_streamService = streamService;
		}

		#region Implementation of ISerialization
		public void Serialize(object obj, System.IO.Stream stream, SerializationFormatting formatting, System.Text.Encoding encoding, ISerializationSettings serializationSettings = null) {
			Serialize<object>(obj, stream, formatting, encoding);
		}

		public void Serialize<T>(T obj, System.IO.Stream stream, SerializationFormatting formatting, System.Text.Encoding encoding, ISerializationSettings serializationSettings = null) {
			var streamWriter = _streamService.GetWriter(stream, encoding);
			_yaxSerializer.Serialize(obj, streamWriter);
		}

		public object Deserialize(System.IO.Stream stream, System.Text.Encoding encoding, ISerializationSettings serializationSettings = null) {
			return _yaxSerializer.Deserialize(_streamService.ReadToString(stream, encoding));
		}

		public T Deserialize<T>(System.IO.Stream stream, System.Text.Encoding encoding, ISerializationSettings serializationSettings = null) {
			return (T)Deserialize(stream, encoding);
		}
		#endregion
	}
}