using System.Collections.Generic;
using System.Diagnostics;
using Fijo.Infrastructure.DesignPattern.Store;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IStore.Generic {
	[PublicAPI]
	public static class StoreManyExtention {
		[PublicAPI, DebuggerStepThrough]
		public static void StoreMany<T>([NotNull] this IStore<T> me, [NotNull] IEnumerable<T> collection) {
			InternalStoreMany(me, collection);
		}

		[PublicAPI, DebuggerStepThrough]
		public static void StoreMany<T>([NotNull] this IStore<T> me, [NotNull] params T[] collection) {
			InternalStoreMany(me, collection);
		}

		[DebuggerStepThrough]
		private static void InternalStoreMany<T>([NotNull] IStore<T> me, [NotNull] IEnumerable<T> collection) {
			lock (me) collection.ForEach(me.Store);
		}
	}
}