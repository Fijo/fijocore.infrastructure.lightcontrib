using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic.IntoCollection;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	// ToDo May Check and Improve the performance
	[PublicAPI, Tested]
	public static class RecursiveEachSelectExtention {
		[NotNull, Pure, DebuggerStepThrough, PublicAPI]
		public static IEnumerable<IEnumerable<TSource>> RecursiveEachSelect<TSource>([NotNull] this IEnumerable<IEnumerable<TSource>> me) {
			var collection = me.Execute();
			if (collection.None()) return Enumerable.Empty<IEnumerable<TSource>>();
			return collection.Skip(1).Aggregate(collection.First().Select(x => x.IntoEnumerable()), RecursiveEachSelectTrasform);
		}

		[DebuggerStepThrough]
		private static IEnumerable<IEnumerable<TSource>> RecursiveEachSelectTrasform<TSource>([NotNull] IEnumerable<IEnumerable<TSource>> last, [NotNull] IEnumerable<TSource> entry) {
			return last.SelectMany(x => entry, (x, subEntry) => x.AddReturn(subEntry));
		}
	}
}