using System;
using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Enum {
	[PublicAPI]
	public static class IsFlagsEnumExtention {
		[Pure, PublicAPI]
		public static bool IsFlagsEnum([NotNull] this object me) {
			return InternalIsFlagsEnum(me.GetType());
		}
		
		[Pure, PublicAPI]
		public static bool IsFlagsEnum([NotNull] this System.Type me) {
			return InternalIsFlagsEnum(me);
		}
		
		[Pure, PublicAPI]
		// ReSharper disable UnusedParameter.Global
		public static bool IsFlagsEnum<T>(this T me) where T : struct, IConvertible {
			// ReSharper restore UnusedParameter.Global
			return InternalIsFlagsEnum(typeof(T));
		}

		[Pure]
		private static bool InternalIsFlagsEnum([NotNull] System.Type type) {
			#region PreCondition
			Debug.Assert(type.IsEnum, "type.IsEnum");
			#endregion
			return type.GetCustomAttributes(typeof (FlagsAttribute), false).Any();
		}
	}
}