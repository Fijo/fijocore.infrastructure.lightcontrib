namespace FijoCore.Infrastructure.LightContrib.Module.Cache.Interface {
	public interface ICache<in TStore> {
		TObj Get<T, TObj>()
			where T : class
			where TObj : class, TStore;
	}

	public interface ICache : ICache<object> {}
}