﻿using FijoCore.Infrastructure.LightContrib.Extentions.Type;
using NUnit.Framework;
using SType = System.Type;

namespace FijoCore.Infrastructure.LightContribTest.Extentions.Type {
	[TestFixture]//(typeof (ImplementsInterfaceExtention))
	public class ImplementsInterfaceExtentionTest {
		#region TestClasses
		private interface ITest1 : ITest2 {}

		private interface ITest2 {}

		private interface ITest3 {}

		private class Testclass1 : Testclass4, ITest1 {}

		private class Testclass2 : ITest2 {}

		private class Testclass3 {}

		private class Testclass4 : ITest3 {}
		#endregion

		[TestCase(true, typeof (ITest1), typeof (Testclass1))]
		[TestCase(true, typeof (ITest2), typeof (Testclass1))]
		[TestCase(false, typeof (ITest1), typeof (Testclass2))]
		[TestCase(true, typeof (ITest2), typeof (Testclass2))]
		[TestCase(false, typeof (ITest1), typeof (Testclass3))]
		[TestCase(false, typeof (ITest2), typeof (Testclass3))]
		[TestCase(true, typeof (ITest3), typeof (Testclass1))]
		[TestCase(true, typeof (ITest2), typeof (ITest1))]
		[TestCase(false, typeof (ITest1), typeof (ITest1))]
		[TestCase(false, typeof (ITest1), typeof (ITest2))]
		public void InterfaceAndType_ImplementsInterface_MustReturnValue(bool expected, SType @interface, SType type) {
			var actual = type.ImplementsInteface(@interface);
			Assert.AreEqual(expected, actual);
		}
	}
}