using System.Diagnostics;
using System.Runtime.CompilerServices;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Object {
	[PublicAPI]
	public static class InstanceEqualsExtention {
		[Pure, DebuggerStepThrough]
		[Desc("returns if the given instances are the same instance")]
		public static bool InstanceEquals(this object me, object other) {
			return RuntimeHelpers.Equals(me, other);
		}
	}
}