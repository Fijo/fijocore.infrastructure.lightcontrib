using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Module.Encoding;
using JetBrains.Annotations;
using Text = System.Text;
using IO = System.IO;

namespace FijoCore.Infrastructure.LightContrib.Module.Stream {
	//#region All
	//[Supports("seeklessStream")]
	//public byte[] ReadToBytes(IO.Stream stream) {
	//	return stream.CanSeek
	//			   ? ReadToBytes(stream, (int) (stream.Length - stream.Position))
	//			   : SeeklessReadToBytes(stream);
	//}

	//[Supports("seeklessStream")]
	//public IEnumerable<byte> ReadToBytesSequence(IO.Stream stream, int bufferSize = 1024) {
	//	return stream.CanSeek
	//			   ? ReadToBytesSequence(stream, (int) (stream.Length - stream.Position), bufferSize)
	//			   : SeeklessReadToBytesSequence(stream, bufferSize);
	//}
	//#endregion
	
	// ToDo add exception handling for stream.CanRead ...

	[UsedImplicitly, Obsolete("use StreamFacade instrad")]
	public class StreamService : IStreamService {
		private IEncodingProvider EncodingProvider { get { return Kernel.Resolve<IEncodingProvider>(); } }

		public string ReadToString(IO.Stream stream, bool autodetect = false) {
			return GetReader(stream, autodetect).ReadToEnd();
		}

		public string ReadToString(IO.Stream stream, Text.Encoding encoding) {
			return GetReader(stream, encoding).ReadToEnd();
		}

		public byte[] ReadToBytes(IO.Stream stream, int bufferSize = 1024) {
			return stream.CanSeek ? ReadToBytes(stream, (int) (stream.Length - stream.Position), 1024) : SeeklessReadToBytes(stream, 1024);
		}

		public IEnumerable<byte> ReadToBytesSequence(IO.Stream stream, int bufferSize = 1024) {
			return stream.CanSeek ? ReadToBytesSequence(stream, (int) (stream.Length - stream.Position), bufferSize) : SeeklessReadToBytesSequence(stream, bufferSize);
		}

		public byte[] SeeklessReadToBytes(IO.Stream stream, int bufferSize = 1024) {
			return SeeklessReadToBytesSequence(stream, bufferSize).ToArray();
		}

		public IEnumerable<byte> SeeklessReadToBytesSequence(IO.Stream stream, int bufferSize = 1024) {
			return InternalSeeklessReadToBytes(stream, bufferSize).OneFlat();
		}

		private IEnumerable<IEnumerable<byte>> InternalSeeklessReadToBytes(IO.Stream stream, int bufferSize = 1024) {
			if (bufferSize <= 0) throw new ArgumentOutOfRangeException("bufferSize", bufferSize, "must be at least 1");
			byte[] buffer;
			int read;
			while (true) {
				buffer = new byte[bufferSize];
				read = stream.Read(buffer, 0, bufferSize);
				if (read == bufferSize) yield return buffer;
				else break;
			}
			yield return buffer.Take(read);
		}

		public byte[] ReadToBytes(IO.Stream stream, int count, int bufferSize = 1024) {
			return InternalReadToBytes(stream, 0, count);
		}

		private static byte[] InternalReadToBytes(IO.Stream stream, int startIndex, int count) {
			var buffer = new byte[count];
			#if DEBUG
			var readCount = 
			#endif
			stream.Read(buffer, startIndex, count);
			#if DEBUG
			Debug.Assert(readCount == count, "readCount == count");
			#endif
			return buffer;
		}

		public IEnumerable<byte> ReadToBytesSequence(IO.Stream stream, int count, int bufferSize = 1024) {
			return InternalReadToBytesSequence(stream, count, bufferSize).OneFlat();
		}

		private static IEnumerable<IEnumerable<byte>> InternalReadToBytesSequence(IO.Stream stream, int count, int bufferSize) {
			var i = 0;
			while (i < count) {
				var bytesToRead = count - i;
				if (bytesToRead > bufferSize) bytesToRead = bufferSize;
				yield return InternalReadToBytes(stream, 0, bytesToRead);
				i += bufferSize;
			}
		}

		public char[] ReadToChars(IO.Stream stream, bool autodetect = false) {
			lock (stream) return InternalReadToBytes(stream, GetReader(stream, autodetect));
		}

		public char[] ReadToChars(IO.Stream stream, Text.Encoding encoding) {
			lock (stream) return InternalReadToBytes(stream, GetReader(stream, encoding));
		}

		protected virtual char[] InternalReadToBytes(IO.Stream stream, StreamReader streamReader) {
			var count = (int) stream.Length;
			var buffer = new char[count];
			streamReader.Read(buffer, 0, count);
			return buffer;
		}

		public StreamReader GetReader(IO.Stream stream, bool autodetect = false) {
			return autodetect ? new StreamReader(stream, true) : GetReader(stream, null);
		}

		public StreamReader GetReader(IO.Stream stream, Text.Encoding encoding) {
			return new StreamReader(stream, MayGetDefault(encoding));
		}

		public StreamWriter GetWriter(IO.Stream stream) {
			return GetWriter(stream, null);
		}

		public StreamWriter GetWriter(IO.Stream stream, Text.Encoding encoding) {
			return new StreamWriter(stream, MayGetDefault(encoding));
		}

		[NotNull, Pure]
		private Text.Encoding MayGetDefault([CanBeNull] Text.Encoding encoding) {
			return encoding ?? EncodingProvider.GetDefault();
		}

		public void WriteToStream(IO.Stream stream, string str) {
			WriteToStream(stream, str, null);
		}

		public void WriteToStream(IO.Stream stream, string str, Text.Encoding encoding) {
			var writer = GetWriter(stream, encoding);
			writer.Write(str);
			writer.Flush();
			// do not dispose the writer, because it will dispose the base stream
		}

		public void WriteToStream(IO.Stream stream, byte[] bytes) {
			WriteToStream(stream, bytes, bytes.Length);
		}

		public void WriteToStream(IO.Stream stream, byte[] bytes, int count) {
			stream.Write(bytes, 0, count);
		}

		public void WriteToStream(IO.Stream stream, IEnumerable<byte> bytes) {
			foreach (var b in bytes)
				stream.WriteByte(b);
		}

		public IO.Stream GetStream(StreamWriter streamWriter) {
			return streamWriter.BaseStream;
		}

		public IO.Stream GetStream(StreamReader streamReader) {
			return streamReader.BaseStream;
		}
	}
}