namespace FijoCore.Infrastructure.LightContrib.Module.Serialization.Settings {
	public class BinarySerializerSettings {
		public bool UseUnsafeDeserialize { get; set; }
	}
}