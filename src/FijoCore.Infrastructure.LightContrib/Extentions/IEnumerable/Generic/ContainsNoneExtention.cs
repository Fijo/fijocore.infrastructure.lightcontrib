﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	[PublicAPI]
	public static class ContainsNoneExtention {
		[Pure, PublicAPI, DebuggerStepThrough]
		public static bool ContainsNone<T>([NotNull] this IEnumerable<T> me, [NotNull] IEnumerable<T> collection) {
			return !me.ContainsAny(collection);
		}

		[Pure, PublicAPI, DebuggerStepThrough]
		public static bool ContainsNone<T>([NotNull] this IEnumerable<T> me, [NotNull] IEnumerable<T> collection, [NotNull] IEqualityComparer<T> equalityComparer) {
			return !me.ContainsAny(collection, equalityComparer);
		}

		[Pure, PublicAPI, DebuggerStepThrough]
		public static bool ContainsNone<T1, T2>([NotNull] this IEnumerable<T1> me, [NotNull] IEnumerable<T2> collection, [NotNull] Func<T1, T2, bool> comparer) {
			return !me.ContainsAny(collection, comparer);
		}
	}
}