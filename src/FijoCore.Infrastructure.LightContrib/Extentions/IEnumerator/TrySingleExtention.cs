using System;
using System.Collections.Generic;
using System.Diagnostics;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator {
	[PublicAPI]
	public static class TrySingleExtention {
		private static IOut _out;

		internal static void Init() {
			_out = Kernel.Resolve<IOut>();
		}

		[Pure, PublicAPI, DebuggerStepThrough]
		public static bool TrySingle<TSource>([NotNull] this IEnumerator<TSource> me, out TSource result) {
			#region PreCondition
			Debug.Assert(_out != null, "_out != null");
			#endregion
			if (!me.MoveNext()) return _out.False(out result);
			result = me.Current;
			if (!me.MoveNext()) return true;
			throw new InvalidOperationException("The input sequence contains more than one element.");
		}
	}
}