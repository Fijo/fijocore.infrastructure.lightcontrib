﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using FijoCore.Infrastructure.LightContrib.Extentions.Object;
using FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Other;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.EqualityComparer {
	[PublicAPI, Serializable]
	public class ReferenceEqualityComaperer<T> : IEqualityComparer<T>, ISerializable {
		private volatile static IEqualityComparer<T> _default;
		public static IEqualityComparer<T> Default { get { return _default ?? (_default = new ReferenceEqualityComaperer<T>()); } }

		protected ReferenceEqualityComaperer() {}

		#region Implementation of IEqualityComparer<in T>
		public bool Equals(T x, T y) {
			return x.InstanceEquals(y);
		}

		public int GetHashCode(T obj) {
			return obj.GetInstanceHashCode();
		}
		#endregion

		#region Implementation of ISerializable
		public void GetObjectData(SerializationInfo info, StreamingContext context) {
			info.SetType(typeof (SingletonSerializer));
		}
		
		[Serializable]
		protected class SingletonSerializer : IObjectReference {
			#region Implementation of IObjectReference
			public object GetRealObject(StreamingContext context) {
				return Default;
			}
			#endregion
		}
		#endregion
	}

	[PublicAPI, Serializable]
	public class ReferenceEqualityComaperer : IEqualityComparer, ISerializable {
		private volatile static IEqualityComparer _default;
		public static IEqualityComparer Default { get { return _default ?? (_default = new ReferenceEqualityComaperer()); } }

		#region Implementation of IEqualityComparer
		public new bool Equals(object x, object y) {
			return x.InstanceEquals(y);
		}

		public int GetHashCode(object obj) {
			return obj.GetInstanceHashCode();
		}
		#endregion

		#region Implementation of ISerializable
		public void GetObjectData(SerializationInfo info, StreamingContext context) {
			info.SetType(typeof (SingletonSerializer));
		}
		
		[Serializable]
		protected class SingletonSerializer : IObjectReference {
			#region Implementation of IObjectReference
			public object GetRealObject(StreamingContext context) {
				return Default;
			}
			#endregion
		}
		#endregion
	}
}