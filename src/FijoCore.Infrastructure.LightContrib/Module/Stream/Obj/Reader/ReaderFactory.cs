using System.IO;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Stream.Obj.Reader {
	public class ReaderFactory : IReaderFactory {
		private readonly IOut _out;
		private readonly IStreamService _streamService;

		public ReaderFactory(IOut @out, IStreamService streamService) {
			_out = @out;
			_streamService = streamService;
		}
		#region Implementation of IReaderFactory
		public IStringReader CreateStringReader(StreamReader streamReader) {
			return new StringReader(streamReader, _out);
		}

		public IStringReader CreateStringReader(System.IO.Stream stream, bool autodetect = false) {
			return CreateStringReader(_streamService.GetReader(stream, autodetect));
		}
		
		public IStringReader CreateStringReader(System.IO.Stream stream, System.Text.Encoding encoding) {
			return CreateStringReader(_streamService.GetReader(stream, encoding));
		}

		public IStringReader CreateStringReader(string content) {
			return CreateStringReader(_streamService.GetReader(GetStream(content)));
		}

		[NotNull, Pure]
		private System.IO.Stream GetStream(string content) {
			var stream = new MemoryStream();
			_streamService.WriteToStream(stream, content);
			stream.Position = 0;
			return stream;
		}
		#endregion
	}
}