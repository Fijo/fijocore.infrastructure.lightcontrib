using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator {
	[PublicAPI]
	public static class ThirdExtention {
		[Pure, PublicAPI, DebuggerStepThrough]
		public static TSource Third<TSource>(this IEnumerator<TSource> me) {
			return me.IndexTh(3);
		}
	}
}