﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using FijoCore.Infrastructure.LightContrib.Dto;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Type.Specific.Dict {
	public static class DictionaryTypeExtention {
		public static DictionaryType GetDictionaryInfo([NotNull] this System.Type me) {
			var dictType = GetDictType(me);
			Debug.Assert(dictType.IsGenericType || me.IsAssignableFrom(typeof (System.Collections.IDictionary)), "The type has to be a IDictionary or IDictionary<,>");
			return dictType == null
				       ? new DictionaryType(typeof (object), typeof (object), false)
				       : MapDictionaryType(dictType);
		}

		private static System.Type GetDictType(System.Type me) {
			System.Type dictType;
			try {
				dictType = GetInterfaces(me).SingleOrDefault(IsGenericIDictionary);
			}
			catch (InvalidOperationException e) {
				throw new InvalidOperationException("The type (arg ´me´) should not implement IDictionary<,> more than one time. (I don´t know which to use in this case)");
			}
			return dictType;
		}

		private static IEnumerable<System.Type> GetInterfaces(System.Type me) {
			if (me.IsInterface) yield return me;
			foreach (var @interface in me.GetInterfaces()) yield return @interface;
		}

		private static DictionaryType MapDictionaryType(System.Type dictType) {
			var arguments = dictType.GetGenericArguments();
			Debug.Assert(arguments.Count() == 2);
			return new DictionaryType(arguments[0], arguments[1], true);
		}
		
		public static bool IsDictionary([NotNull] this System.Type me) {
			return me.IsAssignableFrom(typeof (System.Collections.IDictionary)) || GetInterfaces(me).Any(IsGenericIDictionary);
		}
		
		public static System.Type GetDictionaryType([NotNull] this System.Type me) {
			var dictionaryType = GetDictType(me);
			Debug.Assert(dictionaryType != null || me.IsAssignableFrom(typeof (System.Collections.IDictionary)), "The type has to be a IDictionary or IDictionary<,>");
			return dictionaryType ?? typeof(System.Collections.IDictionary);
		}

		private static bool IsGenericIDictionary(System.Type x) {
			return x.IsGenericType && !x.IsGenericTypeDefinition &&
			       x.GetGenericTypeDefinition() == typeof (IDictionary<,>);
		}
	}
}