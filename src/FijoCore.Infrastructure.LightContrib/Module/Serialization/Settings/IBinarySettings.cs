using Fijo.Infrastructure.DesignPattern.Repository;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization.Settings {
	public interface IBinarySettings : ISerializationSettings, IRepository<BinarySerializerSettings> {}
}