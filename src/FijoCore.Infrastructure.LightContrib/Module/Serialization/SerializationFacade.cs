using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Enums;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Settings;
using JetBrains.Annotations;
using IO = System.IO;
using Text = System.Text;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization {
	[UsedImplicitly]
	public class SerializationFacade : ISerializationFacade {
		private readonly ISerializationService _serializationService;
		private readonly ISerializationProvider _serializationProvider;
		public SerializationFacade(ISerializationService serializationService, ISerializationProvider serializationProvider) {
			_serializationService = serializationService;
			_serializationProvider = serializationProvider;
		}

		public void Serialize(SerializationFormat format, object obj, IO.Stream stream, SerializationFormatting formatting = SerializationFormatting.Default, Text.Encoding encoding = null, ISerializationSettings serializationSettings = null) {
			_serializationService.Serialize(_serializationProvider.Get(format), obj, stream, formatting, encoding, serializationSettings);
		}
		
		public void Serialize<T>(SerializationFormat format, T obj, IO.Stream stream, SerializationFormatting formatting = SerializationFormatting.Default, Text.Encoding encoding = null, ISerializationSettings serializationSettings = null) {
			_serializationService.Serialize(_serializationProvider.Get(format), obj, stream, formatting, encoding, serializationSettings);
		}

		public string Serialize(SerializationFormat format, object obj, SerializationFormatting formatting = SerializationFormatting.Default, Text.Encoding encoding = null, ISerializationSettings serializationSettings = null) {
			return _serializationService.Serialize(_serializationProvider.Get(format), obj, formatting, encoding, serializationSettings);
		}

		public string Serialize<T>(SerializationFormat format, T obj, SerializationFormatting formatting = SerializationFormatting.Default, Text.Encoding encoding = null, ISerializationSettings serializationSettings = null) {
			return _serializationService.Serialize(_serializationProvider.Get(format), obj, formatting, encoding, serializationSettings);
		}

		public object Deserialize(SerializationFormat format, IO.Stream stream, Text.Encoding encoding = null, ISerializationSettings serializationSettings = null) {
			return _serializationService.Deserialize(_serializationProvider.Get(format), stream, encoding, serializationSettings);
		}

		public T Deserialize<T>(SerializationFormat format, IO.Stream stream, Text.Encoding encoding = null, ISerializationSettings serializationSettings = null) {
			return _serializationService.Deserialize<T>(_serializationProvider.Get(format), stream, encoding, serializationSettings);
		}

		public object Deserialize(SerializationFormat format, string source, Text.Encoding encoding = null, ISerializationSettings serializationSettings = null) {
			return _serializationService.Deserialize(_serializationProvider.Get(format), source, encoding, serializationSettings);
		}

		public T Deserialize<T>(SerializationFormat format, string source, Text.Encoding encoding = null, ISerializationSettings serializationSettings = null) {
			return _serializationService.Deserialize<T>(_serializationProvider.Get(format), source, encoding, serializationSettings);
		}
	}
}