using System;
using System.Collections;
using System.Collections.Generic;
using FijoCore.Infrastructure.LightContrib.Delegates;

namespace FijoCore.Infrastructure.LightContrib.Module.Enumerator {
	public class ExpressionEnumenator<T> : IEnumerator<T> {
		private readonly TryFunc<T> _expression;

		public ExpressionEnumenator(TryFunc<T> expression) {
			_expression = expression;
		}

		#region Implementation of IEnumerator
		public bool MoveNext() {
			T next;
			if(!_expression(out next)) return false;
			Current = next;
			return true;
		}

		public void Reset() {
			throw new InvalidOperationException();
		}

		public T Current { get; protected set; }
		object IEnumerator.Current { get { return Current; } }
		#endregion

		#region Implementation of IDisposable
		public void Dispose() {}
		#endregion
	}
}