using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	public static class GetSequenceHashCodeExtention {
		[Pure, PublicAPI, DebuggerStepThrough, PerformanceRiskyBecauseHighAbstraction("and the usage of �ToEnumerable� in �SkipOneInto� is may a big performance problem for this simple function that shouldn�t take a long time to be executed")]
		public static int GetSequenceHashCode<TSource>([NotNull] this IEnumerable<TSource> me, [NotNull] Func<TSource, int> hashCodeProvider) {
			TSource skipedObject;
			bool hadAny;
			var enumerable = me.SkipOneInto(out skipedObject, out hadAny);
			return !hadAny
			       	? 0
			       	: enumerable.AsParallel().Select(hashCodeProvider).Aggregate(hashCodeProvider(skipedObject), (a, x) => unchecked((a * 397) ^ x));
		}

		[Pure, PublicAPI, DebuggerStepThrough, PerformanceRiskyBecauseHighAbstraction("and the usage of �ToEnumerable� in �SkipOneInto� is may a big performance problem for this simple function that shouldn�t take a long time to be executed")]
		public static int GetSequenceHashCode<TSource>([NotNull] this IEnumerable<TSource> me) {
			return me.GetSequenceHashCode(x => x.GetHashCode());
		}

		[Pure, PublicAPI, DebuggerStepThrough, PerformanceRiskyBecauseHighAbstraction("and the usage of �ToEnumerable� in �SkipOneInto� is may a big performance problem for this simple function that shouldn�t take a long time to be executed")]
		public static int GetSequenceHashCode<TSource>([NotNull] this IEnumerable<TSource> me, IEqualityComparer<TSource> equalityComparer) {
			return me.GetSequenceHashCode(equalityComparer.GetHashCode);
		}
	}
}