using Fijo.Infrastructure.Model.Validation;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;

namespace FijoCore.Infrastructure.LightContrib.Factories {
	public class ValidationObjFactory : IValidationObjFactory {
		#region Implementation of IValidationObjFactory
		public ValidationContext<TSource, TValidatior> CreateContext<TSource, TValidatior>(TSource source, TValidatior validatior) {
			return new ValidationContext<TSource, TValidatior>(source, validatior);
		}

		public ValidationResult<TSource, TValidatior, TRule> CreateResult<TSource, TValidatior, TRule>(ValidationContext<TSource, TValidatior> context, TRule rule, bool result) {
			return new ValidationResult<TSource, TValidatior, TRule>(context, rule, result);
		}
		#endregion
	}
}