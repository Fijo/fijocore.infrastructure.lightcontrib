using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	[PublicAPI]
	public static class ContainsAllExtention {
		[Pure, PublicAPI, DebuggerStepThrough]
		[Desc("All entries in collection must be although contained in me")]
		public static bool ContainsAll<T>([NotNull] this IEnumerable<T> me, [NotNull] IEnumerable<T> collection) {
			var executedMe = me.Execute();
			return collection.All(executedMe.Contains);
		}

		[Pure, PublicAPI, DebuggerStepThrough]
		[Desc("All entries in collection must be although contained in me")]
		public static bool ContainsAll<T>([NotNull] this IEnumerable<T> me, [NotNull] IEnumerable<T> collection, [NotNull] IEqualityComparer<T> comparer) {
			var executedMe = me.Execute();
			return collection.All(x => executedMe.Contains(x, comparer));
		}

		[Pure, PublicAPI, DebuggerStepThrough]
		[Desc("All entries in collection must be although contained in me")]
		public static bool ContainsAll<T1, T2>([NotNull] this IEnumerable<T1> me, [NotNull] IEnumerable<T2> collection, [NotNull] Func<T1, T2, bool> comparer) {
			var executedMe = me.Execute();
			return collection.All(x => executedMe.All(y => comparer(y, x)));
		}

		[Pure, PublicAPI, DebuggerStepThrough]
		[Desc("All entries in collection must be although contained in me")]
		public static bool ContainsAll<T>([NotNull] this IEnumerable<T> me, [NotNull] params IEnumerable<T>[] collections) {
			var executedMe = me.Execute();
			return collections.Aggregate(false, (current, collection) => current || collection.ContainsAll(executedMe));
		}

		[Pure, PublicAPI, DebuggerStepThrough]
		[Desc("All entries in collection must be although contained in me")]
		public static bool ContainsAll<T>([NotNull] this IEnumerable<T> me, [NotNull] Func<T, T, bool> equals, [NotNull] params IEnumerable<T>[] collections) {
			var executedMe = me.Execute();
			return collections.Aggregate(false, (current, collection) => current || collection.ContainsAll(executedMe, equals));
		}
	}
}