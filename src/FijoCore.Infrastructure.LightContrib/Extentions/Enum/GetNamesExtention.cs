using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Enum {
	[PublicAPI]
	public static class GetNamesExtention {
		[NotNull, Pure, DebuggerStepThrough, PublicAPI]
		[Link("http://msdn.microsoft.com/en-en/library/system.enum.getnames")]
		[Desc("Returns a list of the names of properties that are contained int the enum.")]
		public static IEnumerable<string> GetNames(this System.Type type) {
			#region PreCondition
			Debug.Assert(type != null, "type != null");
			#endregion
			return InternalGetNames(type);
		}

		// ToDo description anpassen
		[NotNull, Pure, DebuggerStepThrough, PublicAPI]
		[RelatedLink("http://msdn.microsoft.com/en-en/library/system.enum.getnames", "http://msdn.microsoft.com/en-en/library/system.enum.getname.aspx")]
		[Desc("Returns the property name that stands for the value the instance of an enum has.")]
		public static IEnumerable<string> GetNames<T>(this T me) where T : struct, IConvertible {
			return InternalGetNames(typeof (T), me);
		}
		
		// ToDo description anpassen
		[NotNull, Pure, DebuggerStepThrough, PublicAPI]
		[RelatedLink("http://msdn.microsoft.com/en-en/library/system.enum.getnames", "http://msdn.microsoft.com/en-en/library/system.enum.getname.aspx")]
		[Desc("Returns the property name that stands for the value the instance of an enum has.")]
		public static IEnumerable<string> GetNames(this System.Type me, [NotNull] object value) {
			#region PreCondition
			Debug.Assert(me != null, "me != null");
			#endregion
			return InternalGetNames(me, value);
		}
		
		// ToDo description anpassen
		[NotNull, Pure, DebuggerStepThrough, PublicAPI]
		[RelatedLink("http://msdn.microsoft.com/en-en/library/system.enum.getnames", "http://msdn.microsoft.com/en-en/library/system.enum.getname.aspx")]
		[Desc("Returns the property name that stands for the value the instance of an enum has.")]
		public static IEnumerable<string> GetNames<T>(this T me, object value) where T : struct, IConvertible {
			return InternalGetNames(typeof (T), me);
		}
		
		[NotNull, Pure]
		private static IEnumerable<string> InternalGetNames([NotNull] System.Type type, [NotNull] object value) {
			#region PreCondition
			Debug.Assert(type.IsEnum, "type.IsEnum");
			Debug.Assert(type == value.GetType(), "type == value.GetType()");
			#endregion
			return System.Enum.GetValues(type)
				.Cast<object>()
				.Where(flag => type.HasFlag(value, flag))
// ReSharper disable ConvertClosureToMethodGroup
				.Select(x => type.GetName(x));
// ReSharper restore ConvertClosureToMethodGroup
		}

		[Pure, NotNull]
		private static IList<string> InternalGetNames([NotNull] System.Type type) {
			#region PreCondition
			Debug.Assert(type.IsEnum, "type.IsEnum");
			#endregion
			return System.Enum.GetNames(type);
		}
	}
}