﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Conditional.Interface;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Parallelizable;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Parallelizable.Interfaces;

namespace FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Conditional {
	[AboutName("C", "Conditional")]
	[AboutName("H", "Handling")]
	public class CHProcessor<T> : ParallelizableHandlingProcessor<ICH<T>>, ICHProcessor<T> {
		public CHProcessor(IParallelizableHandlingProvider<ICH<T>> handlingRepository) : base(handlingRepository) {}

		#region Process
		protected virtual void ProcessHandlings(IEnumerable<ICH<T>> handlings, bool isParallelizable, T source) {
			handlings.Opt(isParallelizable,
			              x => x.Where(GetIsUsedFunc(source)).ForEach(GetProcessFunc(source)),
			              x => x.Where(GetIsUsedFunc(source)).ForAll(GetProcessFunc(source)));
		}

		public void Process(T source, bool enableParallelization = true) {
			ProcessHandlings(Handlings, IsParallelizable(enableParallelization), source);
		}

		public void Process(IEnumerable<ICH<T>> customHandlings, T source, bool enableParallelization = true) {
			if (customHandlings == null) Process(source);
			else {
				customHandlings = CustomProcessHandlingsWrapper(customHandlings).Execute();
				ProcessHandlings(customHandlings.Concat(Handlings),
				                 IsParallelizable(enableParallelization, customHandlings), source);
			}
		}
		#endregion

		#region Predicates
		private Func<ICH<T>, bool> GetIsUsedFunc(T source) {
			return y => y.IsUsed(source);
		}

		private Action<ICH<T>> GetProcessFunc(T source) {
			return y => y.Process(source);
		}
		#endregion

		#region IsParallelizable
		protected virtual bool IsParallelizable(bool enableParallelization) {
			return SupportParallelization && enableParallelization;
		}

		protected virtual bool IsParallelizable(bool enableParallelization, IEnumerable<ICH<T>> customHandlings) {
			return IsParallelizable(enableParallelization) && GetSupportsParallelization(customHandlings);
		}
		#endregion
	}
}