using System.Collections.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContribTest.Properties;
using NUnit.Framework;

namespace FijoCore.Infrastructure.LightContribTest.Extentions.IEnumerable.Generic {
	[TestFixture]//(typeof (ContainsAllExtention))
	public class ContainsAllExtentionTest {
		private readonly IList<string> _strings = new List<string>
		{
			"hallo",
			"test",
			"lol"
		};

		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
		}


		[TestCase("not contained", "dum")]
		public void ContainsAll1_MustReturnFalse(params string[] collection) {
			Assert.False(_strings.ContainsAll(collection));
		}

		[TestCase("hallo", "lol")]
		public void ContainsAll1_MustReturnTrue(params string[] collection) {
			Assert.True(_strings.ContainsAll(collection));
		}
	}
}