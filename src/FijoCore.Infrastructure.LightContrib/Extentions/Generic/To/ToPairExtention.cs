using System.Collections.Generic;
using System.Diagnostics;
using Fijo.Infrastructure.Model.Pair;
using Fijo.Infrastructure.Model.Pair.Interface.Base;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Generic.To
{
	public static class ToPairExtention
	{
		[NotNull, Pure, DebuggerStepThrough]
		public static IPair<TKey, TValue> ToPair<TSource, TKey, TValue>(this TSource me, [NotNull] System.Func<TSource, TKey> keySelector, [NotNull] System.Func<TSource, TValue> valueSelector) {
			#region PreCondition
			Debug.Assert(!me.IsUndefined(), "!me.IsUndefined()");
			#endregion
			return new Pair<TKey, TValue>(keySelector(me), valueSelector(me));
		}

		[Pure, DebuggerStepThrough]
		public static KeyValuePair<TKey, TValue> ToKeyValuePair<TSource, TKey, TValue>(this TSource me, [NotNull] System.Func<TSource, TKey> keySelector, [NotNull] System.Func<TSource, TValue> valueSelector) {
			#region PreCondition
			Debug.Assert(!me.IsUndefined(), "!me.IsUndefined()");
			#endregion
			return new KeyValuePair<TKey, TValue>(keySelector(me), valueSelector(me));
		}
	}
}