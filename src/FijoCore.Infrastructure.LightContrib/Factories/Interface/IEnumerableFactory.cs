using System.Collections.Generic;
using FijoCore.Infrastructure.LightContrib.Delegates;

namespace FijoCore.Infrastructure.LightContrib.Factories.Interface {
	public interface IEnumerableFactory {
		IEnumerable<int> CreateEndless(int start);
		IEnumerable<T> CreateExpression<T>(TryFunc<T> expression);
	}
}