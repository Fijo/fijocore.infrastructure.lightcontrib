using System.Collections.Generic;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Custom.Interface {
	public interface ICustomInternalRecursiveEqualityComparerFactory {
		[NotNull] IEqualityComparer<T> TypedDefaultRecursiveCompare<T>() where T : class;
	}
}