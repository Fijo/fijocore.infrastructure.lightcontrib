using System.Collections.Generic;
using System.Diagnostics;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	public static class IndexThExtention {
		[DebuggerStepThrough]
		public static TSource IndexTh<TSource>([NotNull] this IEnumerable<TSource> me, int index) {
			return me.GetEnumerator().IndexTh(index);
		}
	}
}