using System;
using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator {
	[PublicAPI]
	public static class SelectWhileExtention {
		[PublicAPI, DebuggerStepThrough]
		public static IEnumerable<TResult> SelectWhile<TSource, TResult>([NotNull] this IEnumerator<TSource> me, [NotNull] Func<TSource, TResult> selector, [NotNull] Func<TResult, bool> @while) {
			while (me.MoveNext()) {
				var current = selector(me.Current);
				if(!@while(current)) yield break;
				yield return current;
			}
		}
	}
}