using System;

namespace FijoCore.Infrastructure.LightContrib.Module.Typing.KeyValuePairType {
	public interface IGenericKeyValuePairAccessorProvider {
		GenericKeyValuePairAccessor Get(Type keyValuePairType);
	}
}