using System.Linq;
using FijoCore.Infrastructure.LightContrib.Extentions.Type;
using FijoCore.Infrastructure.LightContribTest.Properties;
using NUnit.Framework;
using SType = System.Type;

namespace FijoCore.Infrastructure.LightContribTest.Extentions.Type {
	[TestFixture]//(typeof (GetInheritingTypesExtention))
	public class GetInheritingTypesExtentionTest {
		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
		}

		#region TestClasses
		private class BaseClass {}

		private interface ITest1 : ITest2 {}

		private interface ITest2 {}

		private class Testclass1 : Testclass2, ITest1 {}

		private class Testclass2 : BaseClass, ITest2 {}

		private class Testclass3 : BaseClass {}

		private class Testclass4 {}
		#endregion

		[TestCase(typeof (Testclass2), typeof (Testclass1))]
		[TestCase(typeof (BaseClass), typeof (Testclass1), typeof (Testclass2), typeof (Testclass3))]
		[TestCase(typeof (Testclass3))]
		[TestCase(typeof (Testclass1))]
		public void BaseTypeAndType_ImplementsType_MustReturnValue(SType type, params SType[] expectedTypes) {
			var types = type.GetInheritingTypes().ToArray();
			CollectionAssert.AreEquivalent(expectedTypes, types);
		}
	}
}