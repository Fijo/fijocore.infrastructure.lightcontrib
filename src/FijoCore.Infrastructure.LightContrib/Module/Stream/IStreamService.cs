using System.Collections.Generic;
using System.IO;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;
using Text = System.Text;
using IO = System.IO;

namespace FijoCore.Infrastructure.LightContrib.Module.Stream {
	public interface IStreamService {
		string ReadToString([NotNull] IO.Stream stream, bool autodetect = false);
		string ReadToString([NotNull] IO.Stream stream, [CanBeNull] Text.Encoding encoding);

		[NotNull]
		byte[] ReadToBytes([NotNull] IO.Stream stream, int bufferSize = 1024);

		[NotNull, Note("the length of the returned sequence may not equals the count")]
		byte[] ReadToBytes([NotNull] IO.Stream stream, int count, int bufferSize = 1024);

		[NotNull]
		IEnumerable<byte> ReadToBytesSequence([NotNull] IO.Stream stream, int bufferSize = 1024);

		[NotNull, Note("the length of the returned sequence may not equals the count")]
		IEnumerable<byte> ReadToBytesSequence([NotNull] IO.Stream stream, int count, int bufferSize = 1024);

		[NotNull]
		char[] ReadToChars([NotNull] IO.Stream stream, bool autodetect = false);

		[NotNull]
		char[] ReadToChars([NotNull] IO.Stream stream, [CanBeNull] Text.Encoding encoding);
		
		[NotNull]
		StreamReader GetReader([NotNull] IO.Stream stream, bool autodetect = false);

		[NotNull]
		StreamReader GetReader([NotNull] IO.Stream stream, [CanBeNull] Text.Encoding encoding);

		[NotNull]
		StreamWriter GetWriter([NotNull] IO.Stream stream);

		[NotNull]
		StreamWriter GetWriter([NotNull] IO.Stream stream, [CanBeNull] Text.Encoding encoding);

		void WriteToStream([NotNull] IO.Stream stream, string str);
		void WriteToStream([NotNull] IO.Stream stream, string str, [CanBeNull] Text.Encoding encoding);
		void WriteToStream([NotNull] IO.Stream stream, [NotNull] byte[] bytes);
		void WriteToStream([NotNull] IO.Stream stream, [NotNull] byte[] bytes, int count);
		void WriteToStream([NotNull] IO.Stream stream, [NotNull] IEnumerable<byte> bytes);

		[NotNull]
		IO.Stream GetStream([NotNull] StreamWriter streamWriter);

		[NotNull]
		IO.Stream GetStream([NotNull] StreamReader streamReader);
	}
}