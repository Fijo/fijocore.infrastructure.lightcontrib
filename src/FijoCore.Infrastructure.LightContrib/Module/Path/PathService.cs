using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Default.Exceptions;
using FijoCore.Infrastructure.LightContrib.Default.Service.CheckName;
using FijoCore.Infrastructure.LightContrib.Extentions.Char;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using JetBrains.Annotations;
using IO = System.IO;

namespace FijoCore.Infrastructure.LightContrib.Module.Path {
	[PublicAPI, Service]
	public class PathService : IPathService {
		protected const char NormalizeEscapeChar = '%';
		protected readonly ICollection<char> NormalizeInvalidFileName = IO.Path.GetInvalidFileNameChars().Concat(new[] {NormalizeEscapeChar}).ToArray();
		protected readonly IList<char> HexChars = new[] {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
		private const string HexCharIndexExceptionMessage = "The arg have to be a valid hex char";
		private const string InvalidEscapedCharEndExceptionMessage = "The escaped sequence must have a length of 4";
		private readonly string _denormalizeExceptionMessage = string.Format("Invalid escaped chars in the filename. Please use this methode only on fileNames that had been encoded using {0}", string.Format("{0}.{1}", typeof (PathService).Name, CN.Get<PathService>(x => x.Normalize(null))));
		private readonly string _directorySeparatorString = IO.Path.DirectorySeparatorChar.InvariantToString();

		[Pure, PublicAPI, About("CorrectStringPathForComparing")]
		[Desc("don�t ignore microsoft completely to produce no time intense framework bugs")]
		protected string CorrectPath(string path) {
			#if UseCaseInsensitivePathes
			return path.ToLowerInvariant();
			#else
			return path;
			#endif
		}

		[Pure]
		public bool IsInPath(string path, string basePath) {
			var absolutePath = GetCorrectedFullPathNoPathSeperatorAtEnd(path);
			var absoluteBasePath = GetCorrectedFullPathNoPathSeperatorAtEnd(basePath);
			return InternalIsInPath(absolutePath, absoluteBasePath);
		}

		[Pure]
		private bool InternalIsInPath(string absolutePath, string absoluteBasePath) {
			return absolutePath.StartsWith(absoluteBasePath);
		}

		[Pure]
		private string GetCorrectedFullPath(string path) {
			return CorrectPath(IO.Path.GetFullPath(path));
		}

		[Pure]
		private string GetCorrectedFullPathNoPathSeperatorAtEnd(string path) {
			var correctedPath = GetCorrectedFullPath(path);
			return correctedPath.Last() == IO.Path.DirectorySeparatorChar
			       	? path.Substring(0, path.Length - 1)
			       	: path;
		}

		[Pure]
		public string GetPathRelativeTo(string path, string basePath) {
			var absolutePath = GetCorrectedFullPathNoPathSeperatorAtEnd(path);
			var absoluteBasePath = GetCorrectedFullPathNoPathSeperatorAtEnd(basePath);
			if(!InternalIsInPath(absolutePath, absoluteBasePath)) throw new PathMustBeInBasePathException(path, basePath);
			var length = absolutePath.Length;
			var startIndex = absoluteBasePath.Length;
			Debug.Assert(length >= startIndex, "absolutePath.Length >= absoluteBasePath.Length");
			if(startIndex == length) return GetRelativeCurrentPath();
			Debug.Assert(startIndex + 1 < length, "startIndex +1 < absolutePath.Length");
			return absolutePath.Substring(startIndex +1);
		}

		public void MayCreateParentFolders(string path) {
			var parts = path.Split(IO.Path.DirectorySeparatorChar);
			var count = parts.Count();
			if(count <= 1) return;
			foreach(var dict in Enumerable.Range(2, count -1).Reverse().Select(x => string.Join(_directorySeparatorString, parts.Take(x).ToArray())).TakeWhile(x => !Directory.Exists(x)).Reverse())
				Directory.CreateDirectory(dict);
		}

		[Pure]
		private string GetRelativeCurrentPath() {
			return string.Empty;
		}
		
		[Pure]
		public bool IsRelative(string path) {
			return !IsAbsolute(path);
		}

		[Pure]
		public bool IsAbsolute(string path) {
			#if !PLATFORM_UNIX
			return ContainsVolumePattern(path);
			#else
			return path.FirstOrDefault() == Path.DirectorySeparatorChar;
			#endif
		}

		[Pure]
		public bool ContainsVolumePattern(string path) {
			#if !PLATFORM_UNIX
			int skiped;
			return InternalGetVolumePattern(path, out skiped).FirstOrDefault() == IO.Path.VolumeSeparatorChar;
			#else
			return false;
			#endif
		}
		
		[Pure]
		public string GetVolumePatternInPath(string path) {
			#if !PLATFORM_UNIX
			int skiped;
			var array = InternalGetVolumePattern(path, out skiped).ToArray();
			if(array.FirstOrDefault() != IO.Path.VolumeSeparatorChar) return string.Empty;
			#if PLATFORM_WINDOWS
			if(array.Count() > 1 && array[1] == IO.Path.DirectorySeparatorChar) return InternalGetVolumePatternUntilIndex(path, skiped + 2);
			#endif
			return InternalGetVolumePatternUntilIndex(path, skiped + 1);
			#else
			return false;
			#endif
		}
		
		[Pure]
		public string GetVolumeNameFromPattern(string volumePattern) {
			#if !PLATFORM_UNIX
			if(string.IsNullOrEmpty(volumePattern)) throw new ArgumentException("can not be null or empty", "volumePattern");
			#if !PLATFORM_WINDOWS
			Debug.Assert(volumePattern.EndsWith(new string(new[] {Path.VolumeSeparatorChar})));
			return InternalGetVolumePatternUntilIndex(volumePattern, volumePattern.Length -1);
			#else
			Debug.Assert(volumePattern.EndsWith(new string(new[] {IO.Path.VolumeSeparatorChar, IO.Path.DirectorySeparatorChar})) || volumePattern.EndsWith(new string(new[] {IO.Path.VolumeSeparatorChar})));
			return InternalGetVolumePatternUntilIndex(volumePattern, volumePattern.Length - (volumePattern.Last() == IO.Path.DirectorySeparatorChar ? 2 : 1));
			#endif
			#else
			throw new PlatformNotSupportedException();
			#endif
		}

		[Pure]
		private string InternalGetVolumePatternUntilIndex(string pathOrPattern, int nameEndIndex) {
			#region PreCondition
			Debug.Assert(pathOrPattern.Length >= nameEndIndex, "pathOrPattern.Length >= nameEndIndex");
			#endregion
			#if !PLATFORM_UNIX
			return pathOrPattern.Substring(0, nameEndIndex);
			#else
			throw new PlatformNotSupportedException();
			#endif
		}

		[Pure]
		private IEnumerable<char> InternalGetVolumePattern(string path, out int skiped) {
			#if !PLATFORM_UNIX
			var internalSkiped = 0;
			var enumerable = path.SkipWhile(x => {
				var res = IsCharNotVolumeCharSeperatorOrDictionarySeperator(x);
				if(res) internalSkiped++;
				return res;
			}).ToArray();
			skiped = internalSkiped;
			return enumerable;
			#else
			throw new PlatformNotSupportedException();
			#endif
		}

		[Pure]
		private bool IsCharNotVolumeCharSeperatorOrDictionarySeperator(char c) {
			#if !PLATFORM_UNIX
			return c != IO.Path.VolumeSeparatorChar && c != IO.Path.DirectorySeparatorChar;
			#else
			return c != Path.DirectorySeparatorChar;
			#endif
		}

		#region Normalize
		[Pure]
		public virtual string Normalize(string fileName) {
			return new string(fileName.SelectMany(NormalizeSelector).ToArray());
		}

		[Pure]
		protected IEnumerable<char> NormalizeSelector(char current) {
			if (!current.IsIn(NormalizeInvalidFileName)) {
				yield return current;
				yield break;
			}
			yield return NormalizeEscapeChar;
			Debug.Assert(sizeof (char) <= 2);
			unchecked {
				yield return HexChars[current & 15];
				yield return HexChars[(current >> 4) & 15];
				yield return HexChars[(current >> 8) & 15];
				yield return HexChars[current >> 12];
			}
		}
		#endregion

		#region Denormalize
		[Pure]
		public virtual string Denormalize(string fileName) {
			char[] chars;
			try {
				chars = DenormalizeSelector(fileName).ToArray();
			}
			catch(ArgumentException e) {
				Debug.Assert(new[] {HexCharIndexExceptionMessage, InvalidEscapedCharEndExceptionMessage}.Any(e.Message.Contains));
				throw new ArgumentException(_denormalizeExceptionMessage, "fileName", e);
			}
			return new string(chars);
		}

		[Pure]
		protected IEnumerable<char> DenormalizeSelector(string fileName) {
			using(var enumerator = fileName.GetEnumerator())
				while(enumerator.MoveNext()) {
					var current = enumerator.Current;
					if(current != NormalizeEscapeChar) yield return current;
					else yield return (char) (GetNextHexCharIndex(enumerator)
						                     + (GetNextHexCharIndex(enumerator) << 4)
						                     + (GetNextHexCharIndex(enumerator) << 8)
						                     + (GetNextHexCharIndex(enumerator) << 16));
				}
		}

		private int GetNextHexCharIndex(CharEnumerator enumerator) {
			if(!enumerator.MoveNext()) throw new ArgumentException(InvalidEscapedCharEndExceptionMessage, "enumerator");
			return GetHexCharIndex(enumerator.Current);
		}

		[Pure]
		protected int GetHexCharIndex(char c) {
			var index = HexChars.IndexOf(c);
			if(index == -1) throw new ArgumentException(HexCharIndexExceptionMessage, "c");
			return index;
		}
		#endregion

		private const char CygWinDictionarySeperatorChar = '/';

		// ToDo test this
		[Pure, About("path is for file/ dictionary path")]
		public string PathToCygWinPath(string path) {
			var volumePart = GetVolumePatternInPath(path);
			if(volumePart == string.Empty) return path;
			var volumeName = GetVolumeNameFromPattern(volumePart);
			Debug.Assert(!string.IsNullOrEmpty(volumeName));
			return string.Format("{0}{1}{0}{2}", CygWinDictionarySeperatorChar, volumeName, path.Substring(volumePart.Length).Replace(IO.Path.DirectorySeparatorChar, CygWinDictionarySeperatorChar));
		}
	}
}