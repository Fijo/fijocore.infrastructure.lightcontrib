using System.Collections.Generic;
using Fijo.Infrastructure.DesignPattern.Mapping;
using Fijo.Infrastructure.DesignPattern.Repository;
using FijoCore.Infrastructure.LightContrib.Enums;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Impl;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Settings;
using FijoCore.Infrastructure.LightContrib.Module.Stream;
using JetBrains.Annotations;
using Newtonsoft.Json;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization {
	[PublicAPI]
	public class SerializationRepository : RepositoryBase<IDictionary<SerializationFormat, ISerialization>> {
		[PublicAPI] protected readonly IDictionary<SerializationFormat, ISerialization> Content;

		public SerializationRepository(ILookupFactory lookupFactory, IStreamService streamService, IMapping<SerializationFormatting, Formatting> formattingMapping, IJsonSettings jsonDefaultSettings) {
			Content = lookupFactory.CreatePerf(new Dictionary<SerializationFormat, ISerialization>
			{
				{SerializationFormat.Binary, new BinarySerialization()},
				{SerializationFormat.BinaryDataContract, new BinaryDataContractSerialization()},
				{SerializationFormat.Soap, new SoapSerialization()},
				{SerializationFormat.JSON, new JSONSerialization(streamService, formattingMapping, jsonDefaultSettings)},
				{SerializationFormat.XML, new XMLSerialization(streamService)},
			}, true);
		}
		#region Overrides of RepositoryBase<IDictionary<SerializationFormat,ISerialization>>
		public override IDictionary<SerializationFormat, ISerialization> Get() {
			return Content;
		}
		#endregion
	}
}