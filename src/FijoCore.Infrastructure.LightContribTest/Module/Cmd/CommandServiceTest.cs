using System;
using FijoCore.Infrastructure.LightContrib.Module.Cmd;
using NUnit.Framework;

namespace FijoCore.Infrastructure.LightContribTest.Module.Cmd {
	[TestFixture]
	public class CommandServiceTest {
		private ICommandService _commandService;
		[SetUp]
		public void SetUp() {
			_commandService = new CommandService();
		}
		[Test]
		public void Test() {
			var value = _commandService.ExecuteReturn(string.Format("echo {0}", "Sind sie dumm?"));
			Assert.AreEqual(string.Concat("Sind sie dumm?", Environment.NewLine), value);
		}
	}
}