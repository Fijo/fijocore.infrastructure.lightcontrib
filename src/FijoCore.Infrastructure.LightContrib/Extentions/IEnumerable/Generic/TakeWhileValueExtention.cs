using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	[PublicAPI]
	public static class TakeWhileValueExtention {
		[Pure, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<T> TakeWhileValue<T>([NotNull] this IEnumerable<T> me, T value) {
			return me.TakeWhile(x => Equals(x, value));
		}

		[Pure, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<T> TakeWhileValue<T>([NotNull] this IEnumerable<T> me, T value, [NotNull] IEqualityComparer<T> comparer) {
			return me.TakeWhile(x => comparer.Equals(x, value));
		}
		
		[Pure, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<T> TakeWhileValues<T>([NotNull] this IEnumerable<T> me, [NotNull] ICollection<T> values) {
			return me.TakeWhile(values.Contains);
		}
		
		[Pure, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<T> TakeWhileValues<T>([NotNull] this IEnumerable<T> me, [NotNull] params T[] values) {
			return TakeWhileValues(me, (ICollection<T>) values);
		}

		[Pure, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<T> TakeWhileValues<T>([NotNull] this IEnumerable<T> me, [NotNull] ICollection<T> values, [NotNull] IEqualityComparer<T> comparer) {
			return me.TakeWhile(x => values.Contains(x, comparer));
		}

		[Pure, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<T> TakeWhileValues<T>([NotNull] this IEnumerable<T> me, [NotNull] IEqualityComparer<T> comparer, [NotNull] params T[] values) {
			return TakeWhileValues(me, values, comparer);
		}
	}
}