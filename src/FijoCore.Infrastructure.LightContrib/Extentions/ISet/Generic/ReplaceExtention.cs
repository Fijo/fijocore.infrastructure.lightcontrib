using System.Collections.Generic;
using System.Diagnostics;
using FijoCore.Infrastructure.LightContrib.Default.Exceptions;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.ISet.Generic {
	[PublicAPI]
	public static class ReplaceExtention {
		[PublicAPI, DebuggerStepThrough]
		public static void Replace<T>([NotNull] this ISet<T> me, [NotNull] T @new, [NotNull] T old) {
			lock (me) {
				if (!me.Remove(old)) throw new KeyNotFoundException<ISet<T>, T>(me, old);
				me.Add(@new);
			}
		}
	}
}