using System;
using System.Diagnostics;
using JetBrains.Annotations;
using System.Collections.Generic;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic {
	[PublicAPI]
	public static class GetOrCreateExtention {
		[PublicAPI, DebuggerStepThrough]
		public static TValue GetOrCreate<TCreateValue, TKey, TValue>(this IDictionary<TKey, TValue> me, TKey key) where TCreateValue : TValue, new() {
			return me.GetOrCreate(key, () => new TCreateValue());
		}

		[PublicAPI, DebuggerStepThrough]
		public static TValue GetOrCreate<TKey, TValue>(this IDictionary<TKey, TValue> me, TKey key) where TValue : new() {
			return me.GetOrCreate(key, () => new TValue());
		}

		[PublicAPI, DebuggerStepThrough]
		public static TValue GetOrCreate<TKey, TValue>(this IDictionary<TKey, TValue> me, TKey key, [NotNull] Func<TValue> createValue) {
			TValue value;
			lock (me) if (!me.TryGetValue(key, out value)) me.Add(key, value = createValue());
			return value;
		}
	}
}