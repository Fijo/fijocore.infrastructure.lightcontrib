using System;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream.Impl.Specific {
	[UsedImplicitly]
	public class Int64StreamSerializer : FixedSizeStreamSerializer<long> {
		#region Overrides of FixedSizeStreamSerializer<long>
		protected override int Size { get { return 8; } }
		protected override byte[] Serialize(long obj) {
			return BitConverter.GetBytes(obj);
		}

		protected override long Deserialize(byte[] bytes) {
			return BitConverter.ToInt64(bytes, 0);
		}
		#endregion
	}
}