using FijoCore.Components.Extentions.Double;
using NUnit.Framework;

namespace FijoCore.ComponentTest.Extentions.Double {
	[TestFixture]
	public class DoubleGetUnshiftedValueExtentionTest
	{
		[Ignore]
		[TestCase(0, 0)]
		[TestCase(1, 1)]
		[TestCase(2, 0)]
		[TestCase(4, 0)]
		[TestCase(8, 0)]
		[TestCase(16, 0)]
		public void Test(double input, long wanted) {
#pragma warning disable 612,618
			Assert.AreEqual(wanted, input.GetUnshiftedValue());
#pragma warning restore 612,618
		}
	}
}