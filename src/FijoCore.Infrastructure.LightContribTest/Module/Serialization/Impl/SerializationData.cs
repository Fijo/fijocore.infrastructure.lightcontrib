﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using FijoCore.Infrastructure.LightContrib.Default.Service.EqualityHelpers;

namespace FijoCore.Infrastructure.LightContribTest.Module.Serialization.Impl {
	public static class SerializationData {

		[Serializable]
		public class Many : List<DateTime> {
			#region Equality
			protected bool Equals(Many other) {
				return this.SequenceEqual(other);
			}

			public override bool Equals(object obj) {
				return Equality.Equals<Many>(obj, Equals);
			}

			public override int GetHashCode() {
				throw new NotSupportedException();
			}
			#endregion
		}

		[DataContract]
		[Serializable]
		public class A1 {
			[DataMember(Order= 1)]
			public string Test { get; set; }
			[DataMember(Order= 2)]
			public List<string> Lines { get; set; }
			[DataMember(Order= 3)]
			public int Field;
			[DataMember(Order= 4)]
			public Many Many { get; set; }
			[DataMember(Order= 5)]
			public IList<string> Names { get; set; }

			public static A1 Create() {
				return new A1
				{
					Test = "sdfgh",
					Lines = new List<string>{"tgkjdfgk", "gjfdk"},
					Field = 435,
					Many = new Many{new DateTime(2012, 1,1)},
					Names = new List<string>{"test", "lol"}
				};
			}

			#region Equality
			protected bool Equals(A1 other) {
				return Test == other.Test && Lines.SequenceEqual(other.Lines) && Field == other.Field && Many.Equals(other.Many) && Names.SequenceEqual(other.Names);
			}

			public override bool Equals(object obj) {
				return Equality.Equals<A1>(obj, Equals);
			}

			public override int GetHashCode() {
				throw new NotSupportedException();
			}
			#endregion
		}






		public static IEnumerable<object> Data { get {
			yield return A1.Create();
		} } 
	}
}