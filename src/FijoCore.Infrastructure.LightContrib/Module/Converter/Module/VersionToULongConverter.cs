using System;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Converter.Module {
	[PublicAPI, Service]
	public class VersionToULongConverter {
		[Pure, PublicAPI]
		public ulong Convert([NotNull] Version version) {
			return (ulong) ((version.Major << 48) + (version.Minor << 32) + (version.Build << 16) + version.Revision);
		}
	}
}