namespace FijoCore.Infrastructure.LightContrib.Default.Service.DefaultFunctions.Interface
{
	public interface IDefaultFunctions
	{
		bool ReturnFalse<T>(T x);
		bool ReturnTrue<T>(T x);
	}
}