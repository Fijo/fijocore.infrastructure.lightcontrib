using System.Collections.Generic;
using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Char {
	[PublicAPI]
	public static class IsAphabeticExtention {
		[NotNull] private static readonly ICollection<char> AlphabeticChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_".ToCharArray();

		[Pure, DebuggerStepThrough, PublicAPI]
		[Desc("Returns if if the given char is one of these �abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_�. Otherwise it will return false.")]
		public static bool IsAlphabetic([NotNull] this char? me) {
			// ReSharper disable PossibleInvalidOperationException
			return ((char) me).IsAlphabetic();
			// ReSharper restore PossibleInvalidOperationException
		}

		[Pure, DebuggerStepThrough, PublicAPI]
		[Desc("Returns if if the given char is one of these �abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_�. Otherwise it will return false.")]
		public static bool IsAlphabetic(this char me) {
			return AlphabeticChars.Contains(me);
		}
	}
}