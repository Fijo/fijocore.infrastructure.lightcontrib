using System.Collections.Generic;
using System.Diagnostics;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using FijoCore.Infrastructure.LightContrib.Extentions.Type;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Generic.Reflection.Resolve
{
	public static class TryGetEnumeratorExtention {
		private static IOut _out;

		[DebuggerStepThrough]
		internal static void Init() {
			_out = Kernel.Resolve<IOut>();
		}

		[DebuggerStepThrough]
		public static bool TryGetEnumerator<T>(this T me, out System.Collections.IEnumerator result, bool genericEnumerable = true, bool enumerable = true, bool enumerator = true) {
			if (enumerable && me is System.Collections.IEnumerable) return _out.True(out result, ((System.Collections.IEnumerable)me).GetEnumerator());
			if (genericEnumerable && me.GetType().ImplementsInteface(typeof(IEnumerable<>))) return _out.True(out result, me.GenericEnumerableToEnumerator());
			if (enumerator && me is System.Collections.IEnumerator) return _out.True(out result, (System.Collections.IEnumerator)me);
			return _out.False(out result);
		}
	}
}