﻿using System;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Service.ExecptionFormatter {
	[UsedImplicitly]
	public class ExceptionFormatter : IExceptionFormatter {
		public string Format(Exception exception) {
			var innerException = exception.InnerException;
			return string.Format("Exception: {0}, {1}{2}", exception.GetType().Name, exception.Message,
			                     innerException == null ? string.Empty : string.Format(" ( {0} )", Format(innerException)));
		}
	}
}
