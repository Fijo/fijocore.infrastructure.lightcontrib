using System.Diagnostics;

namespace FijoCore.Infrastructure.LightContrib.Module.Converter.Simple {
	public class ProcessPriorityClassConverter : EnumConverter<ProcessPriorityClass> {}
}