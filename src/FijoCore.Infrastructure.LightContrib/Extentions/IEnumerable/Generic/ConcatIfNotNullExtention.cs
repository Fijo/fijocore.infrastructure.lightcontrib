using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	[PublicAPI]
	public static class ConcatIfNotNullExtention {
		[NotNull, Pure, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<TSource> ConcatIfNotNull<TSource>([NotNull] this IEnumerable<TSource> me, IEnumerable<TSource> collection) {
			return collection == null ? me : me.Concat(collection);
		}
	}
}