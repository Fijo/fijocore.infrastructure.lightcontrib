using FijoCore.Components.Extentions.Double;
using NUnit.Framework;

namespace FijoCore.ComponentTest.Extentions.Double {
	[TestFixture]
	public class GetDigitLengthExtentionTest
	{
		[Ignore]
		[TestCase(5345.243, 6)]
		[TestCase(10.0, 0)]
		[TestCase(9.0, 0)]
		[TestCase(1.0, 0)]
		[TestCase(0.0, 0)]
		[TestCase(0.9, 0)]
		[TestCase(534234235.223443, 14)]
		[TestCase(0.5345243, 6)]
		[TestCase(0.0045243, 4)]
		public void Test(double value, int wanted) {
#pragma warning disable 612,618
			Assert.AreEqual(wanted, value.GetDigitLength(value.GetFirstDigitPosition()));
#pragma warning restore 612,618
		}
	}
}