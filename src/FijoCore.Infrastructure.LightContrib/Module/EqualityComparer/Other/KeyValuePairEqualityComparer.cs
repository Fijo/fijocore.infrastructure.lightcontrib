using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using FijoCore.Infrastructure.LightContrib.Extentions.Integer;
using FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Sequence;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Other {
	[PublicAPI, Serializable]
	public class KeyValuePairEqualityComparer<TKey, TValue> : IEqualityComparer<KeyValuePair<TKey, TValue>>, ISerializable {
		private volatile static IEqualityComparer<KeyValuePair<TKey, TValue>> _default;
		public static IEqualityComparer<KeyValuePair<TKey, TValue>> Default { get { return _default ?? (_default = new KeyValuePairEqualityComparer<TKey, TValue>()); } }
		
		protected KeyValuePairEqualityComparer() {}

		#region Implementation of IEqualityComparer<in KeyValuePair<TKey,TValue>>
		public bool Equals(KeyValuePair<TKey, TValue> x, KeyValuePair<TKey, TValue> y) {
			return Equals(x.Key, y.Key) && Equals(x.Value, y.Value);
		}

		public int GetHashCode(KeyValuePair<TKey, TValue> obj) {
			return obj.Key.GetHashCode().GetHashCodeAlgorithm(obj.Value.GetHashCode());
		}
		#endregion

		#region Implementation of ISerializable
		public void GetObjectData(SerializationInfo info, StreamingContext context) {
			info.SetType(typeof (SingletonSerializer));
		}
		
		[Serializable]
		protected class SingletonSerializer : IObjectReference {
			#region Implementation of IObjectReference
			public object GetRealObject(StreamingContext context) {
				return Default;
			}
			#endregion
		}
		#endregion
	}
}