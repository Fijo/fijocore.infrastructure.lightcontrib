using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using FijoCore.Infrastructure.LightContribTest.Properties;
using NUnit.Framework;

namespace FijoCore.Infrastructure.LightContribTest.Default.Service.Out
{
	[TestFixture]//(typeof(IOut))
	public class OutTest
	{
		private IOut _out;

		[SetUp]
		public void SetUp()
		{
			new InternalInitKernel().Init();
			_out = Kernel.Resolve<IOut>();
		}

		[Test]
		public void FalseTest()
		{
			int obj;
			Assert.IsFalse(_out.False(out obj));
			Assert.AreEqual(obj, 0);
		}
		
		[Test]
		public void FalseSetTest()
		{
			int obj;
			Assert.IsFalse(_out.False(out obj, -234));
			Assert.AreEqual(obj, -234);
		}

		[Test]
		public void TrueTest()
		{
			int obj;
			Assert.IsTrue(_out.True(out obj));
			Assert.AreEqual(obj, 0);
		}
		
		[Test]
		public void TrueSetTest()
		{
			int obj;
			Assert.IsTrue(_out.True(out obj, -234));
			Assert.AreEqual(obj, -234);
		}

		[Test]
		public void GenericTest()
		{
			int obj;
			Assert.IsTrue(_out.Generic(out obj, true));
			Assert.AreEqual(obj, 0);
		}
		
		[Test]
		public void GenericSetTest()
		{
			int obj;
			Assert.IsTrue(_out.Generic(out obj, true, -234));
			Assert.AreEqual(obj, -234);
		}
	}
}