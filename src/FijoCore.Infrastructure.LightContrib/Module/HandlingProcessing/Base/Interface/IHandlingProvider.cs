using Fijo.Infrastructure.DesignPattern.Repository.Collection;

namespace FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Base.Interface {
	public interface IHandlingProvider<out THandling> : ICollectionRepository<THandling> {}
}