using System.Collections.Generic;
using System.Reflection;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace FijoCore.Infrastructure.LightContrib.Dto {
	[Dto, Note("only used for exceptions to be passed in as additional info")]
	public class GetConstructor {
		public IList<System.Type> ParamTypes;
		public IList<object> Params;
		public BindingFlags BindingFlags;
	}
}