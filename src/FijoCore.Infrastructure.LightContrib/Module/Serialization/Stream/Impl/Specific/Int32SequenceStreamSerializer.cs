using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Module.Stream;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream.Impl.Specific {
	[UsedImplicitly]
	public class Int32SequenceStreamSerializer : IStreamSerializer<IEnumerable<int>> {
		private readonly IStreamService _streamService = Kernel.Resolve<IStreamService>();
		private readonly IOut _out = Kernel.Resolve<IOut>();
		#region Implementation of IStreamSerializer<IEnumerable<int>>
		public void Serialize(StreamWriter streamWriter, IEnumerable<int> obj) {
			var stream = _streamService.GetStream(streamWriter);
			obj.Select(BitConverter.GetBytes)
				.ForEach(x => _streamService.WriteToStream(stream, x));
		}

		public bool TryDeserialize(StreamReader streamReader, out IEnumerable<int> result) {
			return _out.True(out result,
			                 _streamService.ReadToBytesSequence(_streamService.GetStream(streamReader))
				                 .IntoGroup(4)
				                 .Select(Convert));
		}

		private int Convert(IEnumerable<byte> x) {
			var array = x.ToArray();
			if (array.Length != 4)
				throw new IndexOutOfRangeException(string.Format("array.Length should have a length of 4 but it has {0}. Invalid data read from ´streamReader´- arg. Invalid data to serialize.", array.Length));
			return BitConverter.ToInt32(array, 0);
		}
		#endregion
	}
}