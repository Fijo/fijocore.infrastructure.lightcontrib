using System;
using FijoCore.Infrastructure.LightContrib.Default.Exceptions;
using FijoCore.Infrastructure.LightContrib.Delegates;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Default.Service.Try {
	public interface IExlessTryTimesService {
		TResult ExlessTryTimes<TResult, TRetiesReachedException>([NotNull] Func<TRetiesReachedException> exceptionFactory, [NotNull] TryFunc<TResult> func, int maxTries)
			where TRetiesReachedException : MaxRetriesReachedException;

		TResult ExlessTryTimes<TResult, TRetiesReachedException>([NotNull] TryFunc<TResult> func, int maxTries)
			where TRetiesReachedException : MaxRetriesReachedException, new();
			
		void ExlessTryTimes<TRetiesReachedException>([NotNull] Func<TRetiesReachedException> exceptionFactory, [NotNull] TryFunc func, int maxTries)
			where TRetiesReachedException : MaxRetriesReachedException;

		void ExlessTryTimes<TRetiesReachedException>([NotNull] TryFunc func, int maxTries)
			where TRetiesReachedException : MaxRetriesReachedException, new();
	}
}