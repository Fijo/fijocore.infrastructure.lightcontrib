using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace FijoCore.Infrastructure.LightContrib.Module.Perf.Configuration {
	[Dto]
	public class PerfConfiguration {
		public long RamForApp;
		public long RamLeft;
		public long RamSmallLeft;
		public long RamDefaultLeft;
		public int MaxSizeForFixedListType;
	}
}