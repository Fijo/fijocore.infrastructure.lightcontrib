using System;
using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;
using System.Collections.Generic;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic {
	[PublicAPI]
	public static class TryGetExtention {
		[Pure, PublicAPI, DebuggerStepThrough]
		public static TValue TryGet<TKey, TValue>([NotNull] this IDictionary<TKey, TValue> me, TKey key) {
			return me.TryGet(key, default(TValue));
		}

		[Pure, PublicAPI, DebuggerStepThrough]
		public static TValue TryGet<TKey, TValue>([NotNull] this IDictionary<TKey, TValue> me, TKey key, TValue failValue) {
			TValue value;
			return me.TryGetValue(key, out value) ? value : failValue;
		}

		#region TryGet- Execute Action
		[PublicAPI, DebuggerStepThrough]
		public static void TryGet<TKey, TValue>([NotNull] this IDictionary<TKey, TValue> me, TKey key, [NotNull] Action<IDictionary<TKey, TValue>, TKey, TValue> succsess, [NotNull] Action<IDictionary<TKey, TValue>, TKey> fail) {
			TValue value;
			if (me.TryGetValue(key, out value)) succsess(me, key, value);
			else fail(me, key);
		}

		#region With Dict and Key
		[PublicAPI, DebuggerStepThrough, PerformanceRiskyBecauseHighAbstraction("heavy usage of delegates for this simple function may inline simple functionality directly or use null checks and remove some overloadings using this way")]
		public static void TryGet<TKey, TValue>([NotNull] this IDictionary<TKey, TValue> me, TKey key, [NotNull] Action<IDictionary<TKey, TValue>, TKey, TValue> succsess) {
			TryGet(me, key, succsess, DefaultFail);
		}

		[PublicAPI, DebuggerStepThrough, PerformanceRiskyBecauseHighAbstraction("heavy usage of delegates for this simple function may inline simple functionality directly or use null checks and remove some overloadings using this way")]
		public static void TryGet<TKey, TValue>([NotNull] this IDictionary<TKey, TValue> me, TKey key, [NotNull] Action<IDictionary<TKey, TValue>, TKey> fail) {
			TryGet(me, key, DefaultSuccsess, fail);
		}
		#endregion
		#region With Key
		[PublicAPI, DebuggerStepThrough, PerformanceRiskyBecauseHighAbstraction("heavy usage of delegates for this simple function may inline simple functionality directly or use null checks and remove some overloadings using this way")]
		public static void TryGet<TKey, TValue>([NotNull] this IDictionary<TKey, TValue> me, TKey key, [NotNull] Action<TKey, TValue> succsess, [NotNull] Action<TKey> fail) {
			TryGet(me, key, SuccsessWrapper<IDictionary<TKey, TValue>, TKey, TValue>(succsess), FailWrapper<IDictionary<TKey, TValue>, TKey>(fail));
		}

		[PublicAPI, DebuggerStepThrough, PerformanceRiskyBecauseHighAbstraction("heavy usage of delegates for this simple function may inline simple functionality directly or use null checks and remove some overloadings using this way")]
		public static void TryGet<TKey, TValue>([NotNull] this IDictionary<TKey, TValue> me, TKey key, [NotNull] Action<TKey, TValue> succsess) {
			TryGet(me, key, succsess, DefaultFail);
		}

		[PublicAPI, DebuggerStepThrough, PerformanceRiskyBecauseHighAbstraction("heavy usage of delegates for this simple function may inline simple functionality directly or use null checks and remove some overloadings using this way")]
		public static void TryGet<TKey, TValue>([NotNull] this IDictionary<TKey, TValue> me, TKey key, [NotNull] Action<TKey> fail) {
			TryGet(me, key, DefaultSuccsess, fail);
		}

		#region Wrapper
		[NotNull, Pure]
		private static Action<TDict, TKey> FailWrapper<TDict, TKey>(Action<TKey> fail) {
			return (dict, key) => fail(key);
		}

		[NotNull, Pure]
		private static Action<TDict, TKey, TValue> SuccsessWrapper<TDict, TKey, TValue>(Action<TKey, TValue> succsess) {
			return (dict, key, value) => succsess(key, value);
		}
		#endregion
		#endregion
		#region Without Dict and Key
		[PublicAPI, DebuggerStepThrough, PerformanceRiskyBecauseHighAbstraction("heavy usage of delegates for this simple function may inline simple functionality directly or use null checks and remove some overloadings using this way")]
		public static void TryGet<TKey, TValue>([NotNull] this IDictionary<TKey, TValue> me, TKey key, [NotNull] Action<TValue> succsess, [NotNull] Action fail) {
			TryGet(me, key, SuccsessWrapper<IDictionary<TKey, TValue>, TKey, TValue>(succsess), FailWrapper<IDictionary<TKey, TValue>, TKey>(fail));
		}

		[PublicAPI, DebuggerStepThrough, PerformanceRiskyBecauseHighAbstraction("heavy usage of delegates for this simple function may inline simple functionality directly or use null checks and remove some overloadings using this way")]
		public static void TryGet<TKey, TValue>([NotNull] this IDictionary<TKey, TValue> me, TKey key, [NotNull] Action<TValue> succsess) {
			TryGet(me, key, SuccsessWrapper<IDictionary<TKey, TValue>, TKey, TValue>(succsess), DefaultFail);
		}

		[PublicAPI, DebuggerStepThrough, PerformanceRiskyBecauseHighAbstraction("heavy usage of delegates for this simple function may inline simple functionality directly or use null checks and remove some overloadings using this way")]
		public static void TryGet<TKey, TValue>([NotNull] this IDictionary<TKey, TValue> me, TKey key, [NotNull] Action fail) {
			TryGet(me, key, DefaultSuccsess, FailWrapper<IDictionary<TKey, TValue>, TKey>(fail));
		}

		#region Wrapper
		[NotNull, Pure]
		private static Action<TDict, TKey> FailWrapper<TDict, TKey>(Action fail) {
			return (dict, key) => fail();
		}

		[NotNull, Pure]
		private static Action<TDict, TKey, TValue> SuccsessWrapper<TDict, TKey, TValue>(Action<TValue> succsess) {
			return (dict, key, value) => succsess(value);
		}
		#endregion
		#endregion
		#region Default delegates
		private static void DefaultSuccsess<TDict, TKey, TValue>(TDict dict, TKey key, TValue value) {}
		private static void DefaultSuccsess<TKey, TValue>(TKey key, TValue value) {}
		private static void DefaultSuccsess<TValue>(TValue value) {}
		private static void DefaultFail<TDict, TKey>(TDict dict, TKey key) {}
		private static void DefaultFail<TKey>(TKey key) {}
		private static void DefaultFail() {}
		#endregion
		#endregion
	}
}