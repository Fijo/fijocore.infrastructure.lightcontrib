using System;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream.Impl.Specific {
	[UsedImplicitly]
	public class Int16StreamSerializer : FixedSizeStreamSerializer<short> {
		#region Overrides of FixedSizeStreamSerializer<short>
		protected override int Size { get { return 2; } }
		protected override byte[] Serialize(short obj) {
			return BitConverter.GetBytes(obj);
		}

		protected override short Deserialize(byte[] bytes) {
			return BitConverter.ToInt16(bytes, 0);
		}
		#endregion
	}
}