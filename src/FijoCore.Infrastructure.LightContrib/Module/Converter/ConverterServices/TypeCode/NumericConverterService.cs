using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using FijoCore.Infrastructure.LightContrib.Module.Converter.ConverterServices.TypeCode.Base;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Converter.ConverterServices.TypeCode {
	[PublicAPI, Service]
	public class NumericConverterService : ConverterServiceTypeCodeBase<double> {
		public NumericConverterService() {
			ConvertableTypes = new List<System.TypeCode>
			{
				System.TypeCode.Byte,
				System.TypeCode.Char,
				System.TypeCode.Decimal,
				System.TypeCode.Double,
				System.TypeCode.Int16,
				System.TypeCode.Int32,
				System.TypeCode.Int64,
				System.TypeCode.Object,
				System.TypeCode.SByte,
				System.TypeCode.Single,
				System.TypeCode.UInt16,
				System.TypeCode.UInt32,
				System.TypeCode.UInt64
			};
		}

		[Pure]
		protected override double GetConverted<T>([NotNull] T obj) {
			return System.Convert.ToDouble(obj);
		}
	}
}