using System.Collections.Generic;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.Annotation;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Base;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Parallelizable.Interfaces;

namespace FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Parallelizable {
	public abstract class ParallelizableHandlingProcessor<THandling> : HandlingProcessor<THandling>, IParallelizableHandlingProcessor<THandling> where THandling : ISupportsParallelization {
		#region ProviderSupport
		protected readonly bool ProviderSupportParallelization;
		protected readonly bool HandlingsSupportParallelization;
		protected readonly bool SupportParallelization;
		#endregion

		protected ParallelizableHandlingProcessor(IParallelizableHandlingProvider<THandling> handlingRepository) : base(handlingRepository) {
			ProviderSupportParallelization = handlingRepository.SupportParallelization;
			HandlingsSupportParallelization = GetSupportsParallelization(Handlings);
			SupportParallelization = GetSupportParallelization();
		}

		#region ProviderSupport
		protected virtual bool GetSupportParallelization() {
			return ProviderSupportParallelization && HandlingsSupportParallelization;
		}

		protected virtual bool GetSupportsParallelization(IEnumerable<THandling> handlings) {
			return handlings.All(x => x.SupportParallelization);
		}
		#endregion
	}
}