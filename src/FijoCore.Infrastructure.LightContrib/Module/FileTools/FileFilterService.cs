using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.FileTools {
	[PublicAPI]
	public class FileFilterService : IFileFilterService {
		[Pure, PublicAPI]
		public IEnumerable<string> FilterByExt(IEnumerable<string> files, string ext) {
			return files.Where(file => HasExt(file, ext));
		}

		[Pure, PublicAPI]
		public bool HasExt(string file, string ext) {
			return file.ToLowerInvariant().EndsWith(string.Format(".{0}", ext.ToLowerInvariant()));
		}
	}
}