using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IList.Generic.Main
{
	public static class PushExtention
	{
		[DebuggerStepThrough]
		public static T Push<T>([NotNull] this IList<T> me, T myEntry) {
			me.Add(myEntry);
			return me.Last();
		}
	}
}
