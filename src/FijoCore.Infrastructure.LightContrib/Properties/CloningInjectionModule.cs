﻿using System;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Provider;
using FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Base;
using FijoCore.Infrastructure.LightContrib.Module.Cloning;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners.Impl;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Dtos;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Provider;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Conditional.Interface;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Parallelizable.Interfaces;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Parallelizable.Provider;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;

namespace FijoCore.Infrastructure.LightContrib.Properties {
	public class CloningInjectionModule : ExtendedNinjectModule {
		public override void AddModule(IKernel kernel) {
    		kernel.Load(new LightContribBaseInjectionModule());
		}

		public override void OnLoad(IKernel kernel) {
			//kernel.Bind<IFinder<ICloning, CloningType>>().To<Finder<ICloning, CloningType>>().InSingletonScope();
			kernel.Bind<IAlgorithmProvider<ICloning, CloningType>>().To<CloningProvider>().InSingletonScope();
			kernel.Bind<ICloning>().To<DefaultCloning>().InSingletonScope();
			kernel.Bind<ICloning>().To<DeepCloning>().InSingletonScope();

			//kernel.Bind<IFinder<IClonerCreator, Type>>().To<Finder<IClonerCreator, Type>>().InSingletonScope();
			kernel.Bind<IAlgorithmProvider<IClonerCreator, Type>>().To<ClonerCreatorProvider>().InSingletonScope();

			//kernel.Bind<ICHProcessor<CloneSource>>().To<CHProcessor<CloneSource>>().InSingletonScope();
			kernel.Bind<IParallelizableHandlingProvider<ICH<CloneSource>>>().To<InjectionParallelizableHandlingProvider<ICH<CloneSource>, ICloner>>().InSingletonScope();

			kernel.Bind<ICloneService>().To<CloneService>().InSingletonScope();
		}
	}
}