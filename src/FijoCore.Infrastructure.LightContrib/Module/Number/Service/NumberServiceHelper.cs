using System;
using FijoCore.Infrastructure.LightContrib.Module.Number.Service.Interface;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Number.Service {
	[UsedImplicitly]
	public class NumberServiceHelper : INumberServiceHelper {
		[Pure]
		public TResult GetPossibilities<TSource, TResult>(INumberService<TSource> sourceService, INumberService<TResult> resultService) where TSource : struct, IComparable, IConvertible where TResult : struct, IComparable, IConvertible {
			var res = resultService.Diff(Convert<TSource, TResult>(sourceService.MaxValue), Convert<TSource, TResult>(sourceService.MinValue));
			return resultService.HasZeroValue ? resultService.Sun(res, resultService.Convert(1)) : res;
		}

		[Pure]
		private TResult Convert<TSource, TResult>(TSource source) where TSource : struct, IComparable, IConvertible where TResult : struct, IComparable, IConvertible {
			return (TResult) System.Convert.ChangeType(source, typeof (TResult));
		}
	}
}