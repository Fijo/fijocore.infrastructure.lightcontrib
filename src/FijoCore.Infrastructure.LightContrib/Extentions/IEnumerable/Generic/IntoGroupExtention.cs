using System.Collections.Generic;
using System.Diagnostics;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	[PublicAPI]
	public static class IntoGroupExtention {
		[NotNull, Pure, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<IEnumerable<TSource>> IntoGroup<TSource>([NotNull] this IEnumerable<TSource> me, int count) {
			using(var enumerator = me.GetEnumerator()) return enumerator.IntoGroup(count);
		}
	}
}