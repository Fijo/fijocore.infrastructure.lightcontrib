using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using FijoCore.Infrastructure.LightContrib.Module.Converter.ConverterServices.Base;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Converter.ConverterServices.Type.Base {
	[PublicAPI, Service]
	public abstract class ConverterServiceTypeBase<TResult> : ConverterServiceBase<System.Type, TResult> {
		[NotNull, Pure]
		protected override System.Type GetType<T>() {
			return typeof (T);
		}
	}
}