using Fijo.Infrastructure.DesignPattern.Observer.Interface;

namespace FijoCore.Infrastructure.LightContrib.Module.Loging.Observerbased.Interface {
	public interface ILoggingObserver : IObserver {}
}