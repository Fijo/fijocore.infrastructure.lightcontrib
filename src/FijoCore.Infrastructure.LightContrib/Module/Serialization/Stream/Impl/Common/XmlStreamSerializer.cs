using FijoCore.Infrastructure.LightContrib.Enums;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream.Impl.Specific;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream.Impl.Common {
	[PublicAPI]
	public class XmlStreamSerializer<T> : SerializationBaseStreamSerializer<T> {
		#region Overrides of SerializationBaseStreamSerializer<T>
		protected override SerializationFormat SerializationFormat { get { return SerializationFormat.XML; } }
		#endregion
	}
}