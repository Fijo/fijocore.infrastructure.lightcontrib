using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.Object;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Base.Interface;

namespace FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Base {
	[Note("This class was called ´MethodeManagerBase´ in the past.")]
	public abstract class HandlingProcessor<THandling> : IHandlingProcessor<THandling> {
		protected readonly ICollection<THandling> Handlings;

		protected HandlingProcessor(IHandlingProvider<THandling> handlingRepository) {
			var enumerable = handlingRepository.Get();
			Debug.Assert(enumerable != null);
			Handlings = enumerable.Execute();
			Validation();
		}

		#region Validation
		protected virtual void Validation() {
			Debug.Assert(Handlings.IsUnique((THandling x, THandling y) => x.InstanceEquals(y)), "Your THandlings are not unique. Please make shure that your repository does not return the same THandling more than one time.");
		}

		#region CustomProcess
		protected virtual void ProcessValidation(IEnumerable<THandling> handlings) {
			Debug.Assert(handlings.Concat(Handlings).IsUnique(), "Your THandlings together with the custom ones are not unique. Please make shure that do not add the same THandling more than one time into the HandlingProcessor (you can use it several times, but not add the same one more than one time (that includes although additionalChs) per HandlingProcessor.");
		}

		protected virtual IEnumerable<THandling> CustomProcessHandlingsWrapper(IEnumerable<THandling> customHandlings) {
			return CustomProcessPreCondition(customHandlings);
		}

		protected virtual IEnumerable<THandling> CustomProcessPreCondition(IEnumerable<THandling> customHandlings) {
			#if DEBUG
			if(customHandlings == null) return null;
			customHandlings = customHandlings.Execute();
			ProcessValidation(customHandlings);
			#endif
			return customHandlings;
		}
		#endregion
		#endregion
	}
}