using Fijo.Infrastructure.Model.Pair.Interface.Base;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream.Impl.Specific {
	[PublicAPI]
	public class PairStreamSerializer<TPair, TFirst, TSecond> : PairBaseStreamSerializer<TPair, TFirst, TSecond> where TPair : IPair<TFirst, TSecond>, new() {
		#region Overrides of PairBaseStreamSerializer<TPair,TFirst,TSecond>
		protected override TFirst GetFirst(TPair pair) {
			return pair.First;
		}

		protected override TSecond GetSecond(TPair pair) {
			return pair.Second;
		}

		protected override TPair GetPair(TFirst first, TSecond second) {
			var pair = new TPair();
			pair.Create(first, second);
			return pair;
		}
		#endregion
	}
}