﻿using System.IO;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream {
	public interface IStreamSerializer<T> {
		void Serialize([NotNull] StreamWriter streamWriter, T obj);
		bool TryDeserialize([NotNull] StreamReader streamReader, out T result);
	}
}