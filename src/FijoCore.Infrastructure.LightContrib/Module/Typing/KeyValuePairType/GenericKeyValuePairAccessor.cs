using System;

namespace FijoCore.Infrastructure.LightContrib.Module.Typing.KeyValuePairType {
	public class GenericKeyValuePairAccessor {
		public readonly Func<object, object> Key;
		public readonly Func<object, object> Value;

		public GenericKeyValuePairAccessor(Func<object, object> key, Func<object, object> value) {
			Key = key;
			Value = value;
		}
	}
}