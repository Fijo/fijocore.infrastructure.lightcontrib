using System.Linq;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Objects;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Provider;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic.KeyValuePair;

namespace FijoCore.Infrastructure.LightContrib.Module.AlgorithmProvider {
	public abstract class FallbackInjectionAlgorithmProvider<TAlgorithm, TKey> : InjectionAlgorithmProviderBase<TAlgorithm, TKey>, IAlgorithmProvider<TAlgorithm, TKey> {
		#region Implementation of InjectionAlgorithmProviderBase<TAlgorithm,TKey>
		public Algorithms<TAlgorithm, TKey> Get() {
			var algorithms = GetAlgorithms().Execute();
			var fallback = algorithms.Single(IsFallback);
			var dict = algorithms.Without(fallback).ToDict(KeySelector, ValueSelector);
			return new FallbackAlgorithms<TAlgorithm, TKey>(dict, fallback);
		}

		protected abstract bool IsFallback(TAlgorithm algorithm);
		#endregion
	}
}