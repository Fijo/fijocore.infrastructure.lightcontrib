using Fijo.Infrastructure.DesignPattern.Lazy.Interface;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Lazy.Interface {
	[PublicAPI]
	public interface ILazyInject<out T> : ILazy<T> {}
}