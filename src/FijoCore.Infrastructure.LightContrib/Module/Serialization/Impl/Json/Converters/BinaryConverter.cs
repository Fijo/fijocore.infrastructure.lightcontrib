using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using Fijo.Infrastructure.DesignPattern.Lazy;
using Fijo.Infrastructure.DesignPattern.Lazy.Interface;
using FijoCore.Infrastructure.LightContrib.Enums;
using FijoCore.Infrastructure.LightContrib.Extentions.Byte;
using FijoCore.Infrastructure.LightContrib.Module.Stream;
using JetBrains.Annotations;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization.Impl.Json.Converters {
	[PublicAPI]
	public abstract class BinaryConverter : JsonCreationConverter {
		private readonly IStreamService _streamService;
		private readonly ILazy<System.Text.Encoding> _encoding;
		private readonly ILazy<ISerialization> _serialization;
		private readonly ILazy<ISerializationService> _serializationService;

		protected BinaryConverter(ILazy<ISerializationProvider> serializationProvider, IStreamService streamService, ILazy<ISerializationService> serializationService, ILazy<System.Text.Encoding> encoding) {
			_streamService = streamService;
			_serialization = new LazyField<ISerialization>(() => serializationProvider.Get().Get(SerializationFormat.Binary));
			_serializationService = serializationService;
			_encoding = encoding;
		}

		#region Overrides of JsonConverter
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) {
			var readToString = SerializeToString(value);
			writer.WriteValue(readToString);
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer) {
			if (reader.TokenType == JsonToken.Null) return null;

			//var readAsString = reader.Value;
			//return DeserializeFromString(readAsString);
			Debug.Assert(reader.TokenType == JsonToken.String);
			var readJson = reader.Value;
			//reader.Read();
			return DeserializeFromString((string) readJson);
		}
		#endregion

		private string SerializeToString(object value) {
			using (var stream = new MemoryStream()) {
				_serializationService.Get().Serialize(_serialization.Get(), value, stream, encoding: _encoding.Get());
				stream.Position = 0;
				return _streamService.ReadToBytesSequence(stream).ToHexString();
			}
		}

		private object DeserializeFromString(string value) {
			if (value == null) return null;
			using (var stream = new MemoryStream()) {
				_streamService.WriteToStream(stream, value.ArrayFromHexString());
				stream.Position = 0;
				return _serializationService.Get().Deserialize<MemberInfo>(_serialization.Get(), stream, _encoding.Get());
			}
		}

		protected override object Create(Type objectType, JObject jObject) {
			return jObject;
		}
	}
}