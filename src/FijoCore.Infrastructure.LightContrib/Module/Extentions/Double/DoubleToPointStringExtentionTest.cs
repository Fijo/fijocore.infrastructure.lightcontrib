using FijoCore.Components.Extentions.Double;
using NUnit.Framework;

namespace FijoCore.ComponentTest.Extentions.Double {
	[TestFixture]
	public class DoubleToPointStringExtentionTest
	{
		[TestCase(5345.5234e+24, "5345.5234e+24")]
		[TestCase(234e+23, "234e+23")]
		[TestCase(1313e+23, "1313e+23")]
		[TestCase(7.0, "7")]
		[TestCase(423.0, "423")]
		[TestCase(-295.0, "-295")]
		[TestCase(-4.5, "-4.5")]
		[TestCase(7.9, "7.9")]
		[TestCase(7.7, "7.7")]
		[TestCase(7.2, "7.2")]
		[TestCase(324.3455, "324.3455")]
		[TestCase(4234.234, "4234.234")]
		[TestCase(5546.242324, "5546.242324")]
		[TestCase(3242.52344, "3242.52344")]
		[TestCase(324.4234234, "324.4234234")]
		[TestCase(-3242.234, "-3242.234")]
		[TestCase(0.4234234, ".4234234")]
		[TestCase(0.234, ".234")]
		public void Test(double input, string expected)
		{
			Assert.AreEqual(expected, input.ToCustomPointString(false, false, "0"));
		}
	}
}