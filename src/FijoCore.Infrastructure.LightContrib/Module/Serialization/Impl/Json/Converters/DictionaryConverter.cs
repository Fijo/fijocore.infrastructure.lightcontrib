﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.Infrastructure.Model;
using FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Module.Typing.DictionaryType;
using FijoCore.Infrastructure.LightContrib.Module.Typing.KeyValuePairType;
using JetBrains.Annotations;
using Newtonsoft.Json;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization.Impl.Json.Converters {
	public class DictionaryConverter : JsonConverter {
		#region nested classes
		[Serializable]
		protected class SerializationDictionary {
			public readonly KeyValuePair[] Content;
			public readonly Type DictionaryType;

			[UsedImplicitly]
			[Note("only used for Serialisation")]
			private SerializationDictionary() {}

			public SerializationDictionary(KeyValuePair[] content, Type dictionaryType) {
				Content = content;
				DictionaryType = dictionaryType;
			}
		}
		#endregion
		
		private readonly IDictionaryTypeService _dictionaryTypeService;
		private readonly IGenericKeyValuePairAccessorProvider _genericKeyValuePairAccessorProvider;
		private readonly IDictionary<Type, ConstructorInfo> _constructorCache = new Dictionary<Type, ConstructorInfo>();
		private readonly IDictionary<Type, Type> _dictionaryToKeyValuePair = new Dictionary<Type, Type>();
		private readonly Type[] _constructorTypeArray = new Type[0];

		public override bool CanRead { get { return true; } }

		public DictionaryConverter(IDictionaryTypeService dictionaryTypeService, IGenericKeyValuePairAccessorProvider genericKeyValuePairAccessorProvider) {
			_dictionaryTypeService = dictionaryTypeService;
			_genericKeyValuePairAccessorProvider = genericKeyValuePairAccessorProvider;
		}

		#region Overrides of JsonConverter
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) {
			#region PreCondition
			Debug.Assert(value is IEnumerable);
			#endregion
			serializer.Serialize(writer, MapForSerialize((IEnumerable) value));
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer) {
			return MapDeserialized((SerializationDictionary) serializer.Deserialize(reader, typeof (SerializationDictionary)));
		}

		public override bool CanConvert(Type objectType) {
			return _dictionaryTypeService.IsDictionary(objectType);
		}
		#endregion
		
		private object MapForSerialize(IEnumerable value) {
			var type = value.GetType();
			var accessor = _genericKeyValuePairAccessorProvider.Get(GetKeyValuePairType(_dictionaryTypeService.GetDictionaryType(type)));
			return new SerializationDictionary(value.Cast<object>().Select(x => new KeyValuePair(accessor.Value(x), accessor.Key(x))).ToArray(), type);
		}

		private Type GetKeyValuePairType([NotNull] Type dictionaryType) {
			return _dictionaryToKeyValuePair.GetOrCreate(dictionaryType, () => GetInternKeyValuePairType(dictionaryType));
		}

		private Type GetInternKeyValuePairType([NotNull] Type dictionaryType) {
			if (dictionaryType == typeof (IDictionary)) return typeof (KeyValuePair<object, object>);
			Debug.Assert(dictionaryType.IsGenericType && !dictionaryType.IsGenericTypeDefinition && dictionaryType.GetGenericTypeDefinition() == typeof (IDictionary<,>));
			return typeof (KeyValuePair<,>).MakeGenericType(dictionaryType.GetGenericArguments());
		}

		private object MapDeserialized(SerializationDictionary value) {
			// check if that works 
			if (value == null || value.DictionaryType == null && value.Content == null) return null;

			var dictionaryType = value.DictionaryType;
			var dict = (IDictionary) GetConstructor(dictionaryType).Invoke(null);
			value.Content.ForEach(x => dict.Add(x.Key, x.Value));
			return dict;
		}

		private ConstructorInfo GetConstructor(Type dictionaryType) {
			return _constructorCache.GetOrCreate(dictionaryType, () => dictionaryType.GetConstructor(_constructorTypeArray));
		}
	}
}