using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Generics.Lookup {
	[PublicAPI]
	public interface IGenericLookup<TBaseType, in TKey> {
		TResult Get<TResult>(TKey key) where TResult : TBaseType;
		TBaseType Get(TKey key);
	}
}