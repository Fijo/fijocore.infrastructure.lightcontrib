using System.Collections.Generic;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Parallelizable;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Parallelizable.Interfaces;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Simple.Interface;

namespace FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Simple {
	[Note("This class was called ´ActionMethodeManager´ in the past.")]
	[Note("This class was called ´ActionHandlingProcessor´ in the past.")]
	[Desc("Just Executes all handlings.")]
	[About("AllHandlingProcessor")]
	public class SimpleHandlingProcessor<TSource> : ParallelizableHandlingProcessor<ISimpleHandling<TSource>>, ISimpleHandlingProcessor<TSource> {
		
		protected SimpleHandlingProcessor(IParallelizableHandlingProvider<ISimpleHandling<TSource>> handlingRepository) : base(handlingRepository) {}

		#region Process
		protected virtual void ProcessHandlings(IEnumerable<ISimpleHandling<TSource>> handlings, bool isParallelizable, TSource source) {
			handlings.ForEach(x => x.Process(source), isParallelizable);
		}

		public void Process(TSource source, bool enableParallelization = true) {
			ProcessHandlings(Handlings, IsParallelizable(enableParallelization), source);
		}

		public void Process(IEnumerable<ISimpleHandling<TSource>> customSimpleHandlings, TSource source, bool enableParallelization = true) {
			if (customSimpleHandlings == null) Process(source);
			else {
				customSimpleHandlings = CustomProcessHandlingsWrapper(customSimpleHandlings).Execute();
				ProcessHandlings(customSimpleHandlings.Concat(Handlings),
				                IsParallelizable(enableParallelization, customSimpleHandlings), source);
			}
		}
		#endregion

		protected virtual bool IsParallelizable(bool enableParallelization) {
			return SupportParallelization && enableParallelization;
		}

		protected virtual bool IsParallelizable(bool enableParallelization, IEnumerable<ISimpleHandling<TSource>> customSimpleHandlings) {
			return IsParallelizable(enableParallelization) && GetSupportsParallelization(customSimpleHandlings);
		}
	}
}