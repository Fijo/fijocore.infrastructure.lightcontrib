using System;
using SStream = System.IO.Stream;
using SEncoding = System.Text.Encoding;

namespace FijoCore.Infrastructure.LightContrib.Module.Stream.Obj.Reader {
	public interface IStringReader : IDisposable {
		SEncoding CurrentEncoding { get; }
		bool EndOfStream { get; }
		SStream BaseStream { get; }
		char Peek();
		bool TryPeek(out char result);
		char Read();
		bool TryRead(out char result);
		int ReadBlock(char[] buffer, int index, int count);
		string ReadLine();
		string ReadToEnd();
		void Close();
	}
}