﻿using FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Base;
using FijoCore.Infrastructure.LightContrib.Module.Number;
using FijoCore.Infrastructure.LightContrib.Module.Number.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Number.Service;
using FijoCore.Infrastructure.LightContrib.Module.Number.Service.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Number.Service.Module;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;

namespace FijoCore.Infrastructure.LightContrib.Properties {
	public class NumberInjectionModule : ExtendedNinjectModule {
		public override void AddModule(IKernel kernel) {
    		kernel.Load(new LightContribBaseInjectionModule());
		}

		public override void OnLoad(IKernel kernel) {
			kernel.Bind<INumberServiceProvider>().To<NumberServiceProvider>().InSingletonScope();
			kernel.Bind<INumberConcat>().To<NumberConcat>().InSingletonScope();
			kernel.Bind<INumberServiceHelper>().To<NumberServiceHelper>().InSingletonScope();
			kernel.Bind<INumberService<byte>>().To<ByteNumberService>().InSingletonScope();
			kernel.Bind<INumberService<char>>().To<CharNumberService>().InSingletonScope();
			kernel.Bind<INumberService<short>>().To<ShortNumberService>().InSingletonScope();
			kernel.Bind<INumberService<ushort>>().To<UShortNumberService>().InSingletonScope();
			kernel.Bind<INumberService<int>>().To<IntNumberService>().InSingletonScope();
			kernel.Bind<INumberService<uint>>().To<UIntNumberService>().InSingletonScope();
			kernel.Bind<INumberService<long>>().To<LongNumberService>().InSingletonScope();
			kernel.Bind<INumberService<ulong>>().To<ULongNumberService>().InSingletonScope();
			kernel.Bind<INumberService<decimal>>().To<DecimalNumberService>().InSingletonScope();
			kernel.Bind<INumberService<float>>().To<FloatNumberService>().InSingletonScope();
			kernel.Bind<INumberService<double>>().To<DoubleNumberService>().InSingletonScope();
		}
	}
}