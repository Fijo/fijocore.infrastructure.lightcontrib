using System;
using System.Threading;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Default.Service.CheckName;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Perf.Configuration {
	[UsedImplicitly]
	public class PerfConfigurationUpdater : IPerfConfigurationUpdater {
		private readonly Thread _perfBackgroundUpdater;
		private readonly int _canUpdateAfterMinRequests = 1;
		private readonly int _maxUsedRequests = 128;
		private readonly int waitForSingleLeftRequest = 1500;
		private readonly int minWaitBetweenUpdates = 300000;
		private readonly int waitLessWhenJustUpdated = 210000;

		public PerfConfigurationUpdater() {
			_perfBackgroundUpdater = CreateThread();
		}

		protected virtual Thread CreateThread() {
			return new Thread(BackgroundUpdater) {Name = string.Format("{0}.BackgroundThread", CN.Get<PerfConfigurationUpdater>()), IsBackground = true};
		}

		public void Start(PerfConfigurationUpdaterState state) {
			if(!_perfBackgroundUpdater.ThreadState.HasFlag(ThreadState.Unstarted)) throw new InvalidOperationException("You can only call Start once for an instance - this seems not to be the first time for this instance.");
			_perfBackgroundUpdater.Start(state);
		}

		#region BackgroundUpdater
		protected virtual void BackgroundUpdater(object obj) {
			var state = (PerfConfigurationUpdaterState) obj;
			var perfConfigurationService = Kernel.Resolve<IPerfConfigurationService>();
			InternalBackgroundUpdater(state, perfConfigurationService);
		}
		
// ReSharper disable FunctionNeverReturns
		protected virtual void InternalBackgroundUpdater(PerfConfigurationUpdaterState state, IPerfConfigurationService perfConfigurationService) {
			var perfConfiguration = state.PerfConfiguration;
			ResetRequestsAndSleep(state, true);
			while (true) {
				var updated = IsUpdate(state);
				if (updated) Update(perfConfigurationService, perfConfiguration);
				ResetRequestsAndSleep(state, updated);
			}
		}
// ReSharper restore FunctionNeverReturns

		protected bool IsUpdate(PerfConfigurationUpdaterState state) {
			return state.Requests >= _canUpdateAfterMinRequests;
		}

		protected void Update(IPerfConfigurationService perfConfigurationService, PerfConfiguration perfConfiguration) {
			lock(perfConfiguration)
				perfConfigurationService.Adjust(perfConfiguration);
		}

		protected void ResetRequestsAndSleep(PerfConfigurationUpdaterState state, bool updated) {
			var timeToSleep = GetTimeToSleep(state, updated);
			ResetRequests(state);
			Thread.Sleep(timeToSleep);
		}

		protected void ResetRequests(PerfConfigurationUpdaterState state) {
			state.Requests = 0;
		}

		protected virtual int GetTimeToSleep(PerfConfigurationUpdaterState state, bool updated) {
			var toSleep = Math.Max(_maxUsedRequests - state.Requests, 0) * waitForSingleLeftRequest;
			if (updated) toSleep = Math.Min(Math.Max(toSleep - waitLessWhenJustUpdated, 0), minWaitBetweenUpdates);
			return toSleep;
		}
		#endregion
	}
}