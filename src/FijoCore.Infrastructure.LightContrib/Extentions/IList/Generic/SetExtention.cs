using System;
using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IList.Generic {
	public static class SetExtention {
		// TSource : class - to be shure, that i have not to use default(TSource) and that this can be a value and so not undefined
		[PublicAPI, DebuggerStepThrough]
		public static void Set<TSource>([NotNull] this IList<TSource> me, int index, TSource value) where TSource : class {
			#region PreCondition
			if(index < 0) throw new ArgumentOutOfRangeException("index", index, "have to be positive");
			#endregion
			var count = me.Count;
			while(count++ < index) me.Add(null);
			if(count -1 == index) me.Add(value);
			else me[index] = value;
		}
	}
}