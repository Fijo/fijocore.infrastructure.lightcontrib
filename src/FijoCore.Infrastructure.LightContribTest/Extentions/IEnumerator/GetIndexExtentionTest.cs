using System.Collections.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator;
using NUnit.Framework;

namespace FijoCore.Infrastructure.LightContribTest.Extentions.IEnumerator {
	[TestFixture]//(typeof (GetIndexExtention))
	public class GetIndexExtentionTest {
		[Test]
		public void Test_IndexThExtentionWorksAsTheIndexerDo() {
			var collection = new List<string>
			{
				"test",
				"hallo",
				"neu",
				"blah"
			};
			Assert.AreEqual(collection[0], collection.GetEnumerator().GetIndex(0));
			Assert.AreEqual(collection[1], collection.GetEnumerator().GetIndex(1));
			Assert.AreEqual(collection[2], collection.GetEnumerator().GetIndex(2));
			Assert.AreEqual(collection[3], collection.GetEnumerator().GetIndex(3));
		}
	}
}