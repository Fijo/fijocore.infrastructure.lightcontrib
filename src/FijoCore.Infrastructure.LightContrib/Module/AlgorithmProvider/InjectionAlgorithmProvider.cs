using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Objects;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Provider;

namespace FijoCore.Infrastructure.LightContrib.Module.AlgorithmProvider {
	public abstract class InjectionAlgorithmProvider<TAlgorithm, TKey> : InjectionAlgorithmProviderBase<TAlgorithm, TKey>, IAlgorithmProvider<TAlgorithm, TKey> {
		#region Implementation of InjectionAlgorithmProviderBase<TAlgorithm,TKey>
		public Algorithms<TAlgorithm, TKey> Get() {
			return new Algorithms<TAlgorithm, TKey>(GetDict());
		}
		#endregion
	}
}