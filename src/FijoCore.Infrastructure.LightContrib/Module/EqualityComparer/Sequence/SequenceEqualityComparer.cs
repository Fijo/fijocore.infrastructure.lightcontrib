﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Other;
using FijoCore.Infrastructure.LightContrib.Module.Serialization;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Sequence
{
//[Serializable]
//public sealed class Singleton : ISerializable 
//{
//	// This is the one instance of this type.
//	private static readonly Singleton theOneObject = new Singleton();

//	// Here are the instance fields.
//	private string someString_value;
//	private Int32 someNumber_value;

//   public string SomeString
//   {
//	   get{return someString_value;}
//	   set{someString_value = value;}
//   }

//   public Int32 SomeNumber
//   {
//	   get{return someNumber_value;}
//	   set{someNumber_value = value;}
//   }

//	// Private constructor allowing this type to construct the Singleton.
//	private Singleton() 
//	{ 
//		// Do whatever is necessary to initialize the Singleton.
//		someString_value = "This is a string field";
//		someNumber_value = 123;
//	}

//	// A method returning a reference to the Singleton.
//	public static Singleton GetSingleton() 
//	{ 
//		return theOneObject; 
//	}

//	// A method called when serializing a Singleton.
//	[SecurityPermissionAttribute(SecurityAction.LinkDemand, 
//	Flags=SecurityPermissionFlag.SerializationFormatter)]
//	void ISerializable.GetObjectData(
//		SerializationInfo info, StreamingContext context) 
//	{
//		// Instead of serializing this object, 
//		// serialize a SingletonSerializationHelp instead.
//		info.SetType(typeof(SingletonSerializationHelper));
//		// No other values need to be added.
//	}

//	// Note: ISerializable's special constructor is not necessary 
//	// because it is never called.
//}


//[Serializable]
//internal sealed class SingletonSerializationHelper : IObjectReference 
//{
//	// This object has no fields (although it could).

//	// GetRealObject is called after this object is deserialized.
//	public object GetRealObject(StreamingContext context) 
//	{
//		// When deserialiing this object, return a reference to 
//		// the Singleton object instead.
//		return Singleton.GetSingleton();
//	}
//}

	[PublicAPI, Serializable]
	public class SequenceEqualityComparer<T> : IEqualityComparer<IEnumerable<T>>, ISerializable
	{
		private volatile static IEqualityComparer<IEnumerable<T>> _default;
		public static IEqualityComparer<IEnumerable<T>> Default { get { return _default ?? (_default = new SequenceEqualityComparer<T>()); } }
		
		protected SequenceEqualityComparer() {}

		#region Implementation of IEqualityComparer<in IEnumerable<T>>
		public bool Equals([NotNull] IEnumerable<T> x, [NotNull] IEnumerable<T> y)
		{
			return x.SequenceEqual(y);
		}

		public int GetHashCode([NotNull] IEnumerable<T> obj)
		{
			return obj.GetSequenceHashCode();
		}
		#endregion

		#region Implementation of ISerializable
		public void GetObjectData(SerializationInfo info, StreamingContext context) {
			info.SetType(typeof (SingletonSerializer));
		}

		[Serializable]
		protected class SingletonSerializer : IObjectReference {
			#region Implementation of IObjectReference
			public object GetRealObject(StreamingContext context) {
				return Default;
			}
			#endregion
		}
		#endregion
	}
}
