using System;
using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator {
	[PublicAPI]
	public static class FirstOrExtention {
		[Pure, PublicAPI, DebuggerStepThrough]
		public static TSource FirstOr<TSource>([NotNull] this IEnumerator<TSource> me, [NotNull] Func<TSource> defaultValueProvider) {
			return me.MoveNext()
			       	? me.Current
			       	: defaultValueProvider();
		}
	}
}