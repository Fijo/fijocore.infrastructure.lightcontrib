using System;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Enums;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Default.Exceptions {
	[Desc("Will be thrown when a TryFunc based fail oncurrent, while doing an try x until a time has passed action and the timeout has passed")]
	[PublicAPI, Serializable]
	public class RetryTimeOutException : TryleOutException {
		#region Overrides of TryleOutException
		public override TryleOutType Type { get { return TryleOutType.Timeout; } }
		#endregion

		public RetryTimeOutException() {}

		public RetryTimeOutException(string message, long tryLimit = default(long), Exception innerException = null) : base(message, tryLimit, innerException) {}
	}
}