﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace FijoCore.Infrastructure.LightContrib.Module.Lookup {
	public class ListLookup<TKey, TValue> : ListLookupBase<TKey, TValue> where TKey : IConvertible {
		#region Overrides of ListLookupBase<TKey,TValue>
		public ListLookup(IEnumerable<KeyValuePair<TKey, TValue>> collection) : base(collection) {}

		protected override IList<T> CreateList<T>(IEnumerable<T> collection) {
			return collection.ToList();
		}

		public override void Add(KeyValuePair<TKey, TValue> item) {
			AddItem(GetKeyIndex(item.Key), item);
		}

		public override void Clear() {
			lock (Content) {
				Content.Clear();
				KeyValuePais.Clear();
				Keys.Clear();
				Values.Clear();
			}
		}

		public override bool Remove(KeyValuePair<TKey, TValue> item) {
			lock (Content) {
				var index = GetKeyIndex(item.Key);
				if (index >= Count) return false;
				if (!Content[index].Equals(item.Value)) return false;
				RemoveItem(index);
				return true;
			}
		}

		public override bool IsReadOnly { get { return false; } }
		public override void Add(TKey key, TValue value) {
			AddItem(GetKeyIndex(key), new KeyValuePair<TKey, TValue>(key, value));
		}

		public override bool Remove(TKey key) {
			lock (Content) {
				var index = GetKeyIndex(key);
				if(index >= Count) return false;
				RemoveItem(index);
				return true;
			}
		}

		[Note("lock ´Content´ around the usage of this method")]
		private void RemoveItem(int index) {
			// ToDo impove this
			Debug.Assert(Content.Count == index + 1);
			Content.RemoveAt(index);
			KeyValuePais.RemoveAt(index);
			((IList<TKey>) Keys).RemoveAt(index);
			((IList<TValue>) Values).RemoveAt(index);
		}

		protected override void AddItem(int index, KeyValuePair<TKey, TValue> keyValuePair) {
			lock (Content) {
				// ToDo impove this
				Debug.Assert(Content.Count == index);
				var value = keyValuePair.Value;
				Content.Add(value);
				KeyValuePais.Add(keyValuePair);
				Keys.Add(keyValuePair.Key);
				Values.Add(value);
			}
		}
		#endregion
	}
}