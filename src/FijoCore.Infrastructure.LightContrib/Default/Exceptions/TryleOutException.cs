using System;
using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Enums;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Default.Exceptions {
	[Desc("Will be thrown while doing an try x times action that catches temporarily fails and retrys to do the wanted thing again and again and the retry limit is reached")]
	[PublicAPI, Serializable]
	public abstract class TryleOutException : Exception {
		public abstract TryleOutType Type { get; }
		public virtual long RetryLimit { get; set; }

		protected TryleOutException() {}

		protected TryleOutException(string message, long retryLimit = default(long), Exception innerException = null) : base(message, innerException) {
			Debug.Assert(retryLimit >= 0);
			RetryLimit = retryLimit;
		}
	}
}