using System.Collections.Generic;
using System.IO;
using System.Linq;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream.Impl.Specific {
	[PublicAPI]
	public class GenericCollectionStreamSerializer<T> : IStreamSerializer<ICollection<T>> {
		private readonly IStreamSerializer<T> _streamSerializer = Kernel.Resolve<IStreamSerializer<T>>();
		private readonly IStreamSerializer<long> _headerStreamSerializer = Kernel.Resolve<IStreamSerializer<long>>();
		private readonly IOut _out = Kernel.Resolve<IOut>();
		#region Implementation of IStreamSerializer<ICollection<T>>
		public void Serialize(StreamWriter streamWriter, ICollection<T> obj) {
			_headerStreamSerializer.Serialize(streamWriter, obj.LongCount());
			obj.ForEach(x => _streamSerializer.Serialize(streamWriter, x));
		}

		public bool TryDeserialize(StreamReader streamReader, out ICollection<T> result) {
			long length;
			if(!_headerStreamSerializer.TryDeserialize(streamReader, out length)) return _out.False(out result);
			var array = new T[length];
			for (long i = 0; i < length; i++)
				if(!_streamSerializer.TryDeserialize(streamReader, out array[i])) return _out.False(out result);
			return _out.True(out result, array);
		}
		#endregion
	}
}