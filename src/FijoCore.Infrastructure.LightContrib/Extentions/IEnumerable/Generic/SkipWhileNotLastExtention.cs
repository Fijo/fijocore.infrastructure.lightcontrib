using System;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	public static class SkipWhileNotLastExtention
	{
		[DebuggerStepThrough]
		public static System.Collections.Generic.IEnumerable<TResult> SkipWhileAndSelectLast<TSource, TResult>([NotNull] this System.Collections.Generic.IEnumerable<TSource> me, [NotNull] Func<TSource, bool> predicate, [NotNull] Func<TSource, TResult> selector) {
			var enumerator = me.GetEnumerator();
			var isFirst = true;
			TSource last = default(TSource);
			do {
				if (!enumerator.MoveNext()) {
					if(!isFirst) yield return selector(last);
					yield break;
				}
				if(isFirst) isFirst = false;
				last = enumerator.Current;
			}
			while(predicate(last));
			yield return selector(last);
			while (enumerator.MoveNext()) yield return selector(enumerator.Current);
		}
	}
}