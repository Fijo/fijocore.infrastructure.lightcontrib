﻿using System;
using System.Collections.Generic;

namespace FijoCore.Infrastructure.LightContrib.Factories.Interface {
	public interface ILookupFactory {
		IDictionary<TKey, TValue> Create<TKey, TValue>(IEnumerable<KeyValuePair<TKey, TValue>> collection);
		IDictionary<TKey, TValue> Create<TKey, TValue>(IEnumerable<KeyValuePair<TKey, TValue>> collection, IEqualityComparer<TKey> comparer);
		IDictionary<TKey, TValue> CreatePerf<TKey, TValue>(IEnumerable<KeyValuePair<TKey, TValue>> collection, bool isReadonly) where TKey : IConvertible;
	}
}