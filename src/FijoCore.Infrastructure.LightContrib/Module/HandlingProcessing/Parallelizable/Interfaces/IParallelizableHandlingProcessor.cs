using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Base.Interface;

namespace FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Parallelizable.Interfaces {
	public interface IParallelizableHandlingProcessor<in THandling> : IHandlingProcessor<THandling> where THandling : ISupportsParallelization {}
}