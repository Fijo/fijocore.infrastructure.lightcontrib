using FijoCore.Infrastructure.LightContrib.Module.AlgorithmProvider;
using FijoCore.Infrastructure.LightContrib.Module.Configuration.ConverterKey;
using FijoCore.Infrastructure.LightContrib.Module.Converter.Simple.Interface;

namespace FijoCore.Infrastructure.LightContrib.Module.Configuration {
	public class ConfigurationConverterProvider : FallbackInjectionAlgorithmProvider<IConverter, TypeChangeDto> {
		private readonly ITypeChangeFactory _typeChangeFactory;
		public ConfigurationConverterProvider(ITypeChangeFactory typeChangeFactory) {
			_typeChangeFactory = typeChangeFactory;
		}
		#region Overrides of InjectionAlgorithmProviderBase<IConverter,TypeChangeDto>
		protected override TypeChangeDto KeySelector(IConverter algorithm) {
			return _typeChangeFactory.Create(algorithm.SourceType, algorithm.TargetType);
		}
		#endregion
		#region Overrides of FallbackInjectionAlgorithmProvider<IConverter,TypeChangeDto>
		protected override bool IsFallback(IConverter algorithm) {
			return algorithm.SourceType == typeof (object) && algorithm.TargetType == typeof (object);
		}
		#endregion
	}
}