using System.IO;
using System.Linq;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Stream;
using FijoCore.Infrastructure.LightContribTest.Properties;
using NUnit.Framework;
using IO = System.IO;

namespace FijoCore.Infrastructure.LightContribTest.Module.Stream {
	[TestFixture]//(typeof(IStreamService))
	public class SeeklessStreamServiceTest {
		private IStreamService _streamService;

		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
			_streamService = Kernel.Resolve<ISeeklessStreamService>();
		}

		[Test]
		public void SeeklessStream_ReadToBytes_MustReturnCorrectBytes() {
			var stream = GetStream();
			var contentBytes = Enumerable.Range(0, 1024)
				.Select(x => (byte) x)
				.ToArray();

			stream.Write(contentBytes, 0, contentBytes.Length);
			stream.Position = 0;

			var actual = _streamService.ReadToBytes(stream);
			CollectionAssert.AreEqual(contentBytes, actual);
		}

		private IO.Stream GetStream() {
			return new SeeklessStream();
		}
	}

	public class SeeklessStream : MemoryStream {
		public override bool CanSeek {
			get { return false; }
		}
	}

}