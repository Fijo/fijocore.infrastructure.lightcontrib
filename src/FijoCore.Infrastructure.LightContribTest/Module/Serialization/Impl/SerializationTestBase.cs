﻿using System.IO;
using System.Text;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Enums;
using FijoCore.Infrastructure.LightContrib.Module.Serialization;
using FijoCore.Infrastructure.LightContrib.Module.Stream;
using FijoCore.Infrastructure.LightContribTest.Properties;
using NUnit.Framework;

namespace FijoCore.Infrastructure.LightContribTest.Module.Serialization.Impl {

	[TestFixture]
	public abstract class SerializationTestBase<TSerializer> where TSerializer : ISerialization {
		protected TSerializer Serializer;
		public IStreamService StreamService;

		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
			Serializer = Kernel.Resolve<TSerializer>();
			StreamService = Kernel.Resolve<IStreamService>();
		}

		protected virtual T SerializeDeserializeTest<T>(T input) {
			var stream = new MemoryStream();
			Serializer.Serialize(input, stream, SerializationFormatting.Default, Encoding.UTF8);
			stream.Seek(0, SeekOrigin.Begin);
			var readToString = new StreamService().ReadToString(stream, Encoding.UTF8);
			stream.Seek(0, SeekOrigin.Begin);
			return Serializer.Deserialize<T>(stream, Encoding.UTF8);
		}
	}
}