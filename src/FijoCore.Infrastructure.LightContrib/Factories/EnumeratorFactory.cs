using System.Collections.Generic;
using FijoCore.Infrastructure.LightContrib.Delegates;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Enumerator;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Factories {
	[UsedImplicitly]
	public class EnumeratorFactory : IEnumeratorFactory {
		#region Implementation of IEnumeratorFactory
		public IEnumerator<int> CreateEndless(int start) {
			return new EnumenatorEndless(start);
		}

		public IEnumerator<T> CreateExpression<T>(TryFunc<T> expression) {
			return new ExpressionEnumenator<T>(expression);
		}
		#endregion
	}
}