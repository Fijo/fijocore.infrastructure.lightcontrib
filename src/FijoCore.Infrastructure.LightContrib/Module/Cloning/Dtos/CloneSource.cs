﻿using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners;

namespace FijoCore.Infrastructure.LightContrib.Module.Cloning.Dtos {
	[Dto]
	public class CloneSource {
		public object Source { get; private set; }
		public object Result { get; private set; }
		public ICloning Cloning { get; private set; }

		public CloneSource(object source, object result, ICloning cloning) {
			Result = result;
			Cloning = cloning;
			Source = source;
		}
	}
}