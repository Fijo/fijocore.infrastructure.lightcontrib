using System;
using System.Collections.Generic;
using System.Linq;
using FijoCore.Infrastructure.LightContrib.Extentions.Type;
using Ninject;
using Ninject.Activation;
using Ninject.Components;
using Ninject.Infrastructure;
using Ninject.Modules;
using NUnit.Framework;
using Ninject.Planning.Bindings;
using Ninject.Planning.Bindings.Resolvers;

namespace FijoCore.Infrastructure.LightContribTest.NinjectTests {
	[TestFixture]
	public class NinjectProviderTest {
		class TestModule : NinjectModule {
			public override void Load() {
				Kernel.Components.Add<IBindingResolver, TestResolver>();

				Bind<ITestInterface>().ToProvider<TestProvider>().InSingletonScope();
			}
		}

		interface ITestInterface {
		}

		class TestImpl : ITestInterface {}

		interface ITestInterface<T> : ITestInterface {
			
		}

		class TestImpl<T> : ITestInterface<T> {}
		
		class TestResolver : NinjectComponent, IBindingResolver {
			#region Implementation of IBindingResolver
			public IEnumerable<IBinding> Resolve(Multimap<Type, IBinding> bindings, Type service) {
				if (typeof (ITestInterface).IsAssignableFrom(service) && typeof (ITestInterface) != service) return bindings[typeof (ITestInterface)];
				return Enumerable.Empty<IBinding>();
			}
			#endregion
		}

		class TestProvider : Provider<ITestInterface> {
			protected override ITestInterface CreateInstance(IContext context) {
				var service = context.Request.Service;
				if(service.GetGenericTypeDefinition() == typeof(ITestInterface<>))
					return (ITestInterface) (typeof(TestImpl<>).MakeGenericType(service.GetGenericArguments().ToArray())).New();
				return new TestImpl();
			}
		}

		private StandardKernel _kernel;

		[SetUp]
		public void SetUp() {
			_kernel = new StandardKernel(new TestModule());
		}

		[Test]
		public void Test() {
			var test = _kernel.Get<ITestInterface<string>>();
		}
	}
}