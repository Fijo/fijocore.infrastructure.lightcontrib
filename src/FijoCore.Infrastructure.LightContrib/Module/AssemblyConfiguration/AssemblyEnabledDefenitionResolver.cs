using System.Collections.Generic;
using System.Linq;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Default.Attributes;
using RAssembly = System.Reflection.Assembly;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.AssemblyConfiguration {
	[UsedImplicitly]
	public class AssemblyEnabledDefenitionResolver {
		private readonly AssemblyDynamicCustomConfigurationResolver _assemblyDynamicCustomConfigurationResolver = Kernel.Resolve<AssemblyDynamicCustomConfigurationResolver>();

		public IEnumerable<string> Get([NotNull] RAssembly context) {
			var defaultEnabledDefinitions = GetDefaultEnabledDefinitions(context.GetCustomAttributes(false));
			var customEnabledDefinitions = GetCusomEnabledDefinitions(context);

			return defaultEnabledDefinitions.Concat(customEnabledDefinitions);
		}

		private IEnumerable<string> GetCusomEnabledDefinitions([NotNull] RAssembly context) {
			var dynamicCustomConfiguration = _assemblyDynamicCustomConfigurationResolver.Get(context);
			IEnumerable<string> assemblyEnabledDefenitions;
			if(!dynamicCustomConfiguration.TryGetValue("AssemblyEnableDefenitionAttibute", out assemblyEnabledDefenitions)) return Enumerable.Empty<string>();
			return assemblyEnabledDefenitions;
		}

		private IEnumerable<string> GetDefaultEnabledDefinitions([NotNull] IEnumerable<object> customAttributes) {
			return customAttributes.OfType<AssemblyEnableDefenitionAttibute>().Select(x => x.DefinitionConstantName);
		}
	}
}