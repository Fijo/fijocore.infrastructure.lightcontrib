using System;
using FijoCore.Infrastructure.LightContrib.Module.Converter.Simple.Base;

namespace FijoCore.Infrastructure.LightContrib.Module.Converter.Simple {
	public abstract class EnumConverter<TResult> : ConverterBase<string, TResult> where TResult : struct, IConvertible {
		#region Overrides of ConverterBase<string,ProcessPriorityClass>
		public override TResult Convert(string source) {
			return (TResult) Enum.Parse(typeof (TResult), source);
		}
		#endregion
	}
}