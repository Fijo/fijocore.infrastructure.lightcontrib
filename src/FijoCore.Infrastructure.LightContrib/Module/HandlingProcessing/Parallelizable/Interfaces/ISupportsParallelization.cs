using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Parallelizable.Interfaces {
	[Note("This class was called ´IMayParallelizableMethode´ in the past.")]
	public interface ISupportsParallelization {
		bool SupportParallelization { get; }
	}
}