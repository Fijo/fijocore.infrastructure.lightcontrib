using System;
using System.Diagnostics;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.Annotation;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.Infrastructure.Documentation.Enums;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Type {
	[PublicAPI]
	public static class IsIgnoredForExtention {
		[Pure, PublicAPI, DebuggerStepThrough]
		[Desc("Checks if an type has an IgnoreAttribute, with spec. IgnoreFor enum value.")]
		public static bool IsIgnoredFor(this System.Type me, Func<IgnoreFor, bool> predicate) {
			var attributes = me.GetCustomAttributes(typeof (IgnoreAttribute), false);
			return attributes.Any(ignoreAttribute => predicate(((IgnoreAttribute) ignoreAttribute).IgnoreFor));
		}

		[Pure, PublicAPI, DebuggerStepThrough]
		[Desc("Checks if an type has an IgnoreAttribute, with a given ignoreFor value.")]
		public static bool IsIgnoredFor(this System.Type me, IgnoreFor @for) {
			return me.IsIgnoredFor(ignoreFor => ignoreFor == @for);
		}

		[Pure, PublicAPI, DebuggerStepThrough]
		[Desc("Checks if an type has an IgnoreAttribute, with an ignoreFor value, that is contained in a given list of values.")]
		public static bool IsIgnoredFor(this System.Type me, params IgnoreFor[] forValues) {
			return me.IsIgnoredFor(ignoreFor => ignoreFor.IsIn(forValues));
		}
	}
}