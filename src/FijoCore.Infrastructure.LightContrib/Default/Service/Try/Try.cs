﻿using System;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Default.Exceptions;
using FijoCore.Infrastructure.LightContrib.Delegates;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Default.Service.Try {
	[UsedImplicitly]
	public class Try : ITry {
		protected readonly ITryTimesService TryTimesService = Kernel.Resolve<ITryTimesService>();
		protected readonly IExlessTryTimesService ExlessTryTimesService = Kernel.Resolve<IExlessTryTimesService>();
		protected readonly IExlessTryForService ExlessTryForService = Kernel.Resolve<IExlessTryForService>();

		#region Implementation of ITryTimesService
		public TResult TryTimes<TResult, TCaughtException, TRetiesReachedException>(Func<TRetiesReachedException> exceptionFactory, Func<TResult> func, int maxTries) where TCaughtException : Exception where TRetiesReachedException : ExMaxRetriesReachedException {
			return TryTimesService.TryTimes<TResult, TCaughtException, TRetiesReachedException>(exceptionFactory, func, maxTries);
		}

		public TResult TryTimes<TResult, TRetiesReachedException>(Func<TRetiesReachedException> exceptionFactory, Func<TResult> func, int maxTries) where TRetiesReachedException : ExMaxRetriesReachedException {
			return TryTimesService.TryTimes(exceptionFactory, func, maxTries);
		}

		public TResult TryTimes<TResult, TCaughtException, TRetiesReachedException>(Func<TResult> func, int maxTries) where TCaughtException : Exception where TRetiesReachedException : ExMaxRetriesReachedException, new() {
			return TryTimesService.TryTimes<TResult, TCaughtException, TRetiesReachedException>(func, maxTries);
		}

		public TResult TryTimes<TResult, TRetiesReachedException>(Func<TResult> func, int maxTries) where TRetiesReachedException : ExMaxRetriesReachedException, new() {
			return TryTimesService.TryTimes<TResult, TRetiesReachedException>(func, maxTries);
		}
		#endregion

		#region Implementation of IExlessTryTimesService
		public TResult ExlessTryTimes<TResult, TRetiesReachedException>(Func<TRetiesReachedException> exceptionFactory, TryFunc<TResult> func, int maxTries) where TRetiesReachedException : MaxRetriesReachedException {
			return ExlessTryTimesService.ExlessTryTimes(exceptionFactory, func, maxTries);
		}

		public TResult ExlessTryTimes<TResult, TRetiesReachedException>(TryFunc<TResult> func, int maxTries) where TRetiesReachedException : MaxRetriesReachedException, new() {
			return ExlessTryTimesService.ExlessTryTimes<TResult, TRetiesReachedException>(func, maxTries);
		}

		public void ExlessTryTimes<TRetiesReachedException>(Func<TRetiesReachedException> exceptionFactory, TryFunc func, int maxTries) where TRetiesReachedException : MaxRetriesReachedException {
			ExlessTryTimesService.ExlessTryTimes(exceptionFactory, func, maxTries);
		}

		public void ExlessTryTimes<TRetiesReachedException>(TryFunc func, int maxTries) where TRetiesReachedException : MaxRetriesReachedException, new() {
			ExlessTryTimesService.ExlessTryTimes<TRetiesReachedException>(func, maxTries);
		}
		#endregion

		#region Implementation of IExlessTryForService
		public TResult ExlessTryFor<TResult, TRetryTimeOutException>(Func<TRetryTimeOutException> exceptionFactory, TryFunc<TResult> func, TimeSpan timeout) where TRetryTimeOutException : RetryTimeOutException {
			return ExlessTryForService.ExlessTryFor(exceptionFactory, func, timeout);
		}

		public TResult ExlessTryFor<TResult, TRetryTimeOutException>(TryFunc<TResult> func, TimeSpan timeout) where TRetryTimeOutException : RetryTimeOutException, new() {
			return ExlessTryForService.ExlessTryFor<TResult, TRetryTimeOutException>(func, timeout);
		}

		public void ExlessTryFor<TRetryTimeOutException>(Func<TRetryTimeOutException> exceptionFactory, TryFunc func, TimeSpan timeout) where TRetryTimeOutException : RetryTimeOutException {
			ExlessTryForService.ExlessTryFor(exceptionFactory, func, timeout);
		}

		public void ExlessTryFor<TRetryTimeOutException>(TryFunc func, TimeSpan timeout) where TRetryTimeOutException : RetryTimeOutException, new() {
			ExlessTryForService.ExlessTryFor<TRetryTimeOutException>(func, timeout);
		}
		#endregion
	}
}