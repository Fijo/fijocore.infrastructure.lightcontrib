using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Parallelizable.Interfaces;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Conditional.Interface {
	[AboutName("C", "Conditional")]
	[AboutName("H", "Handling")]
	[Service]
	public interface ICHProcessor<T> : IParallelizableHandlingProcessor<ICH<T>> {
		void Process(T source, [Desc("May execute tha Handlings parallel if possible")] bool enableParallelization = true);
		void Process([CanBeNull] IEnumerable<ICH<T>> customHandlings, T source, [Desc("May execute tha Handlings parallel if possible")] bool enableParallelization = true);
	}
}