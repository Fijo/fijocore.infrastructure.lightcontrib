using Fijo.Infrastructure.DesignPattern.Repository;
using Newtonsoft.Json;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization.Settings {
	public interface IJsonSettings : ISerializationSettings, IRepository<JsonSerializerSettings> {}
}