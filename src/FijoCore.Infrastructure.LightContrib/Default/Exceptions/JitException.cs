using System;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Enums;
using FijoCore.Infrastructure.LightContrib.Extentions.Type;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Default.Exceptions {
	[PublicAPI, Serializable]
	[Desc("A exception that is represents a type error that is caused for example by using reflection like (or reflection) features and getting an use that would cause an compile error when it would not be done jit. For example its such a situation would be when you try to get a the value of a member named �Count� and the type of the instance you want to get the member from does not contain a member named �Count�.")]
	public class JitException : InvalidOperationException {
		public JitFeature JitFeature { get; set; }
		public System.Type TargetType { get; set; }
		public string JitActionString { get { return string.IsNullOrEmpty(CustomJitAction) ? JitAction.ToString() : CustomJitAction; } }
		public JitAction JitAction { get; set; }
		public string CustomJitAction { get; set; }
		public object JitActionData { get; set; }

		public JitException(System.Type targetType, JitAction jitAction, object jitActionData, JitFeature jitFeature, string message = default(string), Exception innerException = null) : base(message, innerException) {
			TargetType = targetType;
			JitAction = jitAction;
			JitActionData = jitActionData;
			JitFeature = jitFeature;
		}
		
		public JitException(System.Type targetType, string customJitAction, object jitActionData, JitFeature jitFeature, string message = default(string), Exception innerException = null) : base(message, innerException) {
			TargetType = targetType;
			CustomJitAction = customJitAction;
			JitActionData = jitActionData;
			JitFeature = jitFeature;
		}
	}
}