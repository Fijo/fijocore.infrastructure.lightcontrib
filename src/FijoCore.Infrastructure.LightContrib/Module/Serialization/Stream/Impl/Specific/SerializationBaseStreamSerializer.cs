using System.Collections.Generic;
using System.IO;
using Fijo.Infrastructure.DesignPattern.Repository;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using FijoCore.Infrastructure.LightContrib.Enums;
using FijoCore.Infrastructure.LightContrib.Module.Stream;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream.Impl.Specific {
	public abstract class SerializationBaseStreamSerializer<T> : IStreamSerializer<T> {
		private readonly ISerializationService _serializationService = Kernel.Resolve<ISerializationService>();
		private readonly IStreamService _streamService = Kernel.Resolve<IStreamService>();
		private readonly IOut _out = Kernel.Resolve<IOut>();
		private readonly ISerialization _serialization;

		public SerializationBaseStreamSerializer() {
			var repository = Kernel.Resolve<IRepository<IDictionary<SerializationFormat, ISerialization>>>();
			_serialization = repository.Get()[SerializationFormat];
		}
		
		#region Implementation of IStreamSerializer<T>
		public virtual void Serialize(StreamWriter streamWriter, T obj) {
			_serializationService.Serialize(_serialization, obj, _streamService.GetStream(streamWriter));
		}

		public virtual bool TryDeserialize(StreamReader streamReader, out T result) {
			return _out.True(out result, _serializationService.Deserialize<T>(_serialization, _streamService.GetStream(streamReader)));
		}
		#endregion

		protected abstract SerializationFormat SerializationFormat { get; }
	}
}