using System.Collections.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IList.Generic;
using NUnit.Framework;

namespace FijoCore.Infrastructure.LightContribTest.Extentions.IList.Generic {
	[TestFixture]//(typeof (RemoveRangeExtention))
	public class RemoveRangeExtentionTest {
		[Test]
		public void Test() {
			var result = (IList<string>) new List<string>
			{
				"hallo",
				"test",
				"dumm",
				"lol"
			};

			var wanted = (IList<string>) new List<string>
			{
				"hallo",
				"lol"
			};

			result.RemoveRange(1, 2);

			CollectionAssert.AreEqual(wanted, result);
		}
	}
}