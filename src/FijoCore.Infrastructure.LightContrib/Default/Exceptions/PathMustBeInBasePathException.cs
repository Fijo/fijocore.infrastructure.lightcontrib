using System;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Default.Exceptions {
	[PublicAPI, Serializable]
	public class PathMustBeInBasePathException : Exception {
		[PublicAPI] public readonly string Path;
		[PublicAPI] public readonly string BasePath;

		public PathMustBeInBasePathException(string path, string basePath, string message = "", Exception innerException = null) : base(message, innerException) {
			Path = path;
			BasePath = basePath;
		}
	}
}