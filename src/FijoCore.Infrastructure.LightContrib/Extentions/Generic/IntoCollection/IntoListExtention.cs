using System.Collections.Generic;
using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Generic.IntoCollection {
	[PublicAPI]
	public static class IntoListExtention {
		[Pure, DebuggerStepThrough, PublicAPI]
		[Desc("Creates a new List<T> with this a only entry")]
		public static List<T> IntoList<T>(this T me) {
			return new List<T> {me};
		}
	}
}