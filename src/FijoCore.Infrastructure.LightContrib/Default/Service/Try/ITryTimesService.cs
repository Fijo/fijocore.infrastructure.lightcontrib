using System;
using FijoCore.Infrastructure.LightContrib.Default.Exceptions;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Default.Service.Try {
	public interface ITryTimesService {
		TResult TryTimes<TResult, TCaughtException, TRetiesReachedException>([NotNull] Func<TRetiesReachedException> exceptionFactory, [NotNull] Func<TResult> func, int maxTries)
			where TRetiesReachedException : ExMaxRetriesReachedException where TCaughtException : Exception;

		TResult TryTimes<TResult, TRetiesReachedException>([NotNull] Func<TRetiesReachedException> exceptionFactory, [NotNull] Func<TResult> func, int maxTries)
			where TRetiesReachedException : ExMaxRetriesReachedException;

		TResult TryTimes<TResult, TCaughtException, TRetiesReachedException>([NotNull] Func<TResult> func, int maxTries)
			where TRetiesReachedException : ExMaxRetriesReachedException, new() where TCaughtException : Exception;

		TResult TryTimes<TResult, TRetiesReachedException>([NotNull] Func<TResult> func, int maxTries)
			where TRetiesReachedException : ExMaxRetriesReachedException, new();
	}
}