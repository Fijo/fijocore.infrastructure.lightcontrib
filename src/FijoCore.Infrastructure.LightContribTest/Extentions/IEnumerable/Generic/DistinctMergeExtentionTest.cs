﻿using System.Collections.Generic;
using System.Linq;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Func;
using FijoCore.Infrastructure.LightContribTest.Properties;
using NUnit.Framework;

namespace FijoCore.Infrastructure.LightContribTest.Extentions.IEnumerable.Generic {
	[TestFixture]
	public class DistinctMergeExtentionTest {
		[Test]
		public void NonUniqueCollection_DistinctMerge_MustBeUnique() {
			var list = GetTestData();
			var result = list.DistinctMerge(string.Concat);
			CollectionAssert.AllItemsAreUnique(result);
		}

		[Test]
		public void NonUniqueCollectionAndCustomEqualityComparer_DistinctMerge_ExpectedCollection() {
			var list = GetTestData();
			var result = list.DistinctMerge((x, y) => x.Substring(0, 2) + y.Substring(2), GetLengthEqualityComparer()).ToList();
			CollectionAssert.AreEqual(new List<string>
			{
				"tefl",
				"lol"
			}, result);
		}

		private FuncEqualityComparer<string> GetLengthEqualityComparer() {
			return new FuncEqualityComparer<string>((x, y) => x.Length == y.Length, x => x.Length.GetHashCode());
		}

		private IEnumerable<string> GetTestData() {
			return new List<string>
			{
				"test",
				"test",
				"lol",
				"rofl"
			};
		}
	}
}