using System;
using System.Diagnostics;
using FijoCore.Infrastructure.LightContrib.Module.Number.Service.Interface;

namespace FijoCore.Infrastructure.LightContrib.Module.Number.Service.Base {
	public abstract class NumberServiceBase<TNumber> : INumberService<TNumber> where TNumber : struct, IComparable, IConvertible {
		private readonly bool _isUnsigned;
		#region Implementation of INumberService<TNumber>
		protected NumberServiceBase() {
// ReSharper disable DoNotCallOverridableMethodsInConstructor
			Debug.Assert(MaxValue.CompareTo(MinValue) > 0, "MaxValue.CompareTo(MinValue) > 0");
			Debug.Assert(MaxValue.CompareTo(ZeroValue) > 0, "MaxValue.CompareTo(ZeroValue)");
			Debug.Assert(MinValue.CompareTo(ZeroValue) <= 0, "MinValue.CompareTo(ZeroValue) <= 0");
			_isUnsigned = MinValue.CompareTo(ZeroValue) >= 0;
// ReSharper restore DoNotCallOverridableMethodsInConstructor
		}

		public abstract TNumber MaxValue { get; }
		public abstract TNumber MinValue { get; }
		public bool IsUnsigned { get { return _isUnsigned; } }
		public virtual TNumber ZeroValue { get { return default(TNumber); } }
		public abstract bool IsInteger { get; }
		public bool HasZeroValue { get { return true; } }
		public virtual TNumber Convert(object value) {
			return (TNumber) System.Convert.ChangeType(value, typeof (TNumber));
		}

		public abstract TNumber Sun(TNumber left, TNumber right);
		public abstract TNumber Diff(TNumber left, TNumber right);
		public abstract TNumber Times(TNumber left, TNumber right);
		public abstract TNumber Divide(TNumber left, TNumber right);
		public abstract TNumber Modulo(TNumber left, TNumber right);
		#endregion
	}
}