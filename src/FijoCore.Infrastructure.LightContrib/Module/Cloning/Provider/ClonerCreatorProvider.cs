﻿using System;
using FijoCore.Infrastructure.LightContrib.Module.AlgorithmProvider;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners;

namespace FijoCore.Infrastructure.LightContrib.Module.Cloning.Provider {
	public class ClonerCreatorProvider : InjectionAlgorithmProvider<IClonerCreator, Type> {
		#region Overrides of InjectionAlgorithmProviderBase<IClonerCreator,Type>
		protected override Type KeySelector(IClonerCreator algorithm) {
			return algorithm.For;
		}
		#endregion
	}
}