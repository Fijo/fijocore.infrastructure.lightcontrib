using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Generic {
	[PublicAPI]
	public static class IsUndefinedExtention {
		[Pure, DebuggerStepThrough, PublicAPI]
		[Desc("Compares T with null being shure that it do not compare a value type with null.")]
		public static bool IsUndefined<T>(this T me) {
			return !typeof(T).IsValueType && Equals(null, me);
		}
	}
}