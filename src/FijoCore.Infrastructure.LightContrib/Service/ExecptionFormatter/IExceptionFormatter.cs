using System;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Service.ExecptionFormatter {
	public interface IExceptionFormatter {
		[NotNull, Pure]
		string Format([NotNull] Exception exception);
	}
}