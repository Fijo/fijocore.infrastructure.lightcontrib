using System;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Typing.DictionaryType {
	public interface IDictionaryTypeService {
		DictionaryTypeDto GetDictionaryTypeDto([NotNull] Type me);
		bool IsDictionary([NotNull] Type me);
		Type GetDictionaryType([NotNull] Type me);
	}
}