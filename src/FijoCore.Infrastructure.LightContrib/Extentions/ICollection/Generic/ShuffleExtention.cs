using System;
using System.Collections.Generic;
using System.Diagnostics;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.ICollection.Generic {
	[PublicAPI]
	public static class ShuffleExtention {
		[NotNull] private static readonly Random Random = new Random();

		[NotNull, Pure, PublicAPI, DebuggerStepThrough]
		public static T ShuffleSingle<T>([NotNull] this ICollection<T> me) {
			lock(me) {
				var count = me.Count;
				if (count == 0) throw new InvalidOperationException("The source sequence is empty.");
				return me.GetIndex(Random.Next(count));
			}
		}
	}
}