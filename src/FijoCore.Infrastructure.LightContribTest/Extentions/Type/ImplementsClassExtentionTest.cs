using FijoCore.Infrastructure.LightContrib.Extentions.Type;
using NUnit.Framework;
using SType = System.Type;

namespace FijoCore.Infrastructure.LightContribTest.Extentions.Type {
	[TestFixture]//(typeof (ImplementsClassExtention))
	public class ImplementsClassExtentionTest {
		#region TestClasses
		private class BaseClass {}

		private class Testclass1 : Testclass2 {}

		private class Testclass2 : BaseClass {}

		private class Testclass3 : BaseClass {
		}

		private class Testclass4 {
		}
		#endregion

		[TestCase(false, typeof (Testclass1), typeof (Testclass1))]
		[TestCase(true, typeof (Testclass2), typeof (Testclass1))]
		[TestCase(false, typeof (Testclass3), typeof (Testclass1))]
		[TestCase(false, typeof (Testclass4), typeof (Testclass1))]
		[TestCase(true, typeof (BaseClass), typeof (Testclass1))]
		[TestCase(true, typeof (BaseClass), typeof (Testclass2))]
		[TestCase(true, typeof (BaseClass), typeof (Testclass3))]
		[TestCase(false, typeof (BaseClass), typeof (Testclass4))]
		public void BaseTypeAndType_ImplementsClass_MustReturnValue(bool expected, SType baseType, SType type) {
			var actual = type.ImplementsClass(baseType);
			Assert.AreEqual(expected, actual);
		}
	}
}