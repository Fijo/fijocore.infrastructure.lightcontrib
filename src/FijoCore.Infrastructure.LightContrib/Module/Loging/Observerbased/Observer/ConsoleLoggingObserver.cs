using System;
using Fijo.Infrastructure.DesignPattern.Observer.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Loging.Observerbased.Interface;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Loging.Observerbased.Observer {
	[PublicAPI]
	public class ConsoleLoggingObserver : ILoggingObserver {
		#region Implementation of IObserver
		public void Observe(IObserverable obj) {
			Console.WriteLine(obj.ToString());
		}
		#endregion
	}
}