using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator {
	[PublicAPI]
	public static class FirstExtention {
		[Pure, PublicAPI, DebuggerStepThrough]
		public static TSource First<TSource>([NotNull] this IEnumerator<TSource> me) {
			return me.IndexTh(1);
		}
	}
}