using System;
using FijoCore.Infrastructure.LightContrib.Default.Exceptions;
using FijoCore.Infrastructure.LightContrib.Delegates;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Default.Service.Try {
	[UsedImplicitly]
	public class ExlessTryTimesService : ExlessTryServiceBase<int, int, MaxRetriesReachedException>, IExlessTryTimesService {
		#region Overrides of ExlessTryServiceBase
		protected override void InternalPreCondition(int maxTries) {
			PreCondition(maxTries);
		}

		protected override bool HasNextTry<TResult, TRetiesReachedException>(int maxTries, ref int @try) {
			return ++@try == maxTries;
		}

		protected override int GetStartTry() {
			return 0;
		}

		protected override TRetiesReachedException CreateException<TRetiesReachedException>(int maxTries) {
			return new TRetiesReachedException {RetryLimit = maxTries};
		}
		#endregion
		#region Implementation of IExlessTryTimesService
		public virtual TResult ExlessTryTimes<TResult, TRetiesReachedException>(Func<TRetiesReachedException> exceptionFactory, TryFunc<TResult> func, int maxTries) where TRetiesReachedException : MaxRetriesReachedException {
			return InternalExlessTry(exceptionFactory, func, maxTries);
		}

		public TResult ExlessTryTimes<TResult, TRetiesReachedException>(TryFunc<TResult> func, int maxTries) where TRetiesReachedException : MaxRetriesReachedException, new() {
			return InternalExlessTry<TResult, TRetiesReachedException>(func, maxTries);
		}

		public void ExlessTryTimes<TRetiesReachedException>(Func<TRetiesReachedException> exceptionFactory, TryFunc func, int maxTries) where TRetiesReachedException : MaxRetriesReachedException {
			InternalExlessTry(exceptionFactory, func, maxTries);
		}

		public void ExlessTryTimes<TRetiesReachedException>(TryFunc func, int maxTries) where TRetiesReachedException : MaxRetriesReachedException, new() {
			InternalExlessTry<TRetiesReachedException>(func, maxTries);
		}
		#endregion
	}
}