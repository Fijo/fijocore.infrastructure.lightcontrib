﻿using System;
using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Boolean {
	[PublicAPI]
	public static class ToIntegerExtention {
		[Pure, DebuggerStepThrough, PublicAPI]
		[Desc("Converts a bool to int")]
		public static int ToInteger(this bool me) {
			return Convert.ToInt32(me);
		}
	}
}