﻿using FijoCore.Infrastructure.LightContrib.Module.AlgorithmProvider;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners;

namespace FijoCore.Infrastructure.LightContrib.Module.Cloning.Provider {
	public class CloningProvider : InjectionAlgorithmProvider<ICloning, CloningType> {
		#region Overrides of InjectionAlgorithmProviderBase<ICloning,CloningType>
		protected override CloningType KeySelector(ICloning algorithm) {
			return algorithm.Type;
		}
		#endregion
	}
}