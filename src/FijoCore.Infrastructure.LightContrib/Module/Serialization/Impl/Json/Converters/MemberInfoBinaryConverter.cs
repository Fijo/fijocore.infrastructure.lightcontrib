using System;
using System.Reflection;
using Fijo.Infrastructure.DesignPattern.Lazy.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Stream;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization.Impl.Json.Converters {
	[PublicAPI]
	public class MemberInfoBinaryConverter : BinaryConverter {
		public MemberInfoBinaryConverter(ILazy<ISerializationProvider> serializationProvider, IStreamService streamService, ILazy<ISerializationService> serializationService, ILazy<System.Text.Encoding> encoding) : base(serializationProvider, streamService, serializationService, encoding) {}
		#region Overrides of JsonConverter
		public override bool CanConvert(Type objectType) {
			return typeof (MemberInfo).IsAssignableFrom(objectType);
		}
		#endregion
	}
}