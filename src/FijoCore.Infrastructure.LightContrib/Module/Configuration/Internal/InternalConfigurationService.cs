using System.Collections.Generic;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Controller;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Default.Exceptions;
using FijoCore.Infrastructure.LightContrib.Module.Configuration.ConverterKey;
using FijoCore.Infrastructure.LightContrib.Module.Converter.Simple.Interface;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Configuration.Internal {
	[PublicAPI, Service]
	public class InternalConfigurationService : IInternalConfigurationService {
		[PublicAPI] protected readonly IFinder<IConverter, TypeChangeDto> ConverterFinder = Kernel.Resolve<IFinder<IConverter, TypeChangeDto>>();
		[PublicAPI] protected readonly ITypeChangeFactory TypeChangeFactory = Kernel.Resolve<ITypeChangeFactory>();
		[PublicAPI] protected readonly IDictionary<string, object> Content;

		public InternalConfigurationService() {
			Content = Kernel.Resolve<IRepository<IDictionary<string, object>>>().Get();
		}

		public virtual T Get<T>(string key) {
			object value;
			if(!Content.TryGetValue(key, out value)) throw new ConfigurationKeyNotFoundException(key, typeof(T));
			return GetConverted<T>(key, value);
		}

		private T GetConverted<T>(string key, object value) {
			var converter = ConverterFinder.Get(CreateTypeChangeDto<T>(value));
			var converted = converter.Convert(value);
			if (!(converted is T)) throw new ConfigurationInvalidValueTypeException(key, value.GetType(), typeof (T), value.ToString());
			return (T) converted;
		}

		private TypeChangeDto CreateTypeChangeDto<T>(object value) {
			return TypeChangeFactory.Create(value.GetType(), typeof(T));
		}
	}
}