using System.Diagnostics;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;
using SType = System.Type;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Type {
	[PublicAPI]
	public static class ImplementsIntefaceExtention {
		[Pure, PublicAPI, DebuggerStepThrough]
		[Desc("returns if this implements the given interface")]
		public static bool ImplementsInteface([NotNull] this SType me, [NotNull] SType @interface) {
			Debug.Assert(@interface.IsInterface, "@interface.IsInterface");
			return me.GetInterfaces().Contains(@interface);
		}

		[Pure, PublicAPI, DebuggerStepThrough]
		[Desc("returns if this implements the given interface")]
		public static bool ImplementsInteface<TInterface>([NotNull] this SType me) {
			return ImplementsInteface(me, typeof (TInterface));
		}
	}
}