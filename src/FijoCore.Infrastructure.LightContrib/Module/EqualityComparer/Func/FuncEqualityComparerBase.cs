using System;
using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Func {
	[Service]
	public abstract class FuncEqualityComparerBase<T, TEntry> : IEqualityComparer<T> {
		[NotNull] protected readonly Func<TEntry, TEntry, bool> EqualsFunc;
		[NotNull] protected readonly Func<TEntry, int> GetHashCodeFunc;

		protected FuncEqualityComparerBase([NotNull] Func<TEntry, TEntry, bool> equals, [CanBeNull] Func<TEntry, int> getHashCode = null) {
			if(getHashCode == null) getHashCode = GetDefaultGetHashCodeFunc();
			EqualsFunc = equals;
			GetHashCodeFunc = getHashCode;
		}

		[NotNull, Pure]
		private Func<TEntry, int> GetDefaultGetHashCodeFunc() {
			return x => 0;
		}

		#region Implementation of IEqualityComparer<in T>
		public abstract bool Equals(T x, T y);
		public abstract int GetHashCode(T obj);
		#endregion
	}
}