using FijoCore.Infrastructure.LightContrib.Extentions.Type;
using NUnit.Framework;

namespace FijoCore.Infrastructure.LightContribTest.Extentions.Type {
	[TestFixture]//(typeof (HasEmptyConstructorExtention))
	public class HasEmptyConstructorTest {
		#region TestClasses
		private class Testclass1 {}

		private class Testclass2 {}

		private class Testclass3 {
			public Testclass3(string testArg) {}
		}

		private class Testclass4 {
			public Testclass4() {}
			public Testclass4(string testArg) {}
		}
		#endregion

		[TestCase(typeof (Testclass1), true)]
		[TestCase(typeof (Testclass2), true)]
		[TestCase(typeof (Testclass3), false)]
		[TestCase(typeof (Testclass4), true)]
		public void Type_HasEmptyConstructor_MustReturnValue(System.Type type, bool value) {
			var actual = type.HasEmptyConstructor();
			Assert.AreEqual(value, actual);
		}
	}
}