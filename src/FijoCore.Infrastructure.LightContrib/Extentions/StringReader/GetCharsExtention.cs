﻿using System.Collections.Generic;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Stream.Obj.Reader;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.StringReader {
	[PublicAPI]
	public static class GetCharsExtention {
		[NotNull] private static IEnumerableFactory _enumerableFactory;

		internal static void Init() {
			_enumerableFactory = Kernel.Resolve<IEnumerableFactory>();
		}
		
		[NotNull, Pure, PublicAPI]
		public static IEnumerable<char> GetChars([NotNull] this IStringReader me) {
			return _enumerableFactory.CreateExpression((out char result) => me.TryRead(out result));
		}
	}
}