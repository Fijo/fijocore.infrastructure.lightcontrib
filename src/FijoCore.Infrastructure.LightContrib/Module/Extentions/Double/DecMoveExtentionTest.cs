using FijoCore.Components.Extentions.Double;
using NUnit.Framework;

namespace FijoCore.ComponentTest.Extentions.Double {
	[TestFixture]
	public class DecMoveExtentionTest
	{
		[TestCase(5345.2, 2, 53.452)]
		[TestCase(10.0, 1, 1.000)]
		[TestCase(9.0, -1, 90.0)]
		[TestCase(1.0, 5, 0.00001)]
		[TestCase(0.0, 2, 0.0)]
		[TestCase(0.9, 1, 0.09)]
		[TestCase(4235.223443, -4, 42352234.43)]
		[TestCase(0.5345, -1, 5.345)]
		[TestCase(0.0045243, -3, 4.5243)]
		public void Test(double value, int movement, double wanted) {
			Assert.AreEqual(wanted, value.DecMove(movement));
		}
	}
}