using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using FijoCore.Infrastructure.LightContrib.Extentions.ICollection.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.ISet.Generic;
using FijoCore.Infrastructure.LightContrib.Module.Pool.Dto;
using FijoCore.Infrastructure.LightContrib.Module.Pool.Interface;

namespace FijoCore.Infrastructure.LightContrib.Module.Pool {
	public abstract class BlockablePool<T, TEntry> : Pool<T, TEntry>, IBlockablePool<T, TEntry> where TEntry : PoolEntry<T> {
		protected readonly ISet<TEntry> Active;

		protected BlockablePool(IEnumerable<TEntry> content, int shouldActives, int maintenanceIntervall) : base(content, shouldActives, maintenanceIntervall) {
			Active = new HashSet<TEntry>();
		}

		public override T Get() {
			CheckDisposed();
			PrepareGet();
			return InternalGet().Content;
		}

		protected virtual void PrepareGet() {
			// ToDo may although start an activate task if Availible.Count is realy low get performance issues
			if (Availible.None())
				lock (Availible) lock (Content) ActivateObjects(4);
		}

		protected override TEntry InternalGet() {
			return InternalGet(base.InternalGet);
		}

		protected TEntry InternalGet(Func<TEntry> get) {
			TEntry entry;
			lock (Availible) {
				entry = get();
				Availible.Remove(entry);
			}
			Active.AddSecure(entry);
			#if DEBUG
			entry.IsInUse = true;
			#endif
			return entry;
		}

		public virtual void Get(Action<T> action) {
			CheckDisposed();
			PrepareGet();
			var entry = InternalGet();
			action(entry.Content);
			lock(Active) InternalFinish(entry);
		}

		public virtual void Finish(T obj) {
			lock(Active) {
				var toRelease = Active.Single(x => x.Content.Equals(obj));
				InternalFinish(toRelease);
			}
		}

		protected virtual void InternalFinish(TEntry toRelease) {
			#if DEBUG
			toRelease.IsInUse = false;
			#endif
			Availible.AddSecure(toRelease);
			Active.RemoveSecure(toRelease);
		}

		public override void Dispose() {
			base.Dispose();
			while(Active.Any()) Thread.Sleep(10);
			DeactivateObjects();
		}
	}
}