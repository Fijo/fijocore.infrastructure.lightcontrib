﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic.Equality.Equals;
using FijoCore.Infrastructure.LightContrib.Module.EqualityCompareing.Interface;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.EqualityCompareing
{
	[PublicAPI, Service]
	[Desc("A class that helps you to create a Service to compare equality using a list of property selectors.")]
	public class EqualityComparingService<T> : IEqualityComparingService<T> {
		[PublicAPI] protected readonly bool AllowNull;
		[PublicAPI] protected readonly ICollection<Func<T, object>> CompareProperties;

		public EqualityComparingService(bool allowNull, [NotNull] params Func<T, object>[] compareProperties) {
			AllowNull = allowNull;
			CompareProperties = compareProperties;
		}

		#region Equals
		[Pure]
		public bool Equals(T me, T obj) {
			return CheckNull(me, obj) && CompareProperties.All(func => EqualsExtention.Equals(me, obj, func));
		}
		
		[Pure]
		private bool CheckNull(T me, T obj) {
			if (AllowNull) return !me.IsUndefined() || obj.IsUndefined();
			if(me.IsUndefined() || obj.IsUndefined()) throw new NoNullAllowedException();
			return true;
		}
		
		[Pure]
		public new bool Equals(object me, object obj) {
			return me is T && Equals((T) me, obj);
		}
		
		[Pure]
		public bool Equals(T me, object obj) {
			return obj is T && Equals(me, (T) obj);
		}
		#endregion
		
		#region GetHashCode
		[Pure]
		public int GetHashCode(T obj) {
			if(CheckNull(obj)) return 0;
			return CompareProperties.Select(func => func(obj)).Aggregate(0, HashCodeAggregateFunc);
		}

		[Pure]
		private int HashCodeAggregateFunc(int current, object property) {
			return (current * 397) ^ (property != null ? property.GetHashCode() : 0);
		}

		[Pure]
		private bool CheckNull(T me) {
			if (AllowNull) return me.IsUndefined();
			if(me.IsUndefined()) throw new NoNullAllowedException();
			return false;
		}
		#endregion
	}
}
