using System.Collections;
using System.Collections.Generic;
using FijoCore.Infrastructure.LightContrib.Delegates;

namespace FijoCore.Infrastructure.LightContrib.Module.SelectedAggregation.Interface {
	[Fijo.Infrastructure.Documentation.Attributes.DesignPattern.Service]
	public interface ISelectedAggregation<TKey, TFuncKey, TAccumulate, in TSource, out TResult, TParam> {
		ISelectedAggregation<TKey, TFuncKey, TAccumulate, TSource, TResult, TParam> Add(TFuncKey key, AggregateFunc<TAccumulate, TParam, TKey> func);
		ISelectedAggregation<TKey, TFuncKey, TAccumulate, TSource, TResult, TParam> SetOrder(IEqualityComparer equalityComparer = null, params TFuncKey[] wantedOrder);
		TResult Process(TKey key, TSource source);
		TResult Process(IEnumerable<KeyValuePair<TFuncKey, AggregateFunc<TAccumulate, TParam, TKey>>> customFuncs, TKey key, TSource source);
	}

	public interface ISelectedAggregation<TKey, TFuncKey, TAccumulate, in TSource, out TResult> : ISelectedAggregation<TKey, TFuncKey, TAccumulate, TSource, TResult, object> {}

	public interface ISelectedAggregation<TKey, TFuncKey, TAccumulate, TParam> : ISelectedAggregation<TKey, TFuncKey, TAccumulate, TAccumulate, TAccumulate, TParam> {}

	public interface ISelectedAggregation<TKey, TFuncKey, TAccumulate> : ISelectedAggregation<TKey, TFuncKey, TAccumulate, object>, ISelectedAggregation<TKey, TFuncKey, TAccumulate, TAccumulate, TAccumulate> {}
}