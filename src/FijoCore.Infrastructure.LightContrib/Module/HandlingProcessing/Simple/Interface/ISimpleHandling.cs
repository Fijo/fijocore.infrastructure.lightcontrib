using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Parallelizable.Interfaces;

namespace FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Simple.Interface {
	[Note("This class was called ŽIActionMethodeŽ in the past.")]
	public interface ISimpleHandling<in TSource> : ISupportsParallelization {
		void Process(TSource source);
	}
}