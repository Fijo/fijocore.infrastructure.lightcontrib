using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Base.Interface {
	[Note("This class was called ´IMethodeManagerBase´ in the past.")]
	public interface IHandlingProcessor<in THandling> {}
}