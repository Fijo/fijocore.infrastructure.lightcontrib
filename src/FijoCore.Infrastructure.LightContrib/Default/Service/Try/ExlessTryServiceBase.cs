using System;
using FijoCore.Infrastructure.LightContrib.Default.Exceptions;
using FijoCore.Infrastructure.LightContrib.Delegates;

namespace FijoCore.Infrastructure.LightContrib.Default.Service.Try {
	public abstract class ExlessTryServiceBase<TTries, TMaxTries, TTryleOutEx> : TryServiceBase where TTries : struct, IConvertible  where TMaxTries : struct, IConvertible where TTryleOutEx : TryleOutException {
		protected virtual TResult InternalExlessTry<TResult, TRetiesReachedException>(Func<TRetiesReachedException> exceptionFactory, TryFunc<TResult> func, TMaxTries maxTries)
			where TRetiesReachedException : TTryleOutEx {
			#region PreCondition
			InternalPreCondition(maxTries);
			#endregion
			var @try = GetStartTry();
			TResult result;
			while(!func(out result)) {
				if(HasNextTry<TResult, TRetiesReachedException>(maxTries, ref @try)) throw exceptionFactory();
			}
			return result;
		}

		protected abstract void InternalPreCondition(TMaxTries maxTries);

		protected abstract bool HasNextTry<TResult, TRetiesReachedException>(TMaxTries maxTries, ref TTries @try) where TRetiesReachedException : TTryleOutEx;

		protected abstract TTries GetStartTry();

		protected virtual void InternalExlessTry<TRetiesReachedException>(Func<TRetiesReachedException> exceptionFactory, TryFunc func, TMaxTries maxTries)
			where TRetiesReachedException : TTryleOutEx {
			InternalExlessTry(exceptionFactory, MapTryFunc(func), maxTries);
		}

		protected virtual TResult InternalExlessTry<TResult, TRetiesReachedException>(TryFunc<TResult> func, TMaxTries maxTries)
			where TRetiesReachedException : TTryleOutEx, new() {
			return InternalExlessTry(() => CreateException<TRetiesReachedException>(maxTries), func, maxTries);
		}

		protected virtual void InternalExlessTry<TRetiesReachedException>(TryFunc func, TMaxTries maxTries)
			where TRetiesReachedException : TTryleOutEx, new() {
			InternalExlessTry<bool, TRetiesReachedException>(MapTryFunc(func), maxTries);
		}

		private TryFunc<bool> MapTryFunc(TryFunc func) {
			return (out bool dummy) => {
				dummy = false;
				return func();
			};
		}

		protected abstract TRetiesReachedException CreateException<TRetiesReachedException>(TMaxTries maxTries) where TRetiesReachedException : TTryleOutEx, new();
	}
}