using System.Diagnostics;
using System.Runtime.CompilerServices;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Object {
	[PublicAPI]
	public static class GetInstanceHashCodeExtention {
		[Pure, DebuggerStepThrough]
		[Desc("returns if the HashCode for �me� ignoring overwritten GetHashCode() functions.")]
		public static int GetInstanceHashCode(this object me) {
			return RuntimeHelpers.GetHashCode(me);
		}
	}
}