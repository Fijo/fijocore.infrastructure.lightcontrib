using System;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream.Impl.Specific {
	[UsedImplicitly]
	public class BoolStreamSerializer : FixedSizeStreamSerializer<bool> {
		#region Overrides of FixedSizeStreamSerializer<bool>
		protected override int Size { get { return 1; } }
		protected override byte[] Serialize(bool obj) {
			return BitConverter.GetBytes(obj);
		}

		protected override bool Deserialize(byte[] bytes) {
			return BitConverter.ToBoolean(bytes, 0);
		}
		#endregion
	}
}