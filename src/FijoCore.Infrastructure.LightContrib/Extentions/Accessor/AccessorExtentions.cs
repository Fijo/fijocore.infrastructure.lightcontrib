using Fijo.Infrastructure.DesignPattern.Accessor;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Accessor {
	[PublicAPI]
	public static class AccessorExtentions {
		[Pure, PublicAPI]
		public static TValue Get<TInstance, TValue>([NotNull] this IAccessor<TInstance, TValue> me, TInstance instance) {
			return me.Getter(instance);
		}

		[Pure, PublicAPI]
		public static TValue Get<TValue>([NotNull] this IAccessor<TValue> me) {
			return me.Getter();
		}

		[PublicAPI]
		public static void Set<TInstance, TValue>([NotNull] this IAccessor<TInstance, TValue> me, TInstance instance, TValue value) {
			me.Setter(instance, value);
		}

		[PublicAPI]
		public static void Set<TValue>([NotNull] this IAccessor<TValue> me, TValue value) {
			me.Setter(value);
		}
	}
}