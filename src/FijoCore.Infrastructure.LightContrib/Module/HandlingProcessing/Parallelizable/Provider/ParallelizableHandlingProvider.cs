using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Base.Provider;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Parallelizable.Interfaces;

namespace FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Parallelizable.Provider {
	public abstract class ParallelizableHandlingProvider<THandling> : HandlingProvider<THandling>, IParallelizableHandlingProvider<THandling> {
		#region Implementation of IParallelizableHandlingProvider<out THandling>
		public virtual bool SupportParallelization { get { return true; } }
		#endregion
	}
}