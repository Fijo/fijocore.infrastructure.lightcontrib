﻿using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	[PublicAPI]
	public static class AddRangeExtention {
		[NotNull, Pure, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<T> AddRangeReturn<T>([NotNull] this IEnumerable<T> me, [NotNull] IEnumerable<T> collection) {
			return me.Concat(collection);
		}
	}
}