using System.Collections.Generic;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream.Impl.Specific {
	[PublicAPI]
	public class KeyValuePairStreamSerializer<TKey, TValue> : PairBaseStreamSerializer<KeyValuePair<TKey, TValue>, TKey, TValue> {
		#region Overrides of PairBaseStreamSerializer<KeyValuePair<TKey,TValue>,TKey,TValue>
		protected override TKey GetFirst(KeyValuePair<TKey, TValue> pair) {
			return pair.Key;
		}

		protected override TValue GetSecond(KeyValuePair<TKey, TValue> pair) {
			return pair.Value;
		}

		protected override KeyValuePair<TKey, TValue> GetPair(TKey first, TValue second) {
			return new KeyValuePair<TKey, TValue>(first, second);
		}
		#endregion
	}
}