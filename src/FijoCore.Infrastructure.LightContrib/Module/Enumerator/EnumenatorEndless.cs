using System;
using System.Collections;
using System.Collections.Generic;

namespace FijoCore.Infrastructure.LightContrib.Module.Enumerator {
	public class EnumenatorEndless : IEnumerator<int> {
		#region Fields
		private int _index;
		#endregion

		#region Constructor
		public EnumenatorEndless(int start) {
			_index = start;
		}
		#endregion

		#region Implementation of IEnumerator
		public bool MoveNext() {
			_index++;
			return true;
		}

		public void Reset() {
			throw new InvalidOperationException();
		}

		public int Current { get { return _index; } }

		object IEnumerator.Current { get { return Current; } }
		#endregion

		#region Implementation of IDisposable
		public void Dispose() {}
		#endregion
	}
}