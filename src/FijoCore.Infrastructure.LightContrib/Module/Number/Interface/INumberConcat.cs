using System;
using System.Collections.Generic;
using FijoCore.Infrastructure.LightContrib.Module.Number.Service.Interface;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Number.Interface {
	public interface INumberConcat {
		[Pure] TResult Concat<TSource, TResult>(TResult startValue = default(TResult), params TSource[] numbers) where TSource : struct, IComparable, IConvertible where TResult : struct, IComparable, IConvertible;
		[Pure] TResult Concat<TSource, TResult>(IEnumerable<TSource> numbers, TResult startValue = default(TResult)) where TSource : struct, IComparable, IConvertible where TResult : struct, IComparable, IConvertible;
		[Pure] TResult Concat<TSource, TResult>(INumberService<TSource> sourceService, INumberService<TResult> resultService, TResult startValue = default(TResult), params TSource[] numbers) where TSource : struct, IComparable, IConvertible where TResult : struct, IComparable, IConvertible;
		[Pure] TResult Concat<TSource, TResult>(IEnumerable<TSource> numbers, INumberService<TSource> sourceService, INumberService<TResult> resultService, TResult startValue = default(TResult)) where TSource : struct, IComparable, IConvertible where TResult : struct, IComparable, IConvertible;
		[Pure] TResult Concat<TSource, TResult>(TResult entryAddValue, TResult startValue = default(TResult), params TSource[] numbers) where TSource : struct, IComparable, IConvertible where TResult : struct, IComparable, IConvertible;
		[Pure] TResult Concat<TSource, TResult>(IEnumerable<TSource> numbers, TResult entryAddValue, TResult startValue = default(TResult)) where TSource : struct, IComparable, IConvertible where TResult : struct, IComparable, IConvertible;
		[Pure] TResult Concat<TSource, TResult>(INumberService<TSource> sourceService, INumberService<TResult> resultService, TResult entryAddValue, TResult startValue = default(TResult), params TSource[] numbers) where TSource : struct, IComparable, IConvertible where TResult : struct, IComparable, IConvertible;
		[Pure] TResult Concat<TSource, TResult>(IEnumerable<TSource> numbers, TResult entryAddValue, INumberService<TSource> sourceService, INumberService<TResult> resultService, TResult startValue = default(TResult)) where TSource : struct, IComparable, IConvertible where TResult : struct, IComparable, IConvertible;
		[Pure] IEnumerable<TResult> Split<TSource, TResult>(TSource number, int length) where TSource : struct, IComparable, IConvertible where TResult : struct, IComparable, IConvertible;
		[Pure] IEnumerable<TResult> Split<TSource, TResult>(TSource number, int length, INumberService<TSource> sourceService, INumberService<TResult> resultService) where TSource : struct, IComparable, IConvertible where TResult : struct, IComparable, IConvertible;
		[Pure] IEnumerable<TResult> Split<TSource, TResult>(TSource number, TSource entryMaxValue, int length) where TSource : struct, IComparable, IConvertible where TResult : struct, IComparable, IConvertible;
		[Pure] IEnumerable<TResult> Split<TSource, TResult>(TSource number, TSource entryMaxValue, int length, INumberService<TSource> sourceService, INumberService<TResult> resultService) where TSource : struct, IComparable, IConvertible where TResult : struct, IComparable, IConvertible;
	}
}