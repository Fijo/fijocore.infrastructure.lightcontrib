//BS:Frei Wild - Mach dich auf

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Default.Service.CheckName;
using FijoCore.Infrastructure.LightContrib.Extentions.ICollection.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.ISet.Generic;
using FijoCore.Infrastructure.LightContrib.Module.Pool.Dto;
using FijoCore.Infrastructure.LightContrib.Module.Pool.Interface;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Pool {
	[Note("you have to call Init() insiede of the constructor of classes that inherits from this one.")]
	public abstract class Pool<T, TEntry> : IPool<T, TEntry> where TEntry : PoolEntry<T> {
		protected readonly ISet<TEntry> Availible;
		protected readonly ISet<TEntry> Content;
		protected readonly int ShouldActives;
		private readonly Thread _maintenanceThread;
		private readonly int _maintenanceIntervall;
		private bool _continueMaintenance = true;
		protected bool Disposed;

		protected Pool([NotNull] IEnumerable<TEntry> content, int shouldActives, int maintenanceIntervall) {
			#region PreCondition
			if (shouldActives <= 0) throw new ArgumentOutOfRangeException("shouldActives", shouldActives, "have to be bigger than 0");
			if (maintenanceIntervall <= 0) throw new ArgumentOutOfRangeException("maintenanceIntervall", "have to be bigger than 0");
			#endregion
			Availible = new HashSet<TEntry>();
			Content = new HashSet<TEntry>(content);
			ShouldActives = shouldActives;
			_maintenanceIntervall = maintenanceIntervall;
			_maintenanceThread = CreateMaintenanceThread();
		}

		protected void Init() {
			ActivateObjects(ShouldActives);
			_maintenanceThread.Start();
		}

		private Thread CreateMaintenanceThread() {
			return new Thread(MaintainTask) {IsBackground = true, Name = string.Format("{0} MaintenanceThread", typeof (Pool<T, TEntry>).Name)};
		}

		protected void MaintainTask() {
			do {
				MaintainActive();
				Thread.Sleep(_maintenanceIntervall);
			} while (_continueMaintenance);
		}

		protected virtual void MaintainActive() {
			lock(Content)
				lock(Availible) {
					DeactivateObjects();
					ActivateObjects();
					LogMaintainActiveEnd();
				}
		}

		[Conditional("PoolMaintainTrace")]
		private void LogMaintainActiveEnd() {
			Trace.WriteLine(string.Format("{0}.{1} processed {2}: {3}, {4}: {5}", typeof (Pool<T, TEntry>).Name,
			                              CN.Get<Pool<T, TEntry>>(x => x.MaintainActive()),
			                              "ShouldActives",
			                              ShouldActives,
			                              CN.Get<Pool<T, TEntry>>(x => x.CurrentAvalibleCount()),
			                              CurrentAvalibleCount()
				                ));
		}

		protected void ActivateObjects() {
			ActivateObjects(ShouldActives - CurrentAvalibleCount());
		}

		protected virtual int CurrentAvalibleCount() {
			return Availible.Count;
		}

		protected void ActivateObjects(int count) {
			ActivateObjects(GetObjectsToActivate, count);
		}

		protected void ActivateObjects(Func<int, IEnumerable<TEntry>> getObjectsToActivate, int count) {
			var objectsToActivate = getObjectsToActivate(count).Execute();
			Availible.AddRange(objectsToActivate);
			Content.RemoveRange(objectsToActivate);
		}

		protected virtual IEnumerable<TEntry> GetObjectsToActivate(int count) {
			return Content.Where(ActivatePredicate)
				.Shuffle()
				.AsParallel()
				.Where(InternalActivate)
				.Take(count);
		}

		protected virtual bool ActivatePredicate(TEntry me) {
			#if DEBUG
			Debug.Assert(!me.IsActive, "!me.IsActive");
			#endif
			return true;
		}

		protected bool InternalActivate(TEntry entry) {
			var internalActivate = Activate(entry);
			#if DEBUG
			if(internalActivate) entry.IsActive = true;
			#endif
			return internalActivate;
		}

		protected abstract bool Activate(TEntry entry);

		protected void DeactivateObjects() {
			var objectsToDeactivate = GetObjectsToDeactivate().Execute();
			Availible.RemoveRange(objectsToDeactivate);
			Content.AddRange(objectsToDeactivate);
		}

		protected virtual IEnumerable<TEntry> GetObjectsToDeactivate() {
			return Content.Where(DeactivatePredicate)
				.Where(IsToRecycle)
				.Select(InternalDeactivate);
		}

		protected virtual bool DeactivatePredicate(TEntry me) {
			return true;
		}

		protected TEntry InternalDeactivate(TEntry entry) {
			#if DEBUG
			entry.IsActive = false;
			#endif
			Deactivate(entry);
			return entry;
		}

		protected abstract void Deactivate(TEntry entry);

		protected abstract bool IsToRecycle(TEntry entry);

		public virtual bool CanGet() {
			return Availible.Any();
		}

		public virtual T Get() {
			CheckDisposed();
			Debug.Assert(CanGet(), "CanGet()");
			return InternalGet().Content;
		}

		protected virtual TEntry InternalGet() {
			return Availible.ShuffleSingle();
		}

		public virtual void Add(IEnumerable<TEntry> candidates) {
			lock(Content) {
				Content.AddRange(candidates);
				ActivateObjects(ShouldActives);
			}
		}

		protected void CheckDisposed() {
			if (Disposed) throw new ObjectDisposedException(typeof (BlockablePool<T, TEntry>).FullName);
		}

		#region Implementation of IDisposable
		public virtual void Dispose() {
			Disposed = true;
			DisposeMaintenanceThread();
			DeactivateObjects();
		}

		private void DisposeMaintenanceThread() {
			_continueMaintenance = false;
			_maintenanceThread.Interrupt();
			while (_maintenanceThread.IsAlive) Thread.Sleep(1);
		}
		#endregion
	}
}