using System.Collections.Generic;
using Fijo.Infrastructure.DesignPattern.Events;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Transaction {
	public interface ITransactionService {
		void AddEvent([NotNull] Transaction transaction, [NotNull] IEvent @event);
		void AddEvents([NotNull] Transaction transaction, [NotNull] IEnumerable<IEvent> events);
		IEnumerable<IEvent> GetEvents([NotNull] Transaction transaction);
	}
}