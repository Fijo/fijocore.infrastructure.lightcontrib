using System;
using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IList.Generic {
	[PublicAPI]
	public static class MaxLengthExtention {
		[PublicAPI, DebuggerStepThrough]
		public static void MaxLength<T>([NotNull] this IList<T> me, int maxLength) {
			#region PreCondition
			if(maxLength < 0) throw new ArgumentOutOfRangeException("maxLength");
			#endregion
			lock (me) {
				if (me.Count > maxLength) me.RemoveRange(maxLength, me.Count - maxLength);
			}
		}
	}
}