using System;
using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic {
	[PublicAPI]
	[Note("I was unshure if I should use Actions for the for fail handling used parameter. Action or Func<Exception>, where the 2 possibilities it thought about. The advantages of an Action where, that when a exception is thrown (thogether with this one of the extentions in this class), the possibility that you will see the line where it is thrown is higher (Because under normal conditions it is right in the assembly that contains the logic that produces that exceptions. The framework does this too, but I think the specific impl is more interesting than the abstract extention in the framework.). So you realy in the class where the logic is in many cases. If I use an Func<Exception> and throw the return value of it, you have to find navigate through the stack trace before you�ll find the place where the exception is thrown. All that may not always be the case, but in the most cases. On the other side the advantages of the Func<Exception> are, that you don�t have to write the 'throw' and may additional brackets (for a delegate/ function body) when using the extention. For code readability resons, I would prefer the Func<Exception> a lot more. Because of the a little bit less depedence of the disadvantage and advantage, of the Func<Exeption> case (I mean the though of the external source handling and the stack trace as it is now - not to optimize it on that, to be flexible for changes), and the importance of code readability, of that I think it is the bigger advantage than of the other advantages, because for example if you have a more readable code you will although, find the place you want earlier, too, I decided to use the Func<Exception> case.")]
	public static class GetOrThrowExtention {
		[Pure, PublicAPI]
		public static TValue GetOrThrow<TKey, TValue>([NotNull] this IDictionary<TKey, TValue> me, TKey key, [NotNull] Func<IDictionary<TKey, TValue>, TKey, Exception> getException) {
			return InternalGetOrThrow(me, key, getException);
		}
		
		[Pure, PublicAPI]
		public static TValue GetOrThrow<TKey, TValue>([NotNull] this IDictionary<TKey, TValue> me, TKey key, [NotNull] Func<TKey, Exception> getException) {
			return InternalGetOrThrow(me, key, GetWraped<TKey, TValue>(getException));
		}
		
		[Pure, PublicAPI]
		public static TValue GetOrThrow<TKey, TValue>([NotNull] this IDictionary<TKey, TValue> me, TKey key, [NotNull] Func<Exception> getException) {
			return InternalGetOrThrow(me, key, GetWraped<TKey, TValue>(getException));
		}

		#region FuncWrapper
		[NotNull, Pure]
		private static Func<IDictionary<TKey, TValue>, TKey, Exception> GetWraped<TKey, TValue>([NotNull] Func<TKey, Exception> func) {
			return (dict, key) => func(key);
		}

		[NotNull, Pure]
		private static Func<IDictionary<TKey, TValue>, TKey, Exception> GetWraped<TKey, TValue>([NotNull] Func<Exception> func) {
			return (dict, key) => func();
		}
		#endregion

		[Pure]
		private static TValue InternalGetOrThrow<TKey, TValue>([NotNull] IDictionary<TKey, TValue> me, TKey key, [NotNull] Func<IDictionary<TKey, TValue>, TKey, Exception> getException) {
			TValue result;
			if (!me.TryGetValue(key, out result)) throw getException(me, key);
			return result;
		}
	}
}