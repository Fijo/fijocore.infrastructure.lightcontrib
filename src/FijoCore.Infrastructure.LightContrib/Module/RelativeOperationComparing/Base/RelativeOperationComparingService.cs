﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Enums;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Converter.ConverterServices.TypeCode;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.RelativeOperationComparing.Base {
	[UsedImplicitly]
	public class RelativeOperationComparingService {
		private readonly NumericConverterService _numericConverterService;
		private readonly IDictionary<RelationOperation, Func<object, object, bool>> _funcs;

		public RelativeOperationComparingService(ILookupFactory lookupFactory, NumericConverterService numericConverterService) {
			_numericConverterService = numericConverterService;
			_funcs = lookupFactory.CreatePerf(new Dictionary<RelationOperation, Func<object, object, bool>>
			{
				{RelationOperation.NotEquals, (left, right) => !EqualsBase(right, left)},
				{RelationOperation.IsIn, IsIn},
				{RelationOperation.NotIn, (left, right) => !IsIn(left, right)},
				{RelationOperation.AtLeast, (left, right) => NummericMoreLessBase((a, b) => a >= b, left, right)},
				{RelationOperation.Less, (left, right) => NummericMoreLessBase((a, b) => a < b, left, right)},
				{RelationOperation.LessOrEqual, (left, right) => NummericMoreLessBase((a, b) => a <= b, left, right)},
				{RelationOperation.More, (left, right) => NummericMoreLessBase((a, b) => a > b, left, right)},
				{RelationOperation.Equals, EqualsBase}
			}, true);
		}

		private static bool EqualsBase(object right, object left) {
			return left.Equals(right);
		}

		private bool IsIn(object left, object right) {
			Debug.Assert(right is IEnumerable);
			return ((IEnumerable) right).Cast<object>().Contains(left);
		}

		private bool NummericMoreLessBase([NotNull] Func<double, double, bool> expression, object left, object right) {
			return expression(_numericConverterService.Convert(left), _numericConverterService.Convert(right));
		}

		public bool Compare(RelationOperation relationOperation, object left, object right) {
			return _funcs[relationOperation](left, right);
		}
	}
}