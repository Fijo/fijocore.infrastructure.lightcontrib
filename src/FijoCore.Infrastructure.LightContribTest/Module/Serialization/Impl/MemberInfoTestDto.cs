﻿using System;
using System.Reflection;
using FijoCore.Infrastructure.LightContrib.Default.Service.EqualityHelpers;

namespace FijoCore.Infrastructure.LightContribTest.Module.Serialization.Impl {
	[Serializable]
	public class MemberInfoTestDto {
		public MemberInfo Member { get; set; }
		#region Equality
		protected bool Equals(MemberInfoTestDto other) {
			return Member.Equals(other.Member);
		}

		public override bool Equals(object obj) {
			return Equality.Equals<MemberInfoTestDto>(obj, Equals);
		}

		public override int GetHashCode() {
			return Member.GetHashCode();
		}
		#endregion
	}
}