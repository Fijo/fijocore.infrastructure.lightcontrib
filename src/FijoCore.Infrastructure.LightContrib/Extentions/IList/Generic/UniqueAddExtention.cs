using System.Collections.Generic;
using System.Diagnostics;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Service.UniqueAdd.Interface;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IList.Generic {
	[PublicAPI]
	public static class UniqueAddExtention {
		private static IUniqueAddRangeService _uniqueAddRangeService;

		[DebuggerStepThrough]
		internal static void Init() {
			_uniqueAddRangeService = Kernel.Resolve<IUniqueAddRangeService>();
		}

		[PublicAPI, DebuggerStepThrough]
		public static void AssertUniqueAddRange<T>([NotNull] this IList<T> me, [NotNull] IEnumerable<T> collection) {
			#region PreCondition
			Debug.Assert(_uniqueAddRangeService != null, "_uniqueAddRangeService != null");
			#endregion
			_uniqueAddRangeService.AssertUniqueAddRange(me, collection, Contains, AddRange);
		}

		[PublicAPI, DebuggerStepThrough]
		public static void UniqueAddRange<T>([NotNull] this IList<T> me, [NotNull] IEnumerable<T> collection) {
			#region PreCondition
			Debug.Assert(_uniqueAddRangeService != null, "_uniqueAddRangeService != null");
			#endregion
			_uniqueAddRangeService.UniqueAddRange(me, collection, Contains, AddRange);
		}

		[DebuggerStepThrough]
		private static bool Contains<T>([NotNull] IList<T> list, T item) {
			return list.Contains(item);
		}

		[DebuggerStepThrough]
		private static void AddRange<T>([NotNull] IList<T> list, [NotNull] IEnumerable<T> collection) {
			list.AddRange(collection);
		}
	}
}