﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Repositories {
	[UsedImplicitly]
	public class AllFileRepository : IAllFileRepository {
		public IEnumerable<string> Get(string folder) {
			return Directory.GetDirectories(folder).SelectMany(Get).Concat(Directory.GetFiles(folder));
		}
	}
}