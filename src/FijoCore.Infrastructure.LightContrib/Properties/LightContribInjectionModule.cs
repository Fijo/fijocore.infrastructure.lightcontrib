﻿using System;
using System.Collections.Generic;
using System.Net;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Controller;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Interface;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Provider;
using Fijo.Infrastructure.DesignPattern.Mapping;
using Fijo.Infrastructure.DesignPattern.Repository;
using FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Base;
using FijoCore.Infrastructure.LightContrib.Default.Service.DefaultFunctions;
using FijoCore.Infrastructure.LightContrib.Default.Service.DefaultFunctions.Interface;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using FijoCore.Infrastructure.LightContrib.Default.Service.Try;
using FijoCore.Infrastructure.LightContrib.Enums;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic.Reflection.Resolve;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic.KeyValuePair;
using FijoCore.Infrastructure.LightContrib.Extentions.IList.Generic;
using FijoCore.Infrastructure.LightContrib.Factories;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;
using FijoCore.Infrastructure.LightContrib.Mappings;
using FijoCore.Infrastructure.LightContrib.Module.Assembly;
using FijoCore.Infrastructure.LightContrib.Module.AssemblyConfiguration;
using FijoCore.Infrastructure.LightContrib.Module.Cmd;
using FijoCore.Infrastructure.LightContrib.Module.Configuration;
using FijoCore.Infrastructure.LightContrib.Module.Configuration.ConverterKey;
using FijoCore.Infrastructure.LightContrib.Module.Configuration.Internal;
using FijoCore.Infrastructure.LightContrib.Module.Converter.ConverterServices.TypeCode;
using FijoCore.Infrastructure.LightContrib.Module.Converter.Module;
using FijoCore.Infrastructure.LightContrib.Module.Converter.Simple;
using FijoCore.Infrastructure.LightContrib.Module.Converter.Simple.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Encoding;
using FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Custom;
using FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Custom.Interface;
using FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Interface;
using FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Recursive;
using FijoCore.Infrastructure.LightContrib.Module.FileTools;
using FijoCore.Infrastructure.LightContrib.Module.Path;
using FijoCore.Infrastructure.LightContrib.Module.Perf.Configuration;
using FijoCore.Infrastructure.LightContrib.Module.Perf.ListType;
using FijoCore.Infrastructure.LightContrib.Module.Perf.ListType.Interface;
using FijoCore.Infrastructure.LightContrib.Module.RelativeOperationComparing.Custom;
using FijoCore.Infrastructure.LightContrib.Module.Serialization;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Impl;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Settings;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Settings.Impl;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream.Impl.Specific;
using FijoCore.Infrastructure.LightContrib.Module.Stream;
using FijoCore.Infrastructure.LightContrib.Module.Stream.Obj.Reader;
using FijoCore.Infrastructure.LightContrib.Module.Transaction;
using FijoCore.Infrastructure.LightContrib.Module.Typing.DictionaryType;
using FijoCore.Infrastructure.LightContrib.Module.Typing.KeyValuePairType;
using FijoCore.Infrastructure.LightContrib.Repositories;
using FijoCore.Infrastructure.LightContrib.Service.ExecptionFormatter;
using FijoCore.Infrastructure.LightContrib.Service.OrderlessEquals;
using FijoCore.Infrastructure.LightContrib.Service.OrderlessEquals.Interface;
using FijoCore.Infrastructure.LightContrib.Service.SequenceEqualService;
using FijoCore.Infrastructure.LightContrib.Service.SequenceEqualService.Interface;
using FijoCore.Infrastructure.LightContrib.Service.UniqueAdd;
using FijoCore.Infrastructure.LightContrib.Service.UniqueAdd.Interface;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;
using Newtonsoft.Json;
using RecursiveSequenceEqualExtention = FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator.RecursiveSequenceEqualExtention;
using SequenceEqualExtention = FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator.SequenceEqualExtention;

namespace FijoCore.Infrastructure.LightContrib.Properties
{
	public class LightContribInjectionModule : ExtendedNinjectModule {
    	public override void AddModule(IKernel kernel) {
    		kernel.Load(new LightContribBaseInjectionModule());
    		kernel.Load(new NumberInjectionModule());
    		kernel.Load(new CloningInjectionModule());
    	}

    	public override void OnLoad(IKernel kernel)
        {
			kernel.Bind<ITryTimesService>().To<TryTimesService>().InSingletonScope();
			kernel.Bind<IExlessTryTimesService>().To<ExlessTryTimesService>().InSingletonScope();
			kernel.Bind<IExlessTryForService>().To<ExlessTryForService>().InSingletonScope();

        	kernel.Bind<IOut>().To<Out>().InSingletonScope();
    		kernel.Bind<ITry>().To<Try>().InSingletonScope();
        	kernel.Bind<IDefaultFunctions>().To<DefaultFunctions>().InSingletonScope();

    		kernel.Bind<IEnumeratorFactory>().To<EnumeratorFactory>().InSingletonScope();
    		kernel.Bind<IEnumerableFactory>().To<EnumerableFactory>().InSingletonScope();

    		kernel.Bind<IPerfConfigurationService>().To<PerfConfigurationService>().InSingletonScope();
    		kernel.Bind<IPerfConfigurationUpdater>().To<PerfConfigurationUpdater>().InSingletonScope();
    		kernel.Bind<IRepository<PerfConfiguration>>().To<PerfConfigurationProvider>().InSingletonScope();

    		kernel.Bind<IListTypeProvider>().To<ListTypeProvider>().InSingletonScope();

			kernel.Bind<IExecutedEnumerableFactory>().To<ExecutedEnumerableFactory>().InSingletonScope();
			kernel.Bind<IOrderlessEqualsService>().To<OrderlessEqualsService>().InSingletonScope();

        	kernel.Bind<AssembliesProvider>().ToSelf().InSingletonScope();
        	kernel.Bind<AssemblyDynamicCustomConfigurationResolver>().ToSelf().InSingletonScope();
        	kernel.Bind<AssemblyEnabledDefenitionResolver>().ToSelf().InSingletonScope();
			
        	kernel.Bind<IRepository<IEnumerable<Type>>>().To<TypeProvider>().InSingletonScope();

        	kernel.Bind<RelativeOperationComparingEnumerableRightService>().ToSelf().InSingletonScope();
        	kernel.Bind<RuntimeConverter>().ToSelf().InSingletonScope();
        	kernel.Bind<VersionToULongConverter>().ToSelf().InSingletonScope();
        	kernel.Bind<NumericConverterService>().ToSelf().InSingletonScope();

    		kernel.Bind<ICommandService>().To<CommandService>().InSingletonScope();

    		kernel.Bind<IGenericKeyValuePairAccessorProvider>().To<GenericKeyValuePairAccessorProvider>().InSingletonScope();
    		kernel.Bind<IDictionaryTypeService>().To<DictionaryTypeService>().InSingletonScope();

    		kernel.Bind<IPairFactory>().To<PairFactory>().InSingletonScope();
    		kernel.Bind<IValidationObjFactory>().To<ValidationObjFactory>().InSingletonScope();
    		kernel.Bind<ILookupFactory>().To<LookupFactory>().InSingletonScope();

    		kernel.Bind<IFileFilterService>().To<FileFilterService>().InSingletonScope();
    		kernel.Bind<IAllFileRepository>().To<AllFileRepository>().InSingletonScope();

    		kernel.Bind<ITransactionFactory>().To<TransactionFactory>().InSingletonScope();
    		kernel.Bind<ITransactionDao>().To<TransactionDao>().InSingletonScope();
    		kernel.Bind<ITransactionService>().To<TransactionService>().InSingletonScope();

    		kernel.Bind<IStreamService>().To<StreamFacade>().InSingletonScope();
    		kernel.Bind<IDefaultStreamService>().To<SeekStreamService>().InSingletonScope();
    		kernel.Bind<ISeeklessStreamService>().To<SeeklessStreamService>().InSingletonScope();

    		kernel.Bind<IReaderFactory>().To<ReaderFactory>().InSingletonScope();

    		kernel.Bind<IRepository<IDictionary<SerializationFormat, ISerialization>>>().To<SerializationRepository>().InSingletonScope();
    		kernel.Bind<IMapping<SerializationFormatting, Formatting>>().To<FormattingMapping>().InSingletonScope();
    		kernel.Bind<ISerializationService>().To<SerializationService>().InSingletonScope();
    		kernel.Bind<ISerializationProvider>().To<SerializationProvider>().InSingletonScope();
    		kernel.Bind<ISerializationFacade>().To<SerializationFacade>().InSingletonScope();

    		kernel.Bind<IJsonSettings>().To<DefaultJsonSettings>().InSingletonScope();

    		kernel.Bind<IConfigurationService>().To<ConfigurationService>().InSingletonScope();
    		kernel.Bind<IInternalConfigurationService>().To<InternalConfigurationService>().InSingletonScope();
    		kernel.Bind<IRepository<IDictionary<string, object>>>().To<ConfigurationRepository>().InSingletonScope();

    		kernel.Bind<IEncodingProvider>().To<EncodingProvider>().InSingletonScope();

    		kernel.Bind<IPathService>().To<PathService>().InSingletonScope();

    		kernel.Bind<IExceptionFormatter>().To<ExceptionFormatter>().InSingletonScope();

			kernel.Bind<IUniqueAddService>().To<UniqueAddService>().InSingletonScope();
			kernel.Bind<IUniqueAddRangeService>().To<UniqueAddRangeService>().InSingletonScope();
			kernel.Bind<ISequenceEqualService>().To<SequenceEqualService>().InSingletonScope();

			kernel.Bind<EqualityComparerFactory>().ToSelf().InSingletonScope();
			kernel.Bind<ICustomRecursiveEqualityComparerFactory>().To<CustomRecursiveEqualityComparerFactory>().InSingletonScope();
			kernel.Bind<ICustomInternalRecursiveEqualityComparerFactory>().To<CustomInternalRecursiveEqualityComparerFactory>().InSingletonScope();
			kernel.Bind<IRecursiveEqualityComparerService>().To<RecursiveEqualityComparerService>().InSingletonScope();
			kernel.Bind<IEqualityComparerFactory>().To<EqualityComparerFactory>().InSingletonScope();

    		kernel.Bind<ITypeChangeFactory>().To<TypeChangeFactory>().InSingletonScope();
    		kernel.Bind<IConverter>().To<DateTimeConverter>().InSingletonScope();
    		kernel.Bind<IConverter>().To<TimeSpanConverter>().InSingletonScope();
    		kernel.Bind<IConverter>().To<Int16Converter>().InSingletonScope();
    		kernel.Bind<IConverter>().To<UInt16Converter>().InSingletonScope();
    		kernel.Bind<IConverter>().To<Int32Converter>().InSingletonScope();
    		kernel.Bind<IConverter>().To<UInt32Converter>().InSingletonScope();
    		kernel.Bind<IConverter>().To<UInt64Converter>().InSingletonScope();
    		kernel.Bind<IConverter>().To<EndPointConverter>().InSingletonScope();
    		kernel.Bind<IConverter>().To<EncodingConverter>().InSingletonScope();
    		kernel.Bind<IConverter>().To<ProcessPriorityClassConverter>().InSingletonScope();
    		kernel.Bind<IConverter>().To<DefaultConverter>().InSingletonScope();
    		kernel.Bind<IAlgorithmProvider<IConverter, TypeChangeDto>>().To<ConfigurationConverterProvider>().InSingletonScope();
			//kernel.Bind<IFinder<IConverter, TypeChangeDto>>().To<Finder<IConverter, TypeChangeDto>>().InSingletonScope();

    		BindSerializer<bool, BoolStreamSerializer>();
    		BindSerializer<byte, ByteStreamSerializer>();
    		BindSerializer<short, Int16StreamSerializer>();
    		BindSerializer<ushort, UInt16StreamSerializer>();
    		BindSerializer<int, Int32StreamSerializer>();
    		BindSerializer<uint, UInt32StreamSerializer>();
    		BindSerializer<long, Int64StreamSerializer>();
    		BindSerializer<ulong, UInt64StreamSerializer>();
    		BindSerializer<string, StringStreamSerializer>();
    		kernel.Bind<IStreamSerializer<IEnumerable<int>>>().To<Int32SequenceStreamSerializer>().InSingletonScope();
    	}

	    private void BindSerializer<T, TSerializer>() where TSerializer : IStreamSerializer<T> {
		    Kernel.Bind<IStreamSerializer<T>>().To<TSerializer>().InSingletonScope();
			Kernel.Bind<IStreamSerializer<ICollection<T>>>().To<GenericCollectionStreamSerializer<T>>().InSingletonScope();
			Kernel.Bind<IStreamSerializer<IList<T>>>().To<GenericListStreamSerializer<T>>().InSingletonScope();
	    }

	    public override void Init(IKernel kernel) {
    		TryGetEnumeratorExtention.Init();
			ExecuteExtention.Init();
			UniqueAddExtention.Init();
			SequenceEqualExtention.Init();
			RecursiveSequenceEqualExtention.Init();
			OrderlessEqualsExtention.Init();
			ToDistinctedDictionaryExtention.Init();
		    ToDictExtention.Init();
    		Extentions.IEnumerator.TryFirstExtention.Init();
			Extentions.IEnumerator.TrySingleExtention.Init();
			Extentions.IEnumerator.TrySingleExtention.Init();
			Extentions.Type.GetInheritingTypesExtention.Init();
			Extentions.StringReader.GetCharsExtention.Init();
    	}
    }
}