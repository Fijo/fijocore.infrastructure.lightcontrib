using System;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream.Impl.Specific {
	[UsedImplicitly]
	public class UInt64StreamSerializer : FixedSizeStreamSerializer<ulong> {
		#region Overrides of FixedSizeStreamSerializer<ulong>
		protected override int Size { get { return 8; } }
		protected override byte[] Serialize(ulong obj) {
			return BitConverter.GetBytes(obj);
		}

		protected override ulong Deserialize(byte[] bytes) {
			return BitConverter.ToUInt64(bytes, 0);
		}
		#endregion
	}
}