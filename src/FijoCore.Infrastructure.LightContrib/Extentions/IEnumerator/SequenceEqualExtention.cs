using System;
using System.Diagnostics;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Factories;
using FijoCore.Infrastructure.LightContrib.Service.SequenceEqualService.Interface;
using JetBrains.Annotations;
using System.Collections.Generic;
using SIEnumerator = System.Collections.IEnumerator;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator {
	[PublicAPI]
	public static class SequenceEqualExtention {
		private static EqualityComparerFactory _equalityComparerFactory;
		private static ISequenceEqualService _sequenceEqual;

		[DebuggerStepThrough]
		internal static void Init() {
			_equalityComparerFactory = Kernel.Resolve<EqualityComparerFactory>();
			_sequenceEqual = Kernel.Resolve<ISequenceEqualService>();
		}

		[Pure, PublicAPI, DebuggerStepThrough]
		public static bool SequenceEqual([NotNull] this SIEnumerator me, [NotNull] SIEnumerator other) {
			return _sequenceEqual.SequenceEqual(me, other, _equalityComparerFactory.Default().Equals);
		}

		[Pure, PublicAPI, DebuggerStepThrough]
		public static bool SequenceEqual([NotNull] this SIEnumerator me, [NotNull] SIEnumerator other, [NotNull] Func<object, object, bool> equalsFunc) {
			return _sequenceEqual.SequenceEqual(me, other, equalsFunc);
		}

		[Pure, PublicAPI, DebuggerStepThrough]
		public static bool SequenceEqual<T>([NotNull] this IEnumerator<T> me, [NotNull] IEnumerator<T> other, [NotNull] Func<T, T, bool> equalsFunc) {
			return _sequenceEqual.SequenceEqual((SIEnumerator) me, other, (x, y) => equalsFunc((T) x, (T) y));
		}
	}
}