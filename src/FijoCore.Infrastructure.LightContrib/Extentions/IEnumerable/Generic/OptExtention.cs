using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	[PublicAPI]
	[About("MayExecuteParallelExtention")]
	[Note("This class was called ´OptActionExtention´ in the past.")]
	public static class OptExtention {
		#region Action
		[PublicAPI, DebuggerStepThrough]
		public static void Opt<TSource>([NotNull] this IEnumerable<TSource> me, bool executeParallel, [NotNull] Action<IEnumerable<TSource>> notOpt, [NotNull] Action<ParallelQuery<TSource>> opt) {
			if(executeParallel) opt(me.AsParallel().WithExecutionMode(ParallelExecutionMode.ForceParallelism));
			else notOpt(me);
		}

		[PublicAPI, DebuggerStepThrough]
		public static void Opt<TSource>([NotNull] this ParallelQuery<TSource> me, bool executeParallel, [NotNull] Action<IEnumerable<TSource>> notOpt, [NotNull] Action<ParallelQuery<TSource>> opt) {
			if(executeParallel) opt(me);
			else notOpt(me.AsEnumerable());
		}
		#endregion

		#region Func
		[NotNull, Pure, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<TResult> Opt<TSource, TResult>([NotNull] this IEnumerable<TSource> me, bool executeParallel, [NotNull] Func<IEnumerable<TSource>, IEnumerable<TResult>> notOpt, [NotNull] Func<ParallelQuery<TSource>, IEnumerable<TResult>> opt) {
			return executeParallel ? opt(me.AsParallel().WithExecutionMode(ParallelExecutionMode.ForceParallelism)) : notOpt(me);
		}

		[NotNull, Pure, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<TResult> Opt<TSource, TResult>([NotNull] this ParallelQuery<TSource> me, bool executeParallel, [NotNull] Func<IEnumerable<TSource>, IEnumerable<TResult>> notOpt, [NotNull] Func<ParallelQuery<TSource>, IEnumerable<TResult>> opt) {
			return executeParallel ? opt(me) : notOpt(me.AsEnumerable());
		}
		#endregion
	}
}