using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using FijoCore.Infrastructure.LightContrib.Enums;
using FijoCore.Infrastructure.LightContrib.Module.EqualityComparer;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Settings;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization.Impl {
	public class BinarySerialization : ExtSerialization<IBinarySettings> {
		private readonly BinaryFormatter _binaryFormatter = new BinaryFormatter();
		#region Implementation of ISerialization
		protected override void Serialize(object obj, System.IO.Stream stream, SerializationFormatting formatting, System.Text.Encoding encoding, IBinarySettings serializationSettings = null) {
			Serialize<object>(obj, stream, formatting, encoding, serializationSettings);
		}

		protected override void Serialize<T>(T obj, System.IO.Stream stream, SerializationFormatting formatting, System.Text.Encoding encoding, IBinarySettings serializationSettings = null) {
			_binaryFormatter.Serialize(stream, obj);
		}

		protected override object Deserialize(System.IO.Stream stream, System.Text.Encoding encoding, IBinarySettings serializationSettings = null) {
			return serializationSettings != null && serializationSettings.Get().UseUnsafeDeserialize
				       ? _binaryFormatter.UnsafeDeserialize(stream, null)
				       : _binaryFormatter.Deserialize(stream);
		}

		protected override T Deserialize<T>(System.IO.Stream stream, System.Text.Encoding encoding, IBinarySettings serializationSettings = null) {
			return (T)Deserialize(stream, encoding, serializationSettings);
		}
		#endregion
	}
}