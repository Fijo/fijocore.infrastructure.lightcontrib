﻿using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners {
	[PublicAPI]
	public interface ICloning {
		CloningType Type { get; }
		T Pass<T>(T source);
		ICloning GetDeeper();
	}
}