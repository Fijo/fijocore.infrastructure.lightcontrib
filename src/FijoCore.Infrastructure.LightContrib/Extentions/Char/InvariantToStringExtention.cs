using System.Diagnostics;
using System.Globalization;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Char {
	[PublicAPI]
	public static class InvariantToStringExtention {
		[Pure, DebuggerStepThrough, PublicAPI]
		[Desc("Converts a char to string using invariant culture settings")]
		public static string InvariantToString(this char me) {
			return me.ToString(CultureInfo.InvariantCulture);
		}
	}
}