using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	public static class IndexOfExtention {
		[Pure, DebuggerStepThrough]
		public static int IndexOf<TSource>([NotNull] this IEnumerable<TSource> me, TSource item, [CanBeNull] IEqualityComparer equalityComparer) {
			using(var enumerator = me.GetEnumerator()) return enumerator.IndexOf(item, equalityComparer);
		}

		[Pure, DebuggerStepThrough]
		public static int IndexOf<TSource>([NotNull] this IEnumerable<TSource> me, TSource item) {
			using(var enumerator = me.GetEnumerator()) return enumerator.IndexOf(item);
		}
	}
}