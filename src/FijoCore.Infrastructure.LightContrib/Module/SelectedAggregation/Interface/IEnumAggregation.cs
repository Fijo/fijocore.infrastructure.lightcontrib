using System;
using System.Collections.Generic;

namespace FijoCore.Infrastructure.LightContrib.Module.SelectedAggregation.Interface {
	public interface IEnumAggregation<TEnum, TAccumulate> : IEnumAggregation<TEnum, TAccumulate, object>, ISelectedAggregation<TEnum, ICollection<TEnum>, TAccumulate> where TEnum : struct, IConvertible {}

	public interface IEnumAggregation<TEnum, TAccumulate, TParam> : IEnumAggregation<TEnum, TAccumulate, TAccumulate, TAccumulate, TParam>, ISelectedAggregation<TEnum, ICollection<TEnum>, TAccumulate, TParam> where TEnum : struct, IConvertible {}
	
	public interface IEnumAggregation<TEnum, TAccumulate, in TSource, out TResult> : IEnumAggregation<TEnum, TAccumulate, TSource, TResult, object>, ISelectedAggregation<TEnum, ICollection<TEnum>, TAccumulate, TSource, TResult> where TEnum : struct, IConvertible {}

	public interface IEnumAggregation<TEnum, TAccumulate, in TSource, out TResult, TParam> : ISelectedAggregation<TEnum, ICollection<TEnum>, TAccumulate, TSource, TResult, TParam> where TEnum : struct, IConvertible {}
}