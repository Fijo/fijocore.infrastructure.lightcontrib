using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	[PublicAPI]
	public static class ShuffleExtention {
		[NotNull] private static readonly Random Random = new Random();

		[NotNull, Pure, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<T> Shuffle<T>([NotNull] this IEnumerable<T> me) {
			return me.OrderBy(x => Random.Next());
		}

		[NotNull, Pure, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<T> Shuffle<T>([NotNull] this IEnumerable<T> me, int countToTake) {
			return me.Shuffle()
				.Take(countToTake);
		}
	}
}