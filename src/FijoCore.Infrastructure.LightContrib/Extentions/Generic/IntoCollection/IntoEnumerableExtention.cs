using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Generic.IntoCollection {
	[PublicAPI]
	public static class IntoEnumerableExtention {
		[Pure, DebuggerStepThrough, LinqTunnel, PublicAPI]
		[Desc("returns a IEnumerable<T> with this a only entry")]
		public static System.Collections.Generic.IEnumerable<T> IntoEnumerable<T>(this T me) {
			yield return me;
		}
	}
}