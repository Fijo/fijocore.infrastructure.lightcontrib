using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	[PublicAPI]
	public static class ForEachExtention {
		[PublicAPI, DebuggerStepThrough]
		public static void ForEach<T>([NotNull] this IEnumerable<T> me, [NotNull] Action<T> action) {
			using (var enumerator = me.GetEnumerator()) enumerator.ForEach(action);
		}

		[PublicAPI, DebuggerStepThrough]
		public static void ForEach<T>([NotNull] this IEnumerable<T> me, [NotNull] Action<T, int> action) {
			using (var enumerator = me.GetEnumerator()) enumerator.ForEach(action);
		}

		[PublicAPI, DebuggerStepThrough]
		public static void ForEach<T>([NotNull] this IEnumerable<T> me, [NotNull] Action<T> action, bool executeParalell) {
			if (executeParalell) Parallel.ForEach(me, action);
			else me.ForEach(action);
		}

		#region Extended
		[PublicAPI, DebuggerStepThrough]
		public static void ForEachAndBetween<T>([NotNull] this IEnumerable<T> me, [NotNull] Action<T> action, [NotNull] Action betweenAction) {
			using (var enumerator = me.GetEnumerator()) enumerator.ForEachAndBetween(action, betweenAction);
		}

		[PublicAPI, DebuggerStepThrough]
		public static void ForEach<T>([NotNull] this IEnumerable<T> me, [NotNull] Action<T> firstAction, [NotNull] Action<T> action) {
			using (var enumerator = me.GetEnumerator()) enumerator.ForEach(firstAction, action);
		}
		#endregion
	}
}