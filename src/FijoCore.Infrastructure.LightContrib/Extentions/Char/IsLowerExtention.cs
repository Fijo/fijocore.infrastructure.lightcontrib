using System.Diagnostics;
using System.Globalization;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Char {
	[PublicAPI]
	public static class IsLowerExtention {
		[Pure, DebuggerStepThrough, PublicAPI]
		[Desc("checks if a char is in lower case using an invariant culture settings")]
		public static bool IsInvariantLower(this char me) {
			return me.InvariantToLower().Equals(me);
		}

		[Pure, DebuggerStepThrough, PublicAPI]
		[Desc("checks if a char is in lower case")]
		public static bool IsLower(this char me, [NotNull] CultureInfo culture) {
			return me.ToLower(culture).Equals(me);
		}
	}
}