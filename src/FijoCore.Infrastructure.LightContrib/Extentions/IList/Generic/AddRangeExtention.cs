using System.Collections.Generic;
using System.Diagnostics;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IList.Generic {
	[PublicAPI]
	public static class AddRangeExtention {
		[PublicAPI, DebuggerStepThrough]
		public static void AddRange<T>([NotNull] this IList<T> me, [NotNull] IEnumerable<T> collection, bool noLock = false) {
			InternalAddRange(me, collection, noLock);
		}

		[PublicAPI, DebuggerStepThrough]
		public static void AddRange<T>([NotNull] this IList<T> me, bool noLock = false, [NotNull] params T[] collection) {
			InternalAddRange(me, collection, noLock);
		}

		[NotNull, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<T> AddRangeReturn<T>([NotNull] this IList<T> me, [NotNull] IEnumerable<T> collection, bool noLock = false) {
			me.AddRange(collection, noLock);
			return me;
		}

		[NotNull, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<T> AddRangeReturn<T>([NotNull] this IList<T> me, bool noLock = false, [NotNull] params T[] collection) {
			me.AddRange(collection, noLock);
			return me;
		}

		[DebuggerStepThrough]
		private static void InternalAddRange<T>([NotNull] IList<T> me, [NotNull] IEnumerable<T> collection, bool noLock = false) {
			if(noLock) InternalAddRange(me, collection);
			else lock (me) InternalAddRange(me, collection);
		}
		
		[DebuggerStepThrough]
		private static void InternalAddRange<T>([NotNull] IList<T> me, [NotNull] IEnumerable<T> collection) {
			collection.ForEach(me.Add);
		}
	}
}