using System.Diagnostics;
using System.Globalization;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Char {
	[PublicAPI]
	public static class ToUpperExtention {
		[Pure, DebuggerStepThrough, PublicAPI]
		[Desc("Gets the uppered value of the current char (�me�) if any using an invariant culture settings. Otherwise it returns the current char.")]
		public static char InvariantToUpper(this char me) {
			return char.ToUpperInvariant(me);
		}

		[Pure, DebuggerStepThrough, PublicAPI]
		[Desc("Gets the uppered value of the current char (�me�) if any. Otherwise it returns the current char.")]
		public static char ToUpper(this char me, [NotNull] CultureInfo culture) {
			return char.ToUpper(me, culture);
		}
	}
}