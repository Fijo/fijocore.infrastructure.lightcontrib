using System;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Default.Exceptions {
	[PublicAPI, Serializable]
	public class InvalidCastException : System.InvalidCastException {
		[About("SourceInstanceType")]
		public Type SourceType { get; private set; }
		[About("CastedToType")]
		public Type TargetType { get; private set; }
		public object SourceInstance { get; private set; }

		public InvalidCastException(object sourceInstance, Type targetType, string message = default(string), Exception innerException = null) : this(sourceInstance.GetType(), targetType, message, innerException) {
			SourceInstance = sourceInstance;
		}
		
		public InvalidCastException(Type sourceType, Type targetType, string message = default(string), Exception innerException = null) : base(message, innerException) {
			SourceType = sourceType;
			TargetType = targetType;
		}
	}
}