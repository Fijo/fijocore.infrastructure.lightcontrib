using System.Collections.Generic;
using Fijo.Infrastructure.DesignPattern.Events;

namespace FijoCore.Infrastructure.LightContrib.Module.Transaction {
	public class Transaction {
		public readonly IList<IEvent> Events = new List<IEvent>();
	}
}