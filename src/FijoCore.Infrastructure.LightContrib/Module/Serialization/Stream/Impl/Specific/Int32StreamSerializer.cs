using System;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream.Impl.Specific {
	[UsedImplicitly]
	public class Int32StreamSerializer : FixedSizeStreamSerializer<int> {
		#region Overrides of FixedSizeStreamSerializer<int>
		protected override int Size { get { return 4; } }
		protected override byte[] Serialize(int obj) {
			return BitConverter.GetBytes(obj);
		}

		protected override int Deserialize(byte[] bytes) {
			return BitConverter.ToInt32(bytes, 0);
		}
		#endregion
	}
}