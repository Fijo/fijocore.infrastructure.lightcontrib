using System;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Default.Exceptions {
	[PublicAPI, Serializable]
	public class KeyAlreadyExistException<TSource, TKey> : KeyAlreadyExistException {
		[PublicAPI] public TSource SourceObj { get; set; }
		[PublicAPI] public TKey Key { get; set; }
		
		public KeyAlreadyExistException(TSource source, TKey key) : this(source, key, null) {}

		public KeyAlreadyExistException(TSource source, TKey key, string message) : this(source, key, message, null) {}

		public KeyAlreadyExistException(TSource source, TKey key, string message, Exception innerException) : base(key.ToString(), message, innerException) {
			SourceObj = source;
			Key = key;}
	}

	[PublicAPI, Serializable]
	public class KeyAlreadyExistException : Exception {
		[PublicAPI] public string KeyString { get; set; }

		public KeyAlreadyExistException(string keyString) {}

		public KeyAlreadyExistException(string keyString, string message) : base(message) {}

		public KeyAlreadyExistException(string keyString, string message, Exception innerException) : base(message, innerException) {}
	}
}