using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FijoCore.Infrastructure.LightContrib.Enums;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.RelativeOperationComparing.Wrapper {
	[UsedImplicitly]
	public class RelativeOperationComparingEnumerableWrapperService {
		private readonly ICollection<RelationOperation> _enumerableNeededFor = new[]
		                                                                       {
		                                                                       	RelationOperation.IsIn,
		                                                                       	RelationOperation.NotIn 
		                                                                       };

		public object MayRemoveEnumerable(RelationOperation relationOperation, object obj) {
			return !_enumerableNeededFor.Contains(relationOperation) && obj is IEnumerable
			       	? ((IEnumerable) obj).Cast<object>().Single()
			       	: obj;
		}
	}
}