using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IList.Generic {
	[PublicAPI]
	public static class ThirdExtention {
		[PublicAPI, DebuggerStepThrough]
		public static TSource Third<TSource>([NotNull] this IList<TSource> me) {
			return me.IndexTh(3);
		}
	}
}