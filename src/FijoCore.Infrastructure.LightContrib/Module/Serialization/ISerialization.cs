using FijoCore.Infrastructure.LightContrib.Enums;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Settings;
using JetBrains.Annotations;
using IO = System.IO;
using Text = System.Text;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization {
	public interface ISerialization {
		void Serialize(object obj, [NotNull] IO.Stream stream, SerializationFormatting formatting, [CanBeNull] Text.Encoding encoding, [CanBeNull] ISerializationSettings serializationSettings = null);
		void Serialize<T>(T obj, [NotNull] IO.Stream stream, SerializationFormatting formatting, [CanBeNull] Text.Encoding encoding, [CanBeNull] ISerializationSettings serializationSettings = null);
		object Deserialize([NotNull] IO.Stream stream, [CanBeNull] Text.Encoding encoding, [CanBeNull] ISerializationSettings serializationSettings = null);
		T Deserialize<T>([NotNull] IO.Stream stream, [CanBeNull] Text.Encoding encoding, [CanBeNull] ISerializationSettings serializationSettings = null);
	}
}