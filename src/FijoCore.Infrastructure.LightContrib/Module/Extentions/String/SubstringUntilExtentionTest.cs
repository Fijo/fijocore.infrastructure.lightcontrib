using FijoCore.Components.Extentions.String;
using NUnit.Framework;

namespace FijoCore.ComponentTest.Extentions.String
{
    [TestFixture]
    public class SubstringUntilExtentionTest
    {
        [Test]
        public void SimpleTest()
        {
            Assert.AreEqual("test hallo".SubstringUntil(' '), "test");
            Assert.AreEqual("test hallo".SubstringUntil("ha"), "test ");
            Assert.AreEqual("test hallo".SubstringUntil("lol"), "test hallo");
            Assert.AreEqual("test hallo".SubstringUntil('_'), "test hallo");
        }

        [Test]
        public void AdditionalValueTest()
        {
            Assert.AreEqual("test hallo".SubstringUntil(' ', 1), "test ");
            Assert.AreEqual("test hallo".SubstringUntil("ha", 2), "test ha");
            Assert.AreEqual("test hallo".SubstringUntil("lol", 1), "test hallo");
            Assert.AreEqual("test hallo".SubstringUntil('_', 4), "test hallo");

            Assert.AreEqual("test hallo".SubstringUntil(' ', -3), "t");
            Assert.AreEqual("test hallo".SubstringUntil(' ', -4), "");
            Assert.AreEqual("test hallo".SubstringUntil(' ', -5), "");
            Assert.AreEqual("test hallo".SubstringUntil("ha", -1), "test");
            Assert.AreEqual("test hallo".SubstringUntil("ha", -5), "");
            Assert.AreEqual("test hallo".SubstringUntil("ha", -8), "");
            Assert.AreEqual("test hallo".SubstringUntil("lol", -3), "test hallo");
            Assert.AreEqual("test hallo".SubstringUntil('_', -4), "test hallo");
        }
    }
}