using System.Collections.Generic;
using System.Diagnostics;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic.IntoCollection;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	[PublicAPI]
	public static class AddExtention {
		[NotNull, Pure, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<T> AddReturn<T>([NotNull] this IEnumerable<T> me, T item) {
			return me.AddRangeReturn(item.IntoEnumerable());
		}
	}
}