using System;
using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator {
	[PublicAPI]
	public static class IntoGroupExtention {
		[NotNull, Pure, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<IEnumerable<TSource>> IntoGroup<TSource>([NotNull] this IEnumerable<TSource> me, int count) {
			using(var enumerator = me.GetEnumerator()) return enumerator.IntoGroup(count);
		}

		[NotNull, Pure, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<IEnumerable<TSource>> IntoGroup<TSource>([NotNull] this IEnumerator<TSource> me, int count) {
			if(count < 1) throw new ArgumentOutOfRangeException("count", count, "must be at least 1");
			while(me.MoveNext())
				yield return GetNextGroup(me, count);
		}
		
		[NotNull, Pure, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<TSource> GetNextGroup<TSource>([NotNull] IEnumerator<TSource> me, int count) {
			Debug.Assert(count > 0, "count must be at least 1");
			yield return me.Current;
			for(var i = 1; i != count && me.MoveNext(); i++)
				yield return me.Current;
		}
	}
}