using System.Collections.Generic;
using Fijo.Infrastructure.DesignPattern.Store.Generic;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Extentions.IStore.Generic;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Generics.Lookup {
	[PublicAPI]
	public abstract class GenericLookup<TBaseType, TKey> : IGenericLookup<TBaseType, TKey> {
		private readonly InternalGS _internalGS;

		[AboutName("G", "Generic")]
		[AboutName("S", "Store")]
		private class InternalGS : GenericStore<TBaseType, TKey> {
			private readonly GenericLookup<TBaseType, TKey> _genericLookup;

			public InternalGS(GenericLookup<TBaseType, TKey> genericLookup) {
				_genericLookup = genericLookup;
			}

			#region Overrides of DefaultStoreBase<TBaseType,TKey,IDictionary<TKey,TBaseType>>
			public override TKey GetKey(TBaseType entity) {
				return _genericLookup.GetKey(entity);
			}
			#endregion
		}

		protected GenericLookup() {
			_internalGS = new InternalGS(this);
			_internalGS.StoreMany(Resolve<TBaseType>());
		}

		protected abstract TKey GetKey(TBaseType entity);
		protected abstract IEnumerable<TBaseType> Resolve<TObj>();

		public virtual TResult Get<TResult>(TKey key) where TResult : TBaseType {
			return _internalGS.Get<TResult>(key);
		}

		public virtual TBaseType Get(TKey key) {
			return _internalGS.Get(key);
		}
	}
}