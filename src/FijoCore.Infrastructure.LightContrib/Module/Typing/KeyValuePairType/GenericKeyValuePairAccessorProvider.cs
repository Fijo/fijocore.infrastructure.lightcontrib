using System;
using System.Collections.Generic;
using System.Diagnostics;
using FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic;

namespace FijoCore.Infrastructure.LightContrib.Module.Typing.KeyValuePairType {
	public class GenericKeyValuePairAccessorProvider : IGenericKeyValuePairAccessorProvider {
		private readonly IDictionary<Type, GenericKeyValuePairAccessor> _content = new Dictionary<Type, GenericKeyValuePairAccessor>();

		#region Implementation of IGenericKeyValuePairAccessorProvider
		public GenericKeyValuePairAccessor Get(Type keyValuePairType) {
			return _content.GetOrCreate(keyValuePairType, () => {
				#region PreCondition
				Debug.Assert(keyValuePairType.IsGenericType && !keyValuePairType.IsGenericTypeDefinition && keyValuePairType.GetGenericTypeDefinition() == typeof(KeyValuePair<,>));
				#endregion
				return new GenericKeyValuePairAccessor(GetPropertyGetMethod(keyValuePairType, "Key"), GetPropertyGetMethod(keyValuePairType, "Value"));
			});
		}

		private Func<object, object> GetPropertyGetMethod(Type keyValuePairType, string name) {
			var method = keyValuePairType.GetProperty(name).GetGetMethod();
			return me => method.Invoke(me, null);
		}
		#endregion
	}
}