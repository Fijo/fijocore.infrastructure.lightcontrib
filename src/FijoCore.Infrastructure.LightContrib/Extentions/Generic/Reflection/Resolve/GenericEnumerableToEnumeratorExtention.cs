using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using FijoCore.Infrastructure.LightContrib.Extentions.Type;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Generic.Reflection.Resolve {
	public static class GenericEnumerableToEnumeratorExtention {
		private readonly static System.Type GenericEnumerableType = typeof (IEnumerable<>);
		private readonly static MethodInfo GetEnumeratorMethod = GenericEnumerableType.GetMethod("GetEnumerator");

		[DebuggerStepThrough]
		public static System.Collections.IEnumerator GenericEnumerableToEnumerator<T>(this T me) {
			#region PreCondition
			Debug.Assert(me.IsUndefined());
			Debug.Assert(me.GetType().ImplementsInteface(GenericEnumerableType));
			#endregion
			var type = me.GetType();
			var genericType = type.GetGenericArguments().FirstOrDefault();
			if (genericType == null) throw new InvalidCastException();
			return (System.Collections.IEnumerator) GetEnumeratorMethod.Invoke(me, null);
		}
	}
}