﻿using System.Reflection;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Cloning {
	public interface ICloneService {
		[NotNull] T Clone<T>([NotNull] T source, CloningType type = CloningType.Default);
		[NotNull] object Clone([NotNull] object source, CloningType type = CloningType.Default);
		[NotNull] T Clone<T>([NotNull] T source, ICloning cloning);
		[NotNull] object Clone([NotNull] object source, ICloning cloning);
		[NotNull] T CloneDyn<T>([NotNull] T source, CloningType type = CloningType.Default);
		[NotNull] T CloneDyn<T>([NotNull] T source, ICloning cloning);
	}
}