using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.ICollection.Generic {
	[PublicAPI]
	public static class AnyExtention {
		[Pure, PublicAPI, DebuggerStepThrough]
		public static bool Any<T>(this ICollection<T> me) {
			return me.Count != 0;
		}
	}
}