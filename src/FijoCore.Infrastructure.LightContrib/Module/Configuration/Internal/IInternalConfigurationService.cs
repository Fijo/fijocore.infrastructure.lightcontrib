namespace FijoCore.Infrastructure.LightContrib.Module.Configuration.Internal {
	public interface IInternalConfigurationService {
		T Get<T>(string key);
	}
}