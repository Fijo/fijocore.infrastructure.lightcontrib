using System;
using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Enum {
	[PublicAPI]
	public static class GetNameExtention {
		[CanBeNull, Pure, DebuggerStepThrough, PublicAPI]
		[Link("http://msdn.microsoft.com/en-en/library/system.enum.getname.aspx")]
		[Desc("Returns the property name that stands for the value the instance of an enum has.")]
		public static string GetName<T>(this T me, object value) where T : struct, IConvertible {
			#region PreCondition
			Debug.Assert(typeof(T).IsEnum, "typeof(T).IsEnum");
			#endregion
			return InternalGetName(typeof (T), value);
		}

		[CanBeNull, Pure, DebuggerStepThrough, PublicAPI]
		[Link("http://msdn.microsoft.com/en-en/library/system.enum.getname.aspx")]
		[Desc("Returns the property name that stands for the value the instance of an enum has.")]
		public static string GetName(this System.Type me, [NotNull] object value) {
			#region PreCondition
			Debug.Assert(me.IsEnum, "typeof(T).IsEnum");
			#endregion
			return InternalGetName(me, value);
		}

		[CanBeNull, Pure]
		private static string InternalGetName([NotNull] System.Type type, [NotNull] object value) {
			return System.Enum.GetName(type, value);
		}
	}
}