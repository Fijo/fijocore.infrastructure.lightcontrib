using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Fijo.Infrastructure.DesignPattern.Lazy.Interface;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Module.Encoding;
using FijoCore.Infrastructure.LightContrib.Module.Lazy;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Stream {
	public abstract class StreamServiceBase : IStreamService {
		private readonly ILazy<IEncodingProvider> _encodingProvider;

		public StreamServiceBase(ILazy<IEncodingProvider> encodingProvider) {
			_encodingProvider = encodingProvider;
		}

		#region String
		#region Read
		public virtual string ReadToString(System.IO.Stream stream, bool autodetect = false) {
			return GetReader(stream, autodetect).ReadToEnd();
		}

		public virtual string ReadToString(System.IO.Stream stream, System.Text.Encoding encoding) {
			return GetReader(stream, encoding).ReadToEnd();
		}
		#endregion

		#region Write
		public void WriteToStream(System.IO.Stream stream, string str) {
			WriteToStream(stream, str, null);
		}

		public virtual void WriteToStream(System.IO.Stream stream, string str, System.Text.Encoding encoding) {
			var streamWriter = GetWriter(stream, encoding);
			streamWriter.Write(str);
			streamWriter.Flush();
			// do not dispose the writer, because it will dispose the base stream
		}
		#endregion
		#endregion

		#region Byte
		#region Read
		#region Bytes
		public byte[] ReadToBytes(System.IO.Stream stream, int count, int bufferSize = 1024) {
			#region PreCondtition
			Debug.Assert(count >= 0);
			#endregion
			lock(stream)
				return InternalReadToBytes(stream, count, bufferSize);
		}

		public virtual byte[] ReadToBytes(System.IO.Stream stream, int bufferSize = 1024) {
			lock(stream) {
				// ToDo handle overflow using an exception when converting too big long length to int
				return ReadToBytes(stream, (int) stream.Length, bufferSize);
			}
		}

		[NotNull]
		protected abstract byte[] InternalReadToBytes([NotNull] System.IO.Stream stream, int count, int bufferSize = 1024);
		#endregion

		public virtual IEnumerable<byte> ReadToBytesSequence(System.IO.Stream stream, int bufferSize = 1024) {
			#region PreCondtition
			Debug.Assert(bufferSize > 0);
			#endregion
			lock(stream) {
				// ToDo handle overflow using an exception when converting too big long length to int
				return ReadToBytes(stream, (int) stream.Length);
			}
		}

		public abstract IEnumerable<byte> ReadToBytesSequence(System.IO.Stream stream, int count, int bufferSize = 1024);
		#endregion
		
		#region Write
		public void WriteToStream(System.IO.Stream stream, byte[] bytes) {
			lock(stream)
				WriteToStream(stream, bytes, bytes.Length);
		}

		public virtual void WriteToStream(System.IO.Stream stream, byte[] bytes, int count) {
			#region PreCondtition
			Debug.Assert(count >= 0);
			Debug.Assert(count <= bytes.Length);
			#endregion
			lock(stream)
				stream.Write(bytes, 0, count);
		}

		public void WriteToStream(System.IO.Stream stream, IEnumerable<byte> bytes) {
			foreach (var b in bytes)
				stream.WriteByte(b);
		}
		#endregion
		#endregion

		#region Char
		#region Read
		public virtual char[] ReadToChars(System.IO.Stream stream, bool autodetect = false) {
			lock (stream)
				return InternalReadToBytes(stream, GetReader(stream, autodetect));
		}

		public virtual char[] ReadToChars(System.IO.Stream stream, System.Text.Encoding encoding) {
			lock (stream)
				return InternalReadToBytes(stream, GetReader(stream, encoding));
		}

		[NotNull]
		protected virtual char[] InternalReadToBytes([NotNull] System.IO.Stream stream, [NotNull] StreamReader streamReader) {
			lock (stream)
				lock (streamReader) {
					var length = (int) stream.Length;
					var chars = new char[length];
					streamReader.Read(chars, 0, length);
					return chars;
				}
		}
		#endregion
		#endregion

		#region Stream
		#region Reader
		public StreamReader GetReader(System.IO.Stream stream, bool autodetect = false) {
			return autodetect
				       ? CreateReader(stream)
				       : GetReader(stream, null);
		}

		public StreamReader GetReader(System.IO.Stream stream, System.Text.Encoding encoding) {
			return CreateReader(stream, MayGetDefault(encoding));
		}

		[NotNull, Pure]
		protected virtual StreamReader CreateReader([NotNull] System.IO.Stream stream) {
			return new StreamReader(stream, true);
		}
		
		[NotNull, Pure]
		protected virtual StreamReader CreateReader([NotNull] System.IO.Stream stream, [NotNull] System.Text.Encoding encoding) {
			return new StreamReader(stream, encoding);
		}
		#endregion

		#region Writer
		public StreamWriter GetWriter(System.IO.Stream stream) {
			return GetWriter(stream, null);
		}

		public StreamWriter GetWriter(System.IO.Stream stream, System.Text.Encoding encoding) {
			return CreateWriter(stream, MayGetDefault(encoding));
		}

		[NotNull, Pure]
		protected virtual StreamWriter CreateWriter([NotNull] System.IO.Stream stream, [NotNull] System.Text.Encoding encoding) {
			return new StreamWriter(stream, encoding);
		}
		#endregion

		#region ToStream
		[Pure]
		[Desc("cannot be virtual because it can only be one impl in the StreamFacade because of the fact, that this func don�t contain an arg of type Stream - so it can�t be decidet which impl to use - it will always use the one bindet to IDefaultStreamService")]
		public System.IO.Stream GetStream(StreamWriter streamWriter) {
			return streamWriter.BaseStream;
		}

		[Pure]
		[Desc("cannot be virtual because it can only be one impl in the StreamFacade because of the fact, that this func don�t contain an arg of type Stream - so it can�t be decidet which impl to use - it will always use the one bindet to IDefaultStreamService")]
		public System.IO.Stream GetStream(StreamReader streamReader) {
			return streamReader.BaseStream;
		}
		#endregion
		#endregion
		
		#region private until
		[NotNull]
		protected System.Text.Encoding MayGetDefault([CanBeNull] System.Text.Encoding encoding) {
			return encoding ?? _encodingProvider.Get().GetDefault();
		}
		#endregion
	}
}