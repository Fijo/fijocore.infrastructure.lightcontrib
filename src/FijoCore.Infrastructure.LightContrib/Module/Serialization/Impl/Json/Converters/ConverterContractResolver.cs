using System;
using Newtonsoft.Json.Serialization;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization.Impl.Json.Converters {
	public class ConverterContractResolver : DefaultContractResolver {
		private readonly MemberInfoBinaryConverter _memberInfoBinaryConverter;

		public ConverterContractResolver(MemberInfoBinaryConverter memberInfoBinaryConverter) {
			_memberInfoBinaryConverter = memberInfoBinaryConverter;
		}

		protected override JsonContract CreateContract(Type objectType) {
			var contract = base.CreateContract(objectType);

			// this will only be called once and then cached
			if (_memberInfoBinaryConverter.CanConvert(objectType))
				contract.Converter = _memberInfoBinaryConverter;

			return contract;
		}
	}
}