using System.Collections.Generic;
using System.Diagnostics;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.ISet.Generic {
	[PublicAPI]
	public static class AddRangeExtention {
		[PublicAPI, DebuggerStepThrough]
		public static void AddRange<T>([NotNull] this ISet<T> me, [NotNull] IEnumerable<T> collection) {
			InternalAddRange(me, collection);
		}

		[PublicAPI, DebuggerStepThrough]
		public static void AddRange<T>([NotNull] this ISet<T> me, [NotNull] params T[] collection) {
			InternalAddRange(me, collection);
		}

		[NotNull, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<T> AddRangeReturn<T>([NotNull] this ISet<T> me, [NotNull] IEnumerable<T> collection) {
			me.AddRange(collection);
			return me;
		}

		[NotNull, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<T> AddRangeReturn<T>([NotNull] this ISet<T> me, [NotNull] params T[] collection) {
			me.AddRange(collection);
			return me;
		}

		[DebuggerStepThrough]
		private static void InternalAddRange<T>([NotNull] ISet<T> me, [NotNull] IEnumerable<T> collection) {
			lock (me) collection.ForEach(me.AddSecure);
		}
	}
}