using System;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Default.Exceptions;
using FijoCore.Infrastructure.LightContrib.Delegates;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Default.Service.Try {
	[About("IExlessTryForTimeService")]
	public interface IExlessTryForService {
		[Note("The execution time can be some time more than you used as your �timeout�. Thats because the method will always let the �func� execute unitl it ends - it do not interupt it in any way. The timeout is only checked when the func has been executed where not successfull.")]
		[About("ExlessTryForTime")]
		TResult ExlessTryFor<TResult, TRetryTimeOutException>([NotNull] Func<TRetryTimeOutException> exceptionFactory, [NotNull] TryFunc<TResult> func, TimeSpan timeout)
			where TRetryTimeOutException : RetryTimeOutException;

		[Note("The execution time can be some time more than you used as your �timeout�. Thats because the method will always let the �func� execute unitl it ends - it do not interupt it in any way. The timeout is only checked when the func has been executed where not successfull.")]
		[About("ExlessTryForTime")]
		TResult ExlessTryFor<TResult, TRetryTimeOutException>([NotNull] TryFunc<TResult> func, TimeSpan timeout)
			where TRetryTimeOutException : RetryTimeOutException, new();
		
		[Note("The execution time can be some time more than you used as your �timeout�. Thats because the method will always let the �func� execute unitl it ends - it do not interupt it in any way. The timeout is only checked when the func has been executed where not successfull.")]
		[About("ExlessTryForTime")]
		void ExlessTryFor<TRetryTimeOutException>([NotNull] Func<TRetryTimeOutException> exceptionFactory, [NotNull] TryFunc func, TimeSpan timeout)
			where TRetryTimeOutException : RetryTimeOutException;

		[Note("The execution time can be some time more than you used as your �timeout�. Thats because the method will always let the �func� execute unitl it ends - it do not interupt it in any way. The timeout is only checked when the func has been executed where not successfull.")]
		[About("ExlessTryForTime")]
		void ExlessTryFor<TRetryTimeOutException>([NotNull] TryFunc func, TimeSpan timeout)
			where TRetryTimeOutException : RetryTimeOutException, new();
	}
}