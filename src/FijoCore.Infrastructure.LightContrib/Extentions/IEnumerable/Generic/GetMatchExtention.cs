using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.Infrastructure.Documentation.Enums;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	[PublicAPI]
	[About("GetEqualObjectExtention")]
	public static class GetMatchExtention {
		[About("GetEqualObject")]
		[Note("the result will be equal (using the equality comparer if you used one) with �item�.")]
		[PublicAPI, DebuggerStepThrough]
		public static T GetMatch<T>([NotNull, ThreadSafety(ThreadSafety.ReqEnumeration | ThreadSafety.ReqParallelEnumeration)] this IEnumerable<T> me, T item, [CanBeNull] IEqualityComparer<T> comparer = null) {
			if(comparer == null) comparer = EqualityComparer<T>.Default;
			return me.AsParallel().Single(x => comparer.Equals(item, x));
		}
	}
}