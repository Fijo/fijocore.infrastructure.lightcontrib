using System.Diagnostics;
using System.Globalization;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Double {
	[PublicAPI]
	public static class InvariantToStringExtention {
		[Pure, DebuggerStepThrough, PublicAPI]
		[Desc("Converts a double to string using invariant culture settings")]
		public static string InvariantToString(this double me) {
			return me.ToString(CultureInfo.InvariantCulture);
		}
	}
}