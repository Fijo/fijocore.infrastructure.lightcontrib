﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FijoCore.Infrastructure.LightContrib.Module.Lookup {
	public class ArrayLookup<TKey, TValue> : ListLookupBase<TKey, TValue> where TKey : IConvertible {
		#region Overrides of ListLookupBase<TKey,TValue>
		public ArrayLookup(IEnumerable<KeyValuePair<TKey, TValue>> collection) : base(collection) {}

		protected override IList<T> CreateList<T>(IEnumerable<T> collection) {
			return collection.ToArray();
		}

		public override void Add(KeyValuePair<TKey, TValue> item) {
			throw GetNotSupportedException();
		}

		public override void Clear() {
			throw GetNotSupportedException();
		}

		public override bool Remove(KeyValuePair<TKey, TValue> item) {
			throw GetNotSupportedException();
		}

		public override bool IsReadOnly { get { return false; } }
		public override void Add(TKey key, TValue value) {
			throw GetNotSupportedException();
		}

		public override bool Remove(TKey key) {
			throw GetNotSupportedException();
		}

		protected override void AddItem(int index, KeyValuePair<TKey, TValue> keyValuePair) {
			throw GetNotSupportedException();
		}
		#endregion

		private NotSupportedException GetNotSupportedException() {
			return new NotSupportedException("The Dictionary has a fixed size");
		}
	}
}