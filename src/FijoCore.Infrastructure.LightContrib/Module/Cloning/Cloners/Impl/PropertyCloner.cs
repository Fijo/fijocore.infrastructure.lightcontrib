using System.Collections.Generic;
using System.Reflection;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;

namespace FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners.Impl {
	public abstract class PropertyCloner<T> : Cloner<T> {
		private readonly ICollection<PropertyInfo> _properties;

		public abstract IEnumerable<PropertyInfo> GetProperties();

		public PropertyCloner() {
			_properties = GetProperties().Execute();
		}

		public override void Copy(T source, T result, ICloning cloning) {
			//Parallel.ForEach(
			_properties.ForEach(x => x.SetValue(result, cloning.Pass(x.GetValue(source, null)), null));
		}
	}
}