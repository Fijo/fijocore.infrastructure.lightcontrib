﻿using System.Collections.Generic;
using FijoCore.Infrastructure.LightContrib.Delegates;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Factories {
	[UsedImplicitly]
	public class EnumerableFactory : IEnumerableFactory {
		private readonly IEnumeratorFactory _enumeratorFactory;

		public EnumerableFactory(IEnumeratorFactory enumeratorFactory) {
			_enumeratorFactory = enumeratorFactory;
		}

		public IEnumerable<int> CreateEndless(int start) {
			return _enumeratorFactory.CreateEndless(start).ToEnumerable();
		}

		public IEnumerable<T> CreateExpression<T>(TryFunc<T> expression) {
			return _enumeratorFactory.CreateExpression(expression).ToEnumerable();
		}
	}
}