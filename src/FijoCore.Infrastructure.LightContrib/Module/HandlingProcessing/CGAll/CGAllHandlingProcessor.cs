using System.Collections.Generic;
using System.Linq;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.CGAll.Interface;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Parallelizable;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Parallelizable.Interfaces;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.CGAll {
	public class CGAllHandlingProcessor<TSource, TResult> : ParallelizableHandlingProcessor<ICGAllHandling<TSource, TResult>>, ICGAllHandlingProcessor<TSource, TResult> {
		public CGAllHandlingProcessor(IParallelizableHandlingProvider<ICGAllHandling<TSource, TResult>> handlingRepository) : base(handlingRepository) {}

		protected virtual IEnumerable<TResult> ProcessHandlings([NotNull] IEnumerable<ICGAllHandling<TSource, TResult>> handlings, bool isParallelizable, TSource source) {
			return handlings.Opt(isParallelizable,
			                     x => x.Where(y => y.IsUsed(source)).Select(y => y.Get(source)),
			                     x => x.Where(y => y.IsUsed(source)).Select(y => y.Get(source)));
		}

		#region Implementation of ICGAllHandlingProcessor<TSource,TResult>
		public IEnumerable<TResult> Get(TSource source, bool enableParallelization = true) {
			return ProcessHandlings(Handlings, IsParallelizable(enableParallelization), source);
		}

		public IEnumerable<TResult> Get(IEnumerable<ICGAllHandling<TSource, TResult>> customHandlings, TSource source, bool enableParallelization = true) {
			if (customHandlings == null) return Get(source, enableParallelization);
			customHandlings = CustomProcessHandlingsWrapper(customHandlings).Execute();
			return ProcessHandlings(customHandlings.Concat(Handlings),
			                        IsParallelizable(enableParallelization, customHandlings), source);
		}
		#endregion

		#region IsParallelizable
		protected virtual bool IsParallelizable(bool enableParallelization) {
			return SupportParallelization && enableParallelization;
		}

		protected virtual bool IsParallelizable(bool enableParallelization, IEnumerable<ICGAllHandling<TSource, TResult>> customHandlings) {
			return IsParallelizable(enableParallelization) && GetSupportsParallelization(customHandlings);
		}
		#endregion
	}
}