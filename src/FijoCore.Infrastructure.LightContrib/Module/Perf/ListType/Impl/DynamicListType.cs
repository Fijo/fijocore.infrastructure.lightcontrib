using System.Collections.Generic;
using FijoCore.Infrastructure.LightContrib.Module.Perf.ListType.Interface;

namespace FijoCore.Infrastructure.LightContrib.Module.Perf.ListType.Impl {
	public class DynamicListType : IListType {
		#region Implementation of IListType
		public void Add<T>(IList<T> list, int index, T item) {
			list.Add(item);
		}

		public IList<T> Create<T>(int size) {
			return new List<T>();
		}
		#endregion
	}
}