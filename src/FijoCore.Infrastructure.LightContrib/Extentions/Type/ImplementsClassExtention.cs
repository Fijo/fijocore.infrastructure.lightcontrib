using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;
using SType = System.Type;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Type {
	[PublicAPI]
	public static class ImplementsClassExtention {
		[Pure, PublicAPI, DebuggerStepThrough]
		[Desc("returns if this class type implements a given class type")]
		public static bool ImplementsClass([NotNull] this SType me, [NotNull] SType baseType) {
			return InternalImplementsClass(me, baseType);
		}

		[Pure, DebuggerStepThrough]
		private static bool InternalImplementsClass([NotNull] SType type, [NotNull] SType baseType) {
			var currentBaseType = type.BaseType;
			return currentBaseType != null && (currentBaseType == baseType || InternalImplementsClass(currentBaseType, baseType));
		}

		[Pure, PublicAPI, DebuggerStepThrough]
		[Desc("returns if this class type implements a given class type")]
		public static bool ImplementsClass<TBaseType>([NotNull] this SType me) {
			return ImplementsClass(me, typeof(TBaseType));
		}
	}
}