using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Parallelizable.Interfaces;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Simple.Interface {
	[Note("This class was called ´IActionMethodeManager´ in the past.")]
	public interface ISimpleHandlingProcessor<T> : IParallelizableHandlingProcessor<ISimpleHandling<T>> {
		void Process(T source, [Desc("May execute tha Handlings parallel if possible")] bool enableParallelization = true);
		void Process([CanBeNull] IEnumerable<ISimpleHandling<T>> customSimpleHandlings, T source, [Desc("May execute tha Handlings parallel if possible")] bool enableParallelization = true);
	}
}