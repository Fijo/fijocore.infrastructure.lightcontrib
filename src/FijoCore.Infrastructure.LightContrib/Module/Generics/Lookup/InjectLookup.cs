using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using JetBrains.Annotations;
using Ninject;

namespace FijoCore.Infrastructure.LightContrib.Module.Generics.Lookup {
	[PublicAPI, About("InjectGenericLookup")]
	public abstract class InjectLookup<TBaseType, TKey> : GenericLookup<TBaseType, TKey> {
		protected override IEnumerable<TBaseType> Resolve<TObj>() {
			return Kernel.Inject.GetAll<TBaseType>();
		}
	}
}