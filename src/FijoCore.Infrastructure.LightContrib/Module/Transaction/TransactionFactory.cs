using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Transaction {
	[PublicAPI]
	public class TransactionFactory : ITransactionFactory {
		#region Implementation of ITransactionFactory
		public Transaction CreateTransaction() {
			return new Transaction();
		}
		#endregion
	}
}