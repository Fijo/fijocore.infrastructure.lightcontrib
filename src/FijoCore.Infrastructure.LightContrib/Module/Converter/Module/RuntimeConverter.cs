﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Enums;
using JetBrains.Annotations;
#if DEBUG
using FijoCore.Infrastructure.LightContrib.Extentions.Char;
#endif

namespace FijoCore.Infrastructure.LightContrib.Module.Converter.Module {
	[PublicAPI, Service]
	public class RuntimeConverter {
		[Dto]
		private class RuntimeVersionCheckEntry {
			public readonly float MinValue;
			public readonly float MaxValue;
			public readonly Runtime Runtime;

			public RuntimeVersionCheckEntry(float minValue, float maxValue, Runtime runtime) {
				MinValue = minValue;
				MaxValue = maxValue;
				Runtime = runtime;
			}
		}

		private readonly ICollection<RuntimeVersionCheckEntry> _runtimeVersions = new List<RuntimeVersionCheckEntry>
		{
			new RuntimeVersionCheckEntry(0.0f, 1.0f, Runtime.Net1_0),
			new RuntimeVersionCheckEntry(1.1f, 1.9f, Runtime.Net1_1),
			new RuntimeVersionCheckEntry(2.0f, 2.9f, Runtime.Net2_0),
			new RuntimeVersionCheckEntry(3.0f, 3.4f, Runtime.Net3_0),
			new RuntimeVersionCheckEntry(3.5f, 3.9f, Runtime.Net3_5),
			new RuntimeVersionCheckEntry(4.0f, 4.4f, Runtime.Net4_0),
			new RuntimeVersionCheckEntry(4.5f, 4.9f, Runtime.Net4_5)
		};

		[Pure, PublicAPI]
		[Desc("Expecting a format for arg ´runtime´ as given by Assembly.ImageRuntimeVersion")]
		public float ConvertToFloat(string runtime, bool onlyTheFirstSubversionDigit = true) {
			#region PreCondition
			RuntimePreCondition(runtime);
			#endregion
			var versionEnumerator = runtime.Substring(1).Split('.').Take(2).GetEnumerator();
			Debug.Assert(versionEnumerator.MoveNext());
			var mainVersion = versionEnumerator.Current;
			Debug.Assert(versionEnumerator.MoveNext());
			var subVersion = versionEnumerator.Current;
			if (onlyTheFirstSubversionDigit) subVersion = subVersion.Substring(0, 1);
			return float.Parse(string.Format("{0}.{1}", mainVersion, subVersion), CultureInfo.InvariantCulture);
		}

		[Pure, PublicAPI]
		[Desc("Expecting a format for arg ´runtime´ as given by Assembly.ImageRuntimeVersion")]
		public Runtime ConvertToRuntime(string runtime) {
			#region PreCondition
			RuntimePreCondition(runtime);
			#endregion
			return ConvertRuntimeByFloat(ConvertToFloat(runtime));
		}

		[Pure]
		private Runtime ConvertRuntimeByFloat(float runtimeVersion) {
			var runtimeVersionCheckEntries = _runtimeVersions.Where(runtimeVersionCheckEntry => runtimeVersion >= runtimeVersionCheckEntry.MinValue &&
			                                                                                    runtimeVersion <= runtimeVersionCheckEntry.MaxValue);
			foreach (var runtimeVersionCheckEntry in runtimeVersionCheckEntries)
				return runtimeVersionCheckEntry.Runtime;

			throw new NotSupportedException(GetNotSupportedMessageForInvalidRuntimeVersion(runtimeVersion));
		}

		[NotNull, Pure]
		private string GetNotSupportedMessageForInvalidRuntimeVersion(float runtimeVersion) {
			return string.Format("Invalid or unknown runtime version ´{0}´", runtimeVersion);
		}

		#region Contracts
		[Conditional("DEBUG")]
		private void RuntimePreCondition(string runtime) {
			#if DEBUG
			Debug.Assert(!string.IsNullOrWhiteSpace(runtime), "Invalid runtime version format - cannot be null or whitespace");
			Debug.Assert(runtime[0] == 'v', @"Invalid runtime version format - must start with ""v""");
			Debug.Assert(runtime.Substring(1).Split('.').Count() > 1, "Invalid runtime version format - must contain at least one point");
			Debug.Assert(runtime.Substring(1).Split('.').Take(2).All(x => !string.IsNullOrWhiteSpace(x)),
			             "Invalid runtime version format - from part after the the ´v´ if you split it by ´.´ the first 2 entries can´t be null or whitespace");
			Debug.Assert(runtime.Substring(1).Split('.').Take(2).All(IsNumeric), "Invalid runtime version format - from part after the the ´v´ if you split it by ´.´ the first 2 entries must be nummeric");
			#endif
		}
		#endregion

		#region Debug Contract Helper
		#if DEBUG
		private bool IsNumeric(string str) {
			return str.All(IsNumeric);
		}

		private bool IsNumeric(char c) {
			return c.IsNumeric();
		}
		#endif
		#endregion
	}
}
