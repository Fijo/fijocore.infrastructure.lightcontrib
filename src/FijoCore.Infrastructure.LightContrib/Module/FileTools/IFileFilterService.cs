using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.FileTools {
	[Service]
	public interface IFileFilterService {
		[NotNull, Pure, PublicAPI]
		IEnumerable<string> FilterByExt([NotNull] IEnumerable<string> files, string ext);

		[Pure, PublicAPI]
		bool HasExt(string file, string ext);
	}
}