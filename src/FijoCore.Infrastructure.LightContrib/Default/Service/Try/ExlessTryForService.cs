using System;
using System.Diagnostics;
using FijoCore.Infrastructure.LightContrib.Default.Exceptions;
using FijoCore.Infrastructure.LightContrib.Delegates;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Default.Service.Try {
	[UsedImplicitly]
	public class ExlessTryForService : ExlessTryServiceBase<long, int, RetryTimeOutException>, IExlessTryForService {
		#region Overrides of ExlessTryServiceBase<TTryleOutEx>
		protected override void InternalPreCondition(int maxTries) {
			PreCondition(maxTries);
		}

		protected override bool HasNextTry<TResult, TRetiesReachedException>(int maxTries, ref long @try) {
			return DateTime.Now.Ticks < maxTries + maxTries;
		}

		protected override long GetStartTry() {
			return DateTime.Now.Ticks;
		}

		protected override TRetiesReachedException CreateException<TRetiesReachedException>(int maxTries) {
			return new TRetiesReachedException {RetryLimit = maxTries};
		}
		#endregion
		#region Implementation of IExlessTryForService
		public TResult ExlessTryFor<TResult, TRetryTimeOutException>(Func<TRetryTimeOutException> exceptionFactory, TryFunc<TResult> func, TimeSpan timeout) where TRetryTimeOutException : RetryTimeOutException {
			return InternalExlessTry(exceptionFactory, func, GetTicks(timeout));
		}

		public TResult ExlessTryFor<TResult, TRetryTimeOutException>(TryFunc<TResult> func, TimeSpan timeout) where TRetryTimeOutException : RetryTimeOutException, new() {
			return InternalExlessTry<TResult, TRetryTimeOutException>(func, GetTicks(timeout));
		}

		public void ExlessTryFor<TRetryTimeOutException>(Func<TRetryTimeOutException> exceptionFactory, TryFunc func, TimeSpan timeout) where TRetryTimeOutException : RetryTimeOutException {
			InternalExlessTry(exceptionFactory, func, GetTicks(timeout));
		}

		public void ExlessTryFor<TRetryTimeOutException>(TryFunc func, TimeSpan timeout) where TRetryTimeOutException : RetryTimeOutException, new() {
			InternalExlessTry<TRetryTimeOutException>(func, GetTicks(timeout));
		}
		#endregion

		private int GetTicks(TimeSpan timeout) {
			#region PreCondition
			TimeoutPreCondition(timeout);
			#endregion
			return (int) timeout.Ticks;
		}

		[Conditional("DEBUG")]
		private void TimeoutPreCondition(TimeSpan timeout) {
			Debug.Assert(timeout.Ticks <= int.MaxValue);
		}
	}
}