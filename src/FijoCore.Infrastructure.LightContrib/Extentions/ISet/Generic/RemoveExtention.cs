using System.Collections.Generic;
using System.Diagnostics;
using FijoCore.Infrastructure.LightContrib.Default.Exceptions;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.ISet.Generic {
	[PublicAPI]
	public static class RemoveExtention {
		[PublicAPI, DebuggerStepThrough]
		public static void RemoveSecure<T>([NotNull] this ISet<T> me, T item) {
			if (!me.Remove(item)) throw new KeyNotFoundException<ISet<T>, T>(me, item);
		}
	}
}