﻿using System;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners {
	[About("ICloneMapping")]
	public interface IClonerCreator<T> : IClonerCreator {
		[About("Map")]
		[NotNull] T Create([NotNull] T source, ICloning cloning);
	}

	[About("ICloneMapping")]
	public interface IClonerCreator {
		[NotNull] Type For { get; }
		[About("Map")]
		[NotNull] object Create([NotNull] object source, ICloning cloning);
	}
}