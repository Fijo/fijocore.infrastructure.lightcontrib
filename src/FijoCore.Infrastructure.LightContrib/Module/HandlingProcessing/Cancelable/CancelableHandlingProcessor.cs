using System.Collections.Generic;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Base;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Base.Interface;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Cancelable.Interface;

namespace FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Cancelable {
	[Note("This class was called ŽInterruptibleMethodeManagerŽ in the past.")]
	[Note("This class was called ŽInterruptibleHandlingProcessorŽ in the past.")]
	public class CancelableHandlingProcessor<TSource> : HandlingProcessor<ICancelableHandling<TSource>>, ICancelableHandlingProcessor<TSource> {
		protected CancelableHandlingProcessor(IHandlingProvider<ICancelableHandling<TSource>> handlingRepository) : base(handlingRepository) {}

		#region Process
		protected virtual void ProcessHandlings(IEnumerable<ICancelableHandling<TSource>> handlings, TSource source) {
			foreach (var interruptibleActionMethode in handlings) {
				if(!interruptibleActionMethode.Process(source)) break;
			}
		}

		public void Process(TSource source) {
			ProcessHandlings(Handlings, source);
		}
		
		public void Process(IEnumerable<ICancelableHandling<TSource>> customCancelableHandlings, TSource source) {
			if (customCancelableHandlings == null) Process(source);
			else {
				customCancelableHandlings = CustomProcessHandlingsWrapper(customCancelableHandlings);
				ProcessHandlings(customCancelableHandlings.Concat(Handlings), source);
			}
		}
		#endregion
	}
}