using System;
using FijoCore.Infrastructure.LightContrib.Module.Converter.Simple.Interface;

namespace FijoCore.Infrastructure.LightContrib.Module.Converter.Simple {
	public class DefaultConverter : IConverter<object, object> {
		#region Implementation of IConverter
		public Type SourceType { get { return typeof (object); } }
		public Type TargetType { get { return typeof (object); } }

		public object Convert(object source) {
			return source;
		}
		#endregion
	}
}