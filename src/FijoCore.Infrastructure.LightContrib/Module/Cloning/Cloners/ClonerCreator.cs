using System;

namespace FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners {
	public abstract class ClonerCreator<T> : IClonerCreator<T> {
		#region Implementation of IClonerCreator
		public abstract T Create(T source, ICloning cloning);

		public Type For { get { return typeof (T); } }

		public object Create(object source, ICloning cloning) {
			return Create((T) source, cloning);
		}
		#endregion
	}
}