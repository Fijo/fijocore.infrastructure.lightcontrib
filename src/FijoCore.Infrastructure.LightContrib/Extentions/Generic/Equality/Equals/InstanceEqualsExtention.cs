using System;
using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.Infrastructure.Documentation.Enums;
using FijoCore.Infrastructure.LightContrib.Extentions.Object;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Generic.Equality.Equals {
	[PublicAPI]
	public static class InstanceEqualsExtention {
		[Pure, DebuggerStepThrough, PublicAPI]
		[RelatedAttribute(ObjectType.Function, "FijoCore.Infrastructure.LightContrib.Extentions.Object.InstanceEqualsExtention.InstanceEquals(object, object)", "same base compareing functionality")]
		[Desc("Compares two instances of a type if they are the same using a selector to get the values to comare. The difference to InstanceEqualsExtention.InstanceEquals(object, object) is, both objects will be passed through the given innerSelector Func before they are compared.")]
		public static bool InstanceEquals<TSource, TEquals>(this TSource me, TSource other, [NotNull] Func<TSource, TEquals> innerSelector) {
			return innerSelector(me).InstanceEquals(innerSelector(other));
		}
	}
}