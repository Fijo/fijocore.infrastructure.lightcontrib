using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Parallelizable.Interfaces;

namespace FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.CGAll.Interface {
	public interface ICGAllHandlingProcessor<TSource, TResult> : IParallelizableHandlingProcessor<ICGAllHandling<TSource, TResult>> {
		IEnumerable<TResult> Get(TSource source, [Desc("May execute tha Handlings parallel if possible")] bool enableParallelization = true);
		IEnumerable<TResult> Get(IEnumerable<ICGAllHandling<TSource, TResult>> customHandlings, TSource source, [Desc("May execute tha Handlings parallel if possible")] bool enableParallelization = true);
	}
}