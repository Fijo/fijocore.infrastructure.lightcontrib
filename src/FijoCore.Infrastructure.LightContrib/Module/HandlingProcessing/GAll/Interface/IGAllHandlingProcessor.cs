using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Parallelizable.Interfaces;

namespace FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.GAll.Interface {
	public interface IGAllHandlingProcessor<TSource, TResult> : IParallelizableHandlingProcessor<IGAllHandling<TSource, TResult>> {
		IEnumerable<TResult> Get(TSource source, [Desc("May execute tha Handlings parallel if possible")] bool enableParallelization = true);
		IEnumerable<TResult> Get(IEnumerable<IGAllHandling<TSource, TResult>> customHandlings, TSource source, [Desc("May execute tha Handlings parallel if possible")] bool enableParallelization = true);
	}
}