using System.Collections.Generic;
using System.IO;
using System.Linq;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream.Impl.Specific {
	[PublicAPI]
	public class GenericListStreamSerializer<T> : IStreamSerializer<IList<T>> {
		private readonly IStreamSerializer<ICollection<T>> _streamSerializer = Kernel.Resolve<IStreamSerializer<ICollection<T>>>();
		private readonly IOut _out = Kernel.Resolve<IOut>();
		#region Implementation of IStreamSerializer<IList<T>>
		public void Serialize(StreamWriter streamWriter, IList<T> obj) {
			_streamSerializer.Serialize(streamWriter, obj.ToArray());
		}

		public bool TryDeserialize(StreamReader streamReader, out IList<T> result) {
			ICollection<T> collection;
			return _streamSerializer.TryDeserialize(streamReader, out collection)
				       ? _out.True(out result, collection.ToArray())
				       : _out.False(out result);
		}
		#endregion
	}
}