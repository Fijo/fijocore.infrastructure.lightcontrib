﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Default.Service.Runtime {
	public static class Run {
		public static IEnumerable<PropertyInfo> GetProperties<T>([NotNull] params Expression<Func<T, object>>[] selectors) {
			return selectors.Select(GetProperty);
		}

		private static PropertyInfo GetProperty<T>(Expression<Func<T, object>> expression) {
			return (PropertyInfo) ((MemberExpression) expression.Body).Member;
		}
	}
}