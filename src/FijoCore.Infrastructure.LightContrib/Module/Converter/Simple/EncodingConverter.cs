using FijoCore.Infrastructure.LightContrib.Module.Converter.Simple.Base;
using SEncoding = System.Text.Encoding;

namespace FijoCore.Infrastructure.LightContrib.Module.Converter.Simple {
	public class EncodingConverter : ConverterBase<string, SEncoding> {
		#region Overrides of ConverterBase<string,SEncoding>
		public override SEncoding Convert(string source) {
			return SEncoding.GetEncoding(source);
		}
		#endregion
	}
}