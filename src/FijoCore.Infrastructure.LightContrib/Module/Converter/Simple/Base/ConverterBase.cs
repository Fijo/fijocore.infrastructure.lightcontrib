using System;
using FijoCore.Infrastructure.LightContrib.Module.Converter.Simple.Interface;

namespace FijoCore.Infrastructure.LightContrib.Module.Converter.Simple.Base {
	public abstract class ConverterBase<TSource, TResult> : IConverter<TSource, TResult> {
		#region Implementation of IConverter
		public Type SourceType { get { return typeof (TSource); } }
		public Type TargetType { get { return typeof (TResult); } }

		public abstract TResult Convert(TSource source);
		public object Convert(object source) {
			return Convert((TSource) source);
		}
		#endregion
	}
}