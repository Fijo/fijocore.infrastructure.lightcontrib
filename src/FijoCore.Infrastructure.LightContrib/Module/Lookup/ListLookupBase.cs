﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;

namespace FijoCore.Infrastructure.LightContrib.Module.Lookup {
	public abstract class ListLookupBase<TKey, TValue> : IDictionary<TKey, TValue> where TKey : IConvertible {
		protected readonly IList<TValue> Content;
		protected readonly IList<KeyValuePair<TKey, TValue>> KeyValuePais;

		protected ListLookupBase(IEnumerable<KeyValuePair<TKey, TValue>> collection) {
			KeyValuePais = CreateList(collection.OrderBy(x => InternGetKeyIndex(x.Key)));
			// ToDo improve this
			Debug.Assert(KeyValuePais.Select(x => InternGetKeyIndex(x.Key)).SequenceEqual(Enumerable.Range(0, KeyValuePais.Count)));
			Content = CreateList(KeyValuePais.OrderBy(x => x.Key).Select(x => x.Value));
			Keys = CreateList(KeyValuePais.Select(x => x.Key));
			Values = CreateList(KeyValuePais.Select(x => x.Value));
		}

		protected abstract IList<T> CreateList<T>(IEnumerable<T> collection);

		#region Implementation of IEnumerable
		public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() {
			return KeyValuePais.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator() {
			return GetEnumerator();
		}
		#endregion
		#region Implementation of ICollection<KeyValuePair<TKey,TValue>>
		public abstract void Add(KeyValuePair<TKey, TValue> item);

		public abstract void Clear();

		public bool Contains(KeyValuePair<TKey, TValue> item) {
			TValue value;
			return TryGetValue(item.Key, out value) && value.Equals(item.Value);
		}

		public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex) {
			#region PreCondition
			Debug.Assert(array.Rank == 1, "multidimensional arrays are not supported");
			Debug.Assert(array.GetLowerBound(0) == 0, "the lower bound of the array has to be 0");
			if (arrayIndex < 0) throw new ArgumentOutOfRangeException("arrayIndex", arrayIndex, "The arrayIndex has to be at least 0");
			var length = array.Length;
			if (arrayIndex >= length) throw new ArgumentOutOfRangeException("arrayIndex", arrayIndex, "The arrayIndex has to be less than the length of the array (array.length: " + length + ")");
			#endregion

			lock (Content) {
				#region PreCondition
				if (length - arrayIndex < Count) throw new ArgumentOutOfRangeException("array", array, "Destination array is not long enough to copy all the items in the collection. Check array index and length.");
				#endregion

				KeyValuePais.ForEach(x => array[arrayIndex++] = x);
			}
		}

		public abstract bool Remove(KeyValuePair<TKey, TValue> item);

		public int Count { get { return Content.Count; } }
		public abstract bool IsReadOnly { get; }
		#endregion
		#region Implementation of IDictionary<TKey,TValue>
		public bool ContainsKey(TKey key) {
			return GetKeyIndex(key) < Count;
		}

		public abstract void Add(TKey key, TValue value);

		public abstract bool Remove(TKey key);

		public bool TryGetValue(TKey key, out TValue value) {
			lock (Content) {
				var index = GetKeyIndex(key);
				if (index < Count) {
					value = Content[index];
					return true;
				}
				value = default(TValue);
				return false;
			}
		}

		public TValue this[TKey key] {
			get {
				lock (Content) {
					return Content[GetItemIndex(key)];
				}
			}
			set {
				lock (Content) {
					var index = GetKeyIndex(key);
					if (index >= Count) AddItem(index, new KeyValuePair<TKey, TValue>(key, value));
				}
			}
		}

		public ICollection<TKey> Keys { get; protected set; }
		public ICollection<TValue> Values { get; protected set; }
		#endregion

		protected abstract void AddItem(int index, KeyValuePair<TKey, TValue> keyValuePair);

		protected int GetItemIndex(TKey key) {
			var index = GetKeyIndex(key);
			Debug.Assert(index >= 0);
			if (index >= Count) throw new KeyNotFoundException();
			return index;
		}

		protected int GetKeyIndex(TKey key) {
			var index = InternGetKeyIndex(key);
			if (index <= 0) throw new ArgumentException("The to int converted key has to be at least 0", "key");
			return index;
		}

		protected virtual int InternGetKeyIndex(TKey key) {
			return Convert.ToInt32(key);
		}
	}
}