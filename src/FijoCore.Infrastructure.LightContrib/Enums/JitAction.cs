using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Enums {
	[PublicAPI]
	public enum JitAction {
		GetConstructor
	}
}