﻿using System;

namespace FijoCore.Infrastructure.LightContrib.Module.Typing.DictionaryType {
	public class DictionaryTypeDto {
		public readonly Type Key;
		public readonly Type Value;
		public readonly bool IsGeneric;

		public DictionaryTypeDto(Type key, Type value, bool isGeneric) {
			Key = key;
			Value = value;
			IsGeneric = isGeneric;
		}
	}
}