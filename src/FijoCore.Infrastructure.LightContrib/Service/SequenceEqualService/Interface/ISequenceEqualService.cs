using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Service.SequenceEqualService.Interface
{
    public interface ISequenceEqualService
    {
    	bool SequenceEqual([NotNull] IEnumerator me, [NotNull] IEnumerator other, [NotNull] Func<object, object, bool> equalsFunc);
    	bool SequenceEqual<T>([NotNull] IEnumerator<T> me, [NotNull] IEnumerator<T> other, [NotNull] Func<T, T, bool> equalsFunc);
    }
}