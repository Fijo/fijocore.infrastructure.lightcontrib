using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	public static class SkipOneIntoExtention {
		[DebuggerStepThrough]
		[PerformanceRisky("because of ´ToEnumerable´")]
		public static IEnumerable<TSource> SkipOneInto<TSource>([NotNull] this IEnumerable<TSource> me, out TSource skipedObject, out bool hadAny) {
			using (var enumerator = me.GetEnumerator()) {
				if (!(hadAny = enumerator.MoveNext())) {
					skipedObject = default(TSource);
					return Enumerable.Empty<TSource>();
				}
				skipedObject = enumerator.Current;
				return enumerator.ToEnumerable();
			}
		}
	}
}