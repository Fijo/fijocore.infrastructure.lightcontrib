using System;
using System.Collections.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IList.Generic;
using NUnit.Framework;

namespace FijoCore.Infrastructure.LightContribTest.Extentions.IList.Generic {
	[TestFixture]//(typeof (MaxLengthExtention))
	public class MaxLengthExtentionTest {
		[TestCase(0)]
		[TestCase(1)]
		[TestCase(2)]
		[TestCase(3)]
		[TestCase(4)]
		public void List_MaxLengthX_MustHaveCountOfX(int maxLength) {
			var result = (IList<string>) new List<string>
			{
				"hallo",
				"test",
				"dumm",
				"lol"
			};

			result.MaxLength(maxLength);

			Assert.AreEqual(maxLength, result.Count);
		}

		[TestCase(5)]
		[TestCase(6)]
		[TestCase(83)]
		[TestCase(400)]
		public void List_MaxLengthX_CountDontChange(int maxLength) {
			var result = (IList<string>) new List<string>
			{
				"hallo",
				"test",
				"dumm",
				"lol"
			};

			result.MaxLength(maxLength);

			Assert.AreEqual(4, result.Count);
		}

		[Test]
		public void List_MaxLengthNegativeNumber_MustThrowArgumentOutOfRangeException() {
			var result = (IList<string>) new List<string>
			{
				"hallo",
				"test",
				"dumm",
				"lol"
			};

			Assert.Throws<ArgumentOutOfRangeException>(() => result.MaxLength(-1));
		}
	}
}