﻿using System;
using System.Diagnostics;
using FijoCore.Infrastructure.LightContrib.Service.UniqueAdd.Interface;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Service.UniqueAdd {
	[UsedImplicitly]
	public class UniqueAddService : IUniqueAddService {
		public void AssertUniqueAdd<TAddable, T>(TAddable me, T entry, Func<TAddable, T, bool> conatins, Action<TAddable, T> add) {
			#region PreCondition
			if (conatins == null) throw new ArgumentNullException("conatins");
			if (add == null) throw new ArgumentNullException("add");
			Debug.Assert(!conatins(me, entry));
			#endregion
			add(me, entry);
		}

		public void UniqueAdd<TAddable, T>(TAddable me, T entry, Func<TAddable, T, bool> conatins, Action<TAddable, T> add) {
			#region PreCondition
			if (conatins == null) throw new ArgumentNullException("conatins");
			if (add == null) throw new ArgumentNullException("add");
			#endregion
			if (!conatins(me, entry)) add(me, entry);
		}
	}
}