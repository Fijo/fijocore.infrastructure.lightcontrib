using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IList.Generic.Main
{
	public static class UnshiftExtention
	{
		[DebuggerStepThrough]
		public static int Unshift<T>([NotNull] this IList<T> me, T myEntry) {
			me.Insert(0, myEntry);
			return me.Count;
		}
	}
}
