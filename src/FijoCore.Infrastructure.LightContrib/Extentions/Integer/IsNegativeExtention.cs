using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Integer {
	[PublicAPI]
	public static class IsNegativeExtention {
		[Pure, DebuggerStepThrough, PublicAPI]
		[Desc("returns if the given int is smaller than 0 what means that it is negative or not")]
		public static bool IsNegative(this int me) {
			return me < 0;
		}
	}
}