using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Fijo.Infrastructure.DesignPattern.Lazy.Interface;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Module.Encoding;
using JetBrains.Annotations;
using IO = System.IO;

namespace FijoCore.Infrastructure.LightContrib.Module.Stream {
	[UsedImplicitly]
	public class SeekStreamService : StreamServiceBase, IDefaultStreamService {
		public SeekStreamService(ILazy<IEncodingProvider> encodingProvider) : base(encodingProvider) {}

		#region Overrides of StreamServiceBase
		#region Bytes
		protected override byte[] InternalReadToBytes(IO.Stream stream, int count, int bufferSize = 1024) {
			#region PreCondtition
			Debug.Assert(count >= 0);
			Debug.Assert(stream.CanSeek);
			#endregion
			byte[] output;
			var bytesRead = InternalReadToBytesCount(stream, count, out output);
			if (bytesRead != count) throw new EndOfStreamException();
			return output;
		}
		
		private int InternalReadToBytesCount([NotNull] IO.Stream stream, int count, out byte[] output) {
			#region PreCondtition
			Debug.Assert(count >= 0);
			Debug.Assert(stream.CanSeek);
			#endregion
			output = new byte[count];
			return stream.Read(output, 0, count);
		}
		#endregion

		#region Sequence
		public override IEnumerable<byte> ReadToBytesSequence(IO.Stream stream, int count, int bufferSize = 1024) {
			#region PreConition
			if(count < 0) throw new ArgumentOutOfRangeException("count", count, "The count size have to be at least 0");
			if(bufferSize < 1) throw new ArgumentOutOfRangeException("bufferSize", bufferSize, "The buffer size have to be greater than 0");
			Debug.Assert(stream.CanSeek);
			#endregion
			return InternalReadToBytesSequence(stream, count, bufferSize).OneFlat();
		}

		[NotNull]
		private IEnumerable<IEnumerable<byte>> InternalReadToBytesSequence([NotNull] IO.Stream stream, int count, int bufferSize = 1024) {
			#region PreCondtition
			Debug.Assert(stream.CanSeek);
			Debug.Assert(count >= 0);
			Debug.Assert(bufferSize > 0);
			#endregion
			for (var i = 0; i < count; i += bufferSize) {
				var bytesToRead = count - i;
				if (bytesToRead > bufferSize) bytesToRead = bufferSize;
				yield return InternalReadToBytes(stream, bytesToRead);
			}
		}
		#endregion
		#endregion
	}
}