using System.Collections.Generic;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.CGAll.Interface;

namespace FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.CGAll.Impl {
	public interface IChildrenProviderHandling<in TSource, out TResult> : ICGAllHandling<TSource, IEnumerable<TResult>> {}
}