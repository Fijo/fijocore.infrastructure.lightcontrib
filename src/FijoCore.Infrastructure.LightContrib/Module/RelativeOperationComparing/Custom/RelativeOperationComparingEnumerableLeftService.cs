using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Enums;
using FijoCore.Infrastructure.LightContrib.Module.RelativeOperationComparing.Base;
using FijoCore.Infrastructure.LightContrib.Module.RelativeOperationComparing.Wrapper;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.RelativeOperationComparing.Custom {
	[UsedImplicitly]
	public class RelativeOperationComparingEnumerableRightService {
		private readonly RelativeOperationComparingService _relativeOperationComparingService;
		private readonly RelativeOperationComparingEnumerableWrapperService _relativeOperationComparingEnumerableWrapperService;
		public RelativeOperationComparingEnumerableRightService(RelativeOperationComparingService relativeOperationComparingService, RelativeOperationComparingEnumerableWrapperService relativeOperationComparingEnumerableWrapperService) {
			_relativeOperationComparingService = relativeOperationComparingService;
			_relativeOperationComparingEnumerableWrapperService = relativeOperationComparingEnumerableWrapperService;
		}

		public bool Compare(RelationOperation relationOperation, object left, object right) {
			return _relativeOperationComparingService.Compare(relationOperation, left, _relativeOperationComparingEnumerableWrapperService.MayRemoveEnumerable(relationOperation, right));
		}
	}
}