using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Char {
	[PublicAPI]
	public static class IsBooleanExtention {
		[Pure, DebuggerStepThrough, PublicAPI]
		[Desc("Returns if if the given char is 0/ 1 what are typical bool values in int notation. Otherwise it will return false.")]
		public static bool IsBoolean([NotNull] this char? me) {
			// ReSharper disable PossibleInvalidOperationException
			return ((char) me).IsNumeric();
			// ReSharper restore PossibleInvalidOperationException
		}

		[Pure, DebuggerStepThrough, PublicAPI]
		[Desc("Returns if if the given char is 0/ 1 what are typical bool values in int notation. Otherwise it will return false.")]
		public static bool IsBoolean(this char me) {
			switch (me) {
				case '0':
				case '1':
					return true;
			}
			return false;
		}
	}
}