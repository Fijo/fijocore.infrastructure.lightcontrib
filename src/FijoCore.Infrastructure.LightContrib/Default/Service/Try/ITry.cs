using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace FijoCore.Infrastructure.LightContrib.Default.Service.Try {
	[Facade]
	public interface ITry : ITryTimesService, IExlessTryTimesService, IExlessTryForService {}
}