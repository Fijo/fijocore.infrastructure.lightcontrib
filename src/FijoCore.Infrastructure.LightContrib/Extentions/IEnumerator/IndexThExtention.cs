using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator {
	[PublicAPI]
	public static class IndexThExtention {
		[Pure, PublicAPI, DebuggerStepThrough]
		public static TSource IndexTh<TSource>([NotNull] this IEnumerator<TSource> me, int index) {
			#region PreCondition
			Debug.Assert(index > 0);
			#endregion
			return me.GetIndex(index - 1);
		}
	}
}