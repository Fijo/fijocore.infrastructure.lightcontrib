using System;
using System.Collections.Generic;
using System.Diagnostics;
using FijoCore.Infrastructure.LightContrib.Extentions.ISet.Generic;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	[PublicAPI]
	public static class DistinctMergeExtention {
		[NotNull, Pure, PublicAPI, DebuggerStepThrough, LinqTunnel]
		public static IEnumerable<TSource> DistinctMerge<TSource>([NotNull] this IEnumerable<TSource> source, [NotNull] Func<TSource, TSource, TSource> merge, [CanBeNull] IEqualityComparer<TSource> comparer = null) {
			var set = new HashSet<TSource>(comparer);
			foreach (var entry in source) {
				if (set.Add(entry)) continue;
				var match = set.GetMatch(entry, comparer);
				#if DEBUG
				Debug.Assert(ComparerEqualCheck(comparer, match, entry));
				#endif
				var merged = merge(match, entry);
				#if DEBUG
				Debug.Assert(ComparerEqualCheck(comparer, match, merged), "the merged candidate (merged using the �merge� arg Func) has to be equal with the both candidates it merges.");
				#endif
				set.Replace(merged, match);
			}
			// I use this to influence the execution behaviour.
			// ReSharper disable LoopCanBeConvertedToQuery
			foreach (var entry in set)
				yield return entry;
			// ReSharper restore LoopCanBeConvertedToQuery
		}
		
		#if DEBUG
		private static bool ComparerEqualCheck<TSource>([CanBeNull] IEqualityComparer<TSource> comparer, TSource me, TSource other) {
			return comparer != null ? comparer.Equals(me, other) : Equals(me, other);
		}
		#endif
	}
}