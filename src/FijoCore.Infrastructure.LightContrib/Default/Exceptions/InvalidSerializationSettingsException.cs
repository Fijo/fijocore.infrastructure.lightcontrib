using System;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Module.Serialization;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Settings;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Default.Exceptions {
	[PublicAPI, Serializable]
	[Desc("Will be thrown when trying to use serializer A with SerializationSettings for serializer B (you have to use SerializationSettings that inherit from the correct base type/ interface, that is used for the used serializer if it supports custom SerializationSettings.")]
	public class InvalidSerializationSettingsException : InvalidCastException {
		public ISerialization TargetSerialization { get; private set; }
		public Type WantedSerializationSettingsBaseType { get { return TargetType; } }
		public ISerializationSettings ActualSerializationSettings { get { return (ISerializationSettings) SourceInstance; } }

		public InvalidSerializationSettingsException(ISerialization targetSerialization, Type wantedSerializationSettingsBaseType, ISerializationSettings actualSerializationSettings, string message = default(string), Exception innerException = null) : base(actualSerializationSettings, wantedSerializationSettingsBaseType, message, innerException) {
			TargetSerialization = targetSerialization;
		}
	}
}