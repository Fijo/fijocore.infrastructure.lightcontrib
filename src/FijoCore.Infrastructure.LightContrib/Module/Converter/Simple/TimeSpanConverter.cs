using System;
using FijoCore.Infrastructure.LightContrib.Module.Converter.Simple.Base;

namespace FijoCore.Infrastructure.LightContrib.Module.Converter.Simple {
	public class TimeSpanConverter : ConverterBase<string, TimeSpan> {
		#region Overrides of ConverterBase<string,TimeSpan>
		public override TimeSpan Convert(string source) {
			return TimeSpan.Parse(source);
		}
		#endregion
	}
}