using System;
using System.Collections.Generic;
using Fijo.Infrastructure.DesignPattern.Lazy;
using Fijo.Infrastructure.DesignPattern.Lazy.Interface;
using Fijo.Infrastructure.DesignPattern.Repository;
using FijoCore.Infrastructure.LightContrib.Module.Encoding;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Impl.Json.Converters;
using FijoCore.Infrastructure.LightContrib.Module.Stream;
using Newtonsoft.Json;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization.Settings.Impl {
	public class DefaultJsonSettings : SingletonRepositoryBase<JsonSerializerSettings>, IJsonSettings {
		private readonly ILazy<ISerializationProvider> _serializationProvider;
		private readonly IStreamService _streamService;
		private readonly ILazy<IEncodingProvider> _encodingProvider;
		private readonly IRepository<IEnumerable<Type>> _typeProvider;
		private readonly ILazy<ISerializationService> _serializationService;

		public DefaultJsonSettings(ILazy<ISerializationProvider> serializationProvider, IStreamService streamService, ILazy<IEncodingProvider> encodingProvider, IRepository<IEnumerable<Type>> typeProvider, ILazy<ISerializationService> serializationService) {
			_serializationProvider = serializationProvider;
			_streamService = streamService;
			_encodingProvider = encodingProvider;
			_typeProvider = typeProvider;
			_serializationService = serializationService;
		}

		#region Overrides of SingletonRepositoryBase<JsonSerializerSettings>
		protected override JsonSerializerSettings Create() {
			var memberInfoBinaryConverter = new MemberInfoBinaryConverter(_serializationProvider, _streamService, _serializationService, new LazyField<System.Text.Encoding>(() => _encodingProvider.Get().GetDefault()));
			
			return new JsonSerializerSettings
			{
				TypeNameHandling = TypeNameHandling.All,
				Converters = new []
				{
					memberInfoBinaryConverter
				},
				ContractResolver = new ConverterContractResolver(memberInfoBinaryConverter)
			};
		}
		#endregion
	}
}