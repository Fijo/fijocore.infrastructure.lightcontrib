using System;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Number.Service.Interface {
	public interface INumberServiceHelper {
		[Pure]
		TResult GetPossibilities<TSource, TResult>([NotNull] INumberService<TSource> sourceService, [NotNull] INumberService<TResult> resultService) where TSource : struct, IComparable, IConvertible where TResult : struct, IComparable, IConvertible;
	}
}