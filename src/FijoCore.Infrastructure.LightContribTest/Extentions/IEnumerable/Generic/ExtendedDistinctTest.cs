using System.Collections.Generic;
using System.Linq;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Other;
using FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Sequence;
using NUnit.Framework;

namespace FijoCore.Infrastructure.LightContribTest.Extentions.IEnumerable.Generic {
	[TestFixture]
	public class ExtendedDistinctTest {
		private readonly IEqualityComparer<KeyValuePair<string, uint>> _keyValuePairEqualityComparer = KeyValuePairEqualityComparer<string, uint>.Default;

		[Test]
		public void Enumerable_ExtendedDistinct_MustReturnCorrectDictionary() {
			var enumerable = GetTestData();
			var dict = enumerable.ExtendedDistinct();
			var wantedDict = new Dictionary<string, uint>
			{
				{"hallo", 1},
				{"welt", 4},
				{"test", 2},
				{"lol", 2},
			};
			CollectionAssert.IsEmpty(wantedDict.Except(dict, _keyValuePairEqualityComparer));
		}

		private IEnumerable<string> GetTestData() {
			return new[]
			{
				"hallo",
				"welt",
				"test",
				"lol",
				"welt",
				"welt",
				"welt",
				"test",
				"lol"
			};
		}
	}
}