using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Cancelable.Interface {
	[Note("This class was called “IInterruptibleMethode“ in the past.")]
	public interface ICancelableHandling<in TSource> {
		bool Process(TSource source);
	}
}