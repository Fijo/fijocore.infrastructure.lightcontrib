using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	[PublicAPI]
	public static class FlatExtention
	{
		[NotNull, Pure, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<T> OneFlat<T>([NotNull] this IEnumerable<IEnumerable<T>> me) {
			return me.SelectMany(x => x);
		}
		
		[NotNull, Pure, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<T> Flat<T>([NotNull] this IEnumerable<IEnumerable<T>> me) {
			return me.SelectMany(x => x);
		}
		
		[NotNull, Pure, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<T> Flat<T>([NotNull] this IEnumerable<IEnumerable<IEnumerable<T>>> me) {
			return me.OneFlat().Flat();
		}
		
		[NotNull, Pure, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<T> Flat<T>([NotNull] this IEnumerable<IEnumerable<IEnumerable<IEnumerable<T>>>> me) {
			return me.OneFlat().Flat();
		}
		
		[NotNull, Pure, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<T> Flat<T>([NotNull] this IEnumerable<IEnumerable<IEnumerable<IEnumerable<IEnumerable<T>>>>> me) {
			return me.OneFlat().Flat();
		}
		
		[NotNull, Pure, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<T> Flat<T>([NotNull] this IEnumerable<IEnumerable<IEnumerable<IEnumerable<IEnumerable<IEnumerable<T>>>>>> me) {
			return me.OneFlat().Flat();
		}
	}
}