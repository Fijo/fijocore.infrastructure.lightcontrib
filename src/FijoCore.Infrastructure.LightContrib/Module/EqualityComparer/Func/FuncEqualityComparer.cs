using System;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Func {
	[Service]
	public class FuncEqualityComparer<T> : FuncEqualityComparerBase<T, T> {
		public FuncEqualityComparer([NotNull] Func<T, T, bool> equals, [CanBeNull] Func<T, int> getHashCode = null) : base(equals, getHashCode) {}

		public override bool Equals(T x, T y) {
			return EqualsFunc(x, y);
		}

		public override int GetHashCode(T obj) {
			return GetHashCodeFunc(obj);
		}
	}
}