﻿using System.Linq;
using FijoCore.ComponentTest.Helper.InitKernel;
using FijoCore.Components.Extentions.IEnumerable.Generic;
using NUnit.Framework;

namespace FijoCore.ComponentTest.Extentions.IEnumerable
{
	[TestFixture]
	public class RecursiveEachSelectExtenitonTest
	{
		[SetUp]
		public void SetUp()
		{
			new InternalInitKernel().Init();
		}

		[Test]
		public void Test()
		{
			var result = new[]
			             	{
			             		new[] {"hallo", "welt", "neu"},
								new[] {"lol", "test", "rofl"},
								new[] {"test", "xD", "abc"}
			             	}.RecursiveEachSelect().ToList();
			var wanted = new[]
			             	{
			             		new[] {"hallo", "lol", "test"},
			             		new[] {"hallo", "lol", "xD"},
			             		new[] {"hallo", "lol", "abc"},
			             		new[] {"hallo", "test", "test"},
			             		new[] {"hallo", "test", "xD"},
			             		new[] {"hallo", "test", "abc"},
			             		new[] {"hallo", "rofl", "test"},
			             		new[] {"hallo", "rofl", "xD"},
			             		new[] {"hallo", "rofl", "abc"},
			             		new[] {"welt", "lol", "test"},
			             		new[] {"welt", "lol", "xD"},
			             		new[] {"welt", "lol", "abc"},
			             		new[] {"welt", "test", "test"},
			             		new[] {"welt", "test", "xD"},
			             		new[] {"welt", "test", "abc"},
			             		new[] {"welt", "rofl", "test"},
			             		new[] {"welt", "rofl", "xD"},
			             		new[] {"welt", "rofl", "abc"},
			             		new[] {"neu", "lol", "test"},
			             		new[] {"neu", "lol", "xD"},
			             		new[] {"neu", "lol", "abc"},
			             		new[] {"neu", "test", "test"},
			             		new[] {"neu", "test", "xD"},
			             		new[] {"neu", "test", "abc"},
			             		new[] {"neu", "rofl", "test"},
			             		new[] {"neu", "rofl", "xD"},
			             		new[] {"neu", "rofl", "abc"}
			             	}.ToList();

			Assert.IsTrue(wanted.OrderlessUniqueEquals(result, (me, other) => me.OrderlessUniqueEquals(other)));
		}
	}
}
