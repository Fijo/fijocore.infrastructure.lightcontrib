using System;

namespace FijoCore.Infrastructure.LightContrib.Default.Attributes {
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false, AllowMultiple = true)]
	public class AssemblyEnableDefenitionAttibute : Attribute {
		public readonly string DefinitionConstantName;

		public AssemblyEnableDefenitionAttibute(string definitionConstantName) {
			DefinitionConstantName = definitionConstantName;
		}
	}
}