using System;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization.Impl.Json.Converters {
	[Link("http://stackoverflow.com/questions/8030538/how-to-implement-custom-jsonconverter-in-json-net-to-deserialize-a-list-of-base")]
	public abstract class JsonCreationConverter : JsonConverter {
		/// <summary>
		/// Create an instance of objectType, based properties in the JSON object
		/// </summary>
		/// <param name="objectType">type of object expected</param>
		/// <param name="jObject">contents of JSON object that will be deserialized</param>
		/// <returns></returns>
		protected abstract object Create(Type objectType, JObject jObject);

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer) {
			// Load JObject from stream
			JObject jObject = JObject.Load(reader);

			// Create target object based on JObject
			var target = Create(objectType, jObject);

			// Populate the object properties
			serializer.Populate(jObject.CreateReader(), target);

			return target;
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) {
			throw new NotImplementedException();
		}
	}
}