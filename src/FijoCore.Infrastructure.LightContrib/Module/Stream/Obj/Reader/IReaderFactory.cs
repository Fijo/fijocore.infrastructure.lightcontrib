using System.IO;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Stream.Obj.Reader {
	public interface IReaderFactory {
		IStringReader CreateStringReader([NotNull] StreamReader streamReader);
		IStringReader CreateStringReader([NotNull] System.IO.Stream stream, bool autodetect = false);
		IStringReader CreateStringReader([NotNull] System.IO.Stream stream, [CanBeNull] System.Text.Encoding encoding);
		IStringReader CreateStringReader(string content);
	}
}