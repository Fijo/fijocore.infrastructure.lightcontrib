using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Generic.Equality.GetHashCode {
	[PublicAPI]
	public static class NullableGetHashCodeExtention {
		[Pure, PublicAPI, DebuggerStepThrough]
		[Desc("returns the hash code of this allowing this to be null")]
		public static int NullableGetHashCode<T>(this T me) {
			return me.IsUndefined() ? 0 : me.GetHashCode();
		}
	}
}