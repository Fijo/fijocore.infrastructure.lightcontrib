using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	[PublicAPI]
	public static class ContainsAnyExtention {
		[Pure, PublicAPI, DebuggerStepThrough]
		public static bool ContainsAny<T>([NotNull] this IEnumerable<T> me, [NotNull] IEnumerable<T> collection) {
			var executedCollection = collection.Execute();
			return me.Any(executedCollection.Contains);
		}

		[Pure, PublicAPI, DebuggerStepThrough]
		public static bool ContainsAny<T>([NotNull] this IEnumerable<T> me, [NotNull] IEnumerable<T> collection, [NotNull] IEqualityComparer<T> equalityComparer) {
			var executedCollection = collection.Execute();
			return me.Any(x => executedCollection.Contains(x, equalityComparer));
		}

		[Pure, PublicAPI, DebuggerStepThrough]
		public static bool ContainsAny<T1, T2>([NotNull] this IEnumerable<T1> me, [NotNull] IEnumerable<T2> collection, [NotNull] Func<T1, T2, bool> comparer) {
			var executedCollection = collection.Execute();
			return me.Any(x => executedCollection.Any(y => comparer(x, y)));
		}

		[Pure, PublicAPI, DebuggerStepThrough]
		public static bool ContainsAny<T>([NotNull] this IEnumerable<T> me, [NotNull] params IEnumerable<T>[] collections) {
			var executedMe = me.Execute();
			return collections.Aggregate(false, (current, collection) => current || collection.ContainsAny(executedMe));
		}

		[Pure, PublicAPI, DebuggerStepThrough]
		public static bool ContainsAny<T>([NotNull] this IEnumerable<T> me, [NotNull] Func<T, T, bool> equals, [NotNull] params IEnumerable<T>[] collections) {
			var executedMe = me.Execute();
			return collections.Aggregate(false, (current, collection) => current || collection.ContainsAny(executedMe, equals));
		}
	}
}