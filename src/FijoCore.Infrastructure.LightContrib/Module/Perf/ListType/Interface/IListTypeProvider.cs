namespace FijoCore.Infrastructure.LightContrib.Module.Perf.ListType.Interface {
	public interface IListTypeProvider {
		IListType GetPreferedList(int size);
	}
}