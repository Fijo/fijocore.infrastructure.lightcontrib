using System;
using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator {
	[PublicAPI]
	public static class ForEachExtention {
		[PublicAPI, DebuggerStepThrough]
		public static void ForEach<T>([NotNull] this IEnumerator<T> me, [NotNull] Action<T> action) {
			while (me.MoveNext()) action(me.Current);
		}

		[PublicAPI, DebuggerStepThrough]
		public static void ForEach<T>([NotNull] this IEnumerator<T> me, [NotNull] Action<T, int> action) {
			var i = 0;
			while (me.MoveNext()) action(me.Current, i++);
		}
		
		#region Extended
		[PublicAPI, DebuggerStepThrough]
		public static void ForEachAndBetween<T>([NotNull] this IEnumerator<T> me, [NotNull] Action<T> action, [NotNull] Action betweenAction) {
			if (!me.MoveNext()) return;
			action(me.Current);
			while (me.MoveNext()) {
				betweenAction();
				action(me.Current);
			}
		}

		[PublicAPI, DebuggerStepThrough]
		public static void ForEach<T>([NotNull] this IEnumerator<T> me, [NotNull] Action<T> firstAction, [NotNull] Action<T> action) {
			if (!me.MoveNext()) return;
			firstAction(me.Current);
			me.ForEach(action);
		}
		#endregion
	}
}