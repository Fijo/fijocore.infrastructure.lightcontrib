namespace FijoCore.Infrastructure.LightContrib.Module.Transaction {
	public interface ITransactionFactory {
		Transaction CreateTransaction();
	}
}