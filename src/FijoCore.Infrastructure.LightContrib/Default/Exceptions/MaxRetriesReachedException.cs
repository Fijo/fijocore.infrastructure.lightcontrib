using System;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Enums;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Default.Exceptions {
	[Desc("Will be thrown when a TryFunc based fail oncurrent, while doing an try x times action and the retry limit is reached")]
	[PublicAPI, Serializable]
	public class MaxRetriesReachedException : TryleOutException {
		#region Overrides of TryleOutException
		public override TryleOutType Type { get { return TryleOutType.MaxRetriesReached; } }
		#endregion

		public MaxRetriesReachedException() {}

		[Note("int retryLimit as int is correct for impls of TryleOutException where Type == TryleOutType.MaxRetriesReached")]
		public MaxRetriesReachedException(string message, int retryLimit = default(int), Exception innerException = null) : base(message, retryLimit, innerException) {}
	}
}