using FijoCore.Infrastructure.LightContrib.Module.Converter.Simple.Base;

namespace FijoCore.Infrastructure.LightContrib.Module.Converter.Simple {
	public class Int16Converter : ConverterBase<long, short> {
		#region Overrides of ConverterBase<long,int>
		public override short Convert(long source) {
			return System.Convert.ToInt16(source);
		}
		#endregion
	}
}