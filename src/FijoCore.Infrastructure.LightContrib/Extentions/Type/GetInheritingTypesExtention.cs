using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic;
using JetBrains.Annotations;
using SType = System.Type;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Type {
	[PublicAPI]
	public static class GetInheritingTypesExtention {
		private static IRepository<IEnumerable<SType>> _allTypeRepository;
		#if InheritedTypeCache
		// ToDo FixMe may remove entries later - possible memory leak
		private static readonly IDictionary<SType, IList<SType>> Cache = new Dictionary<SType, IList<SType>>();
		#endif

		internal static void Init() {
			#if InheritedTypeCache
			AppDomain.CurrentDomain.AssemblyLoad += OnAssemblyLoad;
			#endif
			_allTypeRepository = Kernel.Resolve<IRepository<IEnumerable<SType>>>();
		}

		#if InheritedTypeCache
		private static void OnAssemblyLoad(object sender, AssemblyLoadEventArgs args) {
			Cache.Clear();
		}
		#endif

		[Pure, PublicAPI, DebuggerStepThrough]
		[Desc("returns a list of types that implement the given type")]
		public static IEnumerable<SType> GetInheritingTypes([NotNull] this SType me) {
			#if InheritedTypeCache
			return Cache.GetOrCreate(me, () => (IList<SType>) GetInheritingTypesIntern(me).ToList());
			#else
			return GetInheritingTypesIntern(me);
			#endif
		}

		private static IEnumerable<SType> GetInheritingTypesIntern(SType me) {
			return _allTypeRepository.Get().AsParallel().Where(x => x.ImplementsType(me));
		}
	}
}