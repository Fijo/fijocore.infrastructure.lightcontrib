using System;
using FijoCore.Infrastructure.LightContrib.Module.Converter.Simple.Base;

namespace FijoCore.Infrastructure.LightContrib.Module.Converter.Simple {
	public class DateTimeConverter : ConverterBase<string, DateTime> {
		#region Overrides of ConverterBase<string,DateTime>
		public override DateTime Convert(string source) {
			return DateTime.Parse(source);
		}
		#endregion
	}
}