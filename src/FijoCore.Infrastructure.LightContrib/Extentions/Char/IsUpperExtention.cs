using System.Diagnostics;
using System.Globalization;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Char {
	[PublicAPI]
	public static class IsUpperExtention {
		[Pure, DebuggerStepThrough, PublicAPI]
		[Desc("checks if a char is in upper case using an invariant culture settings")]
		public static bool IsInvariantUpper(this char me) {
			return me.InvariantToUpper().Equals(me);
		}

		[Pure, DebuggerStepThrough, PublicAPI]
		[Desc("checks if a char is in upper case")]
		public static bool IsUpper(this char me, [NotNull] CultureInfo culture) {
			return me.ToUpper(culture).Equals(me);
		}
	}
}