using System;
using Fijo.Infrastructure.DesignPattern.Repository;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Perf.Configuration;
using FijoCore.Infrastructure.LightContrib.Module.Perf.ListType.Impl;
using FijoCore.Infrastructure.LightContrib.Module.Perf.ListType.Interface;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Perf.ListType {
	[UsedImplicitly]
	public class ListTypeProvider : IListTypeProvider {
		protected readonly IRepository<PerfConfiguration> PerfConfigurationRepository = Kernel.Resolve<IRepository<PerfConfiguration>>();
		protected readonly IListType FixedListType = new FixedListType();
		protected readonly IListType DynamicListType = new DynamicListType();

		#region Implementation of IListTypeProvider
		public virtual IListType GetPreferedList(int size) {
			#region PreCondition
			if(size < 0) throw new ArgumentOutOfRangeException("size", size, "has to be unsigned");
			#endregion
			// don�t stop to do this always
			var perfConfiguration = PerfConfigurationRepository.Get();
			return perfConfiguration.MaxSizeForFixedListType < size
				       ? DynamicListType
				       : FixedListType;
		}
		#endregion
	}
}