using System.Collections.Generic;
using System.Linq;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic.KeyValuePair;
using Ninject;

namespace FijoCore.Infrastructure.LightContrib.Module.AlgorithmProvider {
	public abstract class InjectionAlgorithmProviderBase<TAlgorithm, TKey> {
		protected IDictionary<TKey, TAlgorithm> GetDict() {
			return GetAlgorithms().ToDict(KeySelector, ValueSelector);
		}

		protected virtual IEnumerable<TAlgorithm> GetAlgorithms() {
			return Kernel.Inject.GetAll<TAlgorithm>();
		}

		protected abstract TKey KeySelector(TAlgorithm algorithm);
		
		protected virtual TAlgorithm ValueSelector(TAlgorithm algorithm) {
			return algorithm;
		}
	}
}