using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners.Impl {
	[PublicAPI]
	public class DefaultClonerCreator<T> : ClonerCreator<T> where T : new() {
		#region Overrides of ClonerCreator<T>
		public override T Create(T source, ICloning cloning) {
			return new T();
		}
		#endregion
	}
}