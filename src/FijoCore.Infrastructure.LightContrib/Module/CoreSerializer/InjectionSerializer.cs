﻿using System;
using System.Runtime.Serialization;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.CoreSerializer {
	[PublicAPI, Serializable]
	public class InjectionSerializer<T> : IObjectReference {
		#region Implementation of IObjectReference
		public object GetRealObject(StreamingContext context) {
			return Kernel.Resolve<T>();
		}
		#endregion
	}
}