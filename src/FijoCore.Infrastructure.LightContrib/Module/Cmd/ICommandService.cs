namespace FijoCore.Infrastructure.LightContrib.Module.Cmd {
	public interface ICommandService {
		string ExecuteReturn(string binary, string args);
		string ExecuteReturn(string command);
		void Execute(string binary, string args);
		void Execute(string command);
		string EscapeQuotes(string value);
		string WrapWithQuotes(string value);
	}
}