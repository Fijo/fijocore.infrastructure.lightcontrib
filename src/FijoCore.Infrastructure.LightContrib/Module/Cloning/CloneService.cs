﻿using System;
using System.Diagnostics;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Controller;
using Fijo.Infrastructure.DesignPattern.Lazy.Interface;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Dtos;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Conditional.Interface;

namespace FijoCore.Infrastructure.LightContrib.Module.Cloning {
	public class CloneService : ICloneService {
		private readonly IFinder<IClonerCreator, Type> _clonerCreatorFinder;
		private readonly ILazy<IFinder<ICloning, CloningType>> _cloningTypeFinder;
		private readonly ICHProcessor<CloneSource> _clonerProcessor;

		public CloneService(IFinder<IClonerCreator, Type> clonerCreatorFinder, ICHProcessor<CloneSource> clonerProcessor, ILazy<IFinder<ICloning, CloningType>> cloningTypeFinder) {
			_clonerCreatorFinder = clonerCreatorFinder;
			_clonerProcessor = clonerProcessor;
			_cloningTypeFinder = cloningTypeFinder;
		}

		#region Implementation of ICloneService<T>
		[Note("uses the typeof(T) as cloned result type")]
		public T Clone<T>(T source, CloningType type = CloningType.Default) {
			return Clone(source, GetCloning(type));
		}

		public object Clone(object source, CloningType type = CloningType.Default) {
			return Clone(source, GetCloning(type));
		}
		
		[Note("uses the typeof(T) as cloned result type")]
		public T Clone<T>(T source, ICloning cloning) {
			return (T) Clone(typeof (T), source, cloning);
		}

		public object Clone(object source, ICloning cloning) {
			return Clone(source.GetType(), source, cloning);
		}

		[Note("uses the type of the source object as cloned result type")]
		public T CloneDyn<T>(T source, CloningType type = CloningType.Default) {
			return CloneDyn(source, GetCloning(type));
		}

		[Note("uses the type of the source object as cloned result type")]
		public T CloneDyn<T>(T source, ICloning cloning) {
			return (T) Clone(source.GetType(), source, cloning);
		}

		protected object Clone(Type type, object source, ICloning cloning) {
			var result = _clonerCreatorFinder.Get(type).Create(source, cloning);
			_clonerProcessor.Process(new CloneSource(source, result, cloning));
			return result;
		}

		private ICloning GetCloning(CloningType cloningType) {
			return _cloningTypeFinder.Get().Get(cloningType);
		}
		#endregion
	}
}