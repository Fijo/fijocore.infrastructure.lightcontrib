using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Base.Interface;

namespace FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Cancelable.Interface {
	[ChainOfResponsibility]
	[Note("This class was called ŽIInterruptibleMethodeManagerŽ in the past.")]
	[Note("This class was called ŽIInterruptibleHandlingProcessorŽ in the past.")]
	public interface ICancelableHandlingProcessor<T> : IHandlingProcessor<ICancelableHandling<T>> {
		void Process(T source);
		void Process(IEnumerable<ICancelableHandling<T>> customCancelableHandlings, T source);
	}
}