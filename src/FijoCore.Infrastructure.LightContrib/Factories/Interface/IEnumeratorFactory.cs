using System.Collections.Generic;
using FijoCore.Infrastructure.LightContrib.Delegates;

namespace FijoCore.Infrastructure.LightContrib.Factories.Interface {
	public interface IEnumeratorFactory {
		IEnumerator<int> CreateEndless(int start);
		IEnumerator<T> CreateExpression<T>(TryFunc<T> expression);
	}
}