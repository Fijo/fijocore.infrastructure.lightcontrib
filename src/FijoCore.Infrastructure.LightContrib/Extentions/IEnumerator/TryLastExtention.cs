using System.Collections.Generic;
using System.Diagnostics;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator {
	[PublicAPI]
	public static class TryLastExtention {
		private static IOut _out;

		internal static void Init() {
			_out = Kernel.Resolve<IOut>();
		}

		[Pure, PublicAPI, DebuggerStepThrough]
		public static bool TryLast<TSource>([NotNull] this IEnumerator<TSource> me, out TSource result) {
			#region PreCondition
			Debug.Assert(_out != null, "_out != null");
			#endregion
			if (!me.MoveNext()) return _out.False(out result);
			TSource current;
			do current = me.Current;
			while (me.MoveNext());
			return _out.True(out result, current);
		}
	}
}