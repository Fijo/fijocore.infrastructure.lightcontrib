﻿using System.Collections.Generic;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Default.Exceptions;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Converter.ConverterServices.Base {
	[PublicAPI]
	public abstract class ConverterServiceBase<TType, TResult> {
		[PublicAPI]
		protected ICollection<TType> ConvertableTypes;
		private readonly IOut _out = Kernel.Resolve<IOut>();

		[Pure, PublicAPI]
		public bool TryConvert<T>(T obj, out TResult result) {
			return ConvertableTypes.Contains(GetType<T>())
			       	? _out.True(out result, GetConverted(obj))
			       	: _out.False(out result);
		}

		[Pure, PublicAPI]
		public TResult Convert<T>(T obj) {
			if (!ConvertableTypes.Contains(GetType<T>())) throw new NotConvertableException(GetNotConvertableMessage(GetType(), typeof (TType), typeof (TResult)));
			return GetConverted(obj);
		}

		[Pure, NotNull]
		private string GetNotConvertableMessage([NotNull] System.Type classType, [NotNull] System.Type contextType, [NotNull] System.Type resultType) {
			return string.Format("Faild to convert using ´{0}´. Comparing by ´{1}´ - tring to covert to ´{2}´", classType.Name,
			                     contextType.Name, resultType.Name);
		}
		
		[Pure, PublicAPI]
		protected abstract TType GetType<T>();

		[Pure, PublicAPI]
		protected abstract TResult GetConverted<T>(T obj);
	}
}
