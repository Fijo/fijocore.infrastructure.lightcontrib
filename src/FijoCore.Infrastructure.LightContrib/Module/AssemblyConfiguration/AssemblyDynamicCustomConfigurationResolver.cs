﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using JetBrains.Annotations;
using RAssembly = System.Reflection.Assembly;

namespace FijoCore.Infrastructure.LightContrib.Module.AssemblyConfiguration {
	[UsedImplicitly]
	public class AssemblyDynamicCustomConfigurationResolver {
		private readonly string[] _keyValueSeperator = new[] {"::"};
		private const string DynamicCusomConfiguration = "@DynamicCusomConfiguration";

		public IDictionary<string, IEnumerable<string>> Get([NotNull] RAssembly context) {
			var dynamicCustomAttributes = context.GetCustomAttributes(false)
				.OfType<AssemblyConfigurationAttribute>()
				.SingleOrDefault();
			if (dynamicCustomAttributes == null) return new Dictionary<string, IEnumerable<string>>();

			var configuration = dynamicCustomAttributes.Configuration
				.Replace("\r", string.Empty)
				.Split('\n')
				.SkipWhile(x => x != DynamicCusomConfiguration)
				.Skip(1);
			return GetConfigurationDictionary(configuration);
		}

		private IDictionary<string, IEnumerable<string>> GetConfigurationDictionary([NotNull] IEnumerable<string> configuration) {
			var dynamicConfigurationDict = new Dictionary<string, IEnumerable<string>>();
			foreach (var configruationLine in configuration) {
				var splited = configruationLine.Split(_keyValueSeperator, StringSplitOptions.None).Execute();
				Debug.Assert(splited.Count == 2);

				var values = splited.Second().Split('|').ToList();
				Debug.Assert(values.Any());
				Debug.Assert(string.IsNullOrEmpty(values.Last()));

				var valuesCountToUse = values.Count - 1;
				var key = splited.First();
				dynamicConfigurationDict.Add(key, values.Take(valuesCountToUse));
			}
			return dynamicConfigurationDict;
		}
	}
}