using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Fijo.Infrastructure.DesignPattern.Repository;
using JetBrains.Annotations;
using Newtonsoft.Json;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization.Impl.Json.Converters {
	[PublicAPI]
	public class MemberInfoConverter : JsonConverter {
		private readonly IRepository<IEnumerable<Type>> _typeProvider;

		public MemberInfoConverter(IRepository<IEnumerable<Type>> typeProvider) {
			_typeProvider = typeProvider;
		}

		public override bool CanWrite { get { return false; } }

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) {
			throw new NotSupportedException();
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer) {
			if (reader.TokenType == JsonToken.Null)
				return null;

			var type = default(string);
			var name = default(string);
			var assemblyName = default(string);
			var className = default(string);
			var signature = default(string);
			var signature2 = default(string);
			var memberType = 0;

			while (reader.Read() && reader.TokenType != JsonToken.EndObject) {
				switch ((string) reader.Value) {
					case "$type":
						type = reader.ReadAsString();
						break;
					case "Name":
						name = reader.ReadAsString();
						break;
					case "AssemblyName":
						assemblyName = reader.ReadAsString();
						break;
					case "ClassName":
						className = reader.ReadAsString();
						break;
					case "Signature":
						signature = reader.ReadAsString();
						break;
					case "Signature2":
						signature2 = reader.ReadAsString();
						break;
					case "MemberType":
						memberType = (int) reader.ReadAsInt32();
						break;
					case "GenericArguments":
						reader.Read();
						if (reader.TokenType == null) throw new NotImplementedException();
						break;
				}
			}

			var targetType = _typeProvider.Get().Single(x => x.FullName == className);

			var seperatedSignature = signature.Split(' ');
			Debug.Assert(seperatedSignature.Length == 2);
			var returnTypeString = seperatedSignature.First();
			var returnType = _typeProvider.Get().Single(x => x.FullName == returnTypeString);


			switch (type) {
				case "System.Reflection.RuntimePropertyInfo, mscorlib":
				case "System.Reflection.RuntimePropertyInfo":
					Debug.Assert(signature == signature2, "May handle signature2 in another way or just look after this case.");
					Debug.Assert(memberType == 20, "May handle other member types in another way or just look after this case.");
					var memberInfos = targetType.GetProperty(name, returnType);
					return memberInfos;
				default:
					throw new NotSupportedException();
			}
		}

		public override bool CanConvert(Type objectType) {
			return typeof (MemberInfo).IsAssignableFrom(objectType);
		}
	}
}