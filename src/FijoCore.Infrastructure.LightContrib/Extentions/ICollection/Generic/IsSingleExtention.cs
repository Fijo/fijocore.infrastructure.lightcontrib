using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.ICollection.Generic {
	public static class IsSingleExtention {
		[Pure, PublicAPI, DebuggerStepThrough]
		public static bool IsSingle<T>(this ICollection<T> me) {
			return me.Count == 1;
		}
	}
}