using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Default.Exceptions;
using FijoCore.Infrastructure.LightContrib.Dto;
using FijoCore.Infrastructure.LightContrib.Enums;
using FijoCore.Infrastructure.LightContrib.Extentions.ICollection.Generic;
using JetBrains.Annotations;
using SType = System.Type;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Type {
	[PublicAPI]
	public static class NewExtention {
		[Pure, PublicAPI]
		[Desc("Reflection based new (arguments)")]
		public static object New([NotNull] this SType me, [NotNull] params object[] arguments) {
			var paramTypes = arguments.None() ? new SType[0] : arguments.Select(x => x.GetType()).ToArray();
			var constructor = me.GetConstructor(paramTypes);
			if(constructor == null)
				throw new JitException(me, JitAction.GetConstructor,
				                       new GetConstructor {Params = arguments, ParamTypes = paramTypes, BindingFlags = BindingFlags.Public | BindingFlags.Instance}, JitFeature.Reflection,
				                       "The type hasn�t a public constructor that accepts the given parameter types.");
			return constructor.Invoke(arguments);
		}

		[Pure, PublicAPI]
		[Desc("Reflection based new (arguments)")]
		public static T New<T>([NotNull] this SType me, [NotNull] params object[] arguments) {
			#region PreCondition
			Debug.Assert(typeof (T).IsAssignableFrom(me));
			#endregion
			return (T) me.New(arguments);
		}
	}
}