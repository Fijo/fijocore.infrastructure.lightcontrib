﻿using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Cloning {
	[PublicAPI]
	public enum CloningType {
		Default,
		Deep
	}
}