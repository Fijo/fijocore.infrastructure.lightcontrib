using System.Collections.Generic;
using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	[PublicAPI]
	public static class ExtendedDistinctExtention {
		[NotNull, Pure, PublicAPI, DebuggerStepThrough]
		[Note("The retuned dict still uses the comparer.")]
		public static IDictionary<T, uint> ExtendedDistinct<T>([NotNull] this IEnumerable<T> me, [CanBeNull] IEqualityComparer<T> comparer = null) {
			var dict = new Dictionary<T, uint>(comparer);
			me.ForEach(x => dict[x] = dict.TryGet(x, (uint) 0) + 1);
			return dict;
		}
	}
}