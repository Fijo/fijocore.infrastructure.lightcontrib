using FijoCore.Infrastructure.LightContrib.Module.Converter.Simple.Base;

namespace FijoCore.Infrastructure.LightContrib.Module.Converter.Simple {
	public class UInt32Converter : ConverterBase<long, uint> {
		#region Overrides of ConverterBase<long,int>
		public override uint Convert(long source) {
			return System.Convert.ToUInt32(source);
		}
		#endregion
	}
}