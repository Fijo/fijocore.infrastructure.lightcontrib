using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Base.Interface;

namespace FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Parallelizable.Interfaces {
	public interface IParallelizableHandlingProvider<out THandling> : IHandlingProvider<THandling> {
		bool SupportParallelization { get; }
	}
}