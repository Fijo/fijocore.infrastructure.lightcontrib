using System;
using System.Collections.Generic;
using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Service.OrderlessEquals.Interface;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	[PublicAPI]
	public static class OrderlessEqualsExtention {
		private static IOrderlessEqualsService _orderlessEquals;

		[DebuggerStepThrough, PublicAPI]
		internal static void Init() {
			_orderlessEquals = Kernel.Resolve<IOrderlessEqualsService>();
		}

		[DebuggerStepThrough, PublicAPI]
		[Desc("Only use this function on two uniqued IEnumerables.")]
		public static bool OrderlessUniqueEquals<T>([NotNull] this IEnumerable<T> me, [NotNull] IEnumerable<T> other) {
			#region PreCondition
			Debug.Assert(_orderlessEquals != null, "_orderlessEquals != null");
			#endregion
			return _orderlessEquals.OrderlessUniqueEquals(me, other);
		}

		[DebuggerStepThrough, PublicAPI]
		[Desc("Only use this function on two uniqued IEnumerables.")]
		public static bool OrderlessUniqueEquals<T>([NotNull] this IEnumerable<T> me, [NotNull] IEnumerable<T> other, [NotNull] Func<T, T, bool> equals) {
			#region PreCondition
			Debug.Assert(_orderlessEquals != null, "_orderlessEquals != null");
			#endregion
			return _orderlessEquals.OrderlessUniqueEquals(me, other, equals);
		}

		[DebuggerStepThrough, PublicAPI]
		public static bool OrderlessEquals<T>([NotNull] this IEnumerable<T> me, [NotNull] IEnumerable<T> other) {
			#region PreCondition
			Debug.Assert(_orderlessEquals != null, "_orderlessEquals != null");
			#endregion
			return _orderlessEquals.OrderlessEquals(me, other);
		}

		[DebuggerStepThrough, PublicAPI]
		public static bool OrderlessEquals<T>([NotNull] this IEnumerable<T> me, [NotNull] IEnumerable<T> other, [NotNull] Func<T, T, bool> equals) {
			#region PreCondition
			Debug.Assert(_orderlessEquals != null, "_orderlessEquals != null");
			#endregion
			return _orderlessEquals.OrderlessEquals(me, other, equals);
		}
	}
}