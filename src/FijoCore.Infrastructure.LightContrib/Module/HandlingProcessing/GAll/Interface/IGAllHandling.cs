using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Parallelizable.Interfaces;

namespace FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.GAll.Interface {
	public interface IGAllHandling<in TSource, out TResult> : ISupportsParallelization {
		TResult Get(TSource source);
	}
}