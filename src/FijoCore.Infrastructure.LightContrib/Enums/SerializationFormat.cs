namespace FijoCore.Infrastructure.LightContrib.Enums {
	public enum SerializationFormat {
		Binary,
		BinaryDataContract,
		JSON,
		XML,
		Soap
	}
}