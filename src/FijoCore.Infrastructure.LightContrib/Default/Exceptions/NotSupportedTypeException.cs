using System;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Default.Exceptions {
	[PublicAPI, Serializable]
	public class NotSupportedTypeException : NotSupportedException {
		public NotSupportedTypeException() {}

		public NotSupportedTypeException(string message) : base(message) {}

		public NotSupportedTypeException(string message, Exception innerException) : base(message, innerException) {}
	}
}