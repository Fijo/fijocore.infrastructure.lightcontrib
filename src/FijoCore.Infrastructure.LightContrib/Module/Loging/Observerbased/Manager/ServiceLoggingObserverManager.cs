using Fijo.Infrastructure.DesignPattern.Observer.Impl;
using Fijo.Infrastructure.DesignPattern.Observer.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Loging.Observerbased.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Loging.Observerbased.Obj;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Loging.Observerbased.Manager {
	[PublicAPI]
	public class ServiceLoggingObserverManager : ServiceObserverManager<ILoggingObserver, ILoggable> {
		public ServiceLoggingObserverManager([NotNull] ILoggable observerableService, [NotNull] IObserverManager<ILoggingObserver, ILoggable> innerObserverManager) : base(observerableService, innerObserverManager) {}

		#region Overrides of ServiceObserverManager<ILoggingObserver,ILoggable>
		protected override ILoggable CreateContainer(ILoggable observerableService, ILoggable obj) {
			return new LoggingContainer(observerableService, obj);
		}
		#endregion
	}
}