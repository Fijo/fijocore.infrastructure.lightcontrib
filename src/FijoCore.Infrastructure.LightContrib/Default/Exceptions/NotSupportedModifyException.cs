using System;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Default.Exceptions {
	[PublicAPI, Serializable]
	public class NotSupportedModifyException : NotSupportedException {
		public NotSupportedModifyException() {}

		public NotSupportedModifyException(string message) : base(message) {}

		public NotSupportedModifyException(string message, Exception innerException) : base(message, innerException) {}
	}
}