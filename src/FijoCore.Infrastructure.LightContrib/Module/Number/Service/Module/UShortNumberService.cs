using FijoCore.Infrastructure.LightContrib.Module.Number.Service.Base;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Number.Service.Module {
	[UsedImplicitly]
	public class UShortNumberService : NumberServiceBase<ushort> {
		#region Implementation of INumberService<byte>
		public override ushort MaxValue { get { return ushort.MaxValue; } }
		public override ushort MinValue { get { return ushort.MinValue; } }
		public override bool IsInteger { get { return true; } }

		public override ushort Sun(ushort left, ushort right) {
			return (ushort) (left + right);
		}

		public override ushort Diff(ushort left, ushort right) {
			return (ushort) (left - right);
		}

		public override ushort Times(ushort left, ushort right) {
			return (ushort) (left * right);
		}

		public override ushort Divide(ushort left, ushort right) {
			return (ushort) (left / right);
		}

		public override ushort Modulo(ushort left, ushort right) {
			return (ushort) (left % right);
		}
		#endregion
	}
}