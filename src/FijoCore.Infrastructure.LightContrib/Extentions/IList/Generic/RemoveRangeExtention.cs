using System.Collections.Generic;
using System.Diagnostics;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IList.Generic {
	[PublicAPI]
	public static class RemoveRangeExtention {
		[PublicAPI, DebuggerStepThrough]
		public static void RemoveRange<T>([NotNull] this IList<T> me, int index, int count) {
			#region PreCondition
			Debug.Assert(index >= 0, "index >= 0");
			Debug.Assert(count >= 0, "count >= 0");
			Debug.Assert(index + count <= me.Count, "index + count < me.Count");
			#endregion
			lock (me)
				for (var currentIndex = index + count - 1; currentIndex >= index; currentIndex--)
					me.RemoveAt(currentIndex);
		}

		[PublicAPI, DebuggerStepThrough]
		public static void RemoveRange<T>([NotNull] this IList<T> me, [NotNull] IEnumerable<T> candidates)
		{
			#region PreCondition
			#if DEBUG
			candidates = candidates.Execute();
			Debug.Assert(me.ContainsAll(candidates));
			#endif
			#endregion
			candidates.ForEach(x => me.Remove(x));
		}
	}
}