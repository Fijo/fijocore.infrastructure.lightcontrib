using System.Diagnostics;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic.IntoCollection;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream.Impl.Specific {
	[UsedImplicitly]
	public class ByteStreamSerializer : FixedSizeStreamSerializer<byte> {
		#region Overrides of FixedSizeStreamSerializer<byte>
		protected override int Size { get { return 1; } }
		protected override byte[] Serialize(byte obj) {
			return obj.IntoArray();
		}

		protected override byte Deserialize(byte[] bytes) {
			Debug.Assert(bytes.Length == 1);
			return bytes[0];
		}
		#endregion
	}
}