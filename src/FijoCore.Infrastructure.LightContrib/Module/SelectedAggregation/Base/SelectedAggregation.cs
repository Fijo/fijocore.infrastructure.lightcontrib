using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Delegates;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Module.SelectedAggregation.Interface;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.SelectedAggregation.Base {
	public abstract class SelectedAggregation<TKey, TFuncKey, TAccumulate, TSource, TResult, TParam> : ISelectedAggregation<TKey, TFuncKey, TAccumulate, TSource, TResult, TParam> {
		[NotNull] protected readonly IList<KeyValuePair<TFuncKey, AggregateFunc<TAccumulate, TParam, TKey>>> Funcs = new List<KeyValuePair<TFuncKey, AggregateFunc<TAccumulate, TParam, TKey>>>();
		[CanBeNull] protected IList<TFuncKey> ExecutionOrder;
		[CanBeNull] protected IEqualityComparer CustomOrderKeyEqualityComparer;

		#region Init
		public ISelectedAggregation<TKey, TFuncKey, TAccumulate, TSource, TResult, TParam> Add(TFuncKey key, AggregateFunc<TAccumulate, TParam, TKey> func) {
			Funcs.Add(new KeyValuePair<TFuncKey, AggregateFunc<TAccumulate, TParam, TKey>>(key, func));
			return this;
		}
		
		public ISelectedAggregation<TKey, TFuncKey, TAccumulate, TSource, TResult, TParam> SetOrder(IEqualityComparer equalityComparer = null, params TFuncKey[] wantedOrder) {
			CustomOrderKeyEqualityComparer = equalityComparer;
			ExecutionOrder = wantedOrder;
			return this;
		}
		#endregion

		public TResult Process(TKey key, TSource source) {
			return InternalProcess(Funcs, key, source);
		}

		public TResult Process(IEnumerable<KeyValuePair<TFuncKey, AggregateFunc<TAccumulate, TParam, TKey>>> customFuncs, TKey key, TSource source) {
			return InternalProcess(Funcs.Concat(customFuncs), key, source);
		}

		protected virtual TResult InternalProcess([NotNull] IEnumerable<KeyValuePair<TFuncKey, AggregateFunc<TAccumulate, TParam, TKey>>> funcs, TKey key, TSource source) {
			var param = GetParam(source, key);
			var aggregate = GetAccumulate(source, param, key);
			var result = ProcessAggregateFuncs(aggregate, GetAffectedFuncs(key, param, funcs), param, key);
			return GetResult(result, param, key);
		}
		
		[Pure, NotNull]
		protected virtual IEnumerable<KeyValuePair<TFuncKey, AggregateFunc<TAccumulate, TParam, TKey>>> OrderAffectedFuncs([NotNull] IEnumerable<KeyValuePair<TFuncKey, AggregateFunc<TAccumulate, TParam, TKey>>> aggregateFuncs) {
			return ExecutionOrder != null
			       	? aggregateFuncs.OrderBy(x => GetOrderKey(x.Key))
			       	: aggregateFuncs;
		}

		[Pure]
		protected virtual int GetOrderKey(TFuncKey key) {
			#region PreCondition
			Debug.Assert(ExecutionOrder != null, "ExecutionOrder != null");
			#endregion
			return ExecutionOrder.IndexOf(key, CustomOrderKeyEqualityComparer);
		}

		[Pure, NotNull]
		protected virtual IEnumerable<AggregateFunc<TAccumulate, TParam, TKey>> GetAffectedFuncs(TKey key, [CanBeNull] TParam param, [NotNull] IEnumerable<KeyValuePair<TFuncKey, AggregateFunc<TAccumulate, TParam, TKey>>> aggregateFuncs) {
			return Funcs.Where(x => IsFuncUsed(key, x.Key, param)).Select(x => x.Value);
		}

		[Pure]
		protected abstract bool IsFuncUsed(TKey key, TFuncKey funcKey, TParam param);

		protected virtual TAccumulate ProcessAggregateFuncs(TAccumulate accumulate, [NotNull] IEnumerable<AggregateFunc<TAccumulate, TParam, TKey>> aggregateFuncs, [CanBeNull] TParam param, TKey key) {
			return aggregateFuncs.Aggregate(accumulate, (a, c) => c(a, param, key));
		}

		[CanBeNull, About("ParamSelector")]
		protected abstract TParam GetParam(TSource source, TKey key);

		[About("SourceSelector")]
		protected abstract TAccumulate GetAccumulate(TSource source, [CanBeNull] TParam param, TKey key);

		[About("ResultSelector")]
		protected abstract TResult GetResult(TAccumulate accumulate, [CanBeNull] TParam param, TKey key);
	}

	public abstract class SelectedAggregation<TKey, TFuncKey, TAccumulate, TParam> : SelectedAggregation<TKey, TFuncKey, TAccumulate, TAccumulate, TAccumulate, TParam>, ISelectedAggregation<TKey, TFuncKey, TAccumulate, TParam> {
		#region Overrides of SelectedAggregation<TKey,TFuncKey,TAccumulate,TAccumulate,TAccumulate>
		protected override TAccumulate GetAccumulate(TAccumulate source, TParam param, TKey key) {
			return source;
		}

		protected override TAccumulate GetResult(TAccumulate accumulate, TParam param, TKey key) {
			return accumulate;
		}
		#endregion
	}
}