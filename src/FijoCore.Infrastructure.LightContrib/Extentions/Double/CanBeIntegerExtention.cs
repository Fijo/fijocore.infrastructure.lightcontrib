using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Double {
	[PublicAPI]
	public static class CanBeIntegerExtention {
		[Pure, DebuggerStepThrough, PublicAPI]
		[Desc("Returns if a double can be lossless converted into an integer.")]
		public static bool CanBeInteger(this double me) {
			// ReSharper disable CompareOfFloatsByEqualityOperator
			return me - me.ToInteger() == 0;
			// ReSharper restore CompareOfFloatsByEqualityOperator
		}
	}
}