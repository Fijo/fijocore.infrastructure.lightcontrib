using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace FijoCore.Infrastructure.LightContrib.Enums {
	public enum TryleOutType {
		Timeout = 1,
		[About("MaxTriesReached")]
		MaxRetriesReached = 2
	}
}