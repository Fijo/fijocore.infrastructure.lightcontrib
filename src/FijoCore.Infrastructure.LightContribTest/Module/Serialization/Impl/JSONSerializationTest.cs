﻿using System.Linq;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Impl;
using JetBrains.Annotations;
using NUnit.Framework;

namespace FijoCore.Infrastructure.LightContribTest.Module.Serialization.Impl {
	[TestFixture]
	public class JSONSerializationTest : SerializationTestBase<JSONSerialization> {
		[UsedImplicitly]
		public string TestMember { get; set; }

		[Test]
		public void ObjectWithMemberInfo_SerializeDeserialize_ReturnsEqualObject() {
			var expected = GetMemberInfoTestDto();
			var serializeDeserializeTest = SerializeDeserializeTest(GetMemberInfoTestDto());
			Assert.AreEqual(expected, serializeDeserializeTest);
		}

		private MemberInfoTestDto GetMemberInfoTestDto() {
			return new MemberInfoTestDto{Member = typeof(JSONSerializationTest).GetMember("TestMember").Single()};	
		}
	}
}