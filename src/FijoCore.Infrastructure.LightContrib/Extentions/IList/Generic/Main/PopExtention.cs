using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IList.Generic.Main
{
	public static class PopExtention
	{
		[DebuggerStepThrough]
		public static int Pop<T>([NotNull] this IList<T> me) {
			lock (me) {
				var newLength = me.Count - 1;
				me.RemoveAt(newLength);
				return newLength;
			}
		}
	}
}
