using System.IO;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream.Impl.Specific {
	public abstract class PairBaseStreamSerializer<TPair, T1, T2> : IStreamSerializer<TPair> {
		private readonly IStreamSerializer<T1> _firstStreamSerializer = Kernel.Resolve<IStreamSerializer<T1>>();
		private readonly IStreamSerializer<T2> _secondStreamSerializer = Kernel.Resolve<IStreamSerializer<T2>>();
		private readonly IOut _out = Kernel.Resolve<IOut>();
		#region Implementation of IStreamSerializer<TPair>
		[TemplateMethod]
		public void Serialize(StreamWriter streamWriter, TPair obj) {
			_firstStreamSerializer.Serialize(streamWriter, GetFirst(obj));
			_secondStreamSerializer.Serialize(streamWriter, GetSecond(obj));
		}
		
		public bool TryDeserialize(StreamReader streamReader, out TPair result) {
			T1 first;
			T2 second;
			return _firstStreamSerializer.TryDeserialize(streamReader, out first) && _secondStreamSerializer.TryDeserialize(streamReader, out second)
				       ? _out.True(out result, GetPair(first, second))
				       : _out.False(out result);

		}
		#endregion

		protected abstract T1 GetFirst(TPair pair);
		protected abstract T2 GetSecond(TPair pair);
		protected abstract TPair GetPair(T1 first, T2 second);
	}
}