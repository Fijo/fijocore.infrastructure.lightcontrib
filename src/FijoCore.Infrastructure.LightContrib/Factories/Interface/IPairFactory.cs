using System.Collections.Generic;
using Fijo.Infrastructure.Model.Pair.Interface;
using Fijo.Infrastructure.Model.Pair.Interface.Base;

namespace FijoCore.Infrastructure.LightContrib.Factories.Interface {
	public interface IPairFactory {
		IPair<T1, T2> Pair<T1, T2>(T1 first, T2 second);
		ILeftRightPair<TObject> LeftRightPair<TObject>(TObject left, TObject right);
		IReplacePair<TEntry> ReplacePair<TEntry>(TEntry needle, TEntry replacement);
		IReplacePair<TNeelde, TReplacement> ReplacePair<TNeelde, TReplacement>(TNeelde needle, TReplacement replacement);
		KeyValuePair<TKey, TValue> KeyValuePair<TKey, TValue>(TKey key, TValue value);
	}
}