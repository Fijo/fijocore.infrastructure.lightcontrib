//BS:Frei Wild - Allein nach Vorn (Cover 2)

using System;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Configuration.Internal;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Configuration {
	[UsedImplicitly]
	public class ConfigurationService : IConfigurationService {
		private readonly IInternalConfigurationService _configurationService = Kernel.Resolve<IInternalConfigurationService>();

		#region Implementation of IConfigurationService
		public T Get<T>(Type context, string key) {
			return Get<T>(string.Format("{0}.{1}", context.FullName, key));
		}

		public T Get<T, TContext>(string key) {
			return Get<T>(typeof (TContext), key);
		}

		public T Get<T>(string key) {
			return _configurationService.Get<T>(key);
		}
		#endregion
	}
}