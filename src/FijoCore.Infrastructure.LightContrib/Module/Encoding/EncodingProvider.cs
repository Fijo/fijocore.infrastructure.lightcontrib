﻿using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Configuration;
using JetBrains.Annotations;
using SEncoding = System.Text.Encoding;

namespace FijoCore.Infrastructure.LightContrib.Module.Encoding {
	[UsedImplicitly]
	public class EncodingProvider : IEncodingProvider {
		private readonly SEncoding _defaultEncoding;

		public EncodingProvider(IConfigurationService configurationService) {
			_defaultEncoding = configurationService.Get<SEncoding>("Fijo.Infrastructure.LightContrib.Module.Encoding.DefaultEncoding");
		}

		[Pure]
		public SEncoding GetDefault() {
			return _defaultEncoding;
		}
	}
}