using Fijo.Infrastructure.DesignPattern.Repository;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Impl.Json.Converters;
using FijoCore.Infrastructure.LightContrib.Module.Typing.DictionaryType;
using FijoCore.Infrastructure.LightContrib.Module.Typing.KeyValuePairType;
using Newtonsoft.Json;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization.Settings.Impl {
	public class JsonAdvancedDictSettings : SingletonRepositoryBase<JsonSerializerSettings>, IJsonSettings {
		private readonly IDictionaryTypeService _dictionaryTypeService;
		private readonly IGenericKeyValuePairAccessorProvider _genericKeyValuePairAccessorProvider;

		public JsonAdvancedDictSettings(IDictionaryTypeService dictionaryTypeService, IGenericKeyValuePairAccessorProvider genericKeyValuePairAccessorProvider) {
			_dictionaryTypeService = dictionaryTypeService;
			_genericKeyValuePairAccessorProvider = genericKeyValuePairAccessorProvider;
		}

		protected override JsonSerializerSettings Create() {
			var jsonSerializerSettings = new JsonSerializerSettings
			{
				TypeNameHandling = TypeNameHandling.All,
				PreserveReferencesHandling = PreserveReferencesHandling.Objects
			};
			//jsonSerializerSettings.Converters.Add(new DictionaryConverter(_dictionaryTypeService, _genericKeyValuePairAccessorProvider));
			return jsonSerializerSettings;
		}
	}
}