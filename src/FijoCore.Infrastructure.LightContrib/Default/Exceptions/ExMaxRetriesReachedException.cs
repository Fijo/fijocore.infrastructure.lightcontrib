using System;
using System.Collections.Generic;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Enums;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Default.Exceptions {
	[AboutName("Ex", "Exception")]
	[Desc("Will be thrown when a exception based fail oncurrent, while doing an try x times action that catches it and the retry limit is reached")]
	[PublicAPI, Serializable]
	public class ExMaxRetriesReachedException<TCaughtException> : ExMaxRetriesReachedException where TCaughtException : Exception {
		public override IList<Exception> GenericInnerTrysExceptions {
			get { return InnerTrysExceptions.Cast<Exception>().ToArray(); }
			set { InnerTrysExceptions = value.Cast<TCaughtException>().ToArray(); }
		}

		public virtual IList<TCaughtException> InnerTrysExceptions { get; set; }
		
		public ExMaxRetriesReachedException() {}

		[Note("int retryLimit as int is correct for impls of TryleOutException where Type == TryleOutType.MaxRetriesReached")]
		public ExMaxRetriesReachedException(string message, int retryLimit = default(int), Exception innerException = null) : base(message, retryLimit, innerException) {}
	}

	[AboutName("Ex", "Exception")]
	[Desc("Will be thrown when a exception based fail oncurrent, while doing an try x times action that catches it and the retry limit is reached")]
	[PublicAPI, Serializable]
	public class ExMaxRetriesReachedException : TryleOutException {
		#region Overrides of TryleOutException
		public override TryleOutType Type { get { return TryleOutType.MaxRetriesReached; } }
		#endregion

		public virtual IList<Exception> GenericInnerTrysExceptions { get; set; }
		
		public ExMaxRetriesReachedException() {}

		[Note("int retryLimit as int is correct for impls of TryleOutException where Type == TryleOutType.MaxRetriesReached")]
		public ExMaxRetriesReachedException(string message, int retryLimit = default(int), Exception innerException = null) : base(message, retryLimit, innerException) {}
	}
}