using System.Collections.Generic;

namespace FijoCore.Infrastructure.LightContrib.Module.Perf.ListType.Interface {
	public interface IListType {
		void Add<T>(IList<T> list, int index, T item);
		IList<T> Create<T>(int size);
	}
}