using System.Collections.Generic;
using System.IO;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic.KeyValuePair;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream.Impl.Specific {
	[PublicAPI]
	public class GenericDictionaryStreamSerializer<TKey, TValue> : IStreamSerializer<IDictionary<TKey, TValue>> {
		private readonly IStreamSerializer<ICollection<KeyValuePair<TKey, TValue>>> _streamSerializer = Kernel.Resolve<IStreamSerializer<ICollection<KeyValuePair<TKey, TValue>>>>();
		private readonly IOut _out = Kernel.Resolve<IOut>();
		#region Implementation of IStreamSerializer<IDictionary<TKey,TValue>>
		public void Serialize(StreamWriter streamWriter, IDictionary<TKey, TValue> obj) {
			_streamSerializer.Serialize(streamWriter, obj.ToDict());
		}

		public bool TryDeserialize(StreamReader streamReader, out IDictionary<TKey, TValue> result) {
			ICollection<KeyValuePair<TKey, TValue>> collection;
			return _streamSerializer.TryDeserialize(streamReader, out collection)
				       ? _out.True(out result, collection.ToDict())
				       : _out.False(out result);
		}
		#endregion
	}
}