using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	public static class ThirdExtention {
		[DebuggerStepThrough]
		public static TSource Third<TSource>([NotNull] this IEnumerable<TSource> me) {
			return me.IndexTh(3);
		}
	}
}