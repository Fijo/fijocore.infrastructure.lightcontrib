using System;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Configuration.ConverterKey {
	[UsedImplicitly]
	public class TypeChangeFactory : ITypeChangeFactory {
		[Pure]
		public TypeChangeDto Create(Type source, Type target) {
			return new TypeChangeDto(source, target);
		}
	}
}