﻿using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners.Impl {
	[PublicAPI]
	public class DeepCloning : ICloning {
		private readonly ICloneService _cloningService;

		public DeepCloning(ICloneService cloningService) {
			_cloningService = cloningService;
		}
		#region Implementation of ICloning
		public CloningType Type { get { return CloningType.Deep; } }

		public T Pass<T>(T source) {
			return _cloningService.Clone(source, this);
		}

		public ICloning GetDeeper() {
			return this;
		}
		#endregion
	}
}