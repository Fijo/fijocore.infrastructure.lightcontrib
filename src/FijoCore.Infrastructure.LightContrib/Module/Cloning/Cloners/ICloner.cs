﻿using System;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Dtos;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Conditional.Interface;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners {
	public interface ICloner<in T> : ICloner {
		void Copy([NotNull] T source, [NotNull] T result, ICloning cloning);
	}

	public interface ICloner : ICH<CloneSource> {
		[NotNull] Type For { get; }
		void Copy([NotNull] object source, [NotNull] object result, ICloning cloning);
	}
}