using System;

namespace FijoCore.Infrastructure.LightContrib.Default.Service.Try {
	public abstract class TryServiceBase {
		protected virtual void PreCondition(int maxTries) {
			#region PreCondition
			if (maxTries < 0) throw new ArgumentOutOfRangeException("maxTries", maxTries, "have to be unsigned");
			#endregion
		}
	}
}