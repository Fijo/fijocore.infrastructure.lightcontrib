using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	[PublicAPI]
	public static class WithoutExtention {
		#region Enumerable
		[NotNull, Pure, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<T> Without<T>([NotNull] this IEnumerable<T> me, T item) {
			// ToDo may improve performance by using GetHashCode ( I can to prefetch the hashcode of item then ).
			// But I think no one would expect an function to use hashcodes with an IEnumerable<T> - so think about it another time before begin using hash codes here.
			return me.Where(x => !Equals(x, item));
		}
		
		[NotNull, Pure, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<T> Without<T>([NotNull] this IEnumerable<T> me, T item, [NotNull] IEqualityComparer<T> comparer) {
			return me.Where(x => !comparer.Equals(x, item));
		}
		#endregion

		#region ParallelQuery
		[NotNull, Pure, PublicAPI, DebuggerStepThrough]
		public static ParallelQuery<T> Without<T>([NotNull] this ParallelQuery<T> me, T item) {
			// ToDo may improve performance by using GetHashCode ( I can to prefetch the hashcode of item then ).
			// But I think no one would expect an function to use hashcodes with an IEnumerable<T> - so think about it another time before begin using hash codes here.
			return me.Where(x => !Equals(x, item));
		}
		
		[NotNull, Pure, PublicAPI, DebuggerStepThrough]
		public static ParallelQuery<T> Without<T>([NotNull] this ParallelQuery<T> me, T item, [NotNull] IEqualityComparer<T> comparer) {
			return me.Where(x => !comparer.Equals(x, item));
		}
		#endregion
	}
}