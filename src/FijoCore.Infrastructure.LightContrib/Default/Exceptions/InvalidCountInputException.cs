using System;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Default.Exceptions {
	[PublicAPI, Serializable]
	public class InvalidCountInputException : Exception {
		public InvalidCountInputException() {}

		public InvalidCountInputException(string message) : base(message) {}

		public InvalidCountInputException(string message, Exception innerException) : base(message, innerException) {}
	}
}