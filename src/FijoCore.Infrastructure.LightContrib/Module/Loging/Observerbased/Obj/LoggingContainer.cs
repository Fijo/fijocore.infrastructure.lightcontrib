using Fijo.Infrastructure.DesignPattern.Observer.Impl;
using FijoCore.Infrastructure.LightContrib.Module.Loging.Observerbased.Interface;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Loging.Observerbased.Obj {
	[PublicAPI]
	public class LoggingContainer : ObserverableContainer<ILoggable>, ILoggable {
		public LoggingContainer(params ILoggable[] observerables) : base(observerables) {}
	}
}