using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fijo.Infrastructure.Model.Pair.Interface.Base;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic.KeyValuePair {
	[PublicAPI]
	public static class ToDictExtention
	{
		[NotNull] private static ILookupFactory _lookupFactory;

		internal static void Init() {
			_lookupFactory = Kernel.Resolve<ILookupFactory>();
		}

		#region KeyValuePair
		public static IDictionary<TKey, TValue> ToDict<TKey, TValue>(this IEnumerable<KeyValuePair<TKey, TValue>> me) {
			return me.ToDict(DefaultKeySelector, DefaultValueSelector);
		}

		public static IDictionary<TResultKey, TResultValue> ToDict<TSourceKey, TSourceValue, TResultKey, TResultValue>(this IEnumerable<KeyValuePair<TSourceKey, TSourceValue>> me, System.Func<TSourceKey, TResultKey> keySelector, System.Func<TSourceValue, TResultValue> valueSelector) {
			return me.ToDict(x => keySelector(DefaultKeySelector(x)), x => valueSelector(DefaultValueSelector(x)));
		}
		
		public static IDictionary<TResultKey, TValue> ToDict<TSourceKey, TResultKey, TValue>(this IEnumerable<KeyValuePair<TSourceKey, TValue>> me, System.Func<TSourceKey, TResultKey> keySelector) {
			return me.ToDict(x => keySelector(DefaultKeySelector(x)), DefaultValueSelector);
		}

		public static IDictionary<TKey, TResultValue> ToDict<TKey, TSourceValue, TResultValue>(this IEnumerable<KeyValuePair<TKey, TSourceValue>> me, System.Func<TSourceValue, TResultValue> valueSelector) {
			return me.ToDict(DefaultKeySelector, x => valueSelector(DefaultValueSelector(x)));
		}
		#endregion

		#region IPair
		public static IDictionary<TKey, TValue> ToDict<TKey, TValue>(this IEnumerable<IPair<TKey, TValue>> me) {
			return me.ToDict(DefaultKeySelector, DefaultValueSelector);
		}

		public static IDictionary<TResultKey, TResultValue> ToDict<TSourceKey, TSourceValue, TResultKey, TResultValue>(this IEnumerable<IPair<TSourceKey, TSourceValue>> me, System.Func<TSourceKey, TResultKey> keySelector, System.Func<TSourceValue, TResultValue> valueSelector) {
			return me.ToDict(x => keySelector(DefaultKeySelector(x)), x => valueSelector(DefaultValueSelector(x)));
		}
		
		public static IDictionary<TResultKey, TValue> ToDict<TSourceKey, TResultKey, TValue>(this IEnumerable<IPair<TSourceKey, TValue>> me, System.Func<TSourceKey, TResultKey> keySelector) {
			return me.ToDict(x => keySelector(DefaultKeySelector(x)), DefaultValueSelector);
		}

		public static IDictionary<TKey, TResultValue> ToDict<TKey, TSourceValue, TResultValue>(this IEnumerable<IPair<TKey, TSourceValue>> me, System.Func<TSourceValue, TResultValue> valueSelector) {
			return me.ToDict(DefaultKeySelector, x => valueSelector(DefaultValueSelector(x)));
		}
		#endregion

		#region KeySelector
		private static TKey DefaultKeySelector<TKey, TValue>(KeyValuePair<TKey, TValue> entry)
		{
			return entry.Key;
		}
		
		private static TKey DefaultKeySelector<TKey, TValue>(IPair<TKey, TValue> entry)
		{
			return entry.First;
		}
		#endregion
		
		#region ValueSelector
		private static TValue DefaultValueSelector<TKey, TValue>(KeyValuePair<TKey, TValue> entry)
		{
			return entry.Value;
		}
		
		private static TValue DefaultValueSelector<TKey, TValue>(IPair<TKey, TValue> entry)
		{
			return entry.Second;
		}
		#endregion

		[NotNull, Pure, DebuggerStepThrough, PublicAPI]
		public static IDictionary<TKey, TValue> ToDict<TSource, TKey, TValue>([NotNull] this IEnumerable<TSource> me, [NotNull] Func<TSource, TKey> keySelector, [NotNull] Func<TSource, TValue> valueSelector) {
			return ToDict(me, keySelector, valueSelector, null);
		}

		[NotNull, Pure, DebuggerStepThrough, PublicAPI]
		public static IDictionary<TKey, TValue> ToDict<TSource, TKey, TValue>([NotNull] this IEnumerable<TSource> me, [NotNull] Func<TSource, TKey> keySelector, [NotNull] Func<TSource, TValue> valueSelector, IEqualityComparer<TKey> comparer) {
			return _lookupFactory.Create(GetPais(me, keySelector, valueSelector), comparer);
		}

		private static IEnumerable<KeyValuePair<TKey, TValue>> GetPais<TSource, TKey, TValue>(IEnumerable<TSource> me, Func<TSource, TKey> keySelector, Func<TSource, TValue> valueSelector) {
			return me.Select(x => new KeyValuePair<TKey, TValue>(keySelector(x), valueSelector(x)));
		}
	}
}