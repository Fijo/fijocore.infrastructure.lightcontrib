﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Default.Exceptions;
using JetBrains.Annotations;
#if DEBUG
using FijoCore.Infrastructure.LightContrib.Default.Module.DebugView;
#endif

namespace FijoCore.Infrastructure.LightContrib.Module.Collection.Generic {
	#if DEBUG
	[DebuggerTypeProxy(typeof (CollectionDebugView<>))]
	#endif
	[DebuggerDisplay("Count = {Count}")]
	[Serializable]
	public class ExecutedEnumerable<T> : ICollection<T> {
		private readonly ICollection<T> _collection;

		[UsedImplicitly]
		[Note("only used for Serialisation")]
		public ExecutedEnumerable() {
			_collection = new T[0];
		}
		
		public ExecutedEnumerable([NotNull] IList<T> collection) {
			_collection = collection;
		}

		public ExecutedEnumerable([NotNull] ICollection<T> collection) {
			_collection = collection;
		}

		#region Implementation of IEnumerable
		public IEnumerator<T> GetEnumerator() {
			return _collection.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator() {
			return GetEnumerator();
		}
		#endregion

		#region Implementation of ICollection<T>
		public void Add(T item) {
			throw new NotSupportedReadOnlyCollectionModifyException();
		}

		public void Clear() {
			throw new NotSupportedReadOnlyCollectionModifyException();
		}

		public bool Contains(T item) {
			return _collection.Contains(item);
		}

		public void CopyTo(T[] array, int arrayIndex) {
			_collection.CopyTo(array, arrayIndex);
		}

		public bool Remove(T item) {
			throw new NotSupportedReadOnlyCollectionModifyException();
		}

		public int Count { get { return _collection.Count; } }

		public bool IsReadOnly { get { return true; } }
		#endregion
	}
}