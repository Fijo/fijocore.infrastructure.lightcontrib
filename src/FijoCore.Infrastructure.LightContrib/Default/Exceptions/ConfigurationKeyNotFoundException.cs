using System;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Default.Exceptions {
	[PublicAPI, Serializable]
	public class ConfigurationKeyNotFoundException : Exception {
		[PublicAPI] public string Key { get; protected set; }
		[PublicAPI] public Type Type { get; protected set; }

		public ConfigurationKeyNotFoundException(string key, Type type, string message = "", Exception innerException = null) : base(message, innerException) {
			Key = key;
			Type = type;
		}
	}
}