using System;
using System.Collections.Generic;
using System.Diagnostics;
using Fijo.Infrastructure.Model.Pair.Interface.Base;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic.KeyValuePair {
	[PublicAPI]
	public static class ToDistinctedDictionaryExtention
	{
		[NotNull] private static ILookupFactory _lookupFactory;

		internal static void Init() {
			_lookupFactory = Kernel.Resolve<ILookupFactory>();
		}

		[NotNull, Pure, PublicAPI]
		public static IDictionary<TKey, TValue> ToDistinctedDictionary<TElement, TKey, TValue>(this IEnumerable<TElement> me, [NotNull] Func<TElement, TKey> keySelector, [NotNull] Func<TElement, TValue> valueSelector) {
			#region PreCondition
			Debug.Assert(me != null, "me != null");
			#endregion
			var dict = new Dictionary<TKey, TValue>();
			foreach (var entry in me) {
				var key = keySelector(entry);
				if(dict.ContainsKey(key)) continue;
				dict.Add(key, valueSelector(entry));
			}
			return dict;
		}
		
		[NotNull, Pure, PublicAPI]
		public static IDictionary<TKey, TValue> ToDistinctedDictionary<TKey, TValue>(this IEnumerable<KeyValuePair<TKey, TValue>> me) {
			#region PreCondition
			Debug.Assert(me != null, "me != null");
			#endregion
			return me.ToDistinctedDictionary(x => x.Key, x => x.Value);
		}

		[NotNull, Pure, PublicAPI]
		public static IDictionary<TKey, TValue> ToDistinctedDictionary<TKey, TValue>(this IEnumerable<IPair<TKey, TValue>> me) {
			#region PreCondition
			Debug.Assert(me != null, "me != null");
			#endregion
			return me.ToDistinctedDictionary(x => x.First, x => x.Second);
		}
	}
}