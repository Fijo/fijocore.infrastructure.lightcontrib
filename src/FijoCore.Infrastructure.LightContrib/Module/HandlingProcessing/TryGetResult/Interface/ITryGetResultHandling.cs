using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.TryGetResult.Interface {
	[Note("This class was called ´IMethode´ in the past.")]
	public interface ITryGetResultHandling<in TSource, TResult> {
		bool TryProcess(TSource source, out TResult result);
	}
}