﻿using System.Linq;
using Fijo.Infrastructure.Model.Pair;
using FijoCore.Infrastructure.LightContrib.Default.Service.Runtime;
using NUnit.Framework;

namespace FijoCore.Infrastructure.LightContribTest.Default.Service.Runtime {
	[TestFixture]
	public class RunTest {
		[Test]
		public void Properties_GetPropertySelectors_Properties() {
			var test = Run.GetProperties<Pair<string, string>>(x => x.First, x => x.Second).ToList();
			Assert.AreEqual(typeof (Pair<string, string>).GetProperty("First").MetadataToken, test.First().MetadataToken);
		}
	}
}