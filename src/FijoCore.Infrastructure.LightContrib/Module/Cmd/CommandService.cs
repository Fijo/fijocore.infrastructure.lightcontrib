using System.Diagnostics;

namespace FijoCore.Infrastructure.LightContrib.Module.Cmd {
	public class CommandService : ICommandService {
		#region ExecuteReturn
		public string ExecuteReturn(string binary, string args) {
			return InternExecuteReturn(GetProcStartInfo(binary, args, true));
		}

		public string ExecuteReturn(string command) {
			return InternExecuteReturn(GetProcStartInfo(command, true));
		}

		private string InternExecuteReturn(ProcessStartInfo startInfo) {
			var proc = GetProcess(startInfo);
			proc.Start();
			return proc.StandardOutput.ReadToEnd();
		}
		#endregion
		
		#region Execute
		public void Execute(string binary, string args) {
			InternExecute(GetProcStartInfo(binary, args, false));
		}

		public void Execute(string command) {
			InternExecute(GetProcStartInfo(command, false));
		}

		private void InternExecute(ProcessStartInfo startInfo) {
			var proc = GetProcess(startInfo);
			proc.Start();
			proc.WaitForExit();
		}
		#endregion

		private Process GetProcess(ProcessStartInfo procStartInfo) {
			return new Process {StartInfo = procStartInfo};
		}
		
		private ProcessStartInfo GetProcStartInfo(string command, bool redirectOutput) {
			return GetProcStartInfo("cmd", string.Format("/c {0}", command), redirectOutput);
		}

		private ProcessStartInfo GetProcStartInfo(string binary, string args, bool redirectOutput) {
			return new ProcessStartInfo(binary, args) {RedirectStandardOutput = redirectOutput, UseShellExecute = false, CreateNoWindow = false};
		}

		public string EscapeQuotes(string value) {
			return value.Replace(@"""", @"\""");
		}

		public string WrapWithQuotes(string value) {
			return string.Format(@"""{0}""", EscapeQuotes(value));
		}
	}
}