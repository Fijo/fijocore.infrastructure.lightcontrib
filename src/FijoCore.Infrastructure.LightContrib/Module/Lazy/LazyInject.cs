﻿using Fijo.Infrastructure.DesignPattern.Lazy.Base;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Lazy.Interface;
using Ninject;

namespace FijoCore.Infrastructure.LightContrib.Module.Lazy	{
    public class LazyInject<T> : LazyFieldBase<T>, ILazyInject<T>	{
	    private readonly IKernel _kernel;

	    public LazyInject() {
		    
	    }

	    public LazyInject(IKernel kernel) {
		    _kernel = kernel;
	    }

	    protected override T Resolve()	{
            return (_kernel ?? Kernel.Inject).Get<T>();
        }
    }
}
