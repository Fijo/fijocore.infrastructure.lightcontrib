using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;
using SType = System.Type;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Type {
	[PublicAPI]
	public static class ImplementsTypeExtention {
		[Pure, PublicAPI, DebuggerStepThrough]
		[Desc("returns if this type implements a given type")]
		public static bool ImplementsType([NotNull] this SType me, [NotNull] SType baseType) {
			#if PreferIsAssignableFrom
			return me != baseType && baseType.IsAssignableFrom(me);
			#else
			return baseType.IsInterface ? me.ImplementsInteface(baseType) : me.ImplementsClass(baseType);
			#endif
		}

		[Pure, PublicAPI, DebuggerStepThrough]
		[Desc("returns if this type implements a given type")]
		public static bool ImplementsType<TBaseType>([NotNull] this SType me) {
			return ImplementsType(me, typeof(TBaseType));
		}
	}
}