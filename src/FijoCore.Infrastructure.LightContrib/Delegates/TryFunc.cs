namespace FijoCore.Infrastructure.LightContrib.Delegates {
	public delegate bool TryFunc<T>(out T result);
	public delegate bool TryFunc();
}