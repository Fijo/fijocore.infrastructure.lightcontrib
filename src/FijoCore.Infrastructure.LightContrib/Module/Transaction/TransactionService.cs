using System.Collections.Generic;
using System.Diagnostics;
using Fijo.Infrastructure.DesignPattern.Events;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Transaction {
	[PublicAPI]
	public class TransactionService : ITransactionService {
		#region Implementation of ITransactionService
		#region AddEvent
		public void AddEvent(Transaction transaction, IEvent @event) {
			InternalAddEvent(transaction.Events, @event);
		}

		public void AddEvents(Transaction transaction, IEnumerable<IEvent> events) {
			var eventList = transaction.Events;
			events.ForEach(@event => InternalAddEvent(eventList, @event));
		}

		private void InternalAddEvent([NotNull] IList<IEvent> eventList, [NotNull] IEvent @event) {
			#region PreCondition
			Debug.Assert(!eventList.Contains(@event), "Do not add the same event more than one time into an Transaction.");
			#endregion
			eventList.Add(@event);
		}
		#endregion

		public IEnumerable<IEvent> GetEvents(Transaction transaction) {
			return transaction.Events;
		}
		#endregion
	}
}