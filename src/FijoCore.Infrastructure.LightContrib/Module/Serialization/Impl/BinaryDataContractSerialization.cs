using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml;
using FijoCore.Infrastructure.LightContrib.Enums;
using FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic.KeyValuePair;
using FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Func;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Settings;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization.Impl {
	// todo under deveopment
	// ToDo handle generic types
	public class ModelDataContractResolver : DataContractResolver {
		private readonly int _namespacePrefixLength = "http://schemas.datacontract.org/2004/07/".Length;
		private readonly IDictionary<string, Type> _types = AppDomain.CurrentDomain.GetAssemblies().SelectMany(x => x.GetTypes()).SelectMany(GetWithNestedTypes).DistinctMerge((a, b) => a, new FuncEqualityComparer<Type>((a, b) => GetFullName(a) == GetFullName(b), x => x.FullName.GetHashCode())).ToDict(x => GetFullName(x), x => x);

		private static string GetFullName(Type x) {
			return x.FullName.Replace('+', '.');
		}

		private static IEnumerable<Type> GetWithNestedTypes(Type type) {
			yield return type;
			foreach (var subType in type.GetNestedTypes().SelectMany(GetWithNestedTypes))
				yield return subType;
		}

		public override Type ResolveName(string typeName, string typeNamespace, Type declaredType, DataContractResolver knownTypeResolver) {
			
			var fullName = typeNamespace.Substring(_namespacePrefixLength) + "." + typeName;
			return knownTypeResolver.ResolveName(typeName, typeNamespace, declaredType, null) ?? _types[fullName];
		}

		public override bool TryResolveType(Type type, Type declaredType, DataContractResolver knownTypeResolver, out XmlDictionaryString typeName, out XmlDictionaryString typeNamespace) {
			if (knownTypeResolver.TryResolveType(type, declaredType, null, out typeName, out typeNamespace)) {
				return true;
			}

			XmlDictionary dictionary = new XmlDictionary();
			typeName = dictionary.Add(type.Name);
			typeNamespace = dictionary.Add(type.Namespace);

			return true;
		}
	}

	// todo under deveopment
	// todo correct usage ... it this is currently produing some xml not binary
	public class BinaryDataContractSerialization : ISerialization {
		private readonly IDictionary<Type, DataContractSerializer> _serializers = new Dictionary<Type, DataContractSerializer>();

		#region Implementation of ISerialization
		public void Serialize(object obj, System.IO.Stream stream, SerializationFormatting formatting, System.Text.Encoding encoding, ISerializationSettings serializationSettings = null) {
			Serialize(obj.GetType(), obj, stream, formatting, encoding, serializationSettings);
		}

		public void Serialize<T>(T obj, System.IO.Stream stream, SerializationFormatting formatting, System.Text.Encoding encoding, ISerializationSettings serializationSettings = null) {
			Serialize(typeof(T), obj, stream, formatting, encoding, serializationSettings);
		}

		public object Deserialize(System.IO.Stream stream, System.Text.Encoding encoding, ISerializationSettings serializationSettings = null) {
			return Deserialize<object>(stream, encoding, serializationSettings);
		}

		public T Deserialize<T>(System.IO.Stream stream, System.Text.Encoding encoding, ISerializationSettings serializationSettings = null) {
			var binaryReader = XmlDictionaryReader.CreateBinaryReader(stream, new XmlDictionary(), XmlDictionaryReaderQuotas.Max, new XmlBinaryReaderSession());
			return (T) GetSerializer(typeof (T)).ReadObject(binaryReader);
		}
		#endregion

		protected void Serialize(Type type, object obj, System.IO.Stream stream, SerializationFormatting formatting, System.Text.Encoding encoding, ISerializationSettings serializationSettings = null) {
			var binaryWriter = XmlDictionaryWriter.CreateBinaryWriter(stream);
			GetSerializer(type).WriteObject(binaryWriter, obj);
			binaryWriter.Flush();
		}

		protected DataContractSerializer GetSerializer(Type type) {
			return _serializers.GetOrCreate(type, () => GetDataContractSerializer(type));
		}

		private DataContractSerializer GetDataContractSerializer(Type type) {
			var dataContractSerializer = new DataContractSerializer(type, new []{typeof(Array)}, int.MaxValue, false, true, null, new ModelDataContractResolver());
			return dataContractSerializer;
		}
	}
}