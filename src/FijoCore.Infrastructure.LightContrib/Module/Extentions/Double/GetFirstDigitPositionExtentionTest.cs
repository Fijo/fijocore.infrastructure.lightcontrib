using FijoCore.Components.Extentions.Double;
using NUnit.Framework;

namespace FijoCore.ComponentTest.Extentions.Double {
	[TestFixture]
	public class GetFirstDigitPositionExtentionTest
	{
		[TestCase(5345.243, 3)]
		[TestCase(10.0, 1)]
		[TestCase(9.0, 0)]
		[TestCase(1.0, 0)]
		[TestCase(0.0, 0)]
		[TestCase(0.9, -1)]
		[TestCase(534234235.223443, 8)]
		[TestCase(0.5345243, -1)]
		[TestCase(0.0045243, -3)]
		public void Test(double value, int wanted) {
			Assert.AreEqual(wanted, value.GetFirstDigitPosition());
		}
	}
}