using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Enums {
	[PublicAPI]
	public enum CBool : byte {
		Always = 1,
		Never = 2,
		Custom = 3
	}
}