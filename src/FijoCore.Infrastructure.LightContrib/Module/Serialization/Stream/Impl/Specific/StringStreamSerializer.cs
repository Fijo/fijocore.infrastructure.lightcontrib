using System.IO;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Stream;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream.Impl.Specific {
	[UsedImplicitly]
	public class StringStreamSerializer : IStreamSerializer<string> {
		private readonly IStreamService _streamService = Kernel.Resolve<IStreamService>();
		private readonly IOut _out = Kernel.Resolve<IOut>();
		#region Implementation of IStreamSerializer<T>
		public void Serialize(StreamWriter streamWriter, string obj) {
			_streamService.WriteToStream(_streamService.GetStream(streamWriter), System.Text.Encoding.Default.GetBytes(obj));
		}

		public bool TryDeserialize(StreamReader streamReader, out string result) {
			var readToBytes = _streamService.ReadToBytes(_streamService.GetStream(streamReader));
			return _out.True(out result, System.Text.Encoding.Default.GetString(readToBytes));
		}
		#endregion
	}
}