﻿#if DEBUG
using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Default.Module.DebugView {
	[UsedImplicitly]
	internal sealed class CollectionDebugView<T> {
		private readonly ICollection<T> _collection;

		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
		public T[] Items {
			get {
				var collection = _collection;
				var array = new T[collection.Count];
				collection.CopyTo(array, 0);
				return array;
			}
		}

		public CollectionDebugView(ICollection<T> collection) {
			_collection = collection;
		}
	}
}
#endif