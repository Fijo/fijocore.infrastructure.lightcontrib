namespace FijoCore.Infrastructure.LightContrib.Module.Perf.Configuration {
	public class PerfConfigurationUpdaterState {
		public int Requests;
		public PerfConfiguration PerfConfiguration;
	}
}