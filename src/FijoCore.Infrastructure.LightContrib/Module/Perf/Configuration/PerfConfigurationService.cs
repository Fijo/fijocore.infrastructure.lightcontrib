using System;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Perf.Configuration {
	[UsedImplicitly]
	public class PerfConfigurationService : IPerfConfigurationService {
		private int _smallMemSize = 67108864;
		private int _maxSmallPartOfRamLeft = 128;

		#region Adjust
		public void Adjust(PerfConfiguration perfConfiguration) {
			perfConfiguration.RamForApp = GetRamForApp();
			perfConfiguration.RamLeft = GetRamLeft(perfConfiguration);
			perfConfiguration.RamSmallLeft = GetSmallRamLeft(perfConfiguration);
			perfConfiguration.MaxSizeForFixedListType = GetMaxSizeForFixedListType(perfConfiguration);
		}

		private long GetRamForApp() {
			return GC.GetTotalMemory(false);
		}

		private long GetRamLeft(PerfConfiguration perfConfiguration) {
			// only for the current process but I use this for a mashine... - may find a better way to collect that info.
			var used = Environment.WorkingSet;
			var ramLeft = perfConfiguration.RamForApp - used;
			return ramLeft;
		}

		private long GetSmallRamLeft(PerfConfiguration perfConfiguration) {
			return ((perfConfiguration.RamLeft / perfConfiguration.RamForApp) / _maxSmallPartOfRamLeft) & _smallMemSize;
		}

		private int GetMaxSizeForFixedListType(PerfConfiguration perfConfiguration) {
			return Math.Max((int) (perfConfiguration.RamSmallLeft / 256), 16);
		}
		#endregion
	}
}