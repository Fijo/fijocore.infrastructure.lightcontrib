using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Boolean {
	[PublicAPI]
	public static class ToLowerStringExtention {
		[NotNull, Pure, DebuggerStepThrough, PublicAPI]
		[Desc("Converts a bool to a lowercase string (�true�/ �false�)")]
		public static string ToLowerString(this bool me) {
			return me ? "true" : "false";
		}
	}
}