﻿using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using System.Runtime.InteropServices;
using FijoCore.Infrastructure.LightContrib.Default.Attributes;

// Allgemeine Informationen über eine Assembly werden über die folgenden 
// Attribute gesteuert. Ändern Sie diese Attributwerte, um die Informationen zu ändern,
// die mit einer Assembly verknüpft sind.
[assembly: AssemblyTitle("FijoCore.Infrastructure.LightContrib")]
[assembly: AssemblyDescription("The most important base Extention, Services and so on, that are needed as ground base for the FijoCore, especially that they are available in the infrastructure of the FijoCore.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Fijo")]
[assembly: AssemblyProduct("FijoCore.Infrastructure.LightContrib")]
[assembly: AssemblyCopyright("Copyright © Jonas Fischer <fijo.com@googlemail.com> 2012")]
[assembly: AssemblyTrademark("Fijo")]
[assembly: AssemblyCulture("")]

#region AssemblyEnableDefenition
#if DEBUG
[assembly: AssemblyEnableDefenitionAttibute("DEBUG")]
#endif

#if TRACE
[assembly: AssemblyEnableDefenitionAttibute("TRACE")]
#endif

#if PLATFORM_WINDOWS
[assembly: AssemblyEnableDefenitionAttibute("PLATFORM_WINDOWS")]
#endif

#if PLATFORM_UNIX
[assembly: AssemblyEnableDefenitionAttibute("PLATFORM_UNIX")]
#endif

#if PoolMaintainTrace
[assembly: AssemblyEnableDefenitionAttibute("PoolMaintainTrace")]
#endif

#if PreferIsAssignableFrom
[assembly: AssemblyEnableDefenitionAttibute("PreferIsAssignableFrom")]
#endif

#if InheritedTypeCache
[assembly: AssemblyEnableDefenitionAttibute("InheritedTypeCache")]
#endif

#if AllTypeProviderCache
[assembly: AssemblyEnableDefenitionAttibute("AllTypeProviderCache")]
#endif

#if DictionaryTypeCache
[assembly: AssemblyEnableDefenitionAttibute("DictionaryTypeCache")]
#endif

#if DebuggingUtils
[assembly: AssemblyEnableDefenitionAttibute("DebuggingUtils")]
#endif
#endregion


// Durch Festlegen von ComVisible auf "false" werden die Typen in dieser Assembly unsichtbar 
// für COM-Komponenten. Wenn Sie auf einen Typ in dieser Assembly von 
// COM zugreifen müssen, legen Sie das ComVisible-Attribut für diesen Typ auf "true" fest.
[assembly: ComVisible(false)]

// Die folgende GUID bestimmt die ID der Typbibliothek, wenn dieses Projekt für COM verfügbar gemacht wird
[assembly: Guid("f83aed86-726d-4105-8179-affd41e00269")]

// Versionsinformationen für eine Assembly bestehen aus den folgenden vier Werten:
//
//      Hauptversion
//      Nebenversion 
//      Buildnummer
//      Revision
//
// Sie können alle Werte angeben oder die standardmäßigen Build- und Revisionsnummern 
// übernehmen, indem Sie "*" eingeben:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]