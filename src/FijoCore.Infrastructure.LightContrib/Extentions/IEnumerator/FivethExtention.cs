using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator {
	[PublicAPI]
	public static class FivethExtention {
		[Pure, PublicAPI, DebuggerStepThrough]
		public static TSource Fourth<TSource>([NotNull] this IEnumerator<TSource> me) {
			return me.IndexTh(5);
		}
	}
}