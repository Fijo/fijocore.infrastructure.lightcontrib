using System;
using System.Collections.Generic;
using FijoCore.Infrastructure.LightContrib.Module.Pool.Dto;

namespace FijoCore.Infrastructure.LightContrib.Module.Pool.Interface {
	public interface IPool<out T, in TEntry> : IDisposable where TEntry : PoolEntry<T> {
		bool CanGet();
		T Get();
		void Add(IEnumerable<TEntry> candidates);
	}
}