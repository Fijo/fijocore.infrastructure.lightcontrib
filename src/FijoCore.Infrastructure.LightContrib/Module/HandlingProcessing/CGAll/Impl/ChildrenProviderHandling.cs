using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.CGAll.Impl {
	[PublicAPI]
	public abstract class ChildrenProviderHandling<TSource, TResult> : IChildrenProviderHandling<TSource, TResult> {
		protected abstract bool IsFor([NotNull] Type sourceObjType);

		[NotNull]
		protected abstract IEnumerable<TResult> GetChildrenGeneric([NotNull] TSource source);

		#region Implementation of ISupportsParallelization
		[NotNull]
		public IEnumerable<TResult> Get([NotNull] TSource source) {
			return GetChildrenGeneric(source);
		}

		public bool IsUsed([NotNull] TSource source) {
			return IsFor(source.GetType());
		}

		public virtual bool SupportParallelization { get { return true; } }
		#endregion
	}

	[PublicAPI]
	public abstract class ChildrenProviderHandling<T, TSource, TResult> : ChildrenProviderHandling<TSource, TResult> {
		protected override bool IsFor(Type sourceObjType) {
			return typeof (T).IsAssignableFrom(sourceObjType);
		}

		protected override IEnumerable<TResult> GetChildrenGeneric(TSource source) {
			return GetChildren(Cast(source));
		}

		[NotNull]
		protected abstract T Cast([NotNull] TSource source);

		[NotNull]
		protected abstract IEnumerable<TResult> GetChildren([NotNull] T obj);
	}
}