﻿using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Controller;
using Fijo.Infrastructure.DesignPattern.Lazy.Interface;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Base;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.CGAll;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.CGAll.Interface;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Cancelable;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Cancelable.Interface;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Conditional;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Conditional.Interface;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.GAll;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.GAll.Interface;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Simple;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Simple.Interface;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.TryGetResult;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.TryGetResult.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Lazy;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;

namespace FijoCore.Infrastructure.LightContrib.Properties {
	public class LightContribBaseInjectionModule : ExtendedNinjectModule {
		public override void OnLoad(IKernel kernel) {
			kernel.Bind(typeof (IFinder<,>)).To(typeof (Finder<,>)).InSingletonScope();
			kernel.Bind(typeof (ICHProcessor<>)).To(typeof (CHProcessor<>)).InSingletonScope();
			kernel.Bind(typeof (ICancelableHandlingProcessor<>)).To(typeof (CancelableHandlingProcessor<>)).InSingletonScope();
			kernel.Bind(typeof (ICGAllHandlingProcessor<,>)).To(typeof (CGAllHandlingProcessor<,>)).InSingletonScope();
			kernel.Bind(typeof (IGAllHandlingProcessor<,>)).To(typeof (GAllHandlingProcessor<,>)).InSingletonScope();
			kernel.Bind(typeof (ISimpleHandlingProcessor<>)).To(typeof (SimpleHandlingProcessor<>)).InSingletonScope();
			kernel.Bind(typeof (ITryGetResultHandlingProcessor<,>)).To(typeof (TryGetResultHandlingProcessor<,>)).InSingletonScope();
			kernel.Bind(typeof (ILazy<>)).To(typeof (LazyInject<>)).InSingletonScope();
		}
	}
}