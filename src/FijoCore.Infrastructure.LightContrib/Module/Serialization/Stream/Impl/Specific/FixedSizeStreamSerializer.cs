using System.Diagnostics;
using System.IO;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Stream;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream.Impl.Specific {
	public abstract class FixedSizeStreamSerializer<T> : IStreamSerializer<T> {
		private readonly IStreamService _streamService = Kernel.Resolve<IStreamService>();
		private readonly IOut _out = Kernel.Resolve<IOut>();

		protected FixedSizeStreamSerializer() {
			Debug.Assert(Size <= 1024, "please dont use this class for bigger StreamSerializer result objects, than a default buffer size of 1024.");
		}

		#region Implementation of IStreamSerializer<T>
		public virtual void Serialize(StreamWriter streamWriter, T obj) {
			var bytes = Serialize(obj);
			Debug.Assert(bytes.Length == Size, "You serialized object should have the size you have set for your StreamSerializer. Remember, that it has to be fixed. Otherwise you shouldn�t use this base class.");
			_streamService.WriteToStream(_streamService.GetStream(streamWriter), bytes);
		}

		public virtual bool TryDeserialize(StreamReader streamReader, out T result) {
			var readToBytes = _streamService.ReadToBytes(_streamService.GetStream(streamReader), Size, Size);
			return readToBytes.Length == Size
				       ? _out.True(out result, Deserialize(readToBytes))
				       : _out.False(out result);
		}
		#endregion

		protected abstract int Size { get; }
		protected abstract byte[] Serialize(T obj);
		protected abstract T Deserialize(byte[] bytes);
	}
}