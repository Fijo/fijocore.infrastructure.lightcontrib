using System;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream.Impl.Specific {
	[UsedImplicitly]
	public class UInt32StreamSerializer : FixedSizeStreamSerializer<uint> {
		#region Overrides of FixedSizeStreamSerializer<uint>
		protected override int Size { get { return 4; } }
		protected override byte[] Serialize(uint obj) {
			return BitConverter.GetBytes(obj);
		}

		protected override uint Deserialize(byte[] bytes) {
			return BitConverter.ToUInt32(bytes, 0);
		}
		#endregion
	}
}