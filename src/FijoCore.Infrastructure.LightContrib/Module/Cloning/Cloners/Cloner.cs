﻿using System;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Dtos;

namespace FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners {
	public abstract class Cloner<T> : ICloner<T> {
		#region Implementation of ISupportsParallelization
		public abstract void Copy(T source, T result, ICloning cloning);

		public virtual bool SupportParallelization { get { return true; } }
		#endregion
		#region Implementation of ICH<in CloneSource>
		public bool IsUsed(CloneSource obj) {
			return For.IsInstanceOfType(obj.Result);
		}

		public void Process(CloneSource obj) {
			Copy(obj.Source, obj.Result, obj.Cloning);
		}

		public Type For { get { return typeof (T); } }

		public void Copy(object source, object result, ICloning cloning) {
			Copy((T) source, (T) result, cloning);
		}
		#endregion
	}
}