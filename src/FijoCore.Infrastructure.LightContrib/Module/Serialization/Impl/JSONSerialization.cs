using Fijo.Infrastructure.DesignPattern.Mapping;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Enums;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Settings;
using FijoCore.Infrastructure.LightContrib.Module.Stream;
using Newtonsoft.Json;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization.Impl {
	public class JSONSerialization : ExtSerialization<IJsonSettings> {
		private readonly IMapping<SerializationFormatting, Formatting> _mapping;
		private readonly IStreamService _streamService;
		private readonly IJsonSettings _defaultSettings;

		public JSONSerialization(IStreamService streamService, IMapping<SerializationFormatting, Formatting> mapping, IJsonSettings defaultSettings) {
			_streamService = streamService;
			_mapping = mapping;
			_defaultSettings = defaultSettings;
		}

		#region Implementation of ISerialization
		protected override void Serialize(object obj, System.IO.Stream stream, SerializationFormatting formatting, System.Text.Encoding encoding, IJsonSettings serializationSettings = null) {
			Serialize<object>(obj, stream, formatting, encoding, serializationSettings);
		}

		protected override void Serialize<T>(T obj, System.IO.Stream stream, SerializationFormatting formatting, System.Text.Encoding encoding, IJsonSettings serializationSettings = null) {
			_streamService.WriteToStream(stream, JsonConvert.SerializeObject(obj, _mapping.Map(formatting), GetJsonSerializerSettings(serializationSettings)), encoding);
		}

		protected override object Deserialize(System.IO.Stream stream, System.Text.Encoding encoding, IJsonSettings serializationSettings = null) {
			return JsonConvert.DeserializeObject(_streamService.ReadToString(stream, encoding), GetJsonSerializerSettings(serializationSettings));
		}

		protected override T Deserialize<T>(System.IO.Stream stream, System.Text.Encoding encoding, IJsonSettings serializationSettings = null) {
			return JsonConvert.DeserializeObject<T>(_streamService.ReadToString(stream, encoding), GetJsonSerializerSettings(serializationSettings));
		}
		#endregion

		private JsonSerializerSettings GetJsonSerializerSettings(IJsonSettings serializationSettings) {
			return (serializationSettings ?? _defaultSettings).WhenNotNull(x => x.Get());
		}
	}
}