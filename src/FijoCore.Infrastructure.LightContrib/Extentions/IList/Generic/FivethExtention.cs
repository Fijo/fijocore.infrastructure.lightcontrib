using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IList.Generic {
	public static class FivethExtention {
		[DebuggerStepThrough]
		public static TSource Fourth<TSource>([NotNull] this IList<TSource> me) {
			return me.IndexTh(5);
		}
	}
}