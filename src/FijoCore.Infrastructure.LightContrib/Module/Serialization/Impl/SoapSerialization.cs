using System.Runtime.Serialization.Formatters.Soap;
using FijoCore.Infrastructure.LightContrib.Enums;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Settings;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization.Impl {
	public class SoapSerialization : ISerialization {
		private readonly SoapFormatter _soapFormatter = new SoapFormatter();
		#region Implementation of ISerialization
		public void Serialize(object obj, System.IO.Stream stream, SerializationFormatting formatting, System.Text.Encoding encoding, ISerializationSettings serializationSettings = null) {
			Serialize<object>(obj, stream, formatting, encoding);
		}

		public void Serialize<T>(T obj, System.IO.Stream stream, SerializationFormatting formatting, System.Text.Encoding encoding, ISerializationSettings serializationSettings = null) {
			_soapFormatter.Serialize(stream, obj);
		}

		public object Deserialize(System.IO.Stream stream, System.Text.Encoding encoding, ISerializationSettings serializationSettings = null) {
			return _soapFormatter.Deserialize(stream);
		}

		public T Deserialize<T>(System.IO.Stream stream, System.Text.Encoding encoding, ISerializationSettings serializationSettings = null) {
			return (T) Deserialize(stream, encoding);
		}
		#endregion
	}
}