using System;
using System.Collections.Generic;
using Fijo.Infrastructure.DesignPattern.Repository;
using FijoCore.Infrastructure.LightContrib.Enums;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization {
	[UsedImplicitly]
	public class SerializationProvider : ISerializationProvider {
		private readonly IDictionary<SerializationFormat, ISerialization> _serializer;

		public SerializationProvider(IRepository<IDictionary<SerializationFormat, ISerialization>> serializationRepository) {
			_serializer = serializationRepository.Get();
		}

		public ISerialization Get(SerializationFormat format) {
			ISerialization serializer;
			if(!_serializer.TryGetValue(format, out serializer)) throw new NotSupportedException(string.Format("SerializationFormat {0}", Enum.GetName(typeof (SerializationFormat), format)));
			return serializer;
		}
	}
}