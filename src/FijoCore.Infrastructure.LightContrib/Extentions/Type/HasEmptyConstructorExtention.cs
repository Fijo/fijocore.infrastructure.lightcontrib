using System.Diagnostics;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Extentions.ICollection.Generic;
using JetBrains.Annotations;
using SType = System.Type;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Type {
	[PublicAPI]
	public static class HasEmptyConstructorExtention {
		[Pure, PublicAPI, DebuggerStepThrough]
		[Desc("Reflection based new() constrain")]
		public static bool HasEmptyConstructor([NotNull] this SType me) {
			return me.GetConstructors()
				.Any(x => x.GetParameters().None());
		}
	}
}