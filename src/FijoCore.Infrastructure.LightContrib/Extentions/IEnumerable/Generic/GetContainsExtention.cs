using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using FijoCore.Infrastructure.LightContrib.Enums;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic
{
	public static class GetContainsExtention
	{
		[DebuggerStepThrough]
		public static Contains GetContains<TSource>([NotNull] this IEnumerable<TSource> me, [NotNull] System.Func<TSource, bool> predicate)
		{
			var candidates = me.Select(predicate).ToArray();
			if(candidates.All(x => x)) return Contains.All;
			if(candidates.Any(x => x)) return Contains.Some;
			return Contains.None;
		}
	}
}