﻿using System;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Default.Exceptions {
	[PublicAPI, Serializable]
	public class NotConvertableException : Exception {
		public NotConvertableException() {}

		public NotConvertableException(string message) : base(message) {}

		public NotConvertableException(string message, Exception innerException) : base(message, innerException) {}
	}
}
