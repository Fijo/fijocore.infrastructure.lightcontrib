using System.Diagnostics;
using System.Globalization;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Integer {
	[PublicAPI]
	public static class InvariantToStringExtention {
		[Pure, DebuggerStepThrough, PublicAPI]
		[Desc("Converts a int to string using invariant culture settings")]
		public static string InvariantToString(this int me) {
			return me.ToString(CultureInfo.InvariantCulture);
		}
	}
}