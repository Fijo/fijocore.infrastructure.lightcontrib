namespace FijoCore.Infrastructure.LightContrib.Enums {
	public enum SerializationFormatting {
		Default,
		Indented
	}
}