using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	[PublicAPI]
	public static class CountWhereExtention {
		[Pure, PublicAPI, DebuggerStepThrough]
		public static int CountWhere<T>([NotNull] this IEnumerable<T> me, T item) {
			return me.Count(x => x.Equals(item));
		}

		[Pure, PublicAPI, DebuggerStepThrough]
		public static int CountWhere<T>([NotNull] this IEnumerable<T> me, T item, [NotNull] Func<T, T, bool> equals) {
			return me.Count(x => equals(item, x));
		}
	}
}