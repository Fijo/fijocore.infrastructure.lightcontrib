using System;
using System.Collections.Generic;
using System.Linq;
using Fijo.Infrastructure.DesignPattern.Repository;
using FijoCore.Infrastructure.LightContrib.Extentions.IList.Generic;
using FijoCore.Infrastructure.LightContrib.Module.Assembly;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Repositories {
	[UsedImplicitly]
	public class TypeProvider : RepositoryBase<IEnumerable<Type>> {
		private readonly AssembliesProvider _assembliesProvider;
		#if AllTypeProviderCache
		private readonly IList<Type> _cache = new List<Type>();
		private bool _cacheIsSet;
		#endif

		public TypeProvider(AssembliesProvider assembliesProvider) {
			_assembliesProvider = assembliesProvider;
			#if AllTypeProviderCache
			AppDomain.CurrentDomain.AssemblyLoad += OnAssemblyLoad;
			#endif
		}

		#if AllTypeProviderCache
		private void OnAssemblyLoad(object sender, AssemblyLoadEventArgs args) {
			lock (_cache) {
				_cacheIsSet = false;
				_cache.Clear();
			}
		}
		#endif
		
		#region Overrides of RepositoryBase<IEnumerable<Type>>
		public override IEnumerable<Type> Get() {
			#if AllTypeProviderCache
			lock (_cache) {
				if (!_cacheIsSet) {
					_cache.AddRange(CreateIntern(), true);
					_cacheIsSet = true;
				}
				return _cache;
			}
			#else
			return CreateIntern();
			#endif
		}

		private IEnumerable<Type> CreateIntern() {
			return _assembliesProvider.GetAssemblies().SelectMany(x => x.GetTypes()).ToList();
		}
		#endregion
	}
}