using System.Runtime.CompilerServices;
using FijoCore.Components.Extentions.IEnumerable.Generic;
using NUnit.Framework;

namespace FijoCore.ComponentTest.Extentions.IEnumerable {
	[TestFixture]
	public class AddExtentionTest
	{
		[Test]
		public void TestAddReturnDoesNotReturnTheSameAsTheMeContext()
		{
			var result = new[]
			             {
			             	"hallo",
			             	"test",
			             	"lol"
			             };
			var newResult = result.AddReturn("welt");
			Assert.IsFalse(RuntimeHelpers.Equals(result, newResult));
		}

		[Test]
		public void TestAddReturnDoesReturnTheCorrectCollection()
		{
			var result = new[]
			             {
			             	"hallo",
			             	"test",
			             	"lol"
			             };
			var newResult = result.AddReturn("welt");

			var wanted = new[]
			             {
			             	"hallo",
			             	"test",
			             	"lol",
			             	"welt"
			             };

			CollectionAssert.AreEquivalent(wanted, newResult);
		}
	}
}