using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	[PublicAPI]
	public static class ChangeIndexExtention {
		[NotNull, Pure, PublicAPI, DebuggerStepThrough]
		[Desc("Select the �me� Enumerable replacing the value at �index� with �value� (if the collection contains length is not smaller or equal with the index).")]
		public static IEnumerable<T> ChangeIndex<T>([NotNull] this IEnumerable<T> me, int index, T value) {
			#region PreCondition
			Debug.Assert(index >= 0, "index >= 0");
			#endregion
			return me.Select((x, i) => i == index ? value : x);
		}
	}
}