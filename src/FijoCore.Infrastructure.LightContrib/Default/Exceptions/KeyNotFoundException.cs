using System;
using JetBrains.Annotations;
using SKeyNotFoundException = System.Collections.Generic.KeyNotFoundException;

namespace FijoCore.Infrastructure.LightContrib.Default.Exceptions {
	[PublicAPI, Serializable]
	public class KeyNotFoundException : SKeyNotFoundException {
		[PublicAPI] public object SourceObj { get; set; }
		[PublicAPI] public object Key { get; set; }
		
		public KeyNotFoundException([NotNull] object source, [NotNull] object key, string message = default(string), Exception innerException = null) : base(GetMessage(key, message), innerException) {
			SourceObj = source;
			Key = key;
		}

		private static string GetMessage(object key, string message) {
			return string.Format(string.IsNullOrEmpty(message)
				                     ? "Key: �{0}�"
				                     : "Key: �{0}�. {1}", key, message);
		}
	}

	[PublicAPI, Serializable]
	public class KeyNotFoundException<TSource, TKey> : KeyNotFoundException {
		[PublicAPI] public new TSource SourceObj { get; set; }
		[PublicAPI] public new TKey Key { get; set; }
		
		public KeyNotFoundException(TSource source, TKey key, string message = default(string), Exception innerException = null) : base(source, key, message, innerException) {
			SourceObj = source;
			Key = key;
		}
	}
}