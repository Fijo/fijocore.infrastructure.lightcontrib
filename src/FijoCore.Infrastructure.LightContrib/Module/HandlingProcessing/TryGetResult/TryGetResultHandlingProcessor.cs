using System.Collections.Generic;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Default.Exceptions;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Base;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Base.Interface;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.TryGetResult.Interface;

namespace FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.TryGetResult {
	[Note("This class was called ´MethodeManager´ in the past.")]
	[Note("This class was called ´HandlingProcessor´ in the past.")]
	public class TryGetResultHandlingProcessor<TSource, TResult> : HandlingProcessor<ITryGetResultHandling<TSource, TResult>>, ITryGetResultHandlingProcessor<TSource, TResult> {
		#region Process
		public TryGetResultHandlingProcessor(IHandlingProvider<ITryGetResultHandling<TSource, TResult>> handlingRepository) : base(handlingRepository) {}

		protected virtual TResult ProcessHandlings(IEnumerable<ITryGetResultHandling<TSource, TResult>> handlings, TSource source) {
			var result = default(TResult);
			if (handlings.Any(methode => methode.TryProcess(source, out result))) return result;
			throw new NoneSucessfullTryGetResultHandlingFoundException<TSource, TResult>(this, source);
		}

		public TResult Process(TSource source) {
			return ProcessHandlings(Handlings, source);
		}
		
		public TResult Process(IEnumerable<ITryGetResultHandling<TSource, TResult>> customTryGetResultHandlings, TSource source) {
			return customTryGetResultHandlings == null
			       	? Process(source)
			       	: ProcessHandlings(Handlings.Concat(customTryGetResultHandlings), source);
		}
		#endregion
	}
}