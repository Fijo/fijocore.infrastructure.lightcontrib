using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Base.Interface;

namespace FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.TryGetResult.Interface {
	[Note("This class was called ´IMethodeManager´ in the past.")]
	public interface ITryGetResultHandlingProcessor<TSource, TResult> : IHandlingProcessor<ITryGetResultHandling<TSource, TResult>> {
		TResult Process(TSource source);
		TResult Process(IEnumerable<ITryGetResultHandling<TSource, TResult>> customTryGetResultHandlings, TSource source);
	}
}