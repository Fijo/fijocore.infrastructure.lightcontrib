using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IList.Generic {
	[PublicAPI]
	public static class AddExtention {
		[NotNull, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<T> AddReturn<T>([NotNull] this IList<T> me, T item) {
			me.Add(item);
			return me;
		}
	}
}