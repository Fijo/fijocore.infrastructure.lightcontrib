using System.Collections.Generic;
using System.Diagnostics;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.ISet.Generic {
	[PublicAPI]
	public static class RemoveRangeExtention {
		[PublicAPI, DebuggerStepThrough]
		public static void RemoveRange<T>([NotNull] this ISet<T> me, [NotNull] IEnumerable<T> candidates) {
			candidates.ForEach(me.RemoveSecure);
		}
	}
}