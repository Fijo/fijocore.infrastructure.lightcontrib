﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Text;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Byte {
	[PublicAPI]
	public static class HexExtentions {
		private static readonly sbyte[] Lookup = Enumerable.Range(0, 10).Select(x => (sbyte) ('0' + x)).Concat(
			Enumerable.Range(0, 6).Select(x => (sbyte) ('A' + x))).ToArray();

		[NotNull, Pure, DebuggerStepThrough, PublicAPI]
		public unsafe static string ToHexString(this byte me) {
			sbyte* bytes = stackalloc sbyte[2];
			*bytes = Lookup[me >> 4];
			*(bytes+1) = Lookup[me & 15];
			return new string(bytes, 0, 2, Encoding.UTF8);
		}

		[NotNull, Pure, DebuggerStepThrough, PublicAPI]
		public static string ToHexString(this System.Collections.Generic.IEnumerable<byte> me) {
			return string.Join("-", me.Select(ToHexString));
		}

		[Pure, DebuggerStepThrough, PublicAPI]
		public static byte FromHexString(this string me) {
			return (byte) ((GetHexByte((byte) me[0]) << 4) + GetHexByte((byte) me[1]));
		}

		[Pure, DebuggerStepThrough, PublicAPI]
		public static System.Collections.Generic.IEnumerable<byte> ArrayFromHexString(this string me) {
			return me.Split('-').Select(x => x.FromHexString());
		}

		private static byte GetHexByte(byte b) {
			if (b >= '0' && b <= '9') return (byte) (b - '0');
			if (b >= 'A' && b <= 'F') return (byte) (10 + b - 'A');
			throw new IndexOutOfRangeException();
		}
	}
}