using FijoCore.Infrastructure.LightContrib.Module.Number.Service.Base;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Number.Service.Module {
	[UsedImplicitly]
	public class DoubleNumberService : NumberServiceBase<double> {
		#region Implementation of INumberService<byte>
		public override double MaxValue { get { return double.MaxValue; } }
		public override double MinValue { get { return double.MinValue; } }
		public override bool IsInteger { get { return false; } }

		public override double Sun(double left, double right) {
			return left + right;
		}

		public override double Diff(double left, double right) {
			return left - right;
		}

		public override double Times(double left, double right) {
			return left * right;
		}

		public override double Divide(double left, double right) {
			return left / right;
		}

		public override double Modulo(double left, double right) {
			return left % right;
		}
		#endregion
	}
}