namespace FijoCore.Infrastructure.LightContrib.Module.Perf.Configuration {
	public interface IPerfConfigurationUpdater {
		void Start(PerfConfigurationUpdaterState state);
	}
}