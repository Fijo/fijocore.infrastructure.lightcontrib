using System;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Converter.Simple.Interface {
	[PublicAPI]
	public interface IConverter {
		[NotNull, PublicAPI] Type SourceType { get; }
		[NotNull, PublicAPI] Type TargetType { get; }

		[PublicAPI]
		object Convert(object source);
	}

	[PublicAPI]
	public interface IConverter<in TSource, out TResult> : IConverter {
		[PublicAPI] TResult Convert(TSource source);
	}
}