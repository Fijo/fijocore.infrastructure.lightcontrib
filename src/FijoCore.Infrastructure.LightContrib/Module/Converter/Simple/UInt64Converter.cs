using FijoCore.Infrastructure.LightContrib.Module.Converter.Simple.Base;

namespace FijoCore.Infrastructure.LightContrib.Module.Converter.Simple {
	public class UInt64Converter : ConverterBase<long, ulong> {
		#region Overrides of ConverterBase<long,int>
		public override ulong Convert(long source) {
			return System.Convert.ToUInt64(source);
		}
		#endregion
	}
}