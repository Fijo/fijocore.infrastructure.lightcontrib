using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Stream {
	[UsedImplicitly]
	public class StreamFacade : IStreamService {
		private readonly IStreamService _defaultStreamService;
		private readonly IDictionary<Func<System.IO.Stream, bool>, IStreamService> _streamServices;

		public StreamFacade() {
			_defaultStreamService = Kernel.Resolve<IDefaultStreamService>();
			_streamServices = new Dictionary<Func<System.IO.Stream, bool>, IStreamService>
			{
				{s => s.CanSeek, _defaultStreamService},
				{s => !s.CanSeek, Kernel.Resolve<ISeeklessStreamService>()}
			};
		}

		[NotNull]
		protected IStreamService Get([NotNull] System.IO.Stream stream) {
			return _streamServices.First(x => x.Key(stream)).Value;
		}

		#region Implementation of IStreamService
		public string ReadToString(System.IO.Stream stream, bool autodetect = false) {
			return Get(stream).ReadToString(stream, autodetect);
		}

		public string ReadToString(System.IO.Stream stream, System.Text.Encoding encoding) {
			return Get(stream).ReadToString(stream, encoding);
		}

		public byte[] ReadToBytes(System.IO.Stream stream, int bufferSize = 1024) {
			return Get(stream).ReadToBytes(stream, bufferSize);
		}

		public byte[] ReadToBytes(System.IO.Stream stream, int count, int bufferSize = 1024) {
			return Get(stream).ReadToBytes(stream, count, bufferSize);
		}

		public IEnumerable<byte> ReadToBytesSequence(System.IO.Stream stream, int bufferSize = 1024) {
			return Get(stream).ReadToBytesSequence(stream, bufferSize);
		}

		public IEnumerable<byte> ReadToBytesSequence(System.IO.Stream stream, int count, int bufferSize = 1024) {
			return Get(stream).ReadToBytesSequence(stream, count, bufferSize);
		}

		public char[] ReadToChars(System.IO.Stream stream, bool autodetect = false) {
			return Get(stream).ReadToChars(stream, autodetect);
		}

		public char[] ReadToChars(System.IO.Stream stream, System.Text.Encoding encoding) {
			return Get(stream).ReadToChars(stream, encoding);
		}

		public StreamReader GetReader(System.IO.Stream stream, bool autodetect = false) {
			return Get(stream).GetReader(stream, autodetect);
		}

		public StreamReader GetReader(System.IO.Stream stream, System.Text.Encoding encoding) {
			return Get(stream).GetReader(stream, encoding);
		}

		public StreamWriter GetWriter(System.IO.Stream stream) {
			return Get(stream).GetWriter(stream);
		}

		public StreamWriter GetWriter(System.IO.Stream stream, System.Text.Encoding encoding) {
			return Get(stream).GetWriter(stream, encoding);
		}

		public void WriteToStream(System.IO.Stream stream, string str) {
			Get(stream).WriteToStream(stream, str);
		}

		public void WriteToStream(System.IO.Stream stream, string str, System.Text.Encoding encoding) {
			Get(stream).WriteToStream(stream, str, encoding);
		}

		public void WriteToStream(System.IO.Stream stream, byte[] bytes) {
			Get(stream).WriteToStream(stream, bytes);
		}

		public void WriteToStream(System.IO.Stream stream, byte[] bytes, int count) {
			Get(stream).WriteToStream(stream, bytes, count);
		}

		public void WriteToStream(System.IO.Stream stream, IEnumerable<byte> bytes) {
			Get(stream).WriteToStream(stream, bytes);
		}

		[ModificationNote("may modify the description of the StreamServiceBase.GetStream- methodes")]
		public System.IO.Stream GetStream(StreamWriter streamWriter) {
			return _defaultStreamService.GetStream(streamWriter);
		}

		[ModificationNote("may modify the description of the StreamServiceBase.GetStream- methodes")]
		public System.IO.Stream GetStream(StreamReader streamReader) {
			return _defaultStreamService.GetStream(streamReader);
		}
		#endregion
	}
}