using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	[PublicAPI]
	public static class SelectExtention {
		[NotNull, Pure, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<TResult> Select<TSource, TResult>([NotNull] this IEnumerable<TSource> me, [NotNull] Func<TSource, TResult> resultSelector, bool executeParallel) {
			return me.Opt(executeParallel, x => x.Select(resultSelector), x => x.Select(resultSelector));
		}
		
		[NotNull, Pure, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<TResult> Select<TSource, TResult>([NotNull] this IEnumerable<TSource> me, [NotNull] Func<TSource, int, TResult> resultSelector, bool executeParallel) {
			return me.Opt(executeParallel, x => x.Select(resultSelector), x => x.Select(resultSelector));
		}
	}
}