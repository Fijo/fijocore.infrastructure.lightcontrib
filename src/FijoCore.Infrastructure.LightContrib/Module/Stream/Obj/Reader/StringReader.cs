﻿using System;
using System.IO;
using System.Net.Mime;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using JetBrains.Annotations;
using SStream = System.IO.Stream;
using SEncoding = System.Text.Encoding;

namespace FijoCore.Infrastructure.LightContrib.Module.Stream.Obj.Reader {
	[Serializable]
	public class StringReader : IStringReader {
		[NotNull] protected readonly StreamReader Reader;
		[NotNull] protected readonly IOut Out;

		public SEncoding CurrentEncoding { get { return Reader.CurrentEncoding; } }
		public bool EndOfStream { get { return Reader.EndOfStream; } }
		public SStream BaseStream { get { return Reader.BaseStream; } }

		public StringReader([NotNull] StreamReader reader, IOut @out) {
			Reader = reader;
			Out = @out;
		}
		#region Peek
		public char Peek() {
			char result;
			if(!TryPeek(out result)) throw GetEndOfStream();
			return result;
		}

		public bool TryPeek(out char result) {
			return TryReader(out result, Reader.Peek());
		}
		#endregion

		#region Read
		public char Read() {
			char result;
			if(!TryRead(out result)) throw GetEndOfStream();
			return result;
		}

		public bool TryRead(out char result) {
			return TryReader(out result, Reader.Read());
		}

		public int ReadBlock(char[] buffer, int index, int count) {
			return Reader.ReadBlock(buffer, index, count);
		}
		
		public string ReadLine() {
			return Reader.ReadLine();
		}
		
		public string ReadToEnd() {
			return Reader.ReadToEnd();
		}
		#endregion
		
		private bool TryReader(out char result, int code) {
			return Out.Generic(out result, !IsEndOfStream(code), () => (char) code);
		}

		[Pure]
		private Exception GetEndOfStream() {
			return new EndOfStreamException("The stream used in the StreamReader reatched the end.");
		}

		[Pure]
		private bool IsEndOfStream(int code) {
			return code == -1;
		}
		#region Close
		public void Close() {
			Reader.Close();
		}
		#endregion

		#region Implementation of IDisposable
		public void Dispose() {
			Reader.Dispose();
		}
		#endregion
	}
}