﻿using FijoCore.Infrastructure.DependencyInjection.InitKernel.Init;
using FijoCore.Infrastructure.LightContrib.Properties;

namespace FijoCore.Infrastructure.LightContribTest.Properties
{
    public class InternalInitKernel : ExtendedInitKernel
    {
        public override void PreInit()
        {
            LoadModules(new LightContribInjectionModule());
        }
    }
}
