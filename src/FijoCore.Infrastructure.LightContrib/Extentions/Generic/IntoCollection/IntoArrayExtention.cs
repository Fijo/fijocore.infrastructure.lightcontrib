using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Generic.IntoCollection {
	[PublicAPI]
	public static class IntoArrayExtention {
		[Pure, PublicAPI, DebuggerStepThrough]
		[Desc("Creates a new array[1] of type T with this as index 0 value")]
		public static T[] IntoArray<T>(this T me) {
			return new[] {me};
		}
	}
}