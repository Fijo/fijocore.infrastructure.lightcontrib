using System.Diagnostics;
using System.Globalization;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Boolean {
	[PublicAPI]
	public static class InvariantToStringExtention {
		[NotNull, Pure, DebuggerStepThrough, PublicAPI]
		[Desc("Converts a bool to string using invariant culture settings")]
		public static string InvariantToString(this bool me) {
			return me.ToString(CultureInfo.InvariantCulture);
		}
	}
}