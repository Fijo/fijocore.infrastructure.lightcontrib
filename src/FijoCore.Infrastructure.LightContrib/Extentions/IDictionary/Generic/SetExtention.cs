using System.Diagnostics;
using FijoCore.Infrastructure.LightContrib.Delegates;
using JetBrains.Annotations;
using System.Collections.Generic;
using System;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic {
	[PublicAPI]
	public static class SetExtention {
		[Pure, DebuggerStepThrough]
		private static bool DefaultUseExisingValue<TValue>(TValue value) {
			return true;
		}

		[PublicAPI, DebuggerStepThrough]
		public static void Set<TKey, TValue>([NotNull] this IDictionary<TKey, TValue> me, TKey key, TValue value, [NotNull] MergeWithExisting<TValue> mergeWithExistingValue) {
			me.Set(key, value, DefaultUseExisingValue, mergeWithExistingValue);
		}

		[PublicAPI, DebuggerStepThrough]
		public static void Set<TKey, TValue>([NotNull] this IDictionary<TKey, TValue> me, TKey key, TValue value, [NotNull] Func<TValue, bool> useExistingValue, [NotNull] MergeWithExisting<TValue> mergeWithExistingValue) {
			TValue currentValue;
			lock (me)
				me[key] = !me.TryGetValue(key, out currentValue) || !useExistingValue(currentValue)
				          	? value
				          	: mergeWithExistingValue(currentValue, value);
		}
	}
}