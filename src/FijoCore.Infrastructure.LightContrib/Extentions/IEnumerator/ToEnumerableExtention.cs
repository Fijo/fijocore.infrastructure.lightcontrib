using System.Collections.Generic;
using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using SIEnumerator = System.Collections.IEnumerator;
using SIEnumerable = System.Collections.IEnumerable;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator {
	public static class ToEnumerableExtention {
		private class DummyEnumerable<T> : IEnumerable<T> {
			private readonly IEnumerator<T> _enumerator;

			public DummyEnumerable(IEnumerator<T> enumerator) {
				_enumerator = enumerator;
			}

			public IEnumerator<T> GetEnumerator() {
				return _enumerator;
			}

			SIEnumerator SIEnumerable.GetEnumerator() {
				return GetEnumerator();
			}
		}

		[DebuggerStepThrough]
		[PerformanceRisky("because of a on the fly class generation in for such a simple functionality, that is may executed in many loops")]
		public static IEnumerable<TSource> ToEnumerable<TSource>(this IEnumerator<TSource> me) {
			return new DummyEnumerable<TSource>(me);
		}
	}
}