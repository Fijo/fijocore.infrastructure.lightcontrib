using Fijo.Infrastructure.Model.Validation;

namespace FijoCore.Infrastructure.LightContrib.Factories.Interface {
	public interface IValidationObjFactory {
		ValidationContext<TSource, TValidatior> CreateContext<TSource, TValidatior>(TSource source, TValidatior validatior);
		ValidationResult<TSource, TValidatior, TRule> CreateResult<TSource, TValidatior, TRule>(ValidationContext<TSource, TValidatior> context, TRule rule, bool result);
	}
}