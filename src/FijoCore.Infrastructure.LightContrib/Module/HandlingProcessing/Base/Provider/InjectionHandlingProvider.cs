using System.Collections.Generic;
using System.Linq;
using Fijo.Infrastructure.DesignPattern.Repository.Collection;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Base.Interface;
using Ninject;

namespace FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Base.Provider {
	public class InjectionHandlingProvider<THandling> : CollectionRepositoryBase<THandling>, IHandlingProvider<THandling> {
		#region Overrides of CollectionRepositoryBase<THandling>
		protected override IEnumerable<THandling> GetAll() {
			return Kernel.Inject.GetAll<THandling>();
		}
		#endregion
	}
	
	public class InjectionHandlingProvider<THandling, TInjectHandling> : CollectionRepositoryBase<THandling>, IHandlingProvider<THandling> where TInjectHandling : THandling {
		#region Overrides of CollectionRepositoryBase<THandling>
		protected override IEnumerable<THandling> GetAll() {
			return Kernel.Inject.GetAll<TInjectHandling>().Cast<THandling>();
		}
		#endregion
	}
}