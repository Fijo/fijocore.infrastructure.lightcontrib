using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	[PublicAPI]
	public static class SkipWhileValueExtention {
		[Pure, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<T> SkipWhileValue<T>([NotNull] this IEnumerable<T> me, T value) {
			return me.SkipWhile(x => Equals(x, value));
		}

		[Pure, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<T> SkipWhileValue<T>([NotNull] this IEnumerable<T> me, T value, [NotNull] IEqualityComparer<T> comparer) {
			return me.SkipWhile(x => comparer.Equals(x, value));
		}
		
		[Pure, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<T> SkipWhileValues<T>([NotNull] this IEnumerable<T> me, [NotNull] ICollection<T> values) {
			return me.SkipWhile(values.Contains);
		}
		
		[Pure, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<T> SkipWhileValues<T>([NotNull] this IEnumerable<T> me, [NotNull] params T[] values) {
			return SkipWhileValues(me, (ICollection<T>) values);
		}

		[Pure, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<T> SkipWhileValues<T>([NotNull] this IEnumerable<T> me, [NotNull] ICollection<T> values, [NotNull] IEqualityComparer<T> comparer) {
			return me.SkipWhile(x => values.Contains(x, comparer));
		}

		[Pure, PublicAPI, DebuggerStepThrough]
		public static IEnumerable<T> SkipWhileValues<T>([NotNull] this IEnumerable<T> me, [NotNull] IEqualityComparer<T> comparer, [NotNull] params T[] values) {
			return SkipWhileValues(me, values, comparer);
		}
	}
}