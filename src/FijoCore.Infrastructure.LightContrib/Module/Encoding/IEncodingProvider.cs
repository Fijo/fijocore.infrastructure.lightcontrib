using JetBrains.Annotations;
using SEncoding = System.Text.Encoding;

namespace FijoCore.Infrastructure.LightContrib.Module.Encoding {
	public interface IEncodingProvider {
		[NotNull, Pure]
		SEncoding GetDefault();
	}
}