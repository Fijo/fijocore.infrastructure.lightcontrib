using Fijo.Infrastructure.DesignPattern.Repository.Collection;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Base.Interface;

namespace FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Base.Provider {
	public abstract class HandlingProvider<THandling> : CollectionRepositoryBase<THandling>, IHandlingProvider<THandling> {}
}